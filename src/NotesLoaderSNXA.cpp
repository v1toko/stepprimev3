#include "global.h"
#include "NotesLoaderSNXA.h"
#include "BackgroundUtil.h"
#include "GameManager.h"
#include "MsdFile.h" // No JSON here.
#include "NoteTypes.h"
#include "NotesLoaderSM.h" // For programming shortcuts.
#include "RageFileManager.h"
#include "RageLog.h"
#include "RageUtil.h"
#include "Song.h"
#include "SongManager.h"
#include "Steps.h"
#include "Attack.h"
#include "PrefsManager.h"

void SNXALoader::ProcessBPMs( TimingData &out, const RString sParam )
{
	vector<RString> arrayBPMExpressions;
	split( sParam, ",", arrayBPMExpressions );
	
	for( unsigned b=0; b<arrayBPMExpressions.size(); b++ )
	{
		vector<RString> arrayBPMValues;
		split( arrayBPMExpressions[b], "=", arrayBPMValues );
		if( arrayBPMValues.size() != 2 )
		{
			LOG->UserLog("Song file",
				     this->GetSongTitle(),
				     "has an invalid #BPMS value \"%s\" (must have exactly one '='), ignored.",
				     arrayBPMExpressions[b].c_str() );
			continue;
		}
		
		const float fBeat = StringToFloat( arrayBPMValues[0] );
		const float fNewBPM = StringToFloat( arrayBPMValues[1] );
		if( fBeat >= 0 && fNewBPM > 0 )
		{
			out.AddSegment( BPMSegment(BeatToNoteRow(fBeat), fNewBPM) );
		}
		else
		{
			LOG->UserLog("Song file",
				     this->GetSongTitle(),
				     "has an invalid BPM at beat %f, BPM %f.",
				     fBeat, fNewBPM );
		}
	}
}

void SNXALoader::ProcessStops( TimingData &out, const RString sParam )
{
	vector<RString> arrayStopExpressions;
	split( sParam, ",", arrayStopExpressions );
	
	for( unsigned b=0; b<arrayStopExpressions.size(); b++ )
	{
		vector<RString> arrayStopValues;
		split( arrayStopExpressions[b], "=", arrayStopValues );
		if( arrayStopValues.size() != 2 )
		{
			LOG->UserLog("Song file",
				     this->GetSongTitle(),
				     "has an invalid #STOPS value \"%s\" (must have exactly one '='), ignored.",
				     arrayStopExpressions[b].c_str() );
			continue;
		}
		
		const float fBeat = StringToFloat( arrayStopValues[0] );
		const float fNewStop = StringToFloat( arrayStopValues[1] );
		if( fBeat >= 0 && fNewStop > 0 )
			out.AddSegment( StopSegment(BeatToNoteRow(fBeat), fNewStop) );
		else
		{
			LOG->UserLog("Song file",
				     this->GetSongTitle(),
				     "has an invalid Stop at beat %f, length %f.",
				     fBeat, fNewStop );
		}
	}
}

void SNXALoader::ProcessWarps( TimingData &out, const RString sParam )
{
	vector<RString> arrayWarpExpressions;
	split( sParam, ",", arrayWarpExpressions );
	
	for( unsigned b=0; b<arrayWarpExpressions.size(); b++ )
	{
		vector<RString> arrayWarpValues;
		split( arrayWarpExpressions[b], "=", arrayWarpValues );
		if( arrayWarpValues.size() != 2 )
		{
			LOG->UserLog("Song file",
				     this->GetSongTitle(),
				     "has an invalid #WARPS value \"%s\" (must have exactly one '='), ignored.",
				     arrayWarpExpressions[b].c_str() );
			continue;
		}
		
		const float fBeat = StringToFloat( arrayWarpValues[0] );
		const float fNewBeat = StringToFloat( arrayWarpValues[1] );
		// Early versions were absolute in beats. They should be relative.
		if( fNewBeat > fBeat )
		{
			out.AddSegment( WarpSegment(BeatToNoteRow(fBeat), fNewBeat - fBeat) );
		}
		else if( fNewBeat > 0 )
			out.AddSegment( WarpSegment(BeatToNoteRow(fBeat), fNewBeat) );
		else
		{
			LOG->UserLog("Song file",
				     this->GetSongTitle(),
				     "has an invalid Warp at beat %f, BPM %f.",
				     fBeat, fNewBeat );
		}
	}
}

void SNXALoader::ProcessLabels( TimingData &out, const RString sParam )
{
	vector<RString> arrayLabelExpressions;
	split( sParam, ",", arrayLabelExpressions );
	
	for( unsigned b=0; b<arrayLabelExpressions.size(); b++ )
	{
		vector<RString> arrayLabelValues;
		split( arrayLabelExpressions[b], "=", arrayLabelValues );
		if( arrayLabelValues.size() != 2 )
		{
			LOG->UserLog("Song file",
				     this->GetSongTitle(),
				     "has an invalid #LABELS value \"%s\" (must have exactly one '='), ignored.",
				     arrayLabelExpressions[b].c_str() );
			continue;
		}
		
		const float fBeat = StringToFloat( arrayLabelValues[0] );
		RString sLabel = arrayLabelValues[1];
		TrimRight(sLabel);
		if( fBeat >= 0.0f )
			out.AddSegment( LabelSegment(BeatToNoteRow(fBeat), sLabel) );
		else 
		{
			LOG->UserLog("Song file",
				     this->GetSongTitle(),
				     "has an invalid Label at beat %f called %s.",
				     fBeat, sLabel.c_str() );
		}
		
	}
}

void SNXALoader::ProcessCombos( TimingData &out, const RString line, const int rowsPerBeat )
{
	vector<RString> arrayComboExpressions;
	split( line, ",", arrayComboExpressions );
	
	for( unsigned f=0; f<arrayComboExpressions.size(); f++ )
	{
		vector<RString> arrayComboValues;
		split( arrayComboExpressions[f], "=", arrayComboValues );
		unsigned size = arrayComboValues.size();
		if( size < 2 )
		{
			LOG->UserLog("Song file",
				     this->GetSongTitle(),
				     "has an invalid #COMBOS value \"%s\" (must have at least one '='), ignored.",
				     arrayComboExpressions[f].c_str() );
			continue;
		}
		const float fComboBeat = StringToFloat( arrayComboValues[0] );
		const int iCombos = StringToInt( arrayComboValues[1] );
		const int iMisses = (size == 2 ? iCombos : StringToInt(arrayComboValues[2]));
		out.AddSegment( ComboSegment( BeatToNoteRow(fComboBeat), iCombos, iMisses ) );
	}
}

void SNXALoader::ProcessScrolls( TimingData &out, const RString sParam )
{
	vector<RString> vs1;
	split( sParam, ",", vs1 );
	
	FOREACH_CONST( RString, vs1, s1 )
	{
		vector<RString> vs2;
		split( *s1, "=", vs2 );
		
		if( vs2.size() < 2 )
		{
			LOG->UserLog("Song file",
				     this->GetSongTitle(),
				     "has an scroll change with %i values.",
				     static_cast<int>(vs2.size()) );
			continue;
		}

		const float fBeat = StringToFloat( vs2[0] );
		const float fRatio = StringToFloat( vs2[1] );

		if( fBeat < 0 )
		{
			LOG->UserLog("Song file",
				     this->GetSongTitle(),
				     "has an scroll change with beat %f.",
				     fBeat );
			continue;
		}

		out.AddSegment( ScrollSegment(BeatToNoteRow(fBeat), fRatio) );
	}
}

bool SNXALoader::LoadFromSimfile( const RString &sPath, Song &out, bool bFromCache )
{
	LOG->Trace( "Song::LoadFromSSCFile(%s)", sPath.c_str() );

	MsdFile msd;
	if( !msd.ReadFile( sPath, true ) )
	{
		LOG->UserLog( "Song file", sPath, "couldn't be opened: %s", msd.GetError().c_str() );
		return false;
	}

	out.m_SongTiming.m_sFile = sPath; // songs still have their fallback timing.
	out.m_sSongFileName = sPath;

	int state = GETTING_SONG_INFO_SNXA;
	const unsigned values = msd.GetNumValues();
	Steps* pNewNotes = NULL;
	TimingData stepsTiming;
	bool bHasOwnTiming = false;

	for( unsigned i = 0; i < values; i++ )
	{
		const MsdFile::value_t &sParams = msd.GetValue(i);
		RString sValueName = sParams[0];
		sValueName.MakeUpper();
		int iNumParams = msd.GetNumParams(i);

		switch (state)
		{
			case GETTING_SONG_INFO_SNXA:
			{
				if( sValueName=="TITLE" )
				{
					out.m_sMainTitle = sParams[1];
					this->SetSongTitle(sParams[1]);
				}

				else if( sValueName=="SUBTITLE" )
				{
					out.m_sSubTitle = sParams[1];
				}

				else if( sValueName=="ARTIST" )
				{
					out.m_sArtist = sParams[1];
				}

				else if( sValueName=="TITLETRANSLIT" )
				{
					out.m_sMainTitleTranslit = sParams[1];
				}

				else if( sValueName=="SUBTITLETRANSLIT" )
				{
					out.m_sSubTitleTranslit = sParams[1];
				}

				else if( sValueName=="ARTISTTRANSLIT" )
				{
					out.m_sArtistTranslit = sParams[1];
				}

				else if( sValueName=="GENRE" )
				{
					out.m_sGenre = sParams[1];
				}

				else if( sValueName=="CREDIT" )
				{
					out.m_sCredit = sParams[1];
				}

				else if( sValueName=="BANNER" )
				{
					out.m_sBannerFile = sParams[1];
				}

				else if( sValueName=="BACKGROUND" )
				{
					out.m_sBackgroundFile = sParams[1];
				}

				else if( sValueName=="PREVIEWVID" )
				{
					out.m_sPreviewVidFile = sParams[1];
				}

				else if( sValueName=="CDIMAGE" )
				{
					out.m_sCDFile = sParams[1];
				}

				else if( sValueName=="DISCIMAGE" )
				{
					out.m_sDiscFile = sParams[1];
				}

				else if( sValueName=="LYRICSPATH" )
				{
					out.m_sLyricsFile = sParams[1];
				}

				else if( sValueName=="CDTITLE" )
				{
					out.m_sCDTitleFile = sParams[1];
				}

				else if( sValueName=="MUSIC" )
				{
					out.m_sMusicFile = sParams[1];
				}

				else if( sValueName=="LASTBEATHINT" )
				{
					// unable to parse due to tag position. Ignore.
				}
				
				else if (sValueName == "LASTSECONDHINT")
				{
					out.SetSpecifiedLastSecond(StringToFloat(sParams[1]));
				}

				else if( sValueName=="SAMPLESTART" )
				{
					out.m_fMusicSampleStartSeconds = HHMMSSToSeconds( sParams[1] );
				}

				else if( sValueName=="SAMPLELENGTH" )
				{
					out.m_fMusicSampleLengthSeconds = HHMMSSToSeconds( sParams[1] );
				}

				else if( sValueName=="DISPLAYBPM" )
				{
					// #DISPLAYBPM:[xxx][xxx:xxx]|[*]; 
					if( sParams[1] == "*" )
						out.m_DisplayBPMType = DISPLAY_BPM_RANDOM;
					else 
					{
						out.m_DisplayBPMType = DISPLAY_BPM_SPECIFIED;
						out.m_fSpecifiedBPMMin = StringToFloat( sParams[1] );
						if( sParams[2].empty() )
							out.m_fSpecifiedBPMMax = out.m_fSpecifiedBPMMin;
						else
							out.m_fSpecifiedBPMMax = StringToFloat( sParams[2] );
					}
				}

				else if( sValueName=="SELECTABLE" )
				{
					if(sParams[1].EqualsNoCase("YES"))
						out.m_SelectionDisplay = out.SHOW_ALWAYS;
					else if(sParams[1].EqualsNoCase("NO"))
						out.m_SelectionDisplay = out.SHOW_NEVER;
					// ROULETTE from 3.9 is no longer in use.
					else if(sParams[1].EqualsNoCase("ROULETTE"))
						out.m_SelectionDisplay = out.SHOW_ALWAYS;
					/* The following two cases are just fixes to make sure simfiles that
					 * used 3.9+ features are not excluded here */
					else if(sParams[1].EqualsNoCase("ES") || sParams[1].EqualsNoCase("OMES"))
						out.m_SelectionDisplay = out.SHOW_ALWAYS;
					else if( StringToInt(sParams[1]) > 0 )
						out.m_SelectionDisplay = out.SHOW_ALWAYS;
					else
						LOG->UserLog( "Song file", sPath, "has an unknown #SELECTABLE value, \"%s\"; ignored.", sParams[1].c_str() );
				}

				else if( sValueName.Left(strlen("BGCHANGES"))=="BGCHANGES" || sValueName=="ANIMATIONS" )
				{
					SMLoader::ProcessBGChanges( out, sValueName, sPath, sParams[1]);
				}

				else if( sValueName=="FGCHANGES" )
				{
					vector<RString> aFGChangeExpressions;
					split( sParams[1], ",", aFGChangeExpressions );

					for( unsigned b=0; b<aFGChangeExpressions.size(); b++ )
					{
						BackgroundChange change;
						if( LoadFromBGChangesString( change, aFGChangeExpressions[b] ) )
							out.AddForegroundChange( change );
					}
				}

				// Attacks loaded from file
				else if( sValueName=="ATTACKS" )
				{
					ProcessAttackString(out.m_sAttackString, sParams);
					ProcessAttacks(out.m_Attacks, sParams);
				}

				else if( sValueName=="OFFSET" )
				{
					out.m_SongTiming.m_fBeat0OffsetInSeconds = StringToFloat( sParams[1] );
				}
				/* Below are the song based timings that should only be used
				 * if the steps do not have their own timing. */
				else if( sValueName=="STOPS" )
				{
					ProcessStops(out.m_SongTiming, sParams[1]);
				}
				else if( sValueName=="DELAYS" )
				{
					SMLoader::ProcessDelays(out.m_SongTiming, sParams[1]);
				}

				else if( sValueName=="BPMS" )
				{
					ProcessBPMs(out.m_SongTiming, sParams[1]);
				}
				
				else if( sValueName=="WARPS" ) // Older versions allowed em here.
				{
					ProcessWarps( out.m_SongTiming, sParams[1] );
				}
				
				else if( sValueName=="LABELS" )
				{
					ProcessLabels( out.m_SongTiming, sParams[1] );
				}

				else if( sValueName=="TIMESIGNATURES" )
				{
					SMLoader::ProcessTimeSignatures(out.m_SongTiming, sParams[1]);
				}

				else if( sValueName=="TICKCOUNTS" )
				{
					SMLoader::ProcessTickcounts(out.m_SongTiming, sParams[1]);
				}

				else if( sValueName=="COMBOS" )
				{
					ProcessCombos( out.m_SongTiming, sParams[1] );
				}
				else if (sValueName=="SPEEDS")
				{
					ProcessSpeeds(out.m_SongTiming, sParams[1]);
				}
				else if (sValueName=="SCROLLS")
				{
					ProcessScrolls(out.m_SongTiming, sParams[1]);
				}
				else if (sValueName=="FAKES")
				{
					ProcessFakes(out.m_SongTiming, sParams[1]);
				}

				/* The following are cache tags. Never fill their values
				 * directly: only from the cached version. */
				else if( sValueName=="FIRSTBEAT" || sValueName=="LASTBEAT" )
				{
					// no longer used.
				}			

				// This tag will get us to the next section.
				else if( sValueName=="NOTEDATA" )
				{
					state = GETTING_STEP_INFO_SNXA;
					pNewNotes = out.CreateSteps();
					stepsTiming = TimingData( out.m_SongTiming.m_fBeat0OffsetInSeconds );
					bHasOwnTiming = false;
				}
				break;
			}
			case GETTING_STEP_INFO_SNXA:
			{
				if( sValueName=="NOTES" || sValueName=="NOTES2" )
				{
					state = GETTING_SONG_INFO_SNXA;
					if( bHasOwnTiming )
						pNewNotes->m_Timing = stepsTiming;
					bHasOwnTiming = false;
					//pNewNotes->SetSMNoteData( sParams[1] );
					//pNewNotes->TidyUpData();

					if( iNumParams < 7 )
					{
						LOG->UserLog("Song file",
							     sPath,
							     "has %d fields in a #NOTES tag, but should have at least 7.",
							     iNumParams );
						continue;
					}

					//pNewNotes = new Steps(&out);
					
					LoadFromTokens( 
							 sParams[1], 
							 sParams[2], 
							 sParams[3], 
							 sParams[4], 
							 sParams[5], 
							 sParams[6],
							 *pNewNotes );
					pNewNotes->SetFilename(sPath);
					out.AddSteps( pNewNotes );
				}
				
				else if( sValueName=="BPMS" )
				{
					ProcessBPMs(stepsTiming, sParams[1]);
					bHasOwnTiming = true;
				}
				
				else if( sValueName=="STOPS" )
				{
					ProcessStops(stepsTiming, sParams[1]);
					bHasOwnTiming = true;
				}
				
				else if( sValueName=="DELAYS" )
				{
					SMLoader::ProcessDelays(stepsTiming, sParams[1]);
					bHasOwnTiming = true;
				}
				
				else if( sValueName=="TIMESIGNATURES" )
				{
					SMLoader::ProcessTimeSignatures(stepsTiming, sParams[1]);
					bHasOwnTiming = true;
				}
				
				else if( sValueName=="TICKCOUNTS" )
				{
					SMLoader::ProcessTickcounts(stepsTiming, sParams[1]);
					bHasOwnTiming = true;
				}
				
				else if( sValueName=="COMBOS" )
				{
					ProcessCombos(stepsTiming, sParams[1]);
					bHasOwnTiming = true;
				}
				
				else if( sValueName=="WARPS" )
				{
					ProcessWarps(stepsTiming, sParams[1]);
					bHasOwnTiming = true;
				}
				
				else if( sValueName=="SPEEDS" )
				{
					ProcessSpeeds( stepsTiming, sParams[1] );
					bHasOwnTiming = true;
				}
				
				else if( sValueName=="SCROLLS" )
				{
					ProcessScrolls( stepsTiming, sParams[1] );
					bHasOwnTiming = true;
				}
				
				else if( sValueName=="FAKES" )
				{
					ProcessFakes( stepsTiming, sParams[1] );
					bHasOwnTiming = true;
				}
				
				else if( sValueName=="LABELS" )
				{
					ProcessLabels(stepsTiming, sParams[1]);
					bHasOwnTiming = true;
				}
				/* If this is called, the chart does not use the same attacks
				 * as the Song's timing. No other changes are required. */
				else if( sValueName=="ATTACKS" )
				{
					ProcessAttackString(pNewNotes->m_sAttackString, sParams);
					ProcessAttacks(pNewNotes->m_Attacks, sParams);
				}

				else if( sValueName=="OFFSET" )
				{
					stepsTiming.m_fBeat0OffsetInSeconds = StringToFloat( sParams[1] );
					bHasOwnTiming = true;
				}

				else if( sValueName=="DISPLAYBPM" )
				{
					// #DISPLAYBPM:[xxx][xxx:xxx]|[*]; 
					if( sParams[1] == "*" )
						pNewNotes->SetDisplayBPM(DISPLAY_BPM_RANDOM);
					else 
					{
						pNewNotes->SetDisplayBPM(DISPLAY_BPM_SPECIFIED);
						float min = StringToFloat(sParams[1]);
						pNewNotes->SetMinBPM(min);
						if(sParams[2].empty())
							pNewNotes->SetMaxBPM(min);
						else
							pNewNotes->SetMaxBPM(StringToFloat(sParams[2]));
					}
				}
				break;
			}
		}
	}
	//out.m_fVersion = STEPFILE_VERSION_NUMBER;
	TidyUpData(out, bFromCache);
	return true;
}

/*
 * @author v1toko (taking reference from NotesLoaderSSC.cpp) (c) 2015
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
