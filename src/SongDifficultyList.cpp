#include "global.h"
#include "SongDifficultyList.h"
#include "RageUtil.h"
#include "GameConstantsAndTypes.h"
#include "GameState.h"
#include "Course.h"
#include "Style.h"
#include "ActorUtil.h"
#include "CommonMetrics.h"
#include "LocalizedString.h"
#include "Song.h"
#include "Steps.h"
#include "Trail.h"
#include "RageLog.h"
#include "MessageManager.h"
#include "ProfileManager.h"
#include "ThemeManager.h"

#include <limits.h>

REGISTER_ACTOR_CLASS( SongDifficultyList );

const int MAX_POS = 26;
const int MAX_QUEST_POS = 4;

float Xpos[] = {
	-298, 
	-249, 
	-199, 
	-150, 
	-99, 
	-49,
	0,
	50,
	100, 
	149,
    198,	
    249,
    299
};

float XposQuest[] = {
	-74, 
	-24,
	25,
	75
};

SongDifficultyList::SongDifficultyList()
{
	FOREACH_PlayerNumber( pn )
	{
		this->SubscribeToMessage( ssprintf("CurrentStepsP%dChanged",pn+1) );
	}
}

SongDifficultyList::~SongDifficultyList()
{	
}

void SongDifficultyList::HandleMessage( const Message &msg )
{
	if( msg == "CurrentStepsP1ChangedMessageCommand" )
	{

	}
	else if( msg == "CurrentStepsP2ChangedMessageCommand" )
	{

	}

	ActorFrame::HandleMessage(msg);
}

void SongDifficultyList::Load(const RString &sType)
{
	QUESTSTEP_Y.Load( "SongDifficultyList", "QuestStepY" );
	QUESTSTEPCOMPLETE_Y.Load( "SongDifficultyList", "QuestStepCompleteY" );
	TRAINZOOM.Load( "SongDifficultyList", "TrainZoom" );
	TRAIN_Y.Load( "SongDifficultyList", "TrainY" );

	BULLETBACK_Y.Load( "SongDifficultyList", "BulletBackY" );
	BULLETBACK_ZOOM.Load( "SongDifficultyList", "BulletBackZoom" );

	BULLETSPIN_Y.Load( "SongDifficultyList", "BulletSpinY" );
	BULLETSPIN_ZOOM.Load( "SongDifficultyList", "BulletSpinZoom" );
	BULLETSPIN_ALPHA.Load( "SongDifficultyList", "BulletSpinAlpha" );

	//m_backgroundNormal.SetName("BackgroundNormal");
	//m_backgroundNormal.Load( THEME->GetPathG(sType,"background") );	
	//this->AddChild( &m_backgroundNormal );

	//m_backgroundMission.SetName("BackgroundMission");
	//m_backgroundMission.Load( THEME->GetPathG(sType,"backgroundmission") );
	//this->AddChild( &m_backgroundMission );

	for(int i = 0; i < MAX_POS; i++)
	{
		//m_vbullets[i].SetZoom(BULLETS_ZOOM);
		//m_vbullets[i].SetY( BULLETS_Y );

		//m_vlevelText[i].SetZoom(TEXT_ZOOM);
		//m_vlevelText[i].SetY( TEXT_Y );

		m_vbullets[i].Load();

		if( i < 13 )
		{
			m_bulletsSection[0].AddChild( &m_vbullets[i] );
			//m_bulletsSection[0].AddChild( &m_vlevelText[i] );

			m_bulletBack[i].Load( THEME->GetPathG("SongDifficultyListNormal", "bulletback") );
			m_bulletBack[i].SetZoom(BULLETBACK_ZOOM);
			m_bulletBack[i].SetY(BULLETBACK_Y);

			this->AddChild(&m_bulletBack[i]);

			m_bulletSpinner[i].Load( THEME->GetPathG("SongDifficultyListNormal", "bulletspinner") );
			m_bulletSpinner[i].SetName(ssprintf("SpinnerMini%d", i+1));
			m_bulletSpinner[i].SetZoom(BULLETBACK_ZOOM);
			m_bulletSpinner[i].SetY(BULLETBACK_Y);
			//m_bulletSpinner[i].SetEffectSpin(RageVector3(0,0,-90));
			m_bulletSpinner[i].SetDiffuseAlpha(BULLETSPIN_ALPHA);
			m_bulletSpinner[i].SetBlendMode( BLEND_ADD );
			this->AddChild(&m_bulletSpinner[i]);
		}
		else
		{
			m_bulletsSection[1].AddChild( &m_vbullets[i] );
			//m_bulletsSection[1].AddChild( &m_vlevelText[i] );
		}

		m_bulletsSection[0].SetVisible(false);
		m_bulletsSection[1].SetVisible(false);

		this->AddChild(&m_bulletsSection[0]);
		this->AddChild(&m_bulletsSection[1]);
	}

	for(int i = 0; i < MAX_POS/2; i++)
	{
		m_singleTrainPos[i].SetName(ssprintf("TrainCursorSingleN%d", i+1));
		m_singleTrainPos[i].Load(THEME->GetPathG(sType, "traincursor"));
		m_singleTrainPos[i].SetVisible(false);		
		m_singleTrainPos[i].EnableAnimation(false);

		m_doubleTrainPos[i].SetName(ssprintf("TrainCursorDoubleN%d", i+1));
		m_doubleTrainPos[i].Load(THEME->GetPathG(sType, "traincursor"));
		m_doubleTrainPos[i].SetVisible(false);		
		m_doubleTrainPos[i].EnableAnimation(false);
		
		this->AddChild( &m_singleTrainPos[i] );
		this->AddChild( &m_doubleTrainPos[i] );
	}

	for(int i = 0; i < MAX_QUEST_POS; i++)
	{
		m_stepsNumbers[i].SetName(ssprintf("StepNumberEnabledN%d", i+1));
		m_stepsNumbers[i].Load(THEME->GetPathG(sType, "stepsnumber"));
		m_stepsNumbers[i].SetVisible(false);		
		m_stepsNumbers[i].EnableAnimation(false);
		m_stepsNumbers[i].SetY(QUESTSTEP_Y);

		m_stepsNumbersDisabled[i].SetName(ssprintf("StepNumberDisabledN%d", i+1));
		m_stepsNumbersDisabled[i].Load(THEME->GetPathG(sType, "stepsnumbers_disabled"));
		m_stepsNumbersDisabled[i].SetVisible(false);		
		m_stepsNumbersDisabled[i].EnableAnimation(false);
		m_stepsNumbersDisabled[i].SetY(QUESTSTEP_Y);

		m_stepsComplete[i].SetName(ssprintf("StepCompleteN%d", i+1));
		m_stepsComplete[i].Load(THEME->GetPathG(sType, "stepcompletesmall"));
		m_stepsComplete[i].SetVisible(false);		
		m_stepsComplete[i].EnableAnimation(false);
		m_stepsComplete[i].SetY(QUESTSTEPCOMPLETE_Y);
		m_stepsComplete[i].SetState(0);

		m_bulletsSection[0].AddChild( &m_stepsNumbers[i] );
		m_bulletsSection[0].AddChild( &m_stepsNumbersDisabled[i] );
		m_bulletsSection[0].AddChild( &m_stepsComplete[i] );
	}

	FOREACH_PlayerNumber( pn )
	{
		m_playerSelection[pn].Load( THEME->GetPathG(sType, ssprintf("cursorp%i",pn+1)) );
		m_playerSelection[pn].SetName( ssprintf("CursorP%i", pn+1) );
		m_playerSelection[pn].SetX(Xpos[0]);
		m_playerSelection[pn].SetVisible(false);
		this->AddChild( &m_playerSelection[pn] );
	}
}

void SongDifficultyList::SetFromGameState()
{
	/*if( GAMESTATE->GetCurrentGroup() != "quest zone" )
	{
		//m_backgroundNormal.SetVisible(true);
		//m_backgroundMission.SetVisible(false);
	}
	else
	{
		//m_backgroundNormal.SetVisible(false);
		//m_backgroundMission.SetVisible(true);
	}*/	

	for(int i = 0; i < MAX_POS/2; i++)
	{
		m_bulletBack[i].SetVisible(false);
		m_bulletSpinner[i].SetVisible(false);
	}

	for(int i = 0; i < MAX_POS; i++)
	{
		m_vbullets[i].SetVisible(false);
		//m_vlevelText[i].SetVisible(false);		

		if( GAMESTATE->GetCurrentGroup() != "quest zone" && i < MAX_POS / 2 )
		{
			m_bulletBack[i].SetVisible(true);
			m_bulletBack[i].SetX(Xpos[i]);			
		}
	}	

	for(int i = 0; i < MAX_POS/2; i++)
	{
		m_singleTrainPos[i].SetVisible(false);	
		m_doubleTrainPos[i].SetVisible(false);	
	}

	for(int i = 0; i < MAX_QUEST_POS; i++)
	{
		m_stepsNumbers[i].SetVisible(false);		
		m_stepsNumbersDisabled[i].SetVisible(false);		
		m_stepsComplete[i].SetVisible(false);

		if( GAMESTATE->GetCurrentGroup() == "quest zone" )
		{
			m_bulletBack[i].SetVisible(true);
			m_bulletBack[i].SetX(XposQuest[i]);
		}
	}

	/*
	para referencia:

	numbersred
	numbersyellow
	numberspurple
	numbersgreen
	numbersgray
	numbersdiffuse
	numbersblue
	*/
	
	if( GAMESTATE->GetCurrentGroup() != "random" && GAMESTATE->GetCurrentGroup() != "music train" && GAMESTATE->GetCurrentGroup() != "quest zone" )
	{
		Song *pSong = GAMESTATE->m_pCurSong;
		vector<Steps*>	vpSteps;
		SongUtil::GetPlayableSteps( pSong, vpSteps );

		unsigned idx = 0;	
		FOREACH_EnabledPlayer(pn)
		{			
			if( GAMESTATE->IsHumanPlayer(pn) )
			{
				idx = this->GetActiveIndex(vpSteps, pn);

				m_playerSelection[pn].SetVisible(true);
				m_playerSelection[pn].SetX(Xpos[ (idx >= 13 ? idx-13 : idx) ]);
			}
			else
				m_playerSelection[pn].SetVisible(false);
		}

		for( unsigned i = 0; i < vpSteps.size() && i < MAX_POS; i++ )
		{
			int posX = 0;
			if( i >= 13 )
				posX = Xpos[i-13];
			else
				posX = Xpos[i];

			if( i < 13 )
			{
				m_bulletSpinner[i].SetVisible(true);
				m_bulletSpinner[i].SetX(posX);		
			}
			else
			{
				m_bulletSpinner[i].SetVisible(false);
			}

			Steps* s = vpSteps[i];
			RString sDesc = s->GetDescription();
			sDesc.MakeUpper();

			//mirar la referencia de arriba
			int iTypeFinal = 0;

			RString sLvl = "";
			RString sType = "numbersred";
			switch( s->m_StepsType )
			{
				case StepsType_pump_single:
					sLvl = "0";
					break;
				case StepsType_pump_double:
					sLvl = "1";
					sType = "numbersgreen";
					iTypeFinal = 3;
					break;
			}

			int iMeter = s->GetMeter();

			if( sDesc.find("SP") != string::npos )
			{
				sLvl = "4";
				sType = "numberspurple";
				iTypeFinal = 2;
			}
			bool bDP = false;
			if( sDesc.find("DP") != string::npos && iMeter >= 50 )
			{
				sType = "numbersyellow";
				sLvl = "6";
				iTypeFinal = 1;
				bDP = true;
			}
			else if( sDesc.find("DP") != string::npos && iMeter < 50 )
			{
				sType = "numbersblue";
				sLvl = "3";
				iTypeFinal = 6;
				bDP = true;	
			}

			//si sobrepasamos el index 13, no mostramos
			m_vbullets[i].SetVisible(true);			

			if( idx < 13 )
			{
				m_bulletsSection[0].SetVisible(true);
				m_bulletsSection[1].SetVisible(false);		
			}
			else
			{
				m_bulletsSection[0].SetVisible(false);
				m_bulletsSection[1].SetVisible(true);	
			}

			RString sMeterFinal = "";
			if( iMeter < 50 && !bDP )
			{
				sMeterFinal = ssprintf("%02d", iMeter);
			}
			else
			{
				if( iMeter > 50 )
				{
					if( !bDP )
					{
						sMeterFinal = "??";	
					}
					else
					{
						//CO-OPX2
						vector<RString> vDescParts;
						split( sDesc, " ", vDescParts );

						bool bFinded = false;
						unsigned i;
						for(i = 0; i < vDescParts.size(); i++)
						{
							RString part = vDescParts[i];
							if( part.find("CO-OPX") != string::npos )
							{
								bFinded = true;
								break;
							}
						}

						if( bFinded )
						{
							RString sPart = vDescParts[i];
							int iCoop = 0;
							int iRet = sscanf(sPart, "CO-OPX%d", &iCoop);
							sMeterFinal = ssprintf("%02d", iCoop);	
						}
						else
						{
							sMeterFinal = "02";
						}
					}
				}
				else
				{
					sMeterFinal = ssprintf("%02d", iMeter);
				}
			}

			int iState = 0;
			if( sDesc.find("NEW") != string::npos )
				iState = 2;
			if( sDesc.find("UCS") != string::npos )
				iState = 1;
			if( sDesc.find("ANOTHER") != string::npos )
				iState = 3;

			m_vbullets[i].SetFromGameState(sLvl, sMeterFinal, iTypeFinal, posX, iState);
		}

		Message msg("StepPosition");		
		msg.SetParam("Valid", 0);
		MESSAGEMAN->Broadcast(msg);
	}
	else if( GAMESTATE->GetCurrentGroup() == "random" || GAMESTATE->GetCurrentGroup() == "music train" )
	{
		const Course *pCourse = GAMESTATE->m_pCurCourse;

		FOREACH_EnabledPlayer(pn)
			m_playerSelection[pn].SetVisible(false);

		const Trail *pMasterTrail = GAMESTATE->m_pCurTrail[GAMESTATE->GetMasterPlayerNumber()];
		if( pMasterTrail == NULL )
			return;

		unsigned uNumEntriesToShow = pMasterTrail->m_vEntries.size(); 
		CLAMP( uNumEntriesToShow, 0, MAX_POS );		

		for( int i=0; i<(int)uNumEntriesToShow; i++ )
		{
			const TrailEntry *te = &pMasterTrail->m_vEntries[i];
			//const CourseEntry *ce = &pCourse->m_vEntries[i];
			if( te == NULL )
				continue;					

			Steps* s = te->pSteps;
			RString sDesc = s->GetDescription();

			RString sLvl = "";
			RString sType = "numbersred";
			int iTypeFinal = 0;
			switch( s->m_StepsType )
			{
				case StepsType_pump_single:
					sLvl = "0";
					break;
				case StepsType_pump_double:
					sLvl = "1";
					sType = "numbersgreen";
					iTypeFinal = 3;
					break;
			}

			int iMeter = s->GetMeter();

			bool bDP = false;
			if( sDesc.find("DP") != string::npos && iMeter >= 50 )
			{
				sType = "numbersyellow";
				sLvl = "6";
				iTypeFinal = 1;
				bDP = true;
			}
			else if( sDesc.find("DP") != string::npos && iMeter < 50 )
			{
				sType = "numbersblue";
				sLvl = "3";
				iTypeFinal = 6;
				bDP = true;	
			}

			bool bDouble = false;
			FOREACHS_CONST( RString, pCourse->m_setStyles, style )
			{
				if( !style->CompareNoCase("double") ) 
					bDouble = true;
			}

			m_bulletsSection[0].SetVisible(true);
			m_bulletsSection[1].SetVisible(false);

			int posX = Xpos[i];

			//si sobrepasamos el index 13, no mostramos
			m_vbullets[i].SetVisible(true);
			if( i >= 13 )
			{
				m_vbullets[i].SetVisible(false);
				posX = Xpos[i-13];				
			}
			else
			{
				m_bulletSpinner[i].SetVisible(true);
				m_bulletSpinner[i].SetX(posX);	
			}

			RString sMeterFinal = "";
			if( GAMESTATE->GetCurrentGroup() == "music train" )
			{
				sMeterFinal = "";
				if( iMeter < 50 && !bDP )
				{
					sMeterFinal = ssprintf("%02d", iMeter);	
				}
				else
				{
					if( iMeter > 50 )
					{
						if( !bDP )
						{
							sMeterFinal = "??";
						}
						else
						{
							//CO-OPX2
							vector<RString> vDescParts;
							split( sDesc, " ", vDescParts );

							bool bFinded = false;
							unsigned i;
							for(i = 0; i < vDescParts.size(); i++)
							{
								RString part = vDescParts[i];
								if( part.find("CO-OPX") != string::npos )
								{
									bFinded = true;
									break;
								}
							}

							if( bFinded )
							{
								RString sPart = vDescParts[i];
								int iCoop = 0;
								int iRet = sscanf(sPart, "CO-OPX%d", &iCoop);
								sMeterFinal = ssprintf("%02d", iCoop);	
							}
							else
							{
								sMeterFinal = "02";
							}	
						}
					}
					else
					{
						sMeterFinal = ssprintf("%02d", iMeter);	
					}
				}
			}
			else
			{
				sMeterFinal = "??";
			}

			m_vbullets[i].SetFromGameState(sLvl, sMeterFinal, iTypeFinal, posX, 0);

			if( GAMESTATE->GetCurrentGroup() == "music train" )
			{
				if( bDouble )
				{
					Sprite *spr = &m_doubleTrainPos[i];
					spr->SetVisible(true);
					spr->SetZoom(TRAINZOOM);
					spr->SetY(TRAIN_Y);
					spr->SetX(posX);

					if( i == 0 )
					{						
						spr->SetState(0);
						spr->AddX(4);
					}
					else if( (i+1) == (int)uNumEntriesToShow  )
					{						
						spr->SetState(2);		
						spr->AddX(-4);				
					}
					else
					{
						spr->SetState(2);						
						spr->AddX(-5);		
					}
				}
				else
				{
					Sprite *spr = &m_singleTrainPos[i];
					spr->SetVisible(true);
					spr->SetZoom(TRAINZOOM);
					spr->SetY(TRAIN_Y);
					spr->SetX(posX);
					if( i == 0 )
					{						
						spr->SetState(3);		
						spr->AddX(4);
					}
					else if( (i+1) == (int)uNumEntriesToShow  )
					{						
						spr->SetState(5);
						spr->AddX(-5);
					}
					else
					{						
						spr->SetState(4);
					}
				}
			}
		}

		Message msg("StepPosition");		
		msg.SetParam("Valid", 0);
		MESSAGEMAN->Broadcast(msg);
	}
	else //quest zone
	{
		Song *pSong = GAMESTATE->m_pCurSong;
		vector<Steps*>	vpSteps;
		SongUtil::GetPlayableSteps( pSong, vpSteps );

		unsigned idx = 0;	
		FOREACH_EnabledPlayer(pn)
		{			
			if( GAMESTATE->IsHumanPlayer(pn) )
			{
				idx = this->GetActiveIndex(vpSteps, pn);

				m_playerSelection[pn].SetVisible(true);
				m_playerSelection[pn].SetX(XposQuest[idx]);
			}
			else
				m_playerSelection[pn].SetVisible(false);
		}

		unsigned iMaxLvlPassed = 0; //1 para que siempre podamos jugar el primero
		for( unsigned i = 0; i < vpSteps.size(); i++ )
		{
			Steps* s = vpSteps[i];
			bool bPassed = PROFILEMAN->GetMachineProfile()->HasPassedStepsMission(pSong, s);

			if( bPassed )
				iMaxLvlPassed = i+1;
		}

		for( unsigned i = 0; i < vpSteps.size() && i < MAX_QUEST_POS; i++ )
		{
			int posX = XposQuest[i];

			Steps* s = vpSteps[i];
			RString sDesc = s->GetDescription();

			//TODO: dejar elegir al jugar si toma el machine o el personal profile
			bool bPassed = PROFILEMAN->GetMachineProfile()->HasPassedStepsMission(pSong, s);

			RString sLvl = "";
			RString sType = "numbersdiffuse";
			int iTypeFinal = 5;
			switch( s->m_StepsType )
			{
				case StepsType_pump_single:
					sLvl = "0";
					break;
				case StepsType_pump_double:
					sLvl = "1";
					break;
			}

			m_stepsNumbers[i].SetX(posX);		
			m_stepsNumbersDisabled[i].SetX(posX);
			m_stepsComplete[i].SetX(posX);
			m_stepsNumbers[i].SetState(i);
			m_stepsNumbersDisabled[i].SetState(i);

			if( !bPassed )
			{
				if( i > iMaxLvlPassed )
				{
					sLvl = "5";
					sType = "numbersgray";
					iTypeFinal = 4;
					m_stepsNumbersDisabled[i].SetVisible(true);
				}
				else
				{
					sType = "numbersdiffuse";
					iTypeFinal = 5;
					m_stepsNumbers[i].SetVisible(true);

					m_bulletSpinner[i].SetVisible(true);
					m_bulletSpinner[i].SetX(posX);	
				}
			}
			else
			{
				m_stepsNumbers[i].SetVisible(true);				
				if( i < iMaxLvlPassed )
					m_stepsComplete[i].SetVisible(true);
			}

			if( i < 13 )
			{
				m_bulletSpinner[i].SetVisible(true);
				m_bulletSpinner[i].SetX(posX);		
			}

			int iMeter = s->GetMeter();

			//m_vbullets[i].SetText(sLvl);
			//m_vbullets[i].SetX( posX );		

			//m_vlevelText[i].LoadFromFont(THEME->GetPathF("SongDifficultyDisplay", sType));
			//m_vlevelText[i].SetX( posX );

			m_vbullets[i].SetVisible(true);
			//m_vlevelText[i].SetVisible(true);
			
			m_bulletsSection[0].SetVisible(true);
			m_bulletsSection[1].SetVisible(false);

			//m_vbullets[i].SetText(sLvl);		
			//m_vbullets[i].SetX( posX );	
			//m_vlevelText[i].SetText(ssprintf("%02d", iMeter));		
			RString sMeterFinal = ssprintf("%02d", iMeter);

			m_vbullets[i].SetFromGameState(sLvl, sMeterFinal, iTypeFinal, posX, 0);
		}

		Message msg("StepPosition");
		msg.SetParam("Step", idx);
		msg.SetParam("Valid", 1);
		Steps* s2 = vpSteps[idx];
		bool bPassed = PROFILEMAN->GetMachineProfile()->HasPassedStepsMission(pSong, s2);
		msg.SetParam("Cleared", bPassed && idx < iMaxLvlPassed);
		msg.SetParam("Complete", iMaxLvlPassed >= 4);
		msg.SetParam("LevelCompleted", iMaxLvlPassed);
		MESSAGEMAN->Broadcast(msg);
	}
}

int SongDifficultyList::GetActiveIndex(vector<Steps*> vSteps, PlayerNumber pn)
{
	Steps *pSteps = GAMESTATE->m_pCurSteps[pn];

	for( unsigned j = 0; j < vSteps.size(); j++ )
		if( pSteps == vSteps[j] ) 
			return j;
	
	return 0;
}

void DifficultyListBullet::Load()
{
	BULLETS_ZOOM.Load( "SongDifficultyList", "BulletsZoom" );
	TEXT_ZOOM.Load( "SongDifficultyList", "TextZoom" );
	BULLETS_Y.Load( "SongDifficultyList", "BulletsY" );
	TEXT_Y.Load( "SongDifficultyList", "LevelY" );
	NEWSTEP_ZOOM.Load( "SongDifficultyList", "NewStepZoom" );
	NEWSTEP_Y.Load( "SongDifficultyList", "NewStepY" );

	m_vbullets.LoadFromFont(THEME->GetPathF("SongDifficultyDisplay", "normal"));
	m_vbullets.SetZoom(BULLETS_ZOOM);
	m_vbullets.SetY(BULLETS_Y);

	m_vlevelText[0].LoadFromFont(THEME->GetPathF("SongDifficultyDisplay", "numbersred"));
	m_vlevelText[1].LoadFromFont(THEME->GetPathF("SongDifficultyDisplay", "numbersyellow"));
	m_vlevelText[2].LoadFromFont(THEME->GetPathF("SongDifficultyDisplay", "numberspurple"));
	m_vlevelText[3].LoadFromFont(THEME->GetPathF("SongDifficultyDisplay", "numbersgreen"));
	m_vlevelText[4].LoadFromFont(THEME->GetPathF("SongDifficultyDisplay", "numbersgray"));
	m_vlevelText[5].LoadFromFont(THEME->GetPathF("SongDifficultyDisplay", "numbersdiffuse"));
	m_vlevelText[6].LoadFromFont(THEME->GetPathF("SongDifficultyDisplay", "numbersblue"));

	this->AddChild(&m_vbullets);	
	for( unsigned i = 0; i < 7; i++ )
	{
		m_vlevelText[i].SetZoom(TEXT_ZOOM);
		m_vlevelText[i].SetY(TEXT_Y);
		this->AddChild(&m_vlevelText[i]);
	}

	m_newStep.Load( THEME->GetPathG("SongDifficultyListNormal", "stepstate") );
	m_newStep.SetZoom(NEWSTEP_ZOOM);
	m_newStep.SetY(NEWSTEP_Y);
	m_newStep.EnableAnimation(false);
	this->AddChild(&m_newStep);
}

void DifficultyListBullet::SetFromGameState(RString sLevel, RString sMeter, int iType, float fX, int iState)
{
	m_vbullets.SetText(sLevel);
	m_vbullets.SetX(fX);
	for( unsigned i = 0; i < 7; i++ )
	{
		m_vlevelText[i].SetVisible(false);
		m_vlevelText[i].SetX(fX);
	}	

	m_vlevelText[iType].SetText(sMeter);
	m_vlevelText[iType].SetVisible(true);

	if(iState == 0)
	{
		m_newStep.SetVisible(false);
	}
	else
	{
		m_newStep.SetVisible(true);
		m_newStep.SetState(iState-1);
	}
	
	m_newStep.SetX(fX);
}

#include "LuaBinding.h"
/** @brief Allow Lua to have access to the SongDifficultyList. */
class LunaSongDifficultyList: public Luna<SongDifficultyList>
{
public:
	LunaSongDifficultyList()
	{
	}
};

LUA_REGISTER_DERIVED_CLASS( SongDifficultyList, ActorFrame )

/*
 * (c) 2015 Victor Gajardo Henriquez "v1toko"
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */