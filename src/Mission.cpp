#include "global.h"
#include "RageUtil.h"
#include "RageLog.h"
#include "SongUtil.h"
#include "Steps.h"
#include "SongManager.h"
#include "GameState.h"
#include "Mission.h"

static const char *MissionTypeNames[] = {
	"Mission",
	"Warp",
	"WarpSector",
	"Boss",
	"Portal",
	"Switch",
	"DualWarp",
	"HiddenLine",
	"SWarp",
};
XToString( MissionType );
StringToX( MissionType );

static const char *MissionBarrierNames[] = {
	"Blue",
	"Cian",
	"Gray",
	"Green",
	"LGreen",
	"Red",
	"Violet",
};
XToString( MissionBarrier );
StringToX( MissionBarrier );

MissionGoals::MissionGoals()
{
	iLevel = 1;

	fLifeDifficulty = -1;

	sUseExtraStats = RString();
	sCheckNoteSkin = RString();

	//pSong = NULL;
	fposX = 0;//( 0, 0 )
	fposY = 0;

	//links, direcciones
	sLinkUpLeft = RString();
	sLinkUpRight = RString();
	sLinkDownLeft = RString();
	sLinkDownRight = RString();
	//links, posiciones
	sLinkWarpSector = RString();
	sLinkWarpMission = RString();
	sLinkToOtherWarp = RString();
	//links, ocultos
	sLinkUpLeftHidden = RString();
	sLinkUpRightHidden = RString();
	sLinkDownLeftHidden = RString();
	sLinkDownRightHidden = RString();
	sLinkBothDownHidden = RString();
	sLinkBothUpHidden = RString();	

	sPlayable = "YES";//es jugable
	sType = "Mission";//por defecto son misiones normales!
	sExtraInformation = RString();
	sNeedGraphics = "NO";//por default, no necesitan graficos speciales
	sShowArrowMods = "YES";//default, mostrar las arrowmods
	sInitial = "NO";
	sHoldsNoBody = "NO";

	//switches stuff
	sLandsToHide = RString();
	sLandsToShow = RString();
	sSwitchActive = RString();

	sInvalidateMissionWhenIsActive = RString();
	sValidateMissionWhenIsActive = RString();

	sGoalText = RString();
	sGoalText2 = RString();
	sGoalText3 = RString();
	sGoalTextExtra = RString();
	Grade = RString();
	sModifiers = RString();
	sMissionName = RString();
	sMissionSector = RString();
	sRequiredMission = RString();
	sMineHoldMode = "NO";

	iBombPosibility = 0;
	sMissionToThrowWhenFindMine = RString();

	sSongToUnlock = RString();
	sSongPathToLoad = RString();

	sBarrierString = RString();

	sLand = RString();
	sAuthor = RString();

	iRequiredPlayers = 1;
	iGivenMileage = 0;
	sBreak = "YES";

	iNumCodes = 0;

	fRequiredPercent = 0.0f;

	fInitialLife = 0.5f;//barra a la mitad!

	sMissionStringCommand = RString();
	sPlayerActorCommands = RString();
}

Mission::Mission()
{
	//init pointers
	m_pSong = NULL;
	m_pLockedSong = NULL;

	m_pLink0 = NULL;
	m_pLink1 = NULL;
	m_pLink2 = NULL;
	m_pLink3 = NULL;

	m_pWarpSectorToWarpMission = NULL;
	m_pWarpSectorToWarpMissionAlt = NULL;

	m_pWarpMissionToWarpSector = NULL;
	m_pWarpMissionToWarpSectorAlt = NULL;

	m_pWarpToWarp = NULL;
	m_pWarpToWarpAlt = NULL;

	m_pMissionToArriveWhenPassed = NULL;
	m_pMissionToArriveWhenPassedAlt = NULL;

	m_pMissionToThrowWhenFindMine = NULL;
	m_pMissionToThrowWhenFindMineAlt = NULL;

	m_pLink0Hidden = NULL;
	m_pLink1Hidden = NULL;
	m_pLink2Hidden = NULL;
	m_pLink3Hidden = NULL;

	m_pLinkToSwitch = NULL;

	m_pLink0Alt = NULL;
	m_pLink1Alt = NULL;
	m_pLink2Alt = NULL;
	m_pLink3Alt = NULL;

	m_pLinkBothDownHidden = NULL;
	m_pLinkBothUpHidden = NULL;

	m_pMissionReference = NULL;
	m_pMissionReferenceAlt = NULL;

	m_Playable = Playable_Playable;//por defecto jugable
	m_Type = MissionType_Mission;//y mission!
	m_BarrierType = MissionBarrier_Invalid;

	m_bInitialMission = false;
	m_bEnabled = true;
	m_bSwitchOffOn = false;
	m_bUsingMineHoldMode = false;
	m_bStageBreak = false;
	m_bRandomSong = false;
	m_bHasBarrier = false;
	m_bLocked = false;
	m_bUseExtraStats = false;
	m_bHoldsNoBody = false;
	m_bCanUseArrowMods = true;

	m_iNumCodes = 0;
	m_bNeededCodes = false;
}

//en realidad es esta funcion se setea todo...
bool Mission::CheckSong( bool bCheckRandom )
{
	m_Type = StringToMissionType( m_MissionGoals.sType );

	//cuenta tambien el switch, por que no tiene song.
	//¿por que habrian de desbloquearse songs en los warps? o n los switchs?
	bool bWarpState = (GetMissionType() == MissionType_Mission) || (GetMissionType() == MissionType_Boss) || 
						(GetMissionType() == MissionType_DualWarp); /*(GetMissionType() != MissionType_Warp) && (GetMissionType() != MissionType_WarpSector)
						&& (GetMissionType() != MissionType_WarpToWarp) && (GetMissionType() != MissionType_Switch);*/

	//esto lo hacemos selectivamente, por que si lo hacemos mientras cargan las canciones
	//siempre nos retorna null por el vector<song> shuffle
	if( bCheckRandom )//si podemos verificar la canción
		CheckRandomSong();

	if( !m_pSong && bWarpState && !m_bRandomSong )
	{
		LOG->Trace( "Song No Existe! y no es Warp ni WarpSector, Ignorando mision %s", m_MissionGoals.sMissionName.c_str() );
		return false;
	}

	if( m_MissionGoals.sInitial.CompareNoCase( "YES" ) == 0 )
		m_bInitialMission = true;

	if( !bWarpState )//si no, qeda en NULL 
	{
		m_pSong = NULL;
		m_pLockedSong = NULL;//los warps no desbloquean nunca
	}

	if( m_pSong && !m_bRandomSong )
	{
		m_pSong->m_bIsWorldMaxMission = true;
	}
	
	/*if( m_pLockedSong )
	{
		LOG->Trace( "SongLocked: %s", m_pLockedSong->GetTranslitFullTitle().c_str() );
		//m_pLockedSong->m_bEnabledByMission = false;
		//m_pLockedSong->m_sMissionNeededToUnlock = GetMissionName();//despues la buscamos cuando cargamos una cancion
	}*/

	if( m_MissionGoals.sPlayable.CompareNoCase("NO") == 0 )//tambien pueden estar ocultos los wraps
		this->SetPlayable( Playable_Hidden );

	if( m_MissionGoals.sMineHoldMode.CompareNoCase("YES") == 0 )
		m_bUsingMineHoldMode = true;

	if( m_MissionGoals.sBreak.CompareNoCase( "YES" ) == 0 )
		m_bStageBreak = true;

	if( m_MissionGoals.sUseExtraStats.CompareNoCase( "YES" ) == 0 )
		m_bUseExtraStats = true;

	if( m_MissionGoals.sHoldsNoBody.CompareNoCase( "YES" ) == 0 )
		m_bHoldsNoBody = true;

	if( m_MissionGoals.sShowArrowMods.CompareNoCase( "NO" ) == 0 )
		m_bCanUseArrowMods = false;

	return true;
}

//llamada cuando se leen las misiones en mission sector!
void Mission::SetUpMissionState( Profile* pProfile )
{
	{
		RString hidden = m_MissionGoals.sHiddenUntil;
		vector<RString> misions;
		split( hidden, ",", misions );
		vector<bool> vbPlayable;

		FOREACHS_CONST( RString, pProfile->m_vsWorldMaxMissionPlayed, mp )
		{
			RString mission = mp->c_str();

			for( unsigned i = 0 ; i < misions.size(); i++ )
			{
				//si ya pasamos la mission
				if( !stricmp( misions[i], mission ) )
					vbPlayable.push_back( true );
			}
		}

		if( (int)vbPlayable.size() == (int)misions.size() )
			this->SetPlayable( Playable_Playable );
	}

	FOREACHS_CONST( RString, pProfile->m_vsWorldMaxMissionPlayed, mp )
	{
		RString mission = mp->c_str();
		RString missionname = this->GetMissionName();

		if( !stricmp( missionname, mission ) )//ponemos esto despues por que es probable que la hayamos
			this->SetPlayable( Playable_Disabled );//pasado y la pone a disabled!
	}

	CheckBarrierData( pProfile );
}

void Mission::ConfirmSwitchState( const RString sSwitchName )
{
	/*
		Revisamos el estado de los switchs aqui también
		por que cuando volvemos a la screen o ingresamos 
		restaura el estado en que quedo él.
	*/

	m_bSwitchOffOn = true;

	{
		vector<RString> vsInv;
		vector<RString> vsVal;
		split( m_MissionGoals.sInvalidateMissionWhenIsActive, ",", vsInv );
		split( m_MissionGoals.sValidateMissionWhenIsActive, ",", vsVal );

		FOREACH_CONST( RString, vsInv, iter )
		{
			Mission* pmision;
			pmision = SONGMAN->FindMission( (*iter) );

			if( pmision )
				pmision->Invalidate();
		}

		FOREACH_CONST( RString, vsVal, iter )
		{
			Mission* pmision;
			pmision = SONGMAN->FindMission( (*iter) );

			if( pmision )
				pmision->Validate();
		}
	}
}

bool Mission::CheckBarrierData( Profile *pProfile )
{
	if( m_MissionGoals.sBarrierString.size() )
	{
		//aqui dentro para que solo escriba en las que tienen barreras
		FOREACHS_CONST( RString, pProfile->m_vsBarriersBroken, bb )
		{
			RString mission = bb->c_str();

			//si ya desbloqueamos la barrera.. para que otra vez?, return
			if( !stricmp( GetMissionName(), mission ) )
				return false;
		}

		vector<RString> vsStrBar;
		split( m_MissionGoals.sBarrierString,",",vsStrBar );

		ASSERT_M( (int)vsStrBar.size() == 3, ssprintf( "La misión %s no tiene un formato en #BARRIERDATA correcto", m_sPath.c_str() ) );

		//el primer argumento es el tipo de la barrera.
		m_BarrierType = StringToMissionBarrier( vsStrBar[0] );

		if( m_BarrierType != MissionBarrier_Invalid )
			m_bHasBarrier = true;

		RString sRequiredGoal = vsStrBar[1];//el goal como 2do argumento (nombre)
		RString sGoal = vsStrBar[2];//el goal, despues lo 'cast'eamos a numero o string segun el required goal

		if( sRequiredGoal.CompareNoCase("Credits")==0 )
		{
			//int iCreditsGoal = atoi(sGoal);

			//if( iCreditsGoal > pProfile->m_iTotalPlays )//por creditos jugados.
			//	m_bEnabled = false;
		}
		else if( sRequiredGoal.CompareNoCase("MinutesPlaying")==0 )
		{
			int iMinutesGoal = atoi(sGoal);

			if( iMinutesGoal > (pProfile->m_iTotalGameplaySeconds/60) )
				m_bEnabled = false;
		}
		else if( sRequiredGoal.CompareNoCase("PercentCompleted")== 0 )
		{
			int iPercentGoal = atoi(sGoal);

			if( iPercentGoal > pProfile->GetWorldMaxCompletePercent() )
				m_bEnabled = false;
		}
		else if( sRequiredGoal.CompareNoCase("MissionCompleted")==0 )
		{
			bool bPlayable = false;
			FOREACHS_CONST( RString, pProfile->m_vsWorldMaxMissionPlayed, mp )
			{
				RString mission = mp->c_str();

				if( !stricmp( sGoal, mission ) )//si ya pasamos la mission
					bPlayable = true;
			}

			m_bEnabled = bPlayable;
		}

		if( m_bEnabled )
		{
			pProfile->m_vsBarriersBroken.insert( GetMissionName() );
			GAMESTATE->m_LastBarrierType = GetBarrierType();
			GAMESTATE->m_bBarrierPassed = true;
			//LOG->Trace( "Enabling Barrier" );
			return true;
		}
	}

	return false;
}

void Mission::SetUpLinks()
{
	//seteamos el switch primero, por los links
	m_pLinkToSwitch = SONGMAN->FindMission( m_MissionGoals.sSwitchActive );

	{//link 0
		vector<RString> vsMissions;
		split( m_MissionGoals.sLinkUpLeft, ",", vsMissions );
		if( vsMissions.size() > 1 )
		{
			m_pLink0 = SONGMAN->FindMission( vsMissions[0] );
			m_pLink0Alt = SONGMAN->FindMission( vsMissions[1] );
		}
		else if( vsMissions.size() == 1 )
		{
			m_pLink0 = SONGMAN->FindMission( vsMissions[0] );
			m_pLink0Alt = SONGMAN->FindMission( vsMissions[0] );
		}
	}
	{//link 1
		vector<RString> vsMissions;
		split( m_MissionGoals.sLinkUpRight, ",", vsMissions );
		if( vsMissions.size() > 1 )
		{
			m_pLink1 = SONGMAN->FindMission( vsMissions[0] );
			m_pLink1Alt = SONGMAN->FindMission( vsMissions[1] );
		}
		else if( vsMissions.size() == 1 )
		{
			m_pLink1 = SONGMAN->FindMission( vsMissions[0] );
			m_pLink1Alt = SONGMAN->FindMission( vsMissions[0] );
		}
	}
	{//link 2
		vector<RString> vsMissions;
		split( m_MissionGoals.sLinkDownLeft, ",", vsMissions );
		if( vsMissions.size() > 1 )
		{
			m_pLink2 = SONGMAN->FindMission( vsMissions[0] );
			m_pLink2Alt = SONGMAN->FindMission( vsMissions[1] );
		}
		else if( vsMissions.size() == 1 )
		{
			m_pLink2 = SONGMAN->FindMission( vsMissions[0] );
			m_pLink2Alt = SONGMAN->FindMission( vsMissions[0] );
		}
	}
	{//link 3
		vector<RString> vsMissions;
		split( m_MissionGoals.sLinkDownRight, ",", vsMissions );
		if( vsMissions.size() > 1 )
		{
			m_pLink3 = SONGMAN->FindMission( vsMissions[0] );
			m_pLink3Alt = SONGMAN->FindMission( vsMissions[1] );
		}
		else if( vsMissions.size() == 1 )
		{
			m_pLink3 = SONGMAN->FindMission( vsMissions[0] );
			m_pLink3Alt = SONGMAN->FindMission( vsMissions[0] );
		}
	}

	{//m_pWarpSectorToWarpMission
		vector<RString> vsMissions;
		split( m_MissionGoals.sLinkWarpSector, ",", vsMissions );
		if( vsMissions.size() > 1 )
		{
			m_pWarpSectorToWarpMission = SONGMAN->FindMission( vsMissions[0] );
			m_pWarpSectorToWarpMissionAlt = SONGMAN->FindMission( vsMissions[1] );
		}
		else if( vsMissions.size() == 1 )
		{
			m_pWarpSectorToWarpMission = SONGMAN->FindMission( vsMissions[0] );
			m_pWarpSectorToWarpMissionAlt = SONGMAN->FindMission( vsMissions[0] );
		}
	}

	{//m_pWarpMissionToWarpSector
		vector<RString> vsMissions;
		split( m_MissionGoals.sLinkWarpMission, ",", vsMissions );
		if( vsMissions.size() > 1 )
		{
			m_pWarpMissionToWarpSector = SONGMAN->FindMission( vsMissions[0] );
			m_pWarpMissionToWarpSectorAlt = SONGMAN->FindMission( vsMissions[1] );
		}
		else if( vsMissions.size() == 1 )
		{
			m_pWarpMissionToWarpSector = SONGMAN->FindMission( vsMissions[0] );
			m_pWarpMissionToWarpSectorAlt = SONGMAN->FindMission( vsMissions[0] );
		}
	}

	{//m_pMissionToArriveWhenPassed
		vector<RString> vsMissions;
		split( m_MissionGoals.sMissionToSelectWhenPassed, ",", vsMissions );
		if( vsMissions.size() > 1 )
		{
			m_pMissionToArriveWhenPassed = SONGMAN->FindMission( vsMissions[0] );
			m_pMissionToArriveWhenPassedAlt = SONGMAN->FindMission( vsMissions[1] );
		}
		else if( vsMissions.size() == 1 )
		{
			m_pMissionToArriveWhenPassed = SONGMAN->FindMission( vsMissions[0] );
			m_pMissionToArriveWhenPassedAlt = SONGMAN->FindMission( vsMissions[0] );
		}
	}

	{//m_pWarpToWarp
		vector<RString> vsMissions;
		split( m_MissionGoals.sLinkToOtherWarp, ",", vsMissions );
		if( vsMissions.size() > 1 )
		{
			m_pWarpToWarp = SONGMAN->FindMission( vsMissions[0] );
			m_pWarpToWarpAlt = SONGMAN->FindMission( vsMissions[1] );
		}
		else if( vsMissions.size() == 1 )
		{
			m_pWarpToWarp = SONGMAN->FindMission( vsMissions[0] );
			m_pWarpToWarpAlt = SONGMAN->FindMission( vsMissions[0] );
		}
	}

	{//m_pMissionReference
		vector<RString> vsMissions;
		split( m_MissionGoals.sLinkMissionReference, ",", vsMissions );
		if( vsMissions.size() > 1 )
		{
			m_pMissionReference = SONGMAN->FindMission( vsMissions[0] );
			m_pMissionReferenceAlt = SONGMAN->FindMission( vsMissions[1] );
		}
		else if( vsMissions.size() == 1 )
		{
			m_pMissionReference = SONGMAN->FindMission( vsMissions[0] );
			m_pMissionReferenceAlt = SONGMAN->FindMission( vsMissions[0] );
		}
	}

	{//m_pMissionToThrowWhenFindMine
		vector<RString> vsMissions;
		split( m_MissionGoals.sMissionToThrowWhenFindMine, ",", vsMissions );
		if( vsMissions.size() > 1 )
		{
			m_pMissionToThrowWhenFindMine = SONGMAN->FindMission( vsMissions[0] );
			m_pMissionToThrowWhenFindMineAlt = SONGMAN->FindMission( vsMissions[1] );
		}
		else if( vsMissions.size() == 1 )
		{
			m_pMissionToThrowWhenFindMine = SONGMAN->FindMission( vsMissions[0] );
			m_pMissionToThrowWhenFindMineAlt = SONGMAN->FindMission( vsMissions[0] );
		}
	}

	m_pLink0Hidden = SONGMAN->FindMission( m_MissionGoals.sLinkUpLeftHidden );
	m_pLink1Hidden = SONGMAN->FindMission( m_MissionGoals.sLinkUpRightHidden );
	m_pLink2Hidden = SONGMAN->FindMission( m_MissionGoals.sLinkDownLeftHidden );
	m_pLink3Hidden = SONGMAN->FindMission( m_MissionGoals.sLinkDownRightHidden );

	m_pLinkBothDownHidden = SONGMAN->FindMission( m_MissionGoals.sLinkBothDownHidden );
	m_pLinkBothUpHidden = SONGMAN->FindMission( m_MissionGoals.sLinkBothUpHidden );
}

void Mission::SetPath( RString sPath )
{
	LOG->Trace( "%s", sPath.c_str() );
	m_sPath = sPath;
}

bool Mission::Matches( RString sGroup, RString sMission ) const
{
	//LOG->Trace( "Group: %s, sMission: %s, MissionName: %s", sGroup.c_str(), sMission.c_str(), this->m_MissionGoals.sMissionName.c_str() );

	//SE compara con el nombre de la mission!
	if( sMission.EqualsNoCase(this->m_MissionGoals.sMissionName) )
		return true;

	return false;
}

bool Mission::GetSwitchCondition() const
{
	//retornará verdadero si existe el switch y está habilitado ("ON")
	return (m_pLinkToSwitch && m_pLinkToSwitch->GetSwitchState());
}

//TODO: ¿Volver a checkear si existen?
bool Mission::CanGoUpLeft()
{
	if( m_pLink0 && m_pLink0->IsPlayable() && m_pLink0->IsValid() && !GetSwitchCondition() )
		return true;

	if( m_pLink0Alt && m_pLink0Alt->IsPlayable() && m_pLink0Alt->IsValid() && GetSwitchCondition() )
		return true;

	return false;
}

bool Mission::CanGoUpLeftHidden()
{
	if( m_pLink0Hidden && m_pLink0Hidden->IsPlayable() && m_pLink0Hidden->IsValid() && !CanGoUpLeft() )
		return true;

	return false;
}

bool Mission::CanGoUpRight()
{
	if( m_pLink1 && m_pLink1->IsPlayable() && m_pLink1->IsValid()  && !GetSwitchCondition() )
		return true;

	if( m_pLink1Alt && m_pLink1Alt->IsPlayable() && m_pLink1Alt->IsValid() && GetSwitchCondition() )
		return true;

	return false;
}

bool Mission::CanGoUpRightHidden()
{
	if( m_pLink1Hidden && m_pLink1Hidden->IsPlayable() && m_pLink1Hidden->IsValid() && !CanGoUpRight() )
		return true;

	return false;
}

bool Mission::CanGoDownLeft()
{
	if( m_pLink2 && m_pLink2->IsPlayable() && m_pLink2->IsValid()  && !GetSwitchCondition() )
		return true;

	if( m_pLink2Alt && m_pLink2Alt->IsPlayable() && m_pLink2Alt->IsValid() && GetSwitchCondition() )
		return true;

	return false;
}

bool Mission::CanGoDownLeftHidden()
{
	if( m_pLink2Hidden && m_pLink2Hidden->IsPlayable() && m_pLink2Hidden->IsValid() && !CanGoDownLeft() )
		return true;

	return false;
}

bool Mission::CanGoDownRight()
{
	if( m_pLink3 && m_pLink3->IsPlayable() && m_pLink3->IsValid()  && !GetSwitchCondition() )
		return true;

	if( m_pLink3Alt && m_pLink3Alt->IsPlayable() && m_pLink3Alt->IsValid() && GetSwitchCondition() )
		return true;

	return false;
}

bool Mission::CanGoDownRightHidden()
{
	if( m_pLink3Hidden && m_pLink3Hidden->IsPlayable() && m_pLink3Hidden->IsValid() && !CanGoDownRight() )
		return true;

	return false;
}

bool Mission::CanGoBothDownHidden()
{
	if( m_pLinkBothDownHidden && m_pLinkBothDownHidden->IsPlayable() && m_pLinkBothDownHidden->IsValid() )
		return true;

	return false;
}

bool Mission::CanGoBothUpHidden()
{
	if( m_pLinkBothUpHidden && m_pLinkBothUpHidden->IsPlayable() && m_pLinkBothUpHidden->IsValid() )
		return true;

	return false;
}

void Mission::ResetState()
{
	m_Playable = Playable_Playable;//por defecto jugable

	m_Type = MissionType_Mission;

	m_bEnabled = true;

	m_bSwitchOffOn = false;

	//igualmente lo hacemos por que las misiones ya estan escogidas
	//cuales sirven y cuales no en SONGMAN->initmissionfd.
	CheckSong( true );
}

bool Mission::IsSongPlayable()
{
	switch( GetMissionType() )
	{
	case MissionType_Warp:
	case MissionType_WarpSector:
	case MissionType_WarpToWarp:
	case MissionType_Switch:
	case MissionType_HiddenLine:
	case MissionType_SpaceWarp:
		return false;
		break;
	case MissionType_DualWarp:
		if( IsPassed() )
			return false;
		break;
	}

	if( m_pSong )//assert song
		if( GAMESTATE->GetNumStagesLeft( GAMESTATE->GetFirstHumanPlayer() ) < GAMESTATE->GetNumStagesMultiplierForSong( m_pSong ) )
			return false;

	if( !IsPlayable() )
		return false;

	return true;
}

void Mission::CheckRandomSong()
{
	//if( m_pSong ) //si ya tenemos song, return
	//	return;

	//si no encontramos la canción significa que será un random.
	//estas son del tipo.
	//Random,crazy; por ejemplo.
	//si es dw y no la hemos pasado
	bool bDW = GetMissionType() == MissionType_DualWarp && !this->IsPassed();
	if( GetMissionType() == MissionType_Mission || GetMissionType() == MissionType_Boss  || bDW )
	{
	//comprobamos si es null, por que en msLoaderMSF si no encuentra la canción, NULL
		if( m_pSong == NULL )
		{
			vector<RString> vsRandom;
			split( m_MissionGoals.sSongPathToLoad, ",", vsRandom );

			if( vsRandom[0].CompareNoCase( "Random" )==0 )
			{
				Song* pSong = NULL;
				
				for( unsigned i=0; i < 1000; i++ )
				{
					//CHECKPOINT_M( ssprintf( "i = %d", i ) );
					pSong = SONGMAN->GetRandomSong();

					if( pSong == NULL ) //assert
						continue;

					//CHECKPOINT;
					//encuentra el modo
					Steps* pSteps = NULL;
					//CHECKPOINT;
					if( vsRandom[1].CompareNoCase( "Easy" )==0 )
					{
						pSteps = SongUtil::GetStepsByDifficulty( pSong, StepsType_pump_single, Difficulty_Easy, false );
						if ( pSteps == NULL )
							continue;
					}
					else if( vsRandom[1].CompareNoCase( "Hard" )==0 )
					{
						pSteps = SongUtil::GetStepsByDifficulty( pSong, StepsType_pump_single, Difficulty_Medium, false );
						if ( pSteps == NULL )
							continue;
					}
					else if( vsRandom[1].CompareNoCase( "Crazy" )==0 )
					{
						pSteps = SongUtil::GetStepsByDifficulty( pSong, StepsType_pump_single, Difficulty_Hard, false );
						if ( pSteps == NULL )
							continue;
					}
					else if( vsRandom[1].CompareNoCase( "Freestyle" )==0 )
					{
						pSteps = SongUtil::GetStepsByDifficulty( pSong, StepsType_pump_double, Difficulty_Medium, false );
						if ( pSteps == NULL )
							continue;
					}
					else if( vsRandom[1].CompareNoCase( "Nightmare" )==0 )
					{
						pSteps = SongUtil::GetStepsByDifficulty( pSong, StepsType_pump_double, Difficulty_Hard, false );
						if ( pSteps == NULL )
							continue;
					}
					else
						ASSERT_M( 0, ssprintf( "La mission %s no tiene los argumentos correctos para ser random", m_sPath.c_str() ) );

					break;
				}//end for

				//CHECKPOINT;
				//ASSERT_M( pSong, "Tienes Songs para Arcade Station?" );
				if(!pSong)
					LOG->Trace("No se encontro una cancion para setear una mission 'Random', en algun momento habrá un crash");
				this->m_pSong = pSong;

				//77CHECKPOINT;
				//if( pSong )
				//	m_pSong = pSong;

				//CHECKPOINT;
			}//end if random
			//ASSERT_M( m_pSong, ssprintf( "La mission %s no tiene una canción posible para asignar o el tag #songpath:random,<difficulty>; esta mal escrito", m_sPath.c_str() ) );
		}//end if psong ==null
	}//end if missiontype
	//CHECKPOINT_M( ssprintf( "songname: %s", this->m_pSong->GetDisplayMainTitle().c_str() ));
	//ASSERT_M(m_pSong, "Tienes songs para Arcado Station?");
}

Mission::~Mission()
{
	m_pSong = NULL;
	m_pLockedSong = NULL;

	m_pLink0 = NULL;
	m_pLink1 = NULL;
	m_pLink2 = NULL;
	m_pLink3 = NULL;

	m_pWarpSectorToWarpMission = NULL;
	m_pWarpSectorToWarpMissionAlt = NULL;

	m_pWarpMissionToWarpSector = NULL;
	m_pWarpMissionToWarpSectorAlt = NULL;

	m_pWarpToWarp = NULL;
	m_pWarpToWarpAlt = NULL;

	m_pMissionToArriveWhenPassed = NULL;
	m_pMissionToArriveWhenPassedAlt = NULL;

	m_pMissionToThrowWhenFindMine = NULL;
	m_pMissionToThrowWhenFindMineAlt = NULL;

	m_pLink0Hidden = NULL;
	m_pLink1Hidden = NULL;
	m_pLink2Hidden = NULL;
	m_pLink3Hidden = NULL;

	m_pLinkToSwitch = NULL;

	m_pLink0Alt = NULL;
	m_pLink1Alt = NULL;
	m_pLink2Alt = NULL;
	m_pLink3Alt = NULL;

	m_pLinkBothDownHidden = NULL;
	m_pLinkBothUpHidden = NULL;

	m_pMissionReference = NULL;
	m_pMissionReferenceAlt = NULL;
}

// lua start
#include "LuaBinding.h"

class LunaMission: public Luna<Mission>
{
public:
	static int GetMissionName( T* p, lua_State *L )	{ lua_pushstring(L, p->GetMissionName() ); return 1; }
	static int GetMissionSector( T* p, lua_State *L )	{ lua_pushstring(L, p->GetMissionSector() ); return 1; }
	static int GetFirstGoal( T* p, lua_State *L )	{ lua_pushstring(L, p->GetFirstGoal() ); return 1; }
	static int GetSecondGoal( T* p, lua_State *L )	{ lua_pushstring(L, p->GetSecondGoal() ); return 1; }
	static int GetThirdGoal( T* p, lua_State *L )	{ lua_pushstring(L, p->GetThirdGoal() ); return 1; }
	static int GetExtraGoal( T* p, lua_State *L )	{ lua_pushstring(L, p->GetExtraGoal() ); return 1; }
	static int GetLevel( T* p, lua_State *L )		{ lua_pushinteger(L, p->m_MissionGoals.iLevel ); return 1; }
	static int IsWarp( T* p, lua_State *L )			{ lua_pushboolean(L, p->IsWarp() || (p->IsDualWarp() && p->IsPassed()) ); return 1; }
	static int IsWarpSector( T* p, lua_State *L )	{ lua_pushboolean(L, p->IsWarpSector() ); return 1; }
	static int IsWarpToWarp( T* p, lua_State *L )	{ lua_pushboolean(L, p->IsWarpToWarp() ); return 1; }
	static int IsBoss( T* p, lua_State *L )			{ lua_pushboolean(L, p->IsBoss() ); return 1; }
	static int IsPassed( T* p, lua_State *L )			{ lua_pushboolean(L, p->IsPassed() ); return 1; }
	static int IsSwitch( T* p, lua_State *L )			{ lua_pushboolean(L, p->IsSwitch() ); return 1; }
	static int IsHiddenLine( T* p, lua_State *L )		{ lua_pushboolean(L, p->IsHiddenLine() ); return 1; }
	static int IsSpaceWarp( T* p, lua_State *L )		{ lua_pushboolean(L, p->IsSpaceWarp() ); return 1; }
	static int UsingMineHoldMode( T* p, lua_State *L )	{ lua_pushboolean(L, p->IsUsingMineHoldMode() ); return 1; }
	static int GetAuthor( T* p, lua_State *L )	{ lua_pushstring(L, p->GetAuthor() ); return 1; }
	static int StageBreak( T* p, lua_State *L )	{ lua_pushboolean(L, p->IsUsingStageBreak() ); return 1; }
	static int GetRequiredPlayers( T* p, lua_State *L )	{ lua_pushinteger(L, p->m_MissionGoals.iRequiredPlayers ); return 1; }
	static int GetMissionMileage( T* p, lua_State *L )	{ lua_pushinteger(L, p->m_MissionGoals.iGivenMileage ); return 1; }
	static int GetMissionLand( T* p, lua_State *L )	{ lua_pushstring(L, p->m_MissionGoals.sLand ); return 1; }
	static int GetPosX( T* p, lua_State *L )	{ lua_pushnumber(L, p->m_MissionGoals.fposX ); return 1; }
	static int GetPosY( T* p, lua_State *L )	{ lua_pushnumber(L, p->m_MissionGoals.fposY ); return 1; }
	static int GetDescription( T* p, lua_State *L )	{ lua_pushstring(L, p->m_MissionGoals.sExtraInformation ); return 1; }
	static int UseExtraStats( T* p, lua_State *L )	{ lua_pushboolean(L, p->UseExtraStats() ); return 1; }
	static int GetIfAllowShowArrowMods( T* p, lua_State *L )	{ lua_pushboolean(L, p->GetIfAllowUseArrowMods() ); return 1; }
	static int CheckBarrierData( T* p, lua_State *L )
	{
		LUA->YieldLua();

		if( lua_isnil( L, 1 ) )
			;
		else
		{
			Profile* pProfile = Luna<Profile>::check( L, 1, true );
			lua_pushboolean(L, p->CheckBarrierData( pProfile ) );
		}

		LUA->UnyieldLua();
		return 1;
	}
	static int GetBarrierTypeString( T* p, lua_State *L )
	{
		RString bar = MissionBarrierToString( p->GetBarrierType() );
		lua_pushstring( L, bar );
		return 1;
	}

	LunaMission()
	{
		ADD_METHOD( GetMissionName );
		ADD_METHOD( GetMissionSector );
		ADD_METHOD( GetFirstGoal );
		ADD_METHOD( GetSecondGoal );
		ADD_METHOD( GetThirdGoal );
		ADD_METHOD( GetExtraGoal );
		ADD_METHOD( GetLevel );
		ADD_METHOD( IsWarp );
		ADD_METHOD( IsWarpSector );
		ADD_METHOD( IsWarpToWarp );
		ADD_METHOD( IsBoss );
		ADD_METHOD( IsPassed );
		ADD_METHOD( IsSwitch );
		ADD_METHOD( IsHiddenLine );
		ADD_METHOD( UsingMineHoldMode ); 
		ADD_METHOD( GetAuthor );
		ADD_METHOD( StageBreak );
		ADD_METHOD( GetRequiredPlayers );
		ADD_METHOD( GetMissionMileage );
		ADD_METHOD( GetMissionLand );
		ADD_METHOD( GetPosX );
		ADD_METHOD( GetPosY );
		ADD_METHOD( GetDescription );
		ADD_METHOD( CheckBarrierData );
		ADD_METHOD( GetBarrierTypeString );
		ADD_METHOD( UseExtraStats );
		ADD_METHOD( GetIfAllowShowArrowMods );
		ADD_METHOD( IsSpaceWarp );
	}
};

LUA_REGISTER_CLASS( Mission )
// lua end

/*
 * (c) 2009-2016 Victor Gajardo Henríquez "v1t0ko"
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
