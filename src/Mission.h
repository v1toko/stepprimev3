/* Mission - Objeto que guarda una cancion y objetivos (WorldMax). */

//TODO: Cambiar de variables bool a enum.

#ifndef MISSION_H
#define MISSION_H

#include "GameConstantsAndTypes.h"
#include "Song.h"
#include "Profile.h"

class Mission;
struct lua_State;

enum Playable { //jugable, pasada, y oculta
	Playable_Playable, Playable_Disabled, Playable_Hidden,
};

enum MissionType {
	MissionType_Mission,
	MissionType_Warp,
	MissionType_WarpSector,
	MissionType_Boss,
	MissionType_WarpToWarp,
	MissionType_Switch,
	MissionType_DualWarp,
	MissionType_HiddenLine,
	MissionType_SpaceWarp,//XXX:
	NUM_MissionType,
	MissionType_Invalid,
};
const RString& MissionTypeToString( MissionType ms );
MissionType StringToMissionType( const RString& s );

enum MissionBarrier {
	MissionBarrier_Blue,
	MissionBarrier_Cian,
	MissionBarrier_Gray,
	MissionBarrier_Green,
	MissionBarrier_LGreen,
	MissionBarrier_Red,
	MissionBarrier_Violet,
	NUM_MissionBarrier,
	MissionBarrier_Invalid,
};
const RString& MissionBarrierToString( MissionBarrier ms );
MissionBarrier StringToMissionBarrier( const RString& s );

//modificado por mi msf
struct MissionGoals
{
	int iNumCodes;

	//
	//mission configuration stuff
	//

	int iLevel;//[1-5]
	float fInitialLife;

	float fLifeDifficulty;//dificultad de vida, [1.4 , 0.6]

	RString sUseExtraStats;
	RString sCheckNoteSkin;//¿que noteskin revisamos para los 2 extrastats?

	//Song* pSong;
	float fposX;
	float fposY;
	RString sPlayable;
	RString sType;//mission, warp, warpsector, boss
	RString sNeedGraphics;//si tiene un spr distinto a las bullets.
	RString sShowArrowMods;//si es que se muestran los modificadores de mission en screenstage!
	RString sInitial;//punto de inicio del worldmax
	//para los switch, la mission o warpsector que tenemos que invalidar
	//puede tener 2 argumentos, el primero para invalidar una y el segundo para validar otra.
	RString sInvalidateMissionWhenIsActive;
	RString sValidateMissionWhenIsActive;

	//mision en la que nos dejará después de que hallamos pasado esta.
	RString sMissionToSelectWhenPassed;

	RString sHiddenUntil;//nombre de la mission que tiene que estar pasada para que mostremos alguna!
	RString sExtraInformation;
	RString sLand;//lugar en donde está la mission
	RString sAuthor;//autor de la mission

	int iRequiredPlayers;//numero de jugadores que la mission necesita
	int iGivenMileage;//cuanto mileage da
	RString sBreak;//break on off, si tiene stage break
	float fRequiredPercent;//porcentaje requerido para desbloquear la mision
	int iBombPosibility;
	RString sMissionToThrowWhenFindMine;

	//XXX:
	/*RString sUnlockSong;
	RString sUnlockBarrier;*/

	//luego se tranforman en *Mission
	//links direcciones
	RString sLinkUpLeft;
	RString sLinkUpRight;
	RString sLinkDownLeft;
	RString sLinkDownRight;
	//links lugares
	RString sLinkWarpSector;//para select sector ---> mapa
	RString sLinkWarpMission;//para mapa ---> sector select
	RString sLinkToOtherWarp;//cuando ya se es warp ---> otro warp
	//para los hidden line, cuando nos equivocamos de mission, volvemos aqui!
	RString sLinkMissionReference;
	//links ocultos
	RString sLinkUpLeftHidden;
	RString sLinkUpRightHidden;
	RString sLinkDownLeftHidden;
	RString sLinkDownRightHidden;
	RString sLinkBothDownHidden;
	RString sLinkBothUpHidden;

	//switch #LANDSTOHIDE:;
	RString sLandsToHide;
	//switch #LANDSTOSHOW:;
	RString sLandsToShow;

	//link para el switch que necesita estar
	//activo para que los links alternativos
	//trabajen.
	RString sSwitchActive;

	RString sSongToUnlock; //cancion que desbloquearemos.
	RString sSongPathToLoad;//lo llena el #songpath y puede tener random

	RString sHoldsNoBody;

	RString sGoalText;//que hay que hacer?
	RString sGoalText2;//''
	RString sGoalText3;//"
	RString sGoalTextExtra;//"
	RString Grade;//que nota tengo que sacar? ej: S, A, B, C, D, F
	RString sModifiers;//que modificadores tendrá la mission? ej: 4X, RG etc
	RString sMissionName;//que nombre tendra la mission? EJ: "Rune"
	RString sMissionSector;//nombre del sector en que estarán EJ: "MIRTAIN"
	RString sRequiredMission;//mision requerida antes de pasar otra
	RString sMineHoldMode;//si cuando en un hold en medio hay minas ^^
	RString sBarrierString;//identifica barrela y lo que hay que hacer para pasarla

	RString sMissionStringCommand;
	//permite agregar comandos al actor (player)
	RString sPlayerActorCommands;

	virtual ~MissionGoals() {}
	MissionGoals();
};

//invalidar mission y tag para el switch
class Mission
{
	RString m_sPath;
	void CheckRandomSong();//verifica y asigna la canción random, para más orden.
	bool GetSwitchCondition() const;
public:
	Mission();
	virtual ~Mission();

	bool m_bRandomSong;

	Song* m_pSong;
	Song* m_pLockedSong;

	//cuando la tenemos pasado = 0...
	//si no tenemos ingresado un codigo para ella...
	//tiene un valor igual a el numero de codigos 
	//que hay que ingresar para activarla...
	//5 - 10 por los items.
	int m_iNumCodes;
	bool m_bNeededCodes;

	MissionGoals m_MissionGoals;

	bool IsPlayable() { return (m_Playable != Playable_Hidden); };
	bool IsPassed() { return (m_Playable == Playable_Disabled); };
	bool IsSongPlayable();
	bool IsLocked() { return m_bLocked; };

	bool CheckSong( bool bCheckRandom=false );
	bool CheckBarrierData( Profile* pProfile );

	bool Matches(RString sGroup, RString sMission) const;
	void SetPath( RString sPath );

	void SetUpLinks();//se hace despues de cargar todas las misiones
	void SetUpMissionState( Profile* pProfile );//carga el estado de las misiones!
	void ResetState();//no reset a los missiongoals
	void Invalidate() { m_bEnabled = false; };
	void Validate() { m_bEnabled = true; };

	void Lock() { m_bLocked = true; };
	void UnLock() { m_bLocked = false; };

	void ToggleSwitch() 
	{
		if( !IsSwitch() )
			return;

		if( m_bSwitchOffOn ) 
			m_bSwitchOffOn = false;
		else 
			m_bSwitchOffOn = true; 
	};
	//call by CheckSong and WorldMaxScreen
	void ConfirmSwitchState( const RString sSwitchName );//lamada para cerrar los switch abiertos y dejar uno solo.

	bool CanGoUpLeft();//si es que podemos movernos
	bool CanGoUpRight();
	bool CanGoDownLeft();
	bool CanGoDownRight();

	//ellos son usables solo cuando la direccion "que se ve" no está disponible.
	bool CanGoUpLeftHidden();//si es que podemos movernos
	bool CanGoUpRightHidden();
	bool CanGoDownLeftHidden();
	bool CanGoDownRightHidden();

	bool CanGoBothDownHidden();
	bool CanGoBothUpHidden();

	void SetPlayable( Playable NewValue ) { m_Playable = NewValue; };

	bool IsWarp() { return (m_Type == MissionType_Warp); };
	bool IsWarpSector() { return (m_Type == MissionType_WarpSector); };
	bool IsBoss() { return (m_Type == MissionType_Boss); };
	bool IsWarpToWarp() { return (m_Type == MissionType_WarpToWarp); };
	bool IsInitialMission() { return m_bInitialMission; };
	bool IsSwitch() { return (m_Type == MissionType_Switch); };//cambia On Off
	bool IsDualWarp() { return ( m_Type == MissionType_DualWarp ); };
	bool IsHiddenLine() { return (m_Type == MissionType_HiddenLine ); };	
	bool IsSpaceWarp() { return (m_Type == MissionType_SpaceWarp ); };
	bool HasBarrier() { return m_bHasBarrier; };

	bool GetIfAllowUseArrowMods() { return m_bCanUseArrowMods; };

	MissionType GetMissionType() { return m_Type; };
	void SetMissionType( MissionType NewType ) { m_Type = NewType; };

	MissionBarrier GetBarrierType() { return m_BarrierType; };
	void SetBarrierType( MissionBarrier NewType ) { m_BarrierType = NewType; };

	bool GetSwitchState() { return m_bSwitchOffOn; };
	bool IsValid() { return m_bEnabled; };
	bool IsUsingMineHoldMode() { return m_bUsingMineHoldMode; };
	bool IsUsingStageBreak() { return m_bStageBreak; };
	bool UseExtraStats() { return m_bUseExtraStats; };
	bool HoldsNotShowBody() { return m_bHoldsNoBody; };

	// Lua
	void PushSelf( lua_State *L );

	//retornan el links, si el switch está activo (y si existe) retornan el link alternativo
	//si no es asi, retornan el link común.
	Mission* GetLinkUpLeft() { 	return GetSwitchCondition() ? m_pLink0Alt : m_pLink0; };
	Mission* GetLinkUpRight() { return GetSwitchCondition() ? m_pLink1Alt : m_pLink1; };
	Mission* GetLinkDownLeft() { return GetSwitchCondition() ? m_pLink2Alt : m_pLink2; };
	Mission* GetLinkDownRight() { return GetSwitchCondition() ? m_pLink3Alt : m_pLink3; };

	Mission* GetLinkToMap() { return GetSwitchCondition() ? m_pWarpSectorToWarpMissionAlt : m_pWarpSectorToWarpMission; };
	Mission* GetLinkToSectors() { return GetSwitchCondition() ? m_pWarpMissionToWarpSectorAlt : m_pWarpMissionToWarpSector; };
	Mission* GetLinkWarpToWarp() { return GetSwitchCondition() ? m_pWarpToWarpAlt : m_pWarpToWarp; };

	Mission* GetLinkUpLeftHidden() { return m_pLink0Hidden; };//retorna el link!
	Mission* GetLinkUpRightHidden() { return m_pLink1Hidden; };
	Mission* GetLinkDownLeftHidden() { return m_pLink2Hidden; };
	Mission* GetLinkDownRightHidden() { return m_pLink3Hidden; };
	Mission* GetLinkBothDownHidden() { return m_pLinkBothDownHidden; };
	Mission* GetLinkBothUpHidden() { return m_pLinkBothUpHidden; };

	Mission* GetMissionReference() { return GetSwitchCondition() ? m_pMissionReferenceAlt : m_pMissionReference; };
	Mission* GetMissionToArriveWhenPassed() { return GetSwitchCondition() ? m_pMissionToArriveWhenPassedAlt : m_pMissionToArriveWhenPassed; };
	Mission* GetMissionToThrowWhenFindMine() { return GetSwitchCondition() ? m_pMissionToThrowWhenFindMineAlt : m_pMissionToThrowWhenFindMine; };

	RString GetMissionName() const { return m_MissionGoals.sMissionName; };
	RString GetMissionLand() const { return m_MissionGoals.sLand; };
	const RString &GetMissionSector() const { return m_MissionGoals.sMissionSector; };
	const RString &GetFirstGoal() const { return m_MissionGoals.sGoalText; };
	const RString &GetSecondGoal() const { return m_MissionGoals.sGoalText2; };
	const RString &GetThirdGoal() const { return m_MissionGoals.sGoalText3; };
	const RString &GetExtraGoal() const { return m_MissionGoals.sGoalTextExtra; };
	const RString &GetAuthor() const { return m_MissionGoals.sAuthor; };

	const int &GetRequiredPlayers() const { return m_MissionGoals.iRequiredPlayers; };
	const bool &GetStageBreak() const { return m_bStageBreak; };
	const int &GetMissionMileage() const { return m_MissionGoals.iGivenMileage; };

protected:
	Playable m_Playable;
	MissionType m_Type;
	MissionBarrier m_BarrierType;

	bool m_bInitialMission;//es la mission donde empieza worldmax?
	bool m_bSwitchOffOn;//false = off -> true = on, solo si el tipo es "Switch"
	bool m_bEnabled;//Dice si nos podemos mover a esta mision
	bool m_bUsingMineHoldMode;//si usamos minas en medio de los holds
	bool m_bStageBreak;//stage break
	bool m_bHasBarrier;//tiene barrera?
	bool m_bLocked;//esta bloqueada?
	bool m_bUseExtraStats;//cuando usamos más de un noteskins
	bool m_bHoldsNoBody;//no mostrar cuerpo de los holds
	bool m_bCanUseArrowMods;//mostramos los arrowmods

	//cada uno apunta a cualquiera de las 4 direcciones (graficamente)
	Mission* m_pLink0;//up left
	Mission* m_pLink1;// up right
	Mission* m_pLink2;//down left
	Mission* m_pLink3;//down right

	//links alternativos
	Mission* m_pLinkToSwitch; //si este switch está activo
	//ellos trabajan sobre los m_pLink#
	Mission* m_pLink0Alt;//up left
	Mission* m_pLink1Alt;// up right
	Mission* m_pLink2Alt;//down left
	Mission* m_pLink3Alt;//down right

	Mission* m_pWarpSectorToWarpMission;//WarpSector (general map) ----> WarpMission (map, like, rootinia etc.)
	Mission* m_pWarpSectorToWarpMissionAlt;

	Mission* m_pWarpMissionToWarpSector;//WarpMission (map, like, rootinia etc. )----> WarpSector (general map)
	Mission* m_pWarpMissionToWarpSectorAlt;

	Mission* m_pWarpToWarp;//warp ----> warp  ? types
	Mission* m_pWarpToWarpAlt;

	Mission* m_pMissionReference;//hidden line, mission de referencia
	Mission* m_pMissionReferenceAlt;//hidden line, mission de referencia

	Mission* m_pMissionToArriveWhenPassed;//pasamos una cualquiera, aparecemos en esta.
	Mission* m_pMissionToArriveWhenPassedAlt;//pasamos una cualquiera, aparecemos en esta.

	//las 4 direcciones igual que arriba, pero ocultas
	Mission* m_pLink0Hidden;
	Mission* m_pLink1Hidden;
	Mission* m_pLink2Hidden;
	Mission* m_pLink3Hidden;

	Mission* m_pMissionToThrowWhenFindMine;//te pilla la mina, te envia aca (like ufo)
	Mission* m_pMissionToThrowWhenFindMineAlt;//te pilla la mina, te envia aca (like ufo)

	//las flechas pres, juntas y ocultas por defecto
	Mission* m_pLinkBothDownHidden;
	Mission* m_pLinkBothUpHidden;
};

#endif

/*
 * (c) 2009-2016 Victor Gajardo Henríquez "v1t0ko"
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
