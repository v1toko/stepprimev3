#include "global.h"
#include "MsdFile.h"
#include "RageFileManager.h"
#include "RageLog.h"
#include "RageUtil.h"
#include "Song.h"
#include "Mission.h"
#include "SongManager.h"
#include "MissionLoaderMSF.h"

void MSFLoader::GetApplicableFiles( const RString &sPath, vector<RString> &out )
{
	GetDirListing( sPath + RString("*.msf"), out );
}

bool MSFLoader::LoadFromDir( const RString &sPath, Mission &out )
{
	vector<RString> aFileNames;
	MSFLoader::GetApplicableFiles( sPath, aFileNames );

	if( aFileNames.size() > 1 || aFileNames.size() <= 0 )
	{
		//LOG->Trace( "Song", sPath, "has more than one or 0 MSFs file. There should be one!" );
		return false;
	}

	/* Aunque hayan más de 1 .msf igual usamos el primero y seguimos */

	return MSFLoader::LoadFromMSFFile( sPath + aFileNames[0], out );
}

bool MSFLoader::LoadFromMSFFile( const RString &sPath, Mission &out )
{
	LOG->Trace( "Mission::LoadFromMSFFile(%s)", sPath.c_str() );

	MsdFile msd;
	if( !msd.ReadFile( sPath, true ) )  // unescape
	{
		//LOG->Trace( "MSF file", sPath, "couldn't be opened: %s", msd.GetError().c_str() );
		return false;
	}

	//XXX:
	//Set the mision group here.
	vector<RString> sDirs;
	split( sPath, "/", sDirs );
	out.m_MissionGoals.sMissionSector = sDirs[sDirs.size() - 2];

	for( unsigned i=0; i<msd.GetNumValues(); i++ )
	{
		//int iNumParams = msd.GetNumParams(i); sin uso!
		const MsdFile::value_t &sParams = msd.GetValue(i);
		RString sValueName = sParams[0];
		sValueName.MakeUpper();

		if( sValueName=="BARRIERDATA" )
			out.m_MissionGoals.sBarrierString = sParams[1];

		else if( sValueName=="USEEXTRASTATS" )
			out.m_MissionGoals.sUseExtraStats = sParams[1];

		else if( sValueName=="HOLDSNOBODY" )
			out.m_MissionGoals.sHoldsNoBody = sParams[1];

		else if( sValueName=="NOTESKINTOCHECK" )
			out.m_MissionGoals.sCheckNoteSkin = sParams[1];

		else if( sValueName=="MINEPOSIBILITY" )
			out.m_MissionGoals.iBombPosibility = atoi( sParams[1] );

		else if( sValueName=="THROWTOMISSION" )
			out.m_MissionGoals.sMissionToThrowWhenFindMine = sParams[1];

		else if( sValueName=="MISSIONTOSELECTWHENPASSED" )
			out.m_MissionGoals.sMissionToSelectWhenPassed = sParams[1];

		else if( sValueName=="MISSIONOBJECTIVES" )
			out.m_MissionGoals.sMissionStringCommand = sParams[1];

		else if( sValueName=="MISSIONREFERENCE" )
			out.m_MissionGoals.sLinkMissionReference = sParams[1];

		else if( sValueName=="PLAYERACTORCOMMANDS" )
			out.m_MissionGoals.sPlayerActorCommands = sParams[1];

		else if( sValueName=="MINEHOLDMODE" )
			out.m_MissionGoals.sMineHoldMode = sParams[1];

		else if( sValueName=="LAND" )
			out.m_MissionGoals.sLand = sParams[1];

		else if( sValueName=="AUTHOR" )
			out.m_MissionGoals.sAuthor = sParams[1];

		else if( sValueName=="BREAK" )
			out.m_MissionGoals.sBreak = sParams[1];

		else if( sValueName=="REQUIREDPLAYERS" )
			out.m_MissionGoals.iRequiredPlayers = atoi( sParams[1] );

		else if( sValueName=="MILEAGE" )
			out.m_MissionGoals.iGivenMileage = atoi( sParams[1] );
		
		else if( sValueName=="INITIALLIFE" )
			out.m_MissionGoals.fInitialLife = atof( sParams[1] );

		else if( sValueName=="NEEDGRAPHICS" )
			out.m_MissionGoals.sNeedGraphics = sParams[1];

		else if( sValueName=="MISSIONSTOINVALIDATE" )
			out.m_MissionGoals.sInvalidateMissionWhenIsActive = sParams[1];

		else if( sValueName=="MISSIONSTOVALIDATE" )
			out.m_MissionGoals.sValidateMissionWhenIsActive = sParams[1];

		else if( sValueName=="INFORMATION" )
			out.m_MissionGoals.sExtraInformation = sParams[1];

		else if( sValueName=="TYPE" )
			out.m_MissionGoals.sType = sParams[1];

		else if( sValueName=="SHOWARROWMODS" )
			out.m_MissionGoals.sShowArrowMods = sParams[1];

		else if( sValueName=="REQUIREDMISSION" )
			out.m_MissionGoals.sRequiredMission = sParams[1];

		else if( sValueName=="INITIALMISSION" )
			out.m_MissionGoals.sInitial = sParams[1];

		else if( sValueName=="PORTALLINK" )
			out.m_MissionGoals.sLinkToOtherWarp = sParams[1];

		else if( sValueName=="UNLOCKSONG" )
			out.m_pLockedSong = SONGMAN->FindSong( sParams[1] );

		else if( sValueName=="SONGPATH" )//la encontramos altiro por que los songs ya estan cargados!
		{
			out.m_pSong = SONGMAN->FindSong( sParams[1] );
			out.m_MissionGoals.sSongPathToLoad = sParams[1];//guardamos por si es random
			//si hay #songpath pero no hay m_pSong, es probable que sea random.
			if( !out.m_pSong )
				out.m_bRandomSong = true;
		}

		else if( sValueName=="HIDDENUNTIL" )//el nombre de una mission!
			out.m_MissionGoals.sHiddenUntil = sParams[1];

		else if( sValueName=="POSX" )
			out.m_MissionGoals.fposX = atof( sParams[1] );

		else if( sValueName=="POSY" )
			out.m_MissionGoals.fposY = atof( sParams[1] );

		else if( sValueName=="UPLEFTLINK" )
			out.m_MissionGoals.sLinkUpLeft = sParams[1];

		else if( sValueName=="UPRIGHTLINK" )
			out.m_MissionGoals.sLinkUpRight = sParams[1];

		else if( sValueName=="DOWNLEFTLINK" )
			out.m_MissionGoals.sLinkDownLeft = sParams[1];

		else if( sValueName=="DOWNRIGHTLINK" )
			out.m_MissionGoals.sLinkDownRight = sParams[1];

		else if( sValueName=="WARPSECTORLINK" )
			out.m_MissionGoals.sLinkWarpSector = sParams[1];

		else if( sValueName=="MISSIONWARPLINK" )
			out.m_MissionGoals.sLinkWarpMission = sParams[1];

		else if( sValueName=="DOWNLEFTLINKHIDDEN" )
			out.m_MissionGoals.sLinkDownLeftHidden = sParams[1];

		else if( sValueName=="DOWNRIGHTLINKHIDDEN" )
			out.m_MissionGoals.sLinkDownRightHidden = sParams[1];

		else if( sValueName=="UPLEFTLINKHIDDEN" )
			out.m_MissionGoals.sLinkUpLeftHidden = sParams[1];

		else if( sValueName=="UPRIGHTLINKHIDDEN" )
			out.m_MissionGoals.sLinkUpRightHidden = sParams[1];

		else if( sValueName=="BOTHDOWNLINKHIDDEN" )
			out.m_MissionGoals.sLinkBothDownHidden = sParams[1];

		else if( sValueName=="BOTHUPLINKHIDDEN" )
			out.m_MissionGoals.sLinkBothUpHidden = sParams[1];

		else if( sValueName=="SWITCHACTIVE" )
			out.m_MissionGoals.sSwitchActive = sParams[1];

		else if( sValueName=="LANDSTOHIDE" )
			out.m_MissionGoals.sLandsToHide = sParams[1];

		else if( sValueName=="LANDSTOSHOW" )
			out.m_MissionGoals.sLandsToShow = sParams[1];

		else if( sValueName=="LEVEL" )
			out.m_MissionGoals.iLevel = atoi( sParams[1] );

		else if( sValueName=="PLAYABLE" )
			out.m_MissionGoals.sPlayable = sParams[1];

		else if( sValueName=="MISSIONGOAL" )
			out.m_MissionGoals.sGoalText = sParams[1];

		else if( sValueName=="MISSIONGOAL2" )
			out.m_MissionGoals.sGoalText2 = sParams[1];

		else if( sValueName=="MISSIONGOAL3" )
			out.m_MissionGoals.sGoalText3 = sParams[1];

		else if( sValueName=="MISSIONGOALEXTRA" )
			out.m_MissionGoals.sGoalTextExtra = sParams[1];

		else if( sValueName=="LIFEDIFFICULTY" )
			out.m_MissionGoals.fLifeDifficulty = atof( sParams[1] );

		else if( sValueName=="MISSIONNAME" )
			out.m_MissionGoals.sMissionName = sParams[1];

		else if( sValueName=="MODS" )
			out.m_MissionGoals.sModifiers = sParams[1];

		else
		{
			//LOG->UserLog( "MSF file", sPath, "has an unexpected value named \"%s\".", sValueName.c_str() );
			;
		}
	}

	if( out.m_MissionGoals.sMissionName.empty() )
	{
		LOG->Trace( "MSF file %s Missing Parameters, #MISSIONNAME", sPath.c_str() );
		return false;
	}

	if( !out.m_MissionGoals.sCheckNoteSkin.empty() )
	{
		vector<RString> asNoteSkins;
		split( out.m_MissionGoals.sCheckNoteSkin, ",", asNoteSkins );

		if( asNoteSkins.size() > 2 ) //no podemos checkear stats para más de 2 noteskins.
		{
			LOG->Trace( "MSF file %s have many noteskins to check in #NOTESKINTOCHECK, max 2 (ignoring tag)", sPath.c_str() );
			out.m_MissionGoals.sCheckNoteSkin = "";
		}
	}
	//else if( out.m_MissionGoals.sType.empty() ) //si no tenemos type=mission comun.
	//{
	//	LOG->Trace( "MSF file Missing Parameters, #TYPE" );
	//	return false;
	//}

	return true;
}

//
// WorldTour Files
//

void MSFWorldTourLoader::GetApplicableFiles( const RString &sPath, vector<RString> &out )
{
	GetDirListing( sPath + RString("*.msf"), out );
}

bool MSFWorldTourLoader::LoadFromDir( const RString &sPath, Song &out )
{
	vector<RString> aFileNames;
	MSFWorldTourLoader::GetApplicableFiles( sPath, aFileNames );

	if( aFileNames.size() > 1 || aFileNames.size() <= 0 )
	{
		//LOG->UserLog( "Song", sPath, "has more than one or 0 MSFs file. There should be one!" );
		return false;
	}

	/* Aunque hayan más de 1 .msf igual usamos el primero y seguimos */

	return MSFWorldTourLoader::LoadFromMSFFile( sPath + aFileNames[0], out );
}

bool MSFWorldTourLoader::LoadFromMSFFile( const RString &sPath, Song &out )
{
	//LOG->Trace( "MSFWorldTourLoader::LoadFromMSFFile(%s)", sPath.c_str() );

	MsdFile msd;
	if( !msd.ReadFile( sPath, true ) )  // unescape
	{
		LOG->UserLog( "MSF file", sPath, "couldn't be opened: %s", msd.GetError().c_str() );
		return false;
	}

	/*
	#MISSIONNAME:...;
	#MISSIONLEVEL:MissionLevel#;
	#MISSIONSTAGE:MissionStage#;
	#MISSIONGOAL:...;
	#MISSIONGOAL2:...;
	#MODS:..., ...;
	*/

	//XXX:
	//Set the mision group here.
	vector<RString> sDirs;
	split( sPath, "/", sDirs );
	//out.m_MissionGoals.sMissionSector = sDirs[sDirs.size() - 2];

	for( unsigned i=0; i<msd.GetNumValues(); i++ )
	{
		//int iNumParams = msd.GetNumParams(i); sin uso!
		const MsdFile::value_t &sParams = msd.GetValue(i);
		RString sValueName = sParams[0];
		sValueName.MakeUpper();

		//if( sValueName=="BARRIERDATA" )
		//	out.m_MissionGoals.sBarrierString = sParams[1];
		if( sValueName=="MISSIONNAME" )
			out.m_MissionInfo.m_sName = sParams[1];
		else if( sValueName=="MISSIONGOAL" )
			out.m_MissionInfo.m_sGoal1 = sParams[1];
		else if( sValueName=="MISSIONGOAL2" )
			out.m_MissionInfo.m_sGoal2 = sParams[1];
		else if( sValueName=="MISSIONGOAL3" )
			out.m_MissionInfo.m_sGoal3 = sParams[1];
		else if( sValueName=="MODS" )
			out.m_MissionInfo.m_sMods = sParams[1];
		else if( sValueName=="HOLDSNOBODY" )
		{
			if( sParams[1].CompareNoCase("YES") == 0 )
				out.m_MissionInfo.m_bHoldsNoBody = true;
		}
		else if( sValueName=="MINEHOLDMODE" )
		{
			if( sParams[1].CompareNoCase("YES") == 0 )
				out.m_MissionInfo.m_bMineHoldMode = true;
		}
		else if( sValueName=="BREAK" )
		{
			if( sParams[1].CompareNoCase("YES") == 0 )
				out.m_MissionInfo.m_bBreak = true;
		}
		else if( sValueName=="INITIALLIFE" )
			out.m_MissionInfo.m_fInitialLife = atof( sParams[1] );
		else if( sValueName=="LIFEDIFFICULTY" )
			out.m_MissionInfo.m_fLifeDifficulty = atof( sParams[1] );
		else
			;//LOG->Trace( "Unrecognized tag %s in %s", sValueName.c_str(), sPath.c_str() );

	}

	return true;
}

/*
 * (c) 2001-2004 Chris Danford, Glenn Maynard, Victor Manuel Vicente Gajardo Henríquez "v1toko"
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */