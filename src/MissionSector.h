/* MissionSector - Sector del mapa, tiene un conjunto de bullets. Él se moverá */

#ifndef MISSION_SECTOR_H
#define MISSION_SECTOR_H

#include "Sprite.h"
#include "MissionBullet.h"
#include "Mission.h"
#include "Profile.h"

class MissionSectorMap : public ActorFrame
{
public:
	MissionSectorMap();
	virtual ~MissionSectorMap();
	virtual void Init( RString sSectorName, Profile *pProfile );
	virtual void LoadFromNode( const XNode *pNode );
	virtual void Update( float fDeltaTime );
	virtual MissionSectorMap *Copy() const;
	void GetPosition( Mission* pMission, float &fX_out, float &fY_out );
	bool IsMoving() { return GetTweenTimeLeft() > 0; };
	void SetSelected( bool bNewValue ) { m_bSelected = bNewValue; };
	RString GetSectorName() { return m_sSectorName; };

	void ChangeVisibleStateOfActorsByLand( RString sLand, bool bVisible );
protected:
	void DrawHighLight();
	void DrawDirectionalArrows();

	RString m_sSectorName;
	Sprite m_HighLight;
	Sprite m_HighLightBoss;
	Sprite m_HighLightWarp;
	Sprite m_HiddenLineHL;
	Sprite m_ArrowBlue[2];//0 right, 1 left
	Sprite m_ArrowRed[2];//idem
	Sprite m_Line;

	vector<MissionBullet*> m_Bullets;
	vector<Sprite*> m_Lines;
	bool m_bSelected;
};

#endif

/*
 * (c) 2008 Victor Manuel Vicente Gajardo Henríquez "v1toko"
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
