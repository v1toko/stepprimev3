/* ScreenWorldMax - Screen que muestra el WorldMax */

#ifndef SCREEN_WORLD_MAX_H
#define SCREEN_WORLD_MAX_H

#include "ScreenWithMenuElements.h"
#include "MissionSector.h"//un sector del mapa, con sus propios wraps
#include "Steps.h"
#include "MissionSelectSector.h"//el mapa completo con wrap para los sectores
#include "RageSound.h"
#include "Profile.h"
#include "PlayerNumber.h"
#include "InputEventPlus.h"

class ScreenWorldMax : public ScreenWithMenuElements
{
public:
	//ScreenWorldMax();
	virtual ~ScreenWorldMax();

	virtual void Init();

	virtual bool Input( const InputEventPlus &input );
	virtual bool MenuBack( const InputEventPlus &input );
	virtual bool MenuStart( const InputEventPlus &input );

	virtual void HandleScreenMessage( const ScreenMessage SM );
	virtual void BeginScreen();
	virtual void HandleMessage( const Message &msg );
	virtual void Update( float fDeltaTime );
	const int GetMapIndex() const { return m_iSectorIndex; };
	const int GetTotalSectors() const { return m_mapSectors.size(); };

	//SelectState GetSelectState() { return m_SelectState; };

	//
	// Lua
	//
	virtual void PushSelf( lua_State *L );

private:
	void AfterMissionChange();//set up styles and steps
	void SetSectorAndMap();//al volver a elegir misiones en el mapa...
	void CheckRandomSong();//checkea los steps para las misiones con canciones random
	void PathFailed();//informa de eso.

protected:

	vector<MissionSectorMap*> m_mapSectors;//m_mapSectors[m_SectorIndex];
	MissionSelectSector m_mapSectorSelect;
	vector<Steps*> m_vpSteps;

	Profile* m_pProfile;
	PlayerNumber m_pn;

	//SelectState m_SelectState;

	int m_iSectorIndex;//tiene el indice del sector en el que estamos
	bool m_bMissionPlayable;//es por si las stages que tenemos no nos alcanzan!
	bool m_bSelected;
	bool m_bTransitioningInWarp;//es por si estamos pasando de un warp a otro, bloqueamos el input
	bool m_bOutOfTime;
	bool m_bContinueMoving;
	bool m_bUsingShield;

	RString m_sCurrentItem;

	Sprite m_sprTopPanel;
	Sprite m_sprCreditsPanel;
	Sprite m_sprWarps;//animación de los warps
	//vector<Sprite*> m_vMiniMaps;//mini mapa
	//vector<RageSound*> m_vMapSounds;//la carga tiene relacion con #LAND no con ->missionsector
	RString m_sLastSeenLand;
	map<RString,RageSound> m_mapSounds;

	RageSound m_soundMove;//cuando se mueve
	RageSound m_soundNotMove;//cuendo no se puede mover
	RageSound m_soundCenter;//presiona centro... la segunda vez, actua start
	RageSound m_soundStart;//start
	RageSound m_soundNotStart;//cuando no puede start
	RageSound m_soundWarp;//entra a un warp
	RageSound m_soundSwitch;//cambia el estado de un switch
};

#endif

/*
 * (c) 2008 Victor Gajardo Henriquez "v1toko"
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
