#include "global.h"
#include "CommandLineActions.h"
#include "RageFile.h"
#include "RageUtil.h"
#include "IniFile.h"
#include "XmlFile.h"
#include "XmlFileUtil.h"
#include "LuaManager.h"
#include "ProductInfo.h"
#include "DateTime.h"
#include "Foreach.h"
#include "arch/Dialog/Dialog.h"
#include "RageFileManager.h"
#include "SpecialFiles.h"
#include "FileDownload.h"
#include "arch/LoadingWindow/LoadingWindow.h"
#include "Preference.h"
#include "JsonUtil.h"
#include "ScreenInstallOverlay.h"
#include "ver.h"
#include "CryptManager.h"
#include "RageLog.h"

//agregados para el theme encriptado
#include "RageFileDriverMemory.h"
#include "RageFile.h"
#include "libtomcrypt/src/headers/tomcrypt.h"

// only used for Version()
#if defined(_WINDOWS)
#include <windows.h>
#include <conio.h>
#endif

/** @brief The directory where languages should be installed. */
const RString INSTALLER_LANGUAGES_DIR = "Themes/_Installer/Languages/";

vector<CommandLineActions::CommandLineArgs> CommandLineActions::ToProcess;

static void Nsis()
{
	RageFile out;
	if(!out.Open("nsis_strings_temp.inc", RageFile::WRITE))
		RageException::Throw("Error opening file for write.");

	vector<RString> vs;
	GetDirListing(INSTALLER_LANGUAGES_DIR + "*.ini", vs, false, false);
	FOREACH_CONST(RString, vs, s)
	{
		RString sThrowAway, sLangCode;
		splitpath(*s, sThrowAway, sLangCode, sThrowAway);
		const LanguageInfo *pLI = GetLanguageInfo(sLangCode);

		RString sLangNameUpper = pLI->szEnglishName;
		sLangNameUpper.MakeUpper();

		IniFile ini;
		if(!ini.ReadFile(INSTALLER_LANGUAGES_DIR + *s))
			RageException::Throw("Error opening file for read.");
		FOREACH_CONST_Child(&ini, child)
		{
			FOREACH_CONST_Attr(child, attr)
			{
				RString sName = attr->first;
				RString sValue = attr->second->GetValue<RString>();
				sValue.Replace("\\n", "$\\n");
				RString sLine = ssprintf("LangString %s ${LANG_%s} \"%s\"", sName.c_str(), sLangNameUpper.c_str(), sValue.c_str());
				out.PutLine(sLine);
			}
		}
	}
}
static void LuaInformation()
{
	XNode *pNode = LuaHelpers::GetLuaInformation();
	pNode->AppendAttr("xmlns", "http://www.stepmania.com");
	pNode->AppendAttr("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
	pNode->AppendAttr("xsi:schemaLocation", "http://www.stepmania.com Lua.xsd");

	pNode->AppendChild("Version", string(PRODUCT_FAMILY) + product_version);
	pNode->AppendChild("Date", DateTime::GetNowDate().GetString());

	XmlFileUtil::SaveToFile(pNode, "Lua.xml", "Lua.xsl");

	delete pNode;
}

/**
 * @brief Print out version information.
 *
 * HACK: This function is primarily needed for Windows users.
 * Mac OS X and Linux print out version information on the command line
 * regardless of any preferences (tested by shakesoda on Mac). -aj */
static void Version()
{
	#if defined(WIN32)
		RString sProductID = ssprintf("%s", (string(PRODUCT_FAMILY) + product_version).c_str() );
		RString sVersion = "(StepMania was built without HAVE_VERSION_INFO)";
		#if defined(HAVE_VERSION_INFO)
			sVersion = ssprintf("build %lu\nCompile Date: %s @ %s", version_num, version_date, version_time);
		#endif // HAVE_VERSION_INFO

		AllocConsole();
		freopen("CONOUT$","wb", stdout);
		freopen("CONOUT$","wb", stderr);

		fprintf(stdout, "Version Information:\n%s %s\n", sProductID.c_str(), sVersion.c_str());
		fprintf(stdout, "Press any key to exit.");
		_getch();
	#endif // WIN32
}

/*
*****************************************************************
*****************************************************************
Para produccion no deben ir estas funciones ni argumentos
del programa
*****************************************************************
*****************************************************************
*/
static void ProcessEncrypt()
{
	#ifdef RIJNDAEL
	  register_cipher (&aes_desc);
	#endif

	#if defined(SHA1)
	   register_hash(&sha1_desc);
	#else
	   #error YARROW needs at least one HASH
	#endif

    if (register_prng(&yarrow_desc) == -1) {
    	//LOG->Trace("Error registering yarrow PRNG\n");
   	}

	//LOG->Trace( "3) resultado montado: %d, %s", pMem->GetFileSize(), error.c_str() );

	unsigned char plaintext[512],ciphertext[512];
	unsigned char tmpkey[512], key[MAXBLOCKSIZE], IV[MAXBLOCKSIZE];
	unsigned char inbuf[512]; /* i/o block size */
	unsigned long outlen, y, ivsize, x, decrypt;
	symmetric_CTR ctr;
	int cipher_idx, hash_idx, ks;
	FILE *fdin, *fdout;

	RString inPath, outPath;
	prng_state prng;

	cipher_idx = find_cipher("aes");

	if (cipher_idx == -1) {
		//LOG->Trace("Invalid cipher entered on command line.\n");
		return;
	}

	hash_idx = find_hash("sha1");
	if (hash_idx == -1) {
		//LOG->Trace("SHA1 not found...?\n");
		return;
	}

	//bool bDecrypt = false;

	RString fin;
	RString fout;

	fin = SpecialFiles::PACKAGES_DIR + "data.zip";
	fout = SpecialFiles::PACKAGES_DIR + "data.prime";

	fdin = fopen(fin.c_str(),"rb");
	if (fdin == NULL) {
		//LOG->Trace("Can't open input for reading");
		return;
	}

	fdout = fopen(fout.c_str(),"wb");
	if (fdout == NULL) { 
		//LOG->Trace("Can't open output for writing");
		return;
	}

	ivsize = cipher_descriptor[cipher_idx].block_length;
	ks = hash_descriptor[hash_idx].hashsize;
	if (cipher_descriptor[cipher_idx].keysize(&ks) != CRYPT_OK) { 
		//LOG->Trace("Invalid keysize???\n");
		return;
	}

	char* str = "";
	{
		RString sMd5;
#if defined(WINDOWS)
		sMd5 = BinaryToHex( CRYPTMAN->GetMD5ForFile("Program/Stepmania.exe") );	
#elif defined(UNIX)
		sMd5 = BinaryToHex( CRYPTMAN->GetMD5ForFile("stepmania") );
#endif
		LOG->Trace(sMd5);
		str = (char *)sMd5.c_str();
	}		
	strcpy(  (char *)tmpkey, str );	
	outlen = sizeof(key);
	
	int errno;
	if ((errno = hash_memory(hash_idx,tmpkey,strlen((char *)tmpkey),key,&outlen)) != CRYPT_OK) {
		//LOG->Trace("Error hashing key: %s\n", error_to_string(errno));
		return;
	}	

	/* Setup yarrow for random bytes for IV */      
	if ((errno = rng_make_prng(128, find_prng("yarrow"), &prng, NULL)) != CRYPT_OK) {
		//LOG->Trace("Error setting up PRNG, %s\n", error_to_string(errno));
	}      

	/* You can use rng_get_bytes on platforms that support it */
	/* x = rng_get_bytes(IV,ivsize,NULL);*/
	x = yarrow_read(IV,ivsize,&prng);
	if (x != ivsize) {
		//LOG->Trace("Error reading PRNG for IV required.\n");
		return;
	}

	if (fwrite(IV,1,ivsize,fdout) != ivsize) {
		//LOG->Trace("Error writing IV to output.\n");
		return;
	}

	if ((errno = ctr_start(cipher_idx,IV,key,ks,0,CTR_COUNTER_LITTLE_ENDIAN,&ctr)) != CRYPT_OK) {
		//LOG->Trace("ctr_start error: %s\n",error_to_string(errno));
		return;
	}

	do {
		y = fread(inbuf,1,sizeof(inbuf),fdin);

		if ((errno = ctr_encrypt(inbuf,ciphertext,y,&ctr)) != CRYPT_OK) {
			//LOG->Trace("ctr_encrypt error: %s\n", error_to_string(errno));
			return;
		}

		if (fwrite(ciphertext,1,y,fdout) != y) {
			//LOG->Trace("Error writing to output.\n");
			return;
		}
	} while (y == sizeof(inbuf));   
	fclose(fdout);
		fclose(fdin);
}

static void WriteBytes( RageFile &f, RString &sError, const void *buf, int size )
{
	if( sError.size() != 0 )
		return;

	int ret = f.Write( buf, size );
	if( ret == -1 )
		sError = f.GetError();
}

static void ProcessDecrypt()
{
	#ifdef RIJNDAEL
	  register_cipher (&aes_desc);
	#endif

	#if defined(SHA1)
	   register_hash(&sha1_desc);
	#else
	   #error YARROW needs at least one HASH
	#endif

    if (register_prng(&yarrow_desc) == -1) {
    	//LOG->Trace("Error registering yarrow PRNG\n");
   	}

	//LOG->Trace( "3) resultado montado: %d, %s", pMem->GetFileSize(), error.c_str() );

	unsigned char plaintext[512]/*,ciphertext[512]*/;
	unsigned char tmpkey[512], key[MAXBLOCKSIZE], IV[MAXBLOCKSIZE];
	unsigned char inbuf[512]; /* i/o block size */
	unsigned long outlen, y, ivsize, x/*, decrypt*/;
	symmetric_CTR ctr;
	int cipher_idx, hash_idx, ks;
	FILE *fdin/*, *fdout*/;

	RString inPath, outPath;
	//prng_state prng;

	cipher_idx = find_cipher("aes");

	if (cipher_idx == -1) {
		//LOG->Trace("Invalid cipher entered on command line.\n");
		return;
	}

	hash_idx = find_hash("sha1");
	if (hash_idx == -1) {
		//LOG->Trace("SHA1 not found...?\n");
		return;
	}

	bool bDecrypt = true;

	RString fin;
	RString fout;
	if( bDecrypt )
	{
		fout = SpecialFiles::PACKAGES_DIR + "prime.zip";
		fin = SpecialFiles::PACKAGES_DIR + "prime.data";
	}

	fdin = fopen(fin.c_str(),"rb");
	if (fdin == NULL) {
		//LOG->Trace("Can't open input for reading");
		return;
	}

	/*fdout = fopen(fout.c_str(),"wb");
	if (fdout == NULL) { 
		LOG->Trace("Can't open output for writing");
		return;
	}*/

	RageFile fdout;
	if( !fdout.Open( fout, RageFile::WRITE ) )
	{
		//LOG->Trace("Can't open output for writing");
		return;
	}

	ivsize = cipher_descriptor[cipher_idx].block_length;
	ks = hash_descriptor[hash_idx].hashsize;
	if (cipher_descriptor[cipher_idx].keysize(&ks) != CRYPT_OK) { 
		//LOG->Trace("Invalid keysize???\n");
		return;
	}

	char* str;
	{
		RString sMd5;
#if defined(WINDOWS)
		sMd5 = BinaryToHex( CRYPTMAN->GetMD5ForFile("Program/Stepmania.exe") );	
#elif defined(UNIX)
		sMd5 = BinaryToHex( CRYPTMAN->GetMD5ForFile("stepmania") );
#endif
		//LOG->Trace(sMd5);
		str = (char *)sMd5.c_str();
	}		
	strcpy(  (char *)tmpkey, str );	
	outlen = sizeof(key);
	
	int errno;
	if ((errno = hash_memory(hash_idx,tmpkey,strlen((char *)tmpkey),key,&outlen)) != CRYPT_OK) {
		//LOG->Trace("Error hashing key: %s\n", error_to_string(errno));
		return;
	}	

	/* Need to read in IV */
	if (fread(IV,1,ivsize,fdin) != ivsize) {
		//LOG->Trace("Error reading IV from input.\n");
		return;
	}

	if ((errno = ctr_start(cipher_idx,IV,key,ks,0,CTR_COUNTER_LITTLE_ENDIAN,&ctr)) != CRYPT_OK) {
		//LOG->Trace("ctr_start error: %s\n",error_to_string(errno));
		return;
	}

	/* IV done */
	RString sError;
	do {
		y = fread(inbuf,1,sizeof(inbuf),fdin);

		if ((errno = ctr_decrypt(inbuf,plaintext,y,&ctr)) != CRYPT_OK) {
			//LOG->Trace("ctr_decrypt error: %s\n", error_to_string(errno));
			return;
		}

		WriteBytes( fdout, sError, plaintext, y );
		//LOG->Trace("error: %s", sError.c_str());
	} while (y == sizeof(inbuf));
	fclose(fdin);
	if( fdout.Flush() )
	{
		//LOG->Trace("error al flushear");
	}
	fdout.Close();
}

void CommandLineActions::Handle(LoadingWindow* pLW)
{
	CommandLineArgs args;
	for(int i=0; i<g_argc; ++i)
		args.argv.push_back(g_argv[i]);
	ToProcess.push_back(args);

	bool bExitAfter = false;
	if( GetCommandlineArgument("ExportNsisStrings") )
	{
		Nsis();
		bExitAfter = true;
	}
	if( GetCommandlineArgument("ExportLuaInformation") )
	{
		LuaInformation();
		bExitAfter = true;
	}
	if( GetCommandlineArgument("version") )
	{
		Version();
		bExitAfter = true;
	}
	if( GetCommandlineArgument("Encrypt") )
	{
		ProcessEncrypt();
		bExitAfter = true;
	}
	if( GetCommandlineArgument("Decrypt") )
	{
		ProcessDecrypt();
		bExitAfter = true;
	}
	LOG->Flush();
	if( bExitAfter )
		exit(0);
}

/*
 * (c) 2006 Chris Danford, Steve Checkoway
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

