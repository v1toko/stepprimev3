/* MissionBullet, Contiene información y actor grafico en una misión (WorldMax). */

#ifndef MISSIONBULLET_H
#define MISSIONBULLET_H

#include "ActorFrame.h"
#include "Sprite.h" 
//#include "BitmapText.h"
#include "Mission.h"
//#include "Song.h"

class MissionBullet : public ActorFrame
{
public:
	MissionBullet();
	virtual ~MissionBullet();
	virtual void Load( Mission* pMission );
	MissionBullet( const MissionBullet &cpy );
	virtual MissionBullet *Copy() const;
	void UpdateSwitchState( Mission* pMission );
	virtual void HandleMessage( const Message &msg );
	// Lua
	void PushSelf( lua_State *L );
private:
	float m_posX, m_posY;
	Mission* m_pMission;
	//optimize graphics!, use one or two spr's to all types!
	//Sprite m_sprBullet;
	//Sprite m_sprBulletDisabled;
	//Sprite m_sprWarp;
	//Sprite m_sprWarpSector;
	//Sprite m_sprBoss;
	//Sprite m_sprSpecial;//need graphics tag!
	//Sprite m_sprSwitchOff;
	//Sprite m_sprSwitchOn;

	Sprite m_Active;
	Sprite m_Inactive;
	Sprite m_Barrier;
};

#endif

/*
 * (c) 2008 Victor Manuel Vicente Gajardo Henríquez "v1toko"
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
