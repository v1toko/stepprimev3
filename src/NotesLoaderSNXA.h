/** @brief SNXALoader - Reads a Song and its Steps from a .SNXA file. */
#ifndef NotesLoaderSNXA_H
#define NotesLoaderSNXA_H

#include "GameConstantsAndTypes.h"
#include "NotesLoaderSM.h"

class MsdFile;
class Song;
class Steps;
class TimingData;

/**
 * @brief The various states while parsing a .snxa file.
 */
enum SNXALoadingStates
{
	GETTING_SONG_INFO_SNXA, /**< Retrieving song information. */
	GETTING_STEP_INFO_SNXA, /**< Retrieving step information. */
	NUM_SNXALoadingStates /**< The number of states used. */
};

/**
 * @brief The SNXALoader handles all of the parsing needed for .snxa files.
 */
struct SNXALoader : public SMLoader
{
	SNXALoader() : SMLoader(".snxa") {}
	
	/**
	 * @brief Attempt to load the specified snxa file.
	 * @param sPath a const reference to the path on the hard drive to check.
	 * @param out a reference to the Song that will retrieve the song information.
	 * @param bFromCache a check to see if we are getting certain information from the cache file.
	 * @return its success or failure.
	 */
	virtual bool LoadFromSimfile( const RString &sPath, Song &out, bool bFromCache = false );
	
	void ProcessBPMs( TimingData &, const RString );
	void ProcessStops( TimingData &, const RString );
	void ProcessWarps( TimingData &, const RString );
	void ProcessLabels( TimingData &, const RString );
	virtual void ProcessCombos( TimingData &, const RString, const int = -1 );
	void ProcessScrolls( TimingData &, const RString );
};

#endif
/**
 * @file
 * @author v1toko (taking reference from NotesLoaderSSC.h) (c) 2015
 *
 * @section LICENSE
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
