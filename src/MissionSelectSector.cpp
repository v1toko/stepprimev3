#include "global.h"
#include "MissionSelectSector.h"
#include "ThemeManager.h"
#include "ScreenDimensions.h"
#include "SongManager.h"
#include "Mission.h"
#include "GameState.h"
#include "RageMath.h"//pi

#define SHOW_MISSION_LINES	THEME->GetMetricB( "ScreenWorldMax", "ShowLinesBetweenMissions" )

MissionSelectSector::MissionSelectSector()
{
}

MissionSelectSector::~MissionSelectSector()
{
	for( int i = 0; i < m_Bullets.size(); i++ )
		SAFE_DELETE( m_Bullets[i] );

	m_Bullets.clear();

	for( int i = 0; i < m_Lines.size(); i++ )
		SAFE_DELETE( m_Lines[i] );

	m_Lines.clear();
}

void MissionSelectSector::Init()
{
	//m_MapBG.SetName( "SelectSectorMap" );
	//m_MapBG.Load( THEME->GetPathG( "Mission", "generalmap" ) );
	//m_MapBG.ScaleToClipped( SCREEN_WIDTH, SCREEN_HEIGHT );
	//m_MapBG.SetXY( SCREEN_CENTER_X, SCREEN_CENTER_Y );
	//this->AddChild( &m_MapBG );

	CHECKPOINT;

	vector<Mission*> added;
	{
		vector<Mission*> Missions;
		SONGMAN->GetAllMissions( Missions );

		//inicia las canciones por sector
		for( unsigned i = 0; i < Missions.size(); i++ )
		{
			//Missions[i]->SetUpMissionState( pProfile );
			if( Missions[i]->IsWarpSector() )//ahora dibujamos los wrap sectors
				added.push_back( Missions[i] );
		}
	}

	CHECKPOINT;

	for( unsigned i = 0; i < added.size(); i++ )
	{
		Mission* mision = added[i];

		//si no es jugable y no es wrap sector
		if( !mision->IsPlayable() || !mision->IsWarpSector() )
			continue;

		if( !SHOW_MISSION_LINES )
			continue;

		//hack: as� sabemos que lineas ya trazamos
		bool bUsedul = false, bUsedur = false, bUseddl = false, bUseddr = false;

		for( unsigned i = 0; i < 4; i++ )//por cada mission, podemos 4 lineas
		{
			Sprite* sp = new Sprite;
			sp->SetName( ssprintf( "Line%d", i ) );
			//agregar el child mas abajo, para que no aparezcan lineas "sordas"

			if( mision->CanGoDownLeft() && !bUseddl )
			{
				sp->Load( THEME->GetPathG( "Mission", "Line" ) );
				bUseddl = true;
				float x1, x2, y1, y2;
				x1 = mision->m_MissionGoals.fposX;
				y1 = mision->m_MissionGoals.fposY;
				x2 = mision->GetLinkDownLeft()->m_MissionGoals.fposX;
				y2 = mision->GetLinkDownLeft()->m_MissionGoals.fposY;
				float fatan = atanf( ((y2-y1)/(x2-x1)) );
				float fzoomtw = sqrtf( powf( x1-x2, 2 ) + powf( y1-y2, 2 ) );
				fatan *= 180.f/PI;
				//LOG->Trace( "fatan: %.2f", fatan );
				sp->SetRotationZ( fatan );
				sp->ZoomToWidth( fzoomtw );
				sp->SetXY( x1, y1 );

				if( x1 <= x2 )//cambia la orientacion
					sp->SetHorizAlign( HorizAlign_Left );
				else
					sp->SetHorizAlign( HorizAlign_Right );

				m_Lines.push_back( sp );
				this->AddChild( sp );
				continue;
			}
			else if( mision->CanGoDownRight() && !bUseddr )
			{
				sp->Load( THEME->GetPathG( "Mission", "Line" ) );
				bUseddr = true;
				float x1, x2, y1, y2;
				x1 = mision->m_MissionGoals.fposX;
				y1 = mision->m_MissionGoals.fposY;
				x2 = mision->GetLinkDownRight()->m_MissionGoals.fposX;
				y2 = mision->GetLinkDownRight()->m_MissionGoals.fposY;
				float fatan = atanf( ((y2-y1)/(x2-x1)) );
				float fzoomtw = sqrtf( powf( x1-x2, 2 ) + powf( y1-y2, 2 ) );
				fatan *= 180.f/PI;
				//LOG->Trace( "fatan: %.2f", fatan );
				sp->SetRotationZ( fatan );
				sp->ZoomToWidth( fzoomtw );
				sp->SetXY( x1, y1 );

				if( x1 <= x2 )//cambia la orientacion
					sp->SetHorizAlign( HorizAlign_Left );
				else
					sp->SetHorizAlign( HorizAlign_Right );

				m_Lines.push_back( sp );
				this->AddChild( sp );
				continue;
			}
			else if( mision->CanGoUpLeft() && !bUsedul )
			{
				sp->Load( THEME->GetPathG( "Mission", "Line" ) );
				bUsedul = true;
				float x1, x2, y1, y2;
				x1 = mision->m_MissionGoals.fposX;
				y1 = mision->m_MissionGoals.fposY;
				x2 = mision->GetLinkUpLeft()->m_MissionGoals.fposX;
				y2 = mision->GetLinkUpLeft()->m_MissionGoals.fposY;
				float fatan = atanf( ((y2-y1)/(x2-x1)) );
				float fzoomtw = sqrtf( powf( x1-x2, 2 ) + powf( y1-y2, 2 ) );
				fatan *= 180.f/PI;
				//LOG->Trace( "fatan: %.2f", fatan );
				sp->SetRotationZ( fatan );
				sp->ZoomToWidth( fzoomtw );
				sp->SetXY( x1, y1 );

				if( x1 <= x2 )//cambia la orientacion
					sp->SetHorizAlign( HorizAlign_Left );
				else
					sp->SetHorizAlign( HorizAlign_Right );

				m_Lines.push_back( sp );
				this->AddChild( sp );
				continue;
			}
			else if( mision->CanGoUpRight() && !bUsedur )
			{
				sp->Load( THEME->GetPathG( "Mission", "Line" ) );
				bUsedur = true;
				float x1, x2, y1, y2;
				x1 = mision->m_MissionGoals.fposX;
				y1 = mision->m_MissionGoals.fposY;
				x2 = mision->GetLinkUpRight()->m_MissionGoals.fposX;
				y2 = mision->GetLinkUpRight()->m_MissionGoals.fposY;
				float fatan = atanf( ((y2-y1)/(x2-x1)) );
				float fzoomtw = sqrtf( powf( x1-x2, 2 ) + powf( y1-y2, 2 ) );
				fatan *= 180.f/PI;
				//LOG->Trace( "fatan: %.2f", fatan );
				sp->SetRotationZ( fatan );
				sp->ZoomToWidth( fzoomtw );
				sp->SetXY( x1, y1 );

				if( x1 <= x2 )//cambia la orientacion
					sp->SetHorizAlign( HorizAlign_Left );
				else
					sp->SetHorizAlign( HorizAlign_Right );

				m_Lines.push_back( sp );
				this->AddChild( sp );
				continue;
			}
			delete sp;
		}//fin 2do for
	}

	CHECKPOINT;

	for( unsigned i = 0; i < added.size(); i++ )
	{
		Mission* mision = added[i];

		//si no es jugable y no es wrap sector
		if( !mision->IsPlayable() || !mision->IsWarpSector() )
			continue;	

		MissionBullet* mb = new MissionBullet;
		mb->Load( mision );
		m_Bullets.push_back( mb );
		this->AddChild( mb );
	}

	CHECKPOINT;

	m_HighLight.SetName( "SelectSectorHighLight" );
	m_HighLight.Load( THEME->GetPathG( "Mission", "highlight" ) );
	m_HighLight.SetEffectSpin(RageVector3(0,0,180));
	this->AddChild( &m_HighLight );

	CHECKPOINT;
}

void MissionSelectSector::Update( float fDeltaTime )
{
	ActorFrame::Update( fDeltaTime );

	m_HighLight.SetXY( GAMESTATE->m_pCurMission->m_MissionGoals.fposX, GAMESTATE->m_pCurMission->m_MissionGoals.fposY );
}

void MissionSelectSector::HandleMessage( const Message &msg )
{
	if( msg.GetName().CompareNoCase("SwitchStateChanged")==0 )
		;//do something;
	else if( msg.GetName() == "SelectStateChanged" )
	{
		RString sLand;
		msg.GetParam("From", sLand);
	}

	ActorFrame::HandleMessage( msg );
}

/*
 * (c) 2008 Victor Gajardo Henr�quez (v1toko)
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
