#include "global.h"
#include "RageUtil.h"
#include "GameState.h"
#include "RageLog.h"
#include "SongManager.h"
#include "ThemeManager.h"
#include "Song.h"
#include "ActorUtil.h"
#include "ScreenDimensions.h"
#include "MissionSector.h"
#include "RageMath.h"

REGISTER_ACTOR_CLASS( MissionSectorMap );

#define MAP_ZOOM		THEME->GetMetricF( "ScreenWorldMax", "MapZoom" )
#define SHOW_MISSION_LINES	THEME->GetMetricB( "ScreenWorldMax", "ShowLinesBetweenMissions" )

MissionSectorMap::MissionSectorMap() 
{ 
	m_bSelected = false;
	m_sSectorName = RString();

	m_Bullets.clear();
	m_Lines.clear();
}

MissionSectorMap::~MissionSectorMap()
{
	for( int i = 0; i < m_Bullets.size(); i++ )
		SAFE_DELETE( m_Bullets[i] );

	m_Bullets.clear();

	for( int i = 0; i < m_Lines.size(); i++ )
		SAFE_DELETE( m_Lines[i] );

	m_Lines.clear();
}

void MissionSectorMap::Init( RString sSectorName, Profile *pProfile )
{
	//inicia las canciones por sector
	vector<Mission*> added;

	{
		vector<Mission*> Missions;//tiene la info de SONGMAN
		SONGMAN->GetAllMissions( Missions );

		for( unsigned i = 0; i < Missions.size(); i++ )
		{
			if( Missions[i]->GetMissionSector() == sSectorName )
			{
				if( !Missions[i]->IsWarpSector() )//no dibuja wrap sectors
					added.push_back( Missions[i] );
			}
		}
	}

	m_sSectorName = sSectorName;//set sectornames

	m_Line.SetName( "Lines" );
	m_Line.Load( THEME->GetPathG( "WorldMax/Mission", "Line" ) );

	//Lineas detras de las bullets y de las flechas
	for( unsigned i=0; i< added.size(); i++ )
	{
		Mission* mision = added[i];

		if( !mision->IsPlayable() || mision->IsHiddenLine() )
			continue;

		if( !SHOW_MISSION_LINES )
			continue;

		//hack: así sabemos que lineas ya trazamos
		bool bUsedul = false, bUsedur = false, bUseddl = false, bUseddr = false;

		for( unsigned i = 0; i < 4; i++ )//por cada mission, podemos 4 lineas
		{
			Sprite* sp = new Sprite( m_Line );						
			CHECKPOINT;
			//agregar el child mas abajo, para que no aparezcan lineas "sordas"

			if( mision->CanGoDownLeft() && !bUseddl )
			{
				bUseddl = true;

				ASSERT( mision && mision->GetLinkDownLeft() );
				RString sTo = mision->GetLinkDownLeft()->GetMissionLand();
				RString sFrom = mision->GetMissionLand();
				sTo.MakeLower(); 
				sFrom.MakeLower();
				sp->SetName( ssprintf( "Line-%s-%s", sTo.c_str(), sFrom.c_str() ) );

				//ocultamos si son hiddenlines
				if( mision->GetLinkDownLeft()->IsHiddenLine() )
					continue;

				//sp = &m_Line;

				float x1, x2, y1, y2;
				x1 = mision->m_MissionGoals.fposX;
				y1 = mision->m_MissionGoals.fposY;
				x2 = mision->GetLinkDownLeft()->m_MissionGoals.fposX;
				y2 = mision->GetLinkDownLeft()->m_MissionGoals.fposY;
				float fatan = atanf( ((y2-y1)/(x2-x1)) );
				float fzoomtw = sqrtf( powf( x1-x2, 2 ) + powf( y1-y2, 2 ) );
				fatan *= 180.f/PI;
				//LOG->Trace( "fatan: %.2f", fatan );
				sp->SetRotationZ( fatan );
				sp->ZoomToWidth( fzoomtw );
				sp->SetXY( x1, y1 );

				if( x1 <= x2 )//cambia la orientacion
					sp->SetHorizAlign( HorizAlign_Left );
				else
					sp->SetHorizAlign( HorizAlign_Right );

				m_Lines.push_back( sp );
				this->AddChild( sp );
				continue;
			}
			else if( mision->CanGoDownRight() && !bUseddr )
			{
				bUseddr = true;

				ASSERT( mision && mision->GetLinkDownRight() );
				RString sTo = mision->GetLinkDownRight()->GetMissionLand();
				RString sFrom = mision->GetMissionLand();
				sTo.MakeLower(); 
				sFrom.MakeLower();
				sp->SetName( ssprintf( "Line-%s-%s", sTo.c_str(), sFrom.c_str() ) );

				//ocultamos si son hiddenlines
				if( mision->GetLinkDownRight()->IsHiddenLine() )
					continue;

				//sp = &m_Line;

				float x1, x2, y1, y2;
				x1 = mision->m_MissionGoals.fposX;
				y1 = mision->m_MissionGoals.fposY;
				x2 = mision->GetLinkDownRight()->m_MissionGoals.fposX;
				y2 = mision->GetLinkDownRight()->m_MissionGoals.fposY;
				float fatan = atanf( ((y2-y1)/(x2-x1)) );
				float fzoomtw = sqrtf( powf( x1-x2, 2 ) + powf( y1-y2, 2 ) );
				fatan *= 180.f/PI;
				//LOG->Trace( "fatan: %.2f", fatan );
				sp->SetRotationZ( fatan );
				sp->ZoomToWidth( fzoomtw );
				sp->SetXY( x1, y1 );

				if( x1 <= x2 )//cambia la orientacion
					sp->SetHorizAlign( HorizAlign_Left );
				else
					sp->SetHorizAlign( HorizAlign_Right );
				
				m_Lines.push_back( sp );
				this->AddChild( sp );
				continue;
			}
			else if( mision->CanGoUpLeft() && !bUsedul )
			{
				bUsedul = true;

				ASSERT( mision && mision->GetLinkUpLeft() );
				RString sTo = mision->GetLinkUpLeft()->GetMissionLand();
				RString sFrom = mision->GetMissionLand();
				sTo.MakeLower(); 
				sFrom.MakeLower();
				sp->SetName( ssprintf( "Line-%s-%s", sTo.c_str(), sFrom.c_str() ) );

				//ocultamos si son hiddenlines
				if( mision->GetLinkUpLeft()->IsHiddenLine() )
					continue;

				//sp = &m_Line;

				float x1, x2, y1, y2;
				x1 = mision->m_MissionGoals.fposX;
				y1 = mision->m_MissionGoals.fposY;
				x2 = mision->GetLinkUpLeft()->m_MissionGoals.fposX;
				y2 = mision->GetLinkUpLeft()->m_MissionGoals.fposY;
				float fatan = atanf( ((y2-y1)/(x2-x1)) );
				float fzoomtw = sqrtf( powf( x1-x2, 2 ) + powf( y1-y2, 2 ) );
				fatan *= 180.f/PI;
				//LOG->Trace( "fatan: %.2f", fatan );
				sp->SetRotationZ( fatan );
				sp->ZoomToWidth( fzoomtw );
				sp->SetXY( x1, y1 );

				if( x1 <= x2 )//cambia la orientacion
					sp->SetHorizAlign( HorizAlign_Left );
				else
					sp->SetHorizAlign( HorizAlign_Right );

				m_Lines.push_back( sp );
				this->AddChild( sp );
				continue;
			}
			else if( mision->CanGoUpRight() && !bUsedur )
			{
				bUsedur = true;

				ASSERT( mision && mision->GetLinkUpRight() );
				RString sTo = mision->GetLinkUpRight()->GetMissionLand();
				RString sFrom = mision->GetMissionLand();
				sTo.MakeLower(); 
				sFrom.MakeLower();
				sp->SetName( ssprintf( "Line-%s-%s", sTo.c_str(), sFrom.c_str() ) );

				//ocultamos si son hiddenlines
				if( mision->GetLinkUpRight()->IsHiddenLine() )
					continue;

				//sp = &m_Line;

				float x1, x2, y1, y2;
				x1 = mision->m_MissionGoals.fposX;
				y1 = mision->m_MissionGoals.fposY;
				x2 = mision->GetLinkUpRight()->m_MissionGoals.fposX;
				y2 = mision->GetLinkUpRight()->m_MissionGoals.fposY;
				float fatan = atanf( ((y2-y1)/(x2-x1)) );
				float fzoomtw = sqrtf( powf( x1-x2, 2 ) + powf( y1-y2, 2 ) );
				fatan *= 180.f/PI;
				//LOG->Trace( "fatan: %.2f", fatan );
				sp->SetRotationZ( fatan );
				sp->ZoomToWidth( fzoomtw );
				sp->SetXY( x1, y1 );

				if( x1 <= x2 )//cambia la orientacion
					sp->SetHorizAlign( HorizAlign_Left );
				else
					sp->SetHorizAlign( HorizAlign_Right );

				m_Lines.push_back( sp );
				this->AddChild( sp );
				continue;
			}
			delete sp;
		}
	}

	//las bullets sobre las lineas!
	for( unsigned int i=0; i <  added.size(); i++ )
	{//el boss siempre se esta viendo, aunque no la hayamos pasado ni llegado donde el.
		//si nos pide, special graphic, igual la mostramos siempre.
		bool bSpecialgfx = (added[i]->m_MissionGoals.sNeedGraphics.CompareNoCase("YES") == 0);
		//si tiene cualquiera de los caminos ocultos, lo mostramos, pero sin caminos
		bool bHaveHiddenPath = ( added[i]->GetLinkBothDownHidden() || added[i]->GetLinkBothUpHidden() ||
			added[i]->GetLinkDownLeftHidden() || added[i]->GetLinkDownRightHidden() ||
			added[i]->GetLinkUpLeftHidden() || added[i]->GetLinkUpRightHidden() );

		if( !added[i]->IsPlayable() && !added[i]->IsBoss() && !bSpecialgfx && !bHaveHiddenPath && !added[i]->IsHiddenLine() )//si es HL no mostramos ni bullet ni caminos
			continue;

		MissionBullet* mb = new MissionBullet;
		mb->SetName( ssprintf("Bullet%s", added[i]->GetMissionLand().c_str()) );
		mb->Load( added[i] );
		m_Bullets.push_back( mb );
		this->AddChild( mb );
		//delete mb;
	}

	//right
	m_ArrowBlue[0].SetName( "Mission ArrowBlue Right" );
	m_ArrowBlue[0].Load( THEME->GetPathG( "WorldMax/Mission", "arrow blueright" ) );
	m_ArrowBlue[0].SetVisible( false );
	this->AddChild( &m_ArrowBlue[0] );
	
	//left
	m_ArrowBlue[1].SetName( "Mission ArrowBlue Left" );
	m_ArrowBlue[1].Load( THEME->GetPathG( "WorldMax/Mission", "arrow blueleft" ) );
	m_ArrowBlue[1].SetVisible( false );
	this->AddChild( &m_ArrowBlue[1] );

	//right
	m_ArrowRed[0].SetName( "Mission ArrowRed Right" );
	m_ArrowRed[0].Load( THEME->GetPathG( "WorldMax/Mission", "arrow redright" ) );
	m_ArrowRed[0].SetVisible( false );
	this->AddChild( &m_ArrowRed[0] );

	//left
	m_ArrowRed[1].SetName( "Mission ArrowRed Left" );
	m_ArrowRed[1].Load( THEME->GetPathG( "WorldMax/Mission", "arrow redleft" ) );
	m_ArrowRed[1].SetVisible( false );
	this->AddChild( &m_ArrowRed[1] );
	
	//light sobre todo.
	m_HighLight.SetName( "Mission HighLight" );
	m_HighLight.Load( THEME->GetPathG( "WorldMax/Mission", "highlight" ) );
	m_HighLight.SetVisible( false );
	this->AddChild( &m_HighLight );

	m_HighLightWarp.SetName( "Mission HighLightWarp" );
	m_HighLightWarp.Load( THEME->GetPathG( "WorldMax/Mission", "highlightwarp" ) );
	m_HighLightWarp.SetVisible( false );
	this->AddChild( &m_HighLightWarp );

	m_HighLightBoss.SetName( "Mission HighLightBoss" );
	m_HighLightBoss.Load( THEME->GetPathG( "WorldMax/Mission", "highlightboss" ) );
	m_HighLightBoss.SetVisible( false );
	m_HighLightBoss.SetZoom( 0.7f );
	this->AddChild( &m_HighLightBoss );

	m_HiddenLineHL.SetName( "Mission HighLightHiddenLight" );
	m_HiddenLineHL.Load( THEME->GetPathG( "WorldMax/Mission", "highlighthiddenline" ) );
	m_HiddenLineHL.SetVisible( false );
	this->AddChild( &m_HiddenLineHL );

	//CHECKPOINT;

	Mission* pMission = SONGMAN->FindMission( "", pProfile->m_sLastMission );

	//CHECKPOINT;
	
	if( !pMission )
	{
		//CHECKPOINT_M( ssprintf( "Vector Size: %d", added.size() ) );
		for( unsigned i = 0; i < added.size(); i++ )//buscamos una mission que podamos jugar
		{
			//if( added[i]->IsPassed() && added[i]->IsSongPlayable() && !added[i]->IsWarpSector() )
			//CHECKPOINT_M( ssprintf( "Mission: %s", added[i]->GetMissionName() ) );
			if( added[i]->IsInitialMission() )
			{
				GAMESTATE->m_pCurMission.Set( added[i] );
				GAMESTATE->m_pCurSong.Set( added[i]->m_pSong );

				CHECKPOINT_M( ssprintf( "Initial: %s", GAMESTATE->m_pCurMission->GetMissionName().c_str() ) );

				float fx, fy;
				GetPosition( added[i], fx, fy );
				this->SetXY( fx, fy );

				break;
			}
		}
	}
	else
	{//HACK: así actualiza los .lua
		GAMESTATE->m_pCurMission.Set( pMission );
		GAMESTATE->m_pCurSong.Set( pMission->m_pSong );
		float fx, fy;
		GetPosition( pMission, fx, fy );
		this->SetXY( fx, fy );
	}

	ASSERT_M( GAMESTATE->m_pCurMission, "No hay una Mission para seleccionar." );
	//CHECKPOINT;
}

void MissionSectorMap::Update( float fDeltaTime )
{
	ActorFrame::Update( fDeltaTime );

	DrawHighLight();

	DrawDirectionalArrows();
}

void MissionSectorMap::DrawDirectionalArrows()
{
	if( IsMoving() )
	{
		m_ArrowBlue[0].SetVisible( false );
		m_ArrowBlue[1].SetVisible( false );
		m_ArrowRed[0].SetVisible( false );
		m_ArrowRed[1].SetVisible( false );
		return;
	}

	Mission* mision = GAMESTATE->m_pCurMission;

	if( mision->IsHiddenLine() )
		return;

	if( mision->CanGoDownRight() )
	{
		//no mostramos nunca para las hiddenlines
		if( mision->GetLinkDownRight()->IsHiddenLine() )
			return;

		float x1, x2, y1, y2;
		x1 = mision->m_MissionGoals.fposX;
		y1 = mision->m_MissionGoals.fposY;
		x2 = mision->GetLinkDownRight()->m_MissionGoals.fposX;
		y2 = mision->GetLinkDownRight()->m_MissionGoals.fposY;
		float fatan = atanf( ((y2-y1)/(x2-x1)) );
		fatan *= 180.f/PI;

		m_ArrowBlue[0].SetRotationZ( fatan );
		m_ArrowBlue[0].SetXY( x1, y1 );
		m_ArrowBlue[0].SetVisible( true );

		m_ArrowBlue[0].SetHorizAlign( HorizAlign_Left );
	}

	if( mision->CanGoDownLeft() )
	{
		//no mostramos nunca para las hiddenlines
		if( mision->GetLinkDownLeft()->IsHiddenLine() )
			return;

		float x1, x2, y1, y2;
		x1 = mision->m_MissionGoals.fposX;
		y1 = mision->m_MissionGoals.fposY;
		x2 = mision->GetLinkDownLeft()->m_MissionGoals.fposX;
		y2 = mision->GetLinkDownLeft()->m_MissionGoals.fposY;
		float fatan = atanf( ((y2-y1)/(x2-x1)) );
		fatan *= 180.f/PI;

		m_ArrowBlue[1].SetRotationZ( fatan );
		m_ArrowBlue[1].SetXY( x1, y1 );
		m_ArrowBlue[1].SetVisible( true );

		m_ArrowBlue[1].SetHorizAlign( HorizAlign_Right );
	}

	if( mision->CanGoUpRight() )
	{
		//no mostramos nunca para las hiddenlines
		if( mision->GetLinkUpRight()->IsHiddenLine() )
			return;

		float x1, x2, y1, y2;
		x1 = mision->m_MissionGoals.fposX;
		y1 = mision->m_MissionGoals.fposY;
		x2 = mision->GetLinkUpRight()->m_MissionGoals.fposX;
		y2 = mision->GetLinkUpRight()->m_MissionGoals.fposY;
		float fatan = atanf( ((y2-y1)/(x2-x1)) );
		fatan *= 180.f/PI;

		m_ArrowRed[0].SetRotationZ( fatan );
		m_ArrowRed[0].SetXY( x1, y1 );
		m_ArrowRed[0].SetVisible( true );

		m_ArrowRed[0].SetHorizAlign( HorizAlign_Left );
	}

	if( mision->CanGoUpLeft() )
	{
		//no mostramos nunca para las hiddenlines
		if( mision->GetLinkUpLeft()->IsHiddenLine() )
			return;

		float x1, x2, y1, y2;
		x1 = mision->m_MissionGoals.fposX;
		y1 = mision->m_MissionGoals.fposY;
		x2 = mision->GetLinkUpLeft()->m_MissionGoals.fposX;
		y2 = mision->GetLinkUpLeft()->m_MissionGoals.fposY;
		float fatan = atanf( ((y2-y1)/(x2-x1)) );
		fatan *= 180.f/PI;

		m_ArrowRed[1].SetRotationZ( fatan );
		m_ArrowRed[1].SetXY( x1, y1 );
		m_ArrowRed[1].SetVisible( true );

		m_ArrowRed[1].SetHorizAlign( HorizAlign_Right );
	}
}

void MissionSectorMap::DrawHighLight()
{
	Mission* pMision = GAMESTATE->m_pCurMission;
	m_HighLight.SetXY( pMision->m_MissionGoals.fposX, pMision->m_MissionGoals.fposY );
	m_HighLightBoss.SetXY( pMision->m_MissionGoals.fposX, pMision->m_MissionGoals.fposY );
	m_HighLightWarp.SetXY( pMision->m_MissionGoals.fposX, pMision->m_MissionGoals.fposY );
	m_HiddenLineHL.SetXY( pMision->m_MissionGoals.fposX, pMision->m_MissionGoals.fposY );

	if( IsMoving() )
	{
		m_HighLight.SetVisible( false );
		m_HighLightBoss.SetVisible( false );
		m_HighLightWarp.SetVisible( false );
		m_HiddenLineHL.SetVisible( false );
		return;
	}

	switch( pMision->GetMissionType() )
	{
	case MissionType_Boss:
	case MissionType_Switch:
	case MissionType_SpaceWarp:
		m_HighLight.SetVisible( false );
		m_HighLightWarp.SetVisible( false );
		m_HiddenLineHL.SetVisible( false );

		m_HighLightBoss.SetEffectPulse( 1.0f, 0.7f, 0.85f );
		m_HighLightBoss.SetVisible( true );

		break;
	case MissionType_WarpToWarp:
	case MissionType_Warp:
		m_HighLight.SetVisible( false );
		m_HighLightBoss.SetVisible( false );
		m_HiddenLineHL.SetVisible( false );
		
		if( !m_bSelected )
			m_HighLightWarp.SetEffectSpin(RageVector3(0,0,-180));
		else
			m_HighLightWarp.SetEffectPulse( 1.0f, 1.2f, 1.0f );

		m_HighLightWarp.SetVisible( true );

		break;
	case MissionType_Mission://no usar default, por que no estamos en missionselectsector
	case MissionType_DualWarp:
		m_HighLightWarp.SetVisible( false );
		m_HighLightBoss.SetVisible( false );
		m_HiddenLineHL.SetVisible( false );
		
		if( !m_bSelected )
			m_HighLight.SetEffectSpin(RageVector3(0,0,-180));
		else
			m_HighLight.SetEffectPulse( 1.0f, 1.2f, 1.0f );

		m_HighLight.SetVisible( true );
		break;
	case MissionType_HiddenLine:
		m_HighLight.SetVisible( false );
		m_HighLightBoss.SetVisible( false );
		m_HighLightWarp.SetVisible( false );
		m_HiddenLineHL.SetVisible( true );
	}
}

//centra el movimiento
void MissionSectorMap::GetPosition( Mission* pMission, float &fX_out, float &fY_out )
{
	const float fx = pMission->m_MissionGoals.fposX;
	const float fy = pMission->m_MissionGoals.fposY;
	const float fcenterx = SCREEN_CENTER_X/this->GetZoom(), fcentery = (SCREEN_CENTER_Y-(SCREEN_CENTER_Y/4))/this->GetZoom();

	float foffsetx = 0;
	float foffsety = 0;

	if( fcenterx > fx && fcentery > fy ) // +/+
	{
		foffsetx = fcenterx-fx;
		foffsety = fcentery-fy;
	}
	else if( fcenterx > fx && fcentery < fy ) // +/-
	{
		foffsetx = fcenterx-fx;
		foffsety = fcentery-fy;
	}
	else if( fcenterx < fx && fcentery < fy ) // -/-
	{
		foffsetx = fcenterx-fx;
		foffsety = fcentery-fy;
	}
	else if( fcenterx < fx && fcentery > fy )// -/+
	{
		foffsetx = fcenterx-fx;
		foffsety = fcentery-fy;
	}
	else if( fcenterx == fx && fcentery > fy )// =/+
	{
		foffsetx = 0;
		foffsety = fcentery-fy;
	}
	else if( fcenterx > fx && fcentery == fy )// +/=
	{
		foffsetx = fcenterx-fx;
		foffsety = 0;
	}
	else if( fcenterx == fx && fcentery < fy )// =/-
	{
		foffsetx = 0;
		foffsety = fcentery-fy;
	}
	else if( fcenterx < fx && fcentery == fy )// -/=
	{
		foffsetx = fcenterx-fx;
		foffsety = 0;
	}

	fX_out = foffsetx;
	fY_out = foffsety;

	fX_out *= this->GetZoom();
	fY_out *= this->GetZoom();
}

void MissionSectorMap::ChangeVisibleStateOfActorsByLand(RString sLand, bool bVisible )
{
	sLand.MakeLower();
	for( unsigned i = 0; i < m_SubActors.size(); i++ )
	{
		Actor* a = m_SubActors[i];

		if( a->GetName().find( sLand ) != string::npos ||
			a->GetName().CompareNoCase( ssprintf( "Bullet%s", sLand.c_str() ) ) == 0 )
		{
			a->SetVisible( bVisible );
		}
	}
}

void MissionSectorMap::LoadFromNode( const XNode *pNode )
{
	ActorFrame::LoadFromNode( pNode );
}

/*
 * (c) 2008 Victor Manuel Vicente Gajardo Henríquez "v1toko"
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
