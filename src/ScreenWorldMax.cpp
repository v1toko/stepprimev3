#include "global.h"
#include "ScreenWorldMax.h"
#include "ScreenManager.h"
#include "RageUtil.h"
#include "RageLog.h"
#include "GameState.h"
#include "ThemeManager.h"
#include "CodeDetector.h"
#include "SongManager.h"
#include "GameManager.h"
#include "PlayerOptions.h"
#include "SongOptions.h"
#include "PlayerState.h"
#include "SongUtil.h"
#include "Style.h"
#include "MenuTimer.h"
#include "ProfileManager.h"
#include "MessageManager.h"
#include "InputMapper.h"

#define		BASE_ZOOM		THEME->GetMetricF( m_sName, "BaseZoom" )
#define		BOMB_DELAY		THEME->GetMetricF( m_sName, "BombDelay" )
#define		PATH_FAILED_DELAY	THEME->GetMetricF( m_sName, "PathFailedDelay" )

//static const char *SelectStateNames[] = {
//	"SelectingMissions",
//	"SelectingSectors",
//};
//XToString( SelectState );

REGISTER_SCREEN_CLASS( ScreenWorldMax );

AutoScreenMessage( SM_WarpActivated );
AutoScreenMessage( SM_WarpToWarpActivated );
AutoScreenMessage( SM_SwitchStateChanged );
AutoScreenMessage( SM_PathFailed );//para las hidden lines, cuando fallamos un camino
AutoScreenMessage( SM_BombActivated );//cuando pasamos por una mision y activamos una bomba
AutoScreenMessage( SM_LandChanged );//para cambiar la musica de fondo.
AutoScreenMessage( SM_CodeEnteringFinished );

enum SelectState {
	SelectState_Missions,
	SelectState_Sectors,
	SelectState_EnteringCode,
	NUM_SelectState,
	SelectState_Invalid,
};
SelectState g_SelectState;
//const RString& SelectStateToString( SelectState ss );

void ScreenWorldMax::BeginScreen()
{
	ScreenWithMenuElements::BeginScreen();

	//seteamos wm mission info.
	AfterMissionChange();
}

void ScreenWorldMax::Update( float fDeltaTime )
{
	if( m_bOutOfTime && !m_bSelected )
	{		
		if( GAMESTATE->m_pCurMission->IsSongPlayable() )
		{
			//MenuStart( InputEventPlus() );
			InputEventPlus input;
			input.pn = m_pn;

			if( !IsTransitioning() )
			{
				MenuStart( input );
				MenuStart( input );
			}
		}
	}

	ScreenWithMenuElements::Update( fDeltaTime );
}

ScreenWorldMax::~ScreenWorldMax()
{
	//Old BAD style
	/*for( unsigned i = 0; i < m_mapSectors.size(); i++ )
	{		
		SAFE_DELETE( m_mapSectors[i] );
	}*/

	while( m_mapSectors.size() > 0 )
	{
		MissionSectorMap *sector = m_mapSectors[m_mapSectors.size()-1];
		SAFE_DELETE(sector);
		m_mapSectors.pop_back();
	}

	//FOREACHM_CONST( RString, RageSound, m_mapSounds, snd )
	//	delete snd->second;

	//m_mapSectors.clear();
	m_mapSounds.clear();
}

void ScreenWorldMax::Init()
{
	ScreenWithMenuElements::Init();

	m_bMissionPlayable = true;//por defecto, son jugables!
	m_bTransitioningInWarp = false;//no estamos "warping"
	m_bOutOfTime = false;
	m_bContinueMoving = false;
	m_bUsingShield = false;
	m_mapSectors.clear();
	m_mapSounds.clear();

	m_sLastSeenLand = RString();

	m_sCurrentItem = RString();

	m_pProfile = NULL;
	m_pn = PLAYER_INVALID;

	//siempre se inicia en una mission en "MapSector", nunca en "SelectSector"
	g_SelectState = SelectState_Missions;
	m_iSectorIndex = 0;

	m_pn = GAMESTATE->GetFirstHumanPlayer();

	if( GAMESTATE->IsPlayerEnabled( m_pn ) )
		GAMESTATE->m_pPlayerState[m_pn]->ResetToDefaultPlayerOptions( ModsLevel_Preferred );
	GAMESTATE->ResetToDefaultSongOptions( ModsLevel_Preferred );

	m_pProfile = PROFILEMAN->IsPersistentProfile( m_pn ) ? PROFILEMAN->GetProfile( m_pn ) : PROFILEMAN->GetMachineProfile();

	ASSERT( m_pProfile );

	//m_sCurrentItem = m_pProfile->m_sItemsInfo;

	if( m_sCurrentItem.CompareNoCase( "Time+30" ) == 0 )
		m_MenuTimer->SetSeconds( m_MenuTimer->GetSeconds() + 30 );

	if( m_sCurrentItem.CompareNoCase( "Shield" ) == 0 )
		m_bUsingShield = true;

	CHECKPOINT;

	{
		vector<Mission*> missions;
		SONGMAN->GetAllMissions( missions );
		RString sLastSeenMission = RString();
		for( unsigned i = 0; i < missions.size(); i++ )
		{
			Mission* mision = missions[i];
			mision->SetUpMissionState( m_pProfile );//esto se hace antes de GetMissionCompletePercent()

			//setup the switches
			if( mision->IsSwitch() )
			{
				FOREACHS_CONST( RString, m_pProfile->m_vsSwitchActives, sa )
				{
					RString mission = sa->c_str();
					RString missionname = mision->GetMissionName();

					if( !stricmp( missionname, mission ) )//el switch está activo
						mision->ConfirmSwitchState( mission );
				}

				//si algun switch necesita tener que invalidar una mision
				if( !mision->GetSwitchState() )
				{					
					vector<RString> vsVal;
					split( mision->m_MissionGoals.sValidateMissionWhenIsActive, ",", vsVal );

					FOREACH_CONST( RString, vsVal, iter )
					{
						Mission* pmision;
						pmision = SONGMAN->FindMission( (*iter) );

						if( pmision )
							pmision->Invalidate();
					}
				}
			}

			//BG sounds por sector
			if( mision->GetMissionLand() != sLastSeenMission )
			{
				if( (mision->GetMissionType() != MissionType_WarpSector) )
				{
					sLastSeenMission = mision->GetMissionLand();
					RageSound sound;
					sound.Load( THEME->GetPathS( "WorldMax/Mission", sLastSeenMission ), true );

					m_mapSounds.insert( pair<RString,RageSound>( sLastSeenMission, sound ) );
				}
			}
		}
		missions.clear();
	}

	/*{//aqui revisamos las misiones por codigo de bloqueo =B
		vector<RString> vsNames;
		m_Codes.GetCodeNames( vsNames );

		CHECKPOINT;

		for( unsigned i = 0; i < (int)vsNames.size(); i++ )
		{
			Mission* pms = SONGMAN->FindMission( "", vsNames[i] );

			if( pms )
			{
				pms->Lock();

				m_Codes.GetNumKeysCodeByName( vsNames[i], pms->m_iNumCodes );
				pms->m_MissionGoals.iNumCodes = pms->m_iNumCodes;
				pms->m_bNeededCodes = true;

				//LOG->Trace( "Mission: %s, NumCodes: %d", pms->GetMissionName().c_str(), pms->m_iNumCodes );

				FOREACHS_CONST( RString, m_pProfile->m_vsCodesAplieds, ca )
				{
					RString mission = ca->c_str();
					RString missionname = pms->GetMissionName();

					if( !stricmp( missionname, mission ) )//ponemos esto despues por que es probable que la hayamos
					{
						pms->UnLock();//pasado y la pone a disabled!
						pms->m_iNumCodes = 0;
						pms->m_MissionGoals.iNumCodes = 0;						
					}
				}
			}
		}
		vsNames.clear();
	}*/

	vector<RString> names;
	SONGMAN->GetMissionGroupNames( names );
	ASSERT_M( names.size() > 0, "No hay subgrupos creados o no hay misiones para crear worldmax" );
	for( unsigned i = 0; i < names.size(); i++ )
	{
		LOG->Trace( "WorldMax Loading Sector: %s", names[i].c_str() );
		MissionSectorMap* sector = new MissionSectorMap;
		sector->SetName( "MapSector" );
		sector->SetZoom( BASE_ZOOM );
		sector->Init( names[i], m_pProfile );
		sector->SetVisible( false );
		m_mapSectors.push_back( sector );
		this->AddChild( m_mapSectors[m_mapSectors.size()-1] );
	}

	//siempre se inicia en una mission en "MapSector", nunca en "SelectSector"
	m_mapSectorSelect.SetName( "MapSelectSector" );
	m_mapSectorSelect.Init();
	m_mapSectorSelect.SetDrawOrder( DRAW_ORDER_AFTER_EVERYTHING );
	m_mapSectorSelect.SetVisible( false );
	this->AddChild( &m_mapSectorSelect );

	CHECKPOINT;
	SetSectorAndMap();
	CHECKPOINT;

	m_sprWarps.SetName( "WarpTransition" );//efecto del warp
	m_sprWarps.Load( THEME->GetPathG( "ScreenWorldMax", "warptransition" ) );
	m_sprWarps.SetDiffuseAlpha( 0 );
	m_sprWarps.ScaleToClipped( 640, 480 );
	m_sprWarps.SetXY( 320, 240 );
	m_sprWarps.SetDrawOrder( DRAW_ORDER_AFTER_EVERYTHING+1000 );//on top de todo
	this->AddChild( &m_sprWarps );

	m_soundMove.Load( THEME->GetPathS( "WorldMax/WorldMax", "move" ) );//cuando se mueve
	m_soundNotMove.Load( THEME->GetPathS( "WorldMax/WorldMax", "notmove" ) );//cuendo no se puede mover
	m_soundCenter.Load( THEME->GetPathS( "WorldMax/WorldMax", "center" ) );//presiona centro... la segunda vez, actua start
	m_soundStart.Load( THEME->GetPathS( "WorldMax/WorldMax", "start" ) );//start
	m_soundNotStart.Load( THEME->GetPathS( "WorldMax/WorldMax", "notstart" ) );//cuando no puede start
	m_soundWarp.Load( THEME->GetPathS( "WorldMax/WorldMax", "warp" ) );//entra a un warp
	m_soundSwitch.Load( THEME->GetPathS( "WorldMax/WorldMax", "switch" ) );//cambia un switch

	vector<StepsType> vst;
	GAMEMAN->GetStepsTypesForGame( GAMESTATE->m_pCurGame, vst );
	const Style *pStyle = GAMEMAN->GetFirstCompatibleStyle( GAMESTATE->m_pCurGame, GAMESTATE->GetNumSidesJoined(), vst[0] );
	GAMESTATE->SetCurrentStyle( pStyle, GAMESTATE->GetFirstHumanPlayer() );

	m_bSelected = false;
	m_mapSectors[m_iSectorIndex]->SetSelected( m_bSelected );

	{
		vector<Mission*> missions;
		SONGMAN->GetAllMissions( missions );
		for( unsigned i = 0; i < missions.size(); i++ )
		{
			Mission* mision = missions[i];

			//setup the visibility of lands
			if( mision->IsSwitch() )
			{
				if( mision->GetSwitchState() )//switch activado
				{
					vector<RString> vslandsA;//para activar
					vector<RString> vslandsD;//para desactivar
					split( mision->m_MissionGoals.sLandsToShow, ",", vslandsA );
					split( mision->m_MissionGoals.sLandsToHide, ",", vslandsD );

					for( unsigned i = 0; i < vslandsA.size(); i++ )
					{
						for( unsigned j = 0; j < m_mapSectors.size(); j++ )
							m_mapSectors[j]->ChangeVisibleStateOfActorsByLand( vslandsA[i], true );
					}
					for( unsigned i = 0; i < vslandsD.size(); i++ )
					{
						for( unsigned j = 0; j < m_mapSectors.size(); j++ )
							m_mapSectors[j]->ChangeVisibleStateOfActorsByLand( vslandsD[i], false );
					}
				}
				else
				{
					vector<RString> vslandsA;//para activar
					vector<RString> vslandsD;//para desactivar
					split( mision->m_MissionGoals.sLandsToShow, ",", vslandsA );
					split( mision->m_MissionGoals.sLandsToHide, ",", vslandsD );

					for( unsigned i = 0; i < vslandsA.size(); i++ )
					{
						for( unsigned j = 0; j < m_mapSectors.size(); j++ )
							m_mapSectors[j]->ChangeVisibleStateOfActorsByLand( vslandsA[i], false );
					}
					for( unsigned i = 0; i < vslandsD.size(); i++ )
					{
						for( unsigned j = 0; j < m_mapSectors.size(); j++ )
							m_mapSectors[j]->ChangeVisibleStateOfActorsByLand( vslandsD[i], true );
					}
				}
			}
		}
		missions.clear();
	}
}

void ScreenWorldMax::AfterMissionChange()
{
	m_vpSteps.clear();

	//en estos no hay song...
	bool bHasSong = (GAMESTATE->m_pCurMission->GetMissionType() == MissionType_Mission) || (GAMESTATE->m_pCurMission->GetMissionType() == MissionType_Boss);
	bHasSong |= (GAMESTATE->m_pCurMission->GetMissionType() == MissionType_DualWarp) && !GAMESTATE->m_pCurMission->IsPassed();

	if( bHasSong )
		GAMESTATE->m_pCurSong.Set( GAMESTATE->m_pCurMission->m_pSong );
	else//así si nos quedamos en un warp no crashea
	{
		m_bMissionPlayable = true;
		return;
	}

	CHECKPOINT_M( ssprintf( "Mission: %s", GAMESTATE->m_pCurMission->GetMissionName().c_str() ) );
	//ASSERT( GAMESTATE->m_pCurMission->m_pSong );

	if( GAMESTATE->GetNumStagesLeft( m_pn ) < GAMESTATE->GetNumStagesMultiplierForSong( GAMESTATE->m_pCurSong ) )
		m_bMissionPlayable = false;
	else
		m_bMissionPlayable = true;
		
	if( !m_bMissionPlayable )
		return;

	if( GAMESTATE->m_pCurMission->m_bRandomSong )//aqui manda el random
	{
		//llamamos así, por un poco más de orden.
		CheckRandomSong();
	}
	else
	{
		SongUtil::GetPlayableSteps( GAMESTATE->m_pCurSong, m_vpSteps );

		if( m_vpSteps.empty() )
			GAMESTATE->m_pCurSteps[m_pn].Set( NULL );
		else
			GAMESTATE->m_pCurSteps[m_pn].Set( m_vpSteps[0] );

		/* Now that Steps have been chosen, set a Style that can play them. */
		const Style *pStyle = NULL;
		if( pStyle == NULL )
		{
			StepsType stCurrent;
			//PlayerNumber pn = GAMESTATE->GetFirstHumanPlayer();
			stCurrent = GAMESTATE->m_pCurSteps[m_pn]->m_StepsType;

			pStyle = GAMEMAN->GetFirstCompatibleStyle( GAMESTATE->m_pCurGame, GAMESTATE->GetNumSidesJoined(), stCurrent );
		}
		GAMESTATE->SetCurrentStyle( pStyle, GAMESTATE->GetFirstHumanPlayer() );
	}

	//Si no nos estamos moviendo, no puede elegir automaticamente
	//la mision, requisito *exclusivo*, tener que moverse
	//antes de activar una mina.
	if( m_bContinueMoving )
	{
		if( GAMESTATE->m_pCurMission->m_MissionGoals.iBombPosibility > 0 && GAMESTATE->m_pCurMission->m_MissionGoals.iBombPosibility <= 100 )
		{
			int iRandom = RandomInt(1,100);

			if( GAMESTATE->m_pCurMission->m_MissionGoals.iBombPosibility > iRandom )
			{
				m_bTransitioningInWarp = true;
				MESSAGEMAN->Broadcast( "MineActivated" );

				if( m_bUsingShield )
					MESSAGEMAN->Broadcast( "ShieldActivated" );

				this->PostScreenMessage( SM_BombActivated, BOMB_DELAY );
			}
		}
	}
}

void ScreenWorldMax::SetSectorAndMap()
{
	for( unsigned i = 0; i < m_mapSectors.size(); i++ )
	{
		RString MS = GAMESTATE->m_pCurMission->GetMissionSector();
		RString SN = m_mapSectors[i]->GetSectorName();
		if( MS == SN )
		{
			m_mapSectors[i]->SetVisible( true );
			m_iSectorIndex = i;
		}
		else
			m_mapSectors[i]->SetVisible( false );
	}

	m_sLastSeenLand = GAMESTATE->m_pCurMission->GetMissionLand();
	//obligamos el cambio de bgmusic, por que cambiamos de land.
	this->PostScreenMessage( SM_LandChanged, 0 );
}

bool ScreenWorldMax::MenuBack( const InputEventPlus &input )
{
	Cancel( SM_GoToPrevScreen );
	return true;
}

bool ScreenWorldMax::MenuStart( const InputEventPlus &input )
{
	if( input.type != IET_FIRST_PRESS )//si no es firstpress return
		return false;

	if( m_In.IsTransitioning() || m_Cancel.IsTransitioning() || m_mapSectors[m_iSectorIndex]->IsMoving() ) /* not m_Out */
		return false;

	if( !m_bMissionPlayable )//si las stages no alcanzan, no hacemos nada!
		return false;
	
	if( GAMESTATE->m_pCurMission->IsLocked() && g_SelectState != SelectState_EnteringCode )
	{
		//solo podemos entrar codes cuando tenemos 5 o más items
		/*
		if( m_pProfile->m_iTotalItems < GAMESTATE->m_pCurMission->m_MissionGoals.iNumCodes )
		{
			m_soundNotStart.Play();
			MESSAGEMAN->Broadcast( "CannotEnterCode" );
			return;
		}
		*/

		MESSAGEMAN->Broadcast( "EnteringCode" );
		g_SelectState = SelectState_EnteringCode;
		return false;
	}

	//no tenemos codes para entrar.
	/*if( GAMESTATE->m_pCurMission->m_iNumCodes <= 0 && 
		g_SelectState == SelectState_EnteringCode )
		g_SelectState = SelectState_Missions;*/

	if( g_SelectState == SelectState_Missions )//seleccionando missiones
	{
		INPUTQUEUE->ClearQueue( (GameController)input.pn );

		if( !m_bSelected )
		{
			m_bSelected = true;
			m_mapSectors[m_iSectorIndex]->SetSelected( m_bSelected );
			m_soundCenter.Play();
			return false;
		}

		if( GAMESTATE->m_pCurMission->IsWarp() || GAMESTATE->m_pCurMission->IsSpaceWarp() )//si seleccionamos un wrap
		{//cambia el state y vamos a el selector de Sectores
			m_bTransitioningInWarp = true;
			this->PostScreenMessage( SM_WarpActivated, 2.0f );
			MESSAGEMAN->Broadcast( "TransitioningWarp" );
			m_sprWarps.BeginTweening( 2.0f );
			m_sprWarps.SetDiffuseAlpha( 1 );
			m_soundWarp.Play();
			return false;
		}
		///XXX:
		else if( GAMESTATE->m_pCurMission->IsHiddenLine() )
		{
			return false;
		}
		else if( GAMESTATE->m_pCurMission->IsDualWarp() && GAMESTATE->m_pCurMission->IsPassed() )
		{
			m_bTransitioningInWarp = true;
			this->PostScreenMessage( SM_WarpActivated, 2.0f );
			MESSAGEMAN->Broadcast( "TransitioningDualWarp" );
			m_sprWarps.BeginTweening( 2.0f );
			m_sprWarps.SetDiffuseAlpha( 1 );
			m_soundWarp.Play();
			return false;
		}
		else if( GAMESTATE->m_pCurMission->IsWarpToWarp() )//si estamos en un warptowarp
		{
			m_bTransitioningInWarp = true;
			this->PostScreenMessage( SM_WarpToWarpActivated, 2.0f );
			MESSAGEMAN->Broadcast( "TransitioningPortal" );
			m_sprWarps.BeginTweening( 2.0f );
			m_sprWarps.SetDiffuseAlpha( 1 );
			m_soundWarp.Play();
			return false;
		}
		else if( GAMESTATE->m_pCurMission->IsSwitch() )
		{
			//XXX:
			m_bTransitioningInWarp = true;
			
			GAMESTATE->m_pCurMission->ToggleSwitch();
			
			if( GAMESTATE->m_pCurMission->GetSwitchState() )//if true... activating
			{
				vector<RString> vsInv;
				vector<RString> vsVal;
				split( GAMESTATE->m_pCurMission->m_MissionGoals.sInvalidateMissionWhenIsActive, ",", vsInv );
				split( GAMESTATE->m_pCurMission->m_MissionGoals.sValidateMissionWhenIsActive, ",", vsVal );

				FOREACH_CONST( RString, vsInv, iter )
				{
					Mission* pmision;
					pmision = SONGMAN->FindMission( (*iter) );

					if( pmision )
						pmision->Invalidate();
				}

				FOREACH_CONST( RString, vsVal, iter )
				{
					Mission* pmision;
					pmision = SONGMAN->FindMission( (*iter) );

					if( pmision )
						pmision->Validate();
				}

				m_pProfile->m_vsSwitchActives.insert( GAMESTATE->m_pCurMission->GetMissionName() );
			}
			else //desactivating
			{
				vector<RString> vsInv;
				vector<RString> vsVal;
				split( GAMESTATE->m_pCurMission->m_MissionGoals.sInvalidateMissionWhenIsActive, ",", vsInv );
				split( GAMESTATE->m_pCurMission->m_MissionGoals.sValidateMissionWhenIsActive, ",", vsVal );

				FOREACH_CONST( RString, vsInv, iter )
				{
					Mission* pmision;
					pmision = SONGMAN->FindMission( (*iter) );

					if( pmision )
						pmision->Validate();
				}

				FOREACH_CONST( RString, vsVal, iter )
				{
					Mission* pmision;
					pmision = SONGMAN->FindMission( (*iter) );
				
					if( pmision )
						pmision->Invalidate();
				}

				m_pProfile->m_vsSwitchActives.erase( GAMESTATE->m_pCurMission->GetMissionName() );
			}


			//setup the visibility of lands
			if( GAMESTATE->m_pCurMission->GetSwitchState() )//switch activado
			{
				vector<RString> vslandsA;//para activar
				vector<RString> vslandsD;//para desactivar
				split( GAMESTATE->m_pCurMission->m_MissionGoals.sLandsToShow, ",", vslandsA );
				split( GAMESTATE->m_pCurMission->m_MissionGoals.sLandsToHide, ",", vslandsD );

				for( unsigned i = 0; i < vslandsA.size(); i++ )
				{
					for( unsigned j = 0; j < m_mapSectors.size(); j++ )
						m_mapSectors[j]->ChangeVisibleStateOfActorsByLand( vslandsA[i], true );
				}
				for( unsigned i = 0; i < vslandsD.size(); i++ )
				{
					for( unsigned j = 0; j < m_mapSectors.size(); j++ )
						m_mapSectors[j]->ChangeVisibleStateOfActorsByLand( vslandsD[i], false );
				}
			}
			else
			{
				vector<RString> vslandsA;//para activar
				vector<RString> vslandsD;//para desactivar
				split( GAMESTATE->m_pCurMission->m_MissionGoals.sLandsToShow, ",", vslandsA );
				split( GAMESTATE->m_pCurMission->m_MissionGoals.sLandsToHide, ",", vslandsD );

				for( unsigned i = 0; i < vslandsA.size(); i++ )
				{
					for( unsigned j = 0; j < m_mapSectors.size(); j++ )
						m_mapSectors[j]->ChangeVisibleStateOfActorsByLand( vslandsA[i], false );
				}
				for( unsigned i = 0; i < vslandsD.size(); i++ )
				{
					for( unsigned j = 0; j < m_mapSectors.size(); j++ )
						m_mapSectors[j]->ChangeVisibleStateOfActorsByLand( vslandsD[i], true );
				}
			}
			

			{//send message
				Message msg( "SwitchStateChanged" );
				msg.SetParam( "SwitchName", GAMESTATE->m_pCurMission->GetMissionName() );
				
				ActorFrame::HandleMessage( msg );//propaga el mensage!, no lua
			}

			this->PostScreenMessage( SM_SwitchStateChanged, 1.0f );
			MESSAGEMAN->Broadcast( "SwitchStateChanged" );//lua
			m_soundSwitch.Play();
			m_bSelected = false;
			return false;
		}

		StartTransitioningScreen( SM_BeginFadingOut );
		m_soundStart.Play();
	}
	else if( g_SelectState == SelectState_Sectors )//seleccionando sectores
	{//podemos solo seleccionar sectores!
		/*if( GAMESTATE->m_pCurMission->IsLocked() )
			return;*/
		INPUTQUEUE->ClearQueue( (GameController)input.pn );

		if( GAMESTATE->m_pCurMission->IsWarpSector() )
		{
			SetSectorAndMap();
			g_SelectState = SelectState_Missions;
			m_mapSectorSelect.SetVisible( false );
			MESSAGEMAN->Broadcast( "TransitioningToMap" );
					
			float fx, fy;
			m_mapSectors[m_iSectorIndex]->GetPosition( GAMESTATE->m_pCurMission->GetLinkToMap(), fx, fy );
			m_mapSectors[m_iSectorIndex]->SetXY( fx, fy );
			m_bSelected = false;
			m_mapSectors[m_iSectorIndex]->SetSelected( m_bSelected );
			GAMESTATE->m_pCurMission.Set( GAMESTATE->m_pCurMission->GetLinkToMap() );

			Message msg( "MapMoved" );
			msg.SetParam( "PosX", fx );
			msg.SetParam( "PosY", fy );
			RString s = "ScreenWorldMaxWarpActivated";
			msg.SetParam( "From", s );
			MESSAGEMAN->Broadcast( msg );

			{
				Message msg( "SelectStateChanged" );
				msg.SetParam( "To", RString( "Lands" ) );
				MESSAGEMAN->Broadcast( msg );
			}

			AfterMissionChange();
			m_soundCenter.Play();
		}
	}
	else if( g_SelectState == SelectState_EnteringCode )
	{
		;
		//handleamos esto en input
	}

	return false;
}

void ScreenWorldMax::PathFailed()
{
	this->PostScreenMessage( SM_PathFailed, PATH_FAILED_DELAY );
	MESSAGEMAN->Broadcast( "PathFailed" );
	m_bTransitioningInWarp = true; //bloqueamos el movimiento
}

bool ScreenWorldMax::Input( const InputEventPlus &input )
{
	LOG->Trace( "ScreenWorldMax::Input( %d-%d )", input.DeviceI.device, input.DeviceI.button );	// debugging gameport joystick problem

	if( m_In.IsTransitioning() || m_Cancel.IsTransitioning() /*|| m_mapSectors[m_iSectorIndex]->IsMoving()*/ ) /* not m_Out */
		return false;

	if( m_bTransitioningInWarp )//si estamos "warping" no podemos movernos
		return false;

	if( input.type != IET_FIRST_PRESS )//si no es firstpress return
		return false;

	if( g_SelectState == SelectState_Missions )
	{

		float fx = 0, fy = 0;//coordenadas del mapa

		if( input.GameI.button == PUMP_BUTTON_UPLEFT )
		{
			INPUTQUEUE->ClearQueue( (GameController)input.pn );

			m_bContinueMoving = true;

			if( GAMESTATE->m_pCurMission->IsHiddenLine() )
			{
				if( !GAMESTATE->m_pCurMission->CanGoUpLeft() && !GAMESTATE->m_pCurMission->CanGoUpLeftHidden() )
				{
					PathFailed(); //send messages and lock moves.
					return false;
				}
			}

			if( GAMESTATE->m_pCurMission->CanGoUpLeft() )
			{
				m_mapSectors[m_iSectorIndex]->StopTweening();
				m_mapSectors[m_iSectorIndex]->BeginTweening( 0.3f );
				/*float fx, fy;*/
				m_mapSectors[m_iSectorIndex]->GetPosition( GAMESTATE->m_pCurMission->GetLinkUpLeft(), fx, fy );
				m_mapSectors[m_iSectorIndex]->SetXY( fx, fy );
				GAMESTATE->m_pCurMission.Set( GAMESTATE->m_pCurMission->GetLinkUpLeft() );
				m_bSelected = false;
				m_mapSectors[m_iSectorIndex]->SetSelected( m_bSelected );
				AfterMissionChange();
				m_soundMove.Play();
			}
			else if( GAMESTATE->m_pCurMission->CanGoUpLeftHidden() )
			{
				m_mapSectors[m_iSectorIndex]->StopTweening();
				m_mapSectors[m_iSectorIndex]->BeginTweening( 0.3f );
				/*float fx, fy;*/
				m_mapSectors[m_iSectorIndex]->GetPosition( GAMESTATE->m_pCurMission->GetLinkUpLeftHidden(), fx, fy );
				m_mapSectors[m_iSectorIndex]->SetXY( fx, fy );
				GAMESTATE->m_pCurMission.Set( GAMESTATE->m_pCurMission->GetLinkUpLeftHidden() );
				m_bSelected = false;
				m_mapSectors[m_iSectorIndex]->SetSelected( m_bSelected );
				AfterMissionChange();
				m_soundMove.Play();
			}
			else
				m_soundNotMove.Play();
		}
		else if( input.GameI.button == PUMP_BUTTON_UPRIGHT )
		{
			INPUTQUEUE->ClearQueue( (GameController)input.pn );

			m_bContinueMoving = true;
			if( GAMESTATE->m_pCurMission->IsHiddenLine() )
			{
				if( !GAMESTATE->m_pCurMission->CanGoUpRight() && !GAMESTATE->m_pCurMission->CanGoUpRightHidden() )
				{
					PathFailed(); //send messages and lock moves.
					return false;
				}
			}

			if( GAMESTATE->m_pCurMission->CanGoUpRight() )
			{
				m_mapSectors[m_iSectorIndex]->StopTweening();
				m_mapSectors[m_iSectorIndex]->BeginTweening( 0.3f );
				//float fx, fy;
				m_mapSectors[m_iSectorIndex]->GetPosition( GAMESTATE->m_pCurMission->GetLinkUpRight(), fx, fy );
				m_mapSectors[m_iSectorIndex]->SetXY( fx, fy );
				GAMESTATE->m_pCurMission.Set( GAMESTATE->m_pCurMission->GetLinkUpRight() );
				m_bSelected = false;
				m_mapSectors[m_iSectorIndex]->SetSelected( m_bSelected );
				AfterMissionChange();
				m_soundMove.Play();
			}
			else if( GAMESTATE->m_pCurMission->CanGoUpRightHidden() )
			{
				m_mapSectors[m_iSectorIndex]->StopTweening();
				m_mapSectors[m_iSectorIndex]->BeginTweening( 0.3f );
				//float fx, fy;
				m_mapSectors[m_iSectorIndex]->GetPosition( GAMESTATE->m_pCurMission->GetLinkUpRightHidden(), fx, fy );
				m_mapSectors[m_iSectorIndex]->SetXY( fx, fy );
				GAMESTATE->m_pCurMission.Set( GAMESTATE->m_pCurMission->GetLinkUpRightHidden() );
				m_bSelected = false;
				m_mapSectors[m_iSectorIndex]->SetSelected( m_bSelected );
				AfterMissionChange();
				m_soundMove.Play();
			}
			else
				m_soundNotMove.Play();
		}
		else if( input.GameI.button == PUMP_BUTTON_DOWNLEFT )
		{
			INPUTQUEUE->ClearQueue( (GameController)input.pn );

			m_bContinueMoving = true;
			if( GAMESTATE->m_pCurMission->IsHiddenLine() )
			{
				if( !GAMESTATE->m_pCurMission->CanGoDownLeft() && !GAMESTATE->m_pCurMission->CanGoDownLeftHidden() )
				{
					PathFailed(); //send messages and lock moves.
					return false;
				}
			}

			if( GAMESTATE->m_pCurMission->CanGoDownLeft() )
			{
				m_mapSectors[m_iSectorIndex]->StopTweening();
				m_mapSectors[m_iSectorIndex]->BeginTweening( 0.3f );
				//float fx, fy;
				m_mapSectors[m_iSectorIndex]->GetPosition( GAMESTATE->m_pCurMission->GetLinkDownLeft(), fx, fy );
				m_mapSectors[m_iSectorIndex]->SetXY( fx, fy );
				GAMESTATE->m_pCurMission.Set( GAMESTATE->m_pCurMission->GetLinkDownLeft() );
				m_bSelected = false;
				m_mapSectors[m_iSectorIndex]->SetSelected( m_bSelected );
				AfterMissionChange();
				m_soundMove.Play();
			}
			else if( GAMESTATE->m_pCurMission->CanGoDownLeftHidden() )
			{
				m_mapSectors[m_iSectorIndex]->StopTweening();
				m_mapSectors[m_iSectorIndex]->BeginTweening( 0.3f );
				//float fx, fy;
				m_mapSectors[m_iSectorIndex]->GetPosition( GAMESTATE->m_pCurMission->GetLinkDownLeftHidden(), fx, fy );
				m_mapSectors[m_iSectorIndex]->SetXY( fx, fy );
				GAMESTATE->m_pCurMission.Set( GAMESTATE->m_pCurMission->GetLinkDownLeftHidden() );
				m_bSelected = false;
				m_mapSectors[m_iSectorIndex]->SetSelected( m_bSelected );
				AfterMissionChange();
				m_soundMove.Play();
			}
			else
				m_soundNotMove.Play();
		}
		else if( input.GameI.button == PUMP_BUTTON_DOWNRIGHT )
		{
			INPUTQUEUE->ClearQueue( (GameController)input.pn );

			m_bContinueMoving = true;
			if( GAMESTATE->m_pCurMission->IsHiddenLine() )
			{
				if( !GAMESTATE->m_pCurMission->CanGoDownRight() && !GAMESTATE->m_pCurMission->CanGoDownRightHidden() )
				{
					PathFailed(); //send messages and lock moves.
					return false;
				}
			}

			if( GAMESTATE->m_pCurMission->CanGoDownRight() )
			{
				m_mapSectors[m_iSectorIndex]->StopTweening();
				m_mapSectors[m_iSectorIndex]->BeginTweening( 0.3f );
				//float fx, fy;
				m_mapSectors[m_iSectorIndex]->GetPosition( GAMESTATE->m_pCurMission->GetLinkDownRight(), fx, fy );
				m_mapSectors[m_iSectorIndex]->SetXY( fx, fy );
				GAMESTATE->m_pCurMission.Set( GAMESTATE->m_pCurMission->GetLinkDownRight() );
				m_bSelected = false;
				m_mapSectors[m_iSectorIndex]->SetSelected( m_bSelected );
				AfterMissionChange();
				m_soundMove.Play();
			}
			else if( GAMESTATE->m_pCurMission->CanGoDownRightHidden() )
			{
				m_mapSectors[m_iSectorIndex]->StopTweening();
				m_mapSectors[m_iSectorIndex]->BeginTweening( 0.3f );
				//float fx, fy;
				m_mapSectors[m_iSectorIndex]->GetPosition( GAMESTATE->m_pCurMission->GetLinkDownRightHidden(), fx, fy );
				m_mapSectors[m_iSectorIndex]->SetXY( fx, fy );
				GAMESTATE->m_pCurMission.Set( GAMESTATE->m_pCurMission->GetLinkDownRightHidden() );
				m_bSelected = false;
				m_mapSectors[m_iSectorIndex]->SetSelected( m_bSelected );
				AfterMissionChange();
				m_soundMove.Play();
			}
			else
				m_soundNotMove.Play();
		}

		{
			Message msg( "MapMoved" );
			msg.SetParam( "PosX", fx );
			msg.SetParam( "PosY", fy );
			RString s = "ScreenWorldMax";
			msg.SetParam( "From", s );
			MESSAGEMAN->Broadcast( msg );
		}

		if( m_sLastSeenLand != GAMESTATE->m_pCurMission->GetMissionLand() )
			this->PostScreenMessage( SM_LandChanged, 0 );

		switch( input.GameI.button )
		{
		case PUMP_BUTTON_UPLEFT:
		case PUMP_BUTTON_UPRIGHT:
		case PUMP_BUTTON_DOWNLEFT:
		case PUMP_BUTTON_DOWNRIGHT:
			m_sLastSeenLand = GAMESTATE->m_pCurMission->GetMissionLand();
		}
	}
	else if( g_SelectState == SelectState_Sectors )//seleccionando sectores!
	{
		if( input.GameI.button == PUMP_BUTTON_UPLEFT )
		{
			INPUTQUEUE->ClearQueue( (GameController)input.pn );

			m_bContinueMoving = true;
			if( GAMESTATE->m_pCurMission->CanGoUpLeft() )
			{
				GAMESTATE->m_pCurMission.Set( GAMESTATE->m_pCurMission->GetLinkUpLeft() );
				m_bSelected = false;
				m_soundMove.Play();
			}
			else if( GAMESTATE->m_pCurMission->CanGoUpLeftHidden() )
			{
				GAMESTATE->m_pCurMission.Set( GAMESTATE->m_pCurMission->GetLinkUpLeftHidden() );
				m_bSelected = false;
				m_soundMove.Play();
			}
			else
				m_soundNotMove.Play();
		}
		else if( input.GameI.button == PUMP_BUTTON_UPRIGHT )
		{
			INPUTQUEUE->ClearQueue( (GameController)input.pn );

			m_bContinueMoving = true;
			if( GAMESTATE->m_pCurMission->CanGoUpRight() )
			{
				GAMESTATE->m_pCurMission.Set( GAMESTATE->m_pCurMission->GetLinkUpRight() );
				m_bSelected = false;
				m_soundMove.Play();
			}
			else if( GAMESTATE->m_pCurMission->CanGoUpRightHidden() )
			{
				GAMESTATE->m_pCurMission.Set( GAMESTATE->m_pCurMission->GetLinkUpRightHidden() );
				m_bSelected = false;
				m_soundMove.Play();
			}
			else
				m_soundNotMove.Play();
		}
		else if( input.GameI.button == PUMP_BUTTON_DOWNLEFT )
		{
			INPUTQUEUE->ClearQueue( (GameController)input.pn );

			m_bContinueMoving = true;
			if( GAMESTATE->m_pCurMission->CanGoDownLeft() )
			{
				GAMESTATE->m_pCurMission.Set( GAMESTATE->m_pCurMission->GetLinkDownLeft() );
				m_bSelected = false;
				m_soundMove.Play();
			}
			else if( GAMESTATE->m_pCurMission->CanGoDownLeftHidden() )
			{
				GAMESTATE->m_pCurMission.Set( GAMESTATE->m_pCurMission->GetLinkDownLeftHidden() );
				m_bSelected = false;
				m_soundMove.Play();
			}
			else
				m_soundNotMove.Play();
		}
		else if( input.GameI.button == PUMP_BUTTON_DOWNRIGHT )
		{
			INPUTQUEUE->ClearQueue( (GameController)input.pn );

			m_bContinueMoving = true;
			if( GAMESTATE->m_pCurMission->CanGoDownRight() )
			{
				GAMESTATE->m_pCurMission.Set( GAMESTATE->m_pCurMission->GetLinkDownRight() );
				m_bSelected = false;
				m_soundMove.Play();
			}
			else if( GAMESTATE->m_pCurMission->CanGoDownRightHidden() )
			{
				GAMESTATE->m_pCurMission.Set( GAMESTATE->m_pCurMission->GetLinkDownRightHidden() );
				m_bSelected = false;
				m_soundMove.Play();
			}
			else
				m_soundNotMove.Play();
		}
	}
	/*else if( g_SelectState == SelectState_EnteringCode )
	{
		if( GAMESTATE->m_pCurMission->m_iNumCodes > 0 )
			GAMESTATE->m_pCurMission->m_iNumCodes--;

		//XXX:
		if( input.GameI.button == PUMP_BUTTON_UPLEFT )
		{
			if( m_pProfile->m_iTotalItems > 0 )
				m_pProfile->m_iTotalItems--;
			MESSAGEMAN->Broadcast( "UpLeftItemUsed" );
		}
		else if( input.GameI.button == PUMP_BUTTON_UPRIGHT )
		{
			if( m_pProfile->m_iTotalItems > 0 )
				m_pProfile->m_iTotalItems--;
			MESSAGEMAN->Broadcast( "UpRightItemUsed" );
		}
		else if( input.GameI.button == PUMP_BUTTON_DOWNLEFT )
		{
			if( m_pProfile->m_iTotalItems > 0 )
				m_pProfile->m_iTotalItems--;		
			MESSAGEMAN->Broadcast( "DownLeftItemUsed" );
		}
		else if( input.GameI.button == PUMP_BUTTON_DOWNRIGHT )
		{
			if( m_pProfile->m_iTotalItems > 0 )
				m_pProfile->m_iTotalItems--;
			MESSAGEMAN->Broadcast( "DownRightItemUsed" );
		}
		
		if( input.GameI.button == PUMP_BUTTON_CENTER )
		{
			MESSAGEMAN->Broadcast( "CenterItemUsed" );

			if( m_pProfile->m_iTotalItems > 0 )
				m_pProfile->m_iTotalItems--;
		}

		MESSAGEMAN->Broadcast( "ItemUsed" );

		if( GAMESTATE->m_pCurMission->m_iNumCodes <= 0 )
		{
			//g_SelectState = SelectState_Missions;
			MESSAGEMAN->Broadcast( "FinishedCode" );
			GAMESTATE->m_pCurMission->m_iNumCodes = GAMESTATE->m_pCurMission->m_MissionGoals.iNumCodes;
			this->PostScreenMessage( SM_CodeEnteringFinished, 0 );
		}
	}*/

	return ScreenWithMenuElements::Input( input );
}

void ScreenWorldMax::HandleScreenMessage( const ScreenMessage SM )
{
	if( SM == SM_BeginFadingOut )
	{
		this->PostScreenMessage( SM_GoToNextScreen, 0 );
	}
	else if( SM == SM_GoToNextScreen )
	{
		PlayerOptions po = GAMESTATE->m_pPlayerState[m_pn]->m_PlayerOptions.GetStage();
		SongOptions so = GAMESTATE->m_SongOptions.GetStage();
		so.FromString( GAMESTATE->m_pCurMission->m_MissionGoals.sModifiers );

		po.FromString( GAMESTATE->m_pCurMission->m_MissionGoals.sModifiers );
		GAMESTATE->m_pPlayerState[m_pn]->m_PlayerOptions.Assign( ModsLevel_Preferred, po );

		GAMESTATE->m_SongOptions.Assign( ModsLevel_Preferred, so );

		ASSERT( GAMESTATE->m_pCurSong );
	}
	else if( SM == SM_MenuTimer )
	{
		m_bOutOfTime = true;//Handle in update
		return;
	}
	else if( SM == SM_BombActivated )
	{
		if( m_bUsingShield )
		{
			m_bTransitioningInWarp = false;
			return;
		}

		if( GAMESTATE->m_pCurMission->GetMissionToThrowWhenFindMine() )
		{
			GAMESTATE->m_pCurMission.Set( GAMESTATE->m_pCurMission->GetMissionToThrowWhenFindMine() );
			m_bContinueMoving = false;
			m_bTransitioningInWarp = false;
			SetSectorAndMap();

			float fx, fy;
			m_mapSectors[m_iSectorIndex]->GetPosition( GAMESTATE->m_pCurMission, fx, fy );
			m_mapSectors[m_iSectorIndex]->SetXY( fx, fy );

			AfterMissionChange();
			return;
		} /* dejar seguir eligiendo o elegir automaticamente? */

		this->PostScreenMessage( SM_BeginFadingOut, 0 );
	}
	else if( SM == SM_WarpActivated )
	{
		{
			Message msg( "SelectStateChanged" );
			msg.SetParam( "To", RString( "GeneralMap" ) );
			msg.SetParam( "From", GAMESTATE->m_pCurMission->m_MissionGoals.sLand );
			MESSAGEMAN->Broadcast( msg );
		}
		g_SelectState = SelectState_Sectors;
		m_mapSectors[m_iSectorIndex]->SetVisible( false );
		m_mapSectorSelect.SetVisible( true );
		GAMESTATE->m_pCurMission.Set( GAMESTATE->m_pCurMission->GetLinkToSectors() );
		//restauramos la animacion y el movimiento
		m_bTransitioningInWarp = false;
		m_sprWarps.SetDiffuseAlpha( 0 );
	}
	else if( SM == SM_WarpToWarpActivated )
	{
		GAMESTATE->m_pCurMission.Set( GAMESTATE->m_pCurMission->GetLinkWarpToWarp() );
		SetSectorAndMap();
		float fx, fy;
		m_mapSectors[m_iSectorIndex]->GetPosition( GAMESTATE->m_pCurMission, fx, fy );
		m_mapSectors[m_iSectorIndex]->SetXY( fx, fy );
		m_bSelected = false;
		m_mapSectors[m_iSectorIndex]->SetSelected( m_bSelected );
		//restauramos la animacion y el movimiento
		m_bTransitioningInWarp = false;
		m_sprWarps.SetDiffuseAlpha( 0 );

		m_mapSectors[m_iSectorIndex]->GetPosition( GAMESTATE->m_pCurMission, fx, fy );
		Message msg( "MapMoved" );
		msg.SetParam( "PosX", fx );
		msg.SetParam( "PosY", fy );
		RString s = "ScreenWorldMaxWarpToWarpActivated";
		msg.SetParam( "From", s );
		MESSAGEMAN->Broadcast( msg );
	}
	else if( SM == SM_SwitchStateChanged )
	{
		m_bTransitioningInWarp = false;
	}
	else if( SM == SM_PathFailed )
	{
		m_bTransitioningInWarp = false;

		GAMESTATE->m_pCurMission.Set( GAMESTATE->m_pCurMission->GetMissionReference() );

		float fx, fy;
		m_mapSectors[m_iSectorIndex]->GetPosition( GAMESTATE->m_pCurMission, fx, fy );
		m_mapSectors[m_iSectorIndex]->SetXY( fx, fy );

		m_bSelected = false;
		m_mapSectors[m_iSectorIndex]->SetSelected( m_bSelected );

		AfterMissionChange();
	}
	else if( SM == SM_LandChanged )//cambio de musica
	{
		FOREACHM( RString, RageSound, m_mapSounds, map )
		{
			//LOG->Trace( ssprintf( "Sectors posibles to Play: %s", map->first.c_str() ) );
			map->second.Stop();
		}
		RageSoundParams params;
		params.StopMode = RageSoundParams::M_LOOP;
		//m_mapSounds[m_sLastSeenLand]->Play( &params );
		//LOG->Trace( ssprintf( "Trying Play: %s", m_sLastSeenLand.c_str() ) );
		std::map<RString,RageSound>::iterator iter = m_mapSounds.find( m_sLastSeenLand );
		if( iter != m_mapSounds.end() )
		{
			//LOG->Trace( "Can play bgmusic: %s", iter->first.c_str() );
			iter->second.Play( &params );
		}
	}
	else if( SM == SM_CodeEnteringFinished )
	{
		g_SelectState = SelectState_Missions;

		if( GAMESTATE->m_pCurMission->m_iNumCodes > 0 )
			MESSAGEMAN->Broadcast( "CodeEnteringFailed" );
	}
	else if( SM == SM_GainFocus )
	{
		//En este punto ya debe haber una mision lista.
		float fx,fy;
		m_mapSectors[m_iSectorIndex]->GetPosition( GAMESTATE->m_pCurMission, fx, fy );
		Message msg( "MapMoved" );
		msg.SetParam( "PosX", fx );
		msg.SetParam( "PosY", fy );
		RString s = "ScreenWorldMaxInit";
		msg.SetParam( "From", s );
		MESSAGEMAN->Broadcast( msg );
	}

	ScreenWithMenuElements::HandleScreenMessage( SM );
}

void ScreenWorldMax::CheckRandomSong()
{
	vector<RString> vsRandom;
	split( GAMESTATE->m_pCurMission->m_MissionGoals.sSongPathToLoad, ",", vsRandom );

	if( vsRandom[0].CompareNoCase( "Random" )==0 )
	{
		Song* pSong = GAMESTATE->m_pCurMission->m_pSong;
		
		CHECKPOINT;
		//encuentra el modo
		Steps* pSteps = NULL;
		const Style *pStyle = NULL;

		CHECKPOINT;
		if( vsRandom[1].CompareNoCase( "Easy" )==0 )
		{
			pSteps = SongUtil::GetStepsByDifficulty( pSong, StepsType_pump_single, Difficulty_Easy, false );
			pStyle = GAMEMAN->GetFirstCompatibleStyle( GAMESTATE->m_pCurGame, GAMESTATE->GetNumSidesJoined(), StepsType_pump_single );
			/*if ( pSteps == NULL )
				continue;*/
		}
		else if( vsRandom[1].CompareNoCase( "Hard" )==0 )
		{
			pSteps = SongUtil::GetStepsByDifficulty( pSong, StepsType_pump_single, Difficulty_Medium, false );
			pStyle = GAMEMAN->GetFirstCompatibleStyle( GAMESTATE->m_pCurGame, GAMESTATE->GetNumSidesJoined(), StepsType_pump_single);
			/*if ( pSteps == NULL )
				continue;*/
		}
		else if( vsRandom[1].CompareNoCase( "Crazy" )==0 )
		{
			pSteps = SongUtil::GetStepsByDifficulty( pSong, StepsType_pump_single, Difficulty_Hard, false );
			pStyle = GAMEMAN->GetFirstCompatibleStyle( GAMESTATE->m_pCurGame, GAMESTATE->GetNumSidesJoined(), StepsType_pump_single);
			/*if ( pSteps == NULL )
				continue;*/
		}
		else if( vsRandom[1].CompareNoCase( "Freestyle" )==0 )
		{
			pSteps = SongUtil::GetStepsByDifficulty( pSong, StepsType_pump_double, Difficulty_Medium, false );
			pStyle = GAMEMAN->GetFirstCompatibleStyle( GAMESTATE->m_pCurGame, GAMESTATE->GetNumSidesJoined(), StepsType_pump_double);
			/*if ( pSteps == NULL )
				continue;*/
		}
		else if( vsRandom[1].CompareNoCase( "Nightmare" )==0 )
		{
			pSteps = SongUtil::GetStepsByDifficulty( pSong, StepsType_pump_double, Difficulty_Hard, false );
			pStyle = GAMEMAN->GetFirstCompatibleStyle( GAMESTATE->m_pCurGame, GAMESTATE->GetNumSidesJoined(), StepsType_pump_double);
			/*if ( pSteps == NULL )
				continue;*/
		}
		else
			ASSERT_M( 0, ssprintf( "El argumento #SONGPATH: para random, tiene mal formato en: %s", GAMESTATE->m_pCurMission->GetMissionName().c_str() ) );

		//CHECKPOINT_M( ssprintf( "%s : %s";
		//SCREENMAN->SystemMessageNoAnimate( ssprintf( "%d, %d, %s", pSteps->GetDifficulty(), pSteps->m_StepsType, pStyle->m_szName ) );
		GAMESTATE->m_pCurSteps[m_pn].Set( pSteps );

		GAMESTATE->SetCurrentStyle( pStyle, GAMESTATE->GetFirstHumanPlayer() );

		ASSERT_M( pSong,  "Song in Random (Song) mission failed" );
		ASSERT_M( pSteps, "Steps in Random (Song) mission failed" );
		ASSERT_M( pStyle, "Style in Random (Song) mission failed" );
	}
}

void ScreenWorldMax::HandleMessage( const Message &msg )
{
	if( msg.GetName().CompareNoCase("Code")==0 )
	{
		RString sName;

		if( msg.GetParam( "Name", sName ) )
		{
			FOREACHS_CONST( RString, m_pProfile->m_vsCodesAplieds, ca )
			{
				RString mission = ca->c_str();

				if( !stricmp( sName, mission ) )//ponemos esto despues por que es probable que la hayamos
					return;//desbloqueado
			}

			m_pProfile->m_vsCodesAplieds.insert( sName );
		}

		{
			Mission* ms = SONGMAN->FindMission( "", sName );

			//si la mision no existe, kill
			ASSERT_M( ms, ssprintf("Mission para el code %s no existe", sName.c_str() ) );

			if( ms )
			{
				ms->UnLock();
				ms->m_iNumCodes = 0;
				GAMESTATE->m_pCurMission->m_MissionGoals.iNumCodes = 0;
				Message msg( "WMCodeEntered" );
				msg.SetParam( "Name", sName );

				ActorFrame::HandleMessage( msg );//explicitamente

				MESSAGEMAN->Broadcast( msg );
				MESSAGEMAN->Broadcast( "CodeEnteringDone" );				
			}
		}
	}

	ScreenWithMenuElements::HandleMessage( msg );
}

// lua start
#include "LuaBinding.h"

class LunaScreenWorldMax: public Luna<ScreenWorldMax>
{
public:
	static int IsSelectingSectors( T* p, lua_State *L ) { lua_pushboolean( L, (g_SelectState == SelectState_Sectors) ); return 1; }
	static int GetNumSwitchActives( T* p, lua_State *L )
	{
		PlayerNumber pn = GAMESTATE->GetFirstHumanPlayer();
		Profile* pProfile = PROFILEMAN->IsPersistentProfile( pn ) ? PROFILEMAN->GetProfile( pn ) : PROFILEMAN->GetMachineProfile();

		lua_pushnumber( L, pProfile->m_vsSwitchActives.size() );

		return 1;
	}
	static int GetSwitchActive( T* p, lua_State *L )
	{
		PlayerNumber pn = GAMESTATE->GetFirstHumanPlayer();
		Profile* pProfile = PROFILEMAN->IsPersistentProfile( pn ) ? PROFILEMAN->GetProfile( pn ) : PROFILEMAN->GetMachineProfile();
		RString sSwitchName = SArg( 1 );

		FOREACHS_CONST( RString, pProfile->m_vsSwitchActives, sa )
		{
			RString mission = sa->c_str();

			if( !stricmp( sSwitchName, mission ) )//el switch está activo
			{
				lua_pushboolean( L, true );
				return 1;
			}
		}

		lua_pushboolean( L, false );

		return 1;
	}
	static int GetCurrentMapIndex( T* p, lua_State *L )
	{
		lua_pushnumber( L, p->GetMapIndex() );
		return 1;
	}
	static int GetTotalSectors( T* p, lua_State *L )
	{
		lua_pushnumber( L, p->GetTotalSectors() );
		return 1;
	}
	static int GetCurrentSector( T* p, lua_State *L )
	{
		if( !GAMESTATE->m_pCurMission )
			ASSERT_M( 0, "Mission no es seteada aún" );

		lua_pushstring( L, GAMESTATE->m_pCurMission->GetMissionSector() );

		return 1;
	}

	LunaScreenWorldMax()
	{
  		ADD_METHOD( IsSelectingSectors );
		ADD_METHOD( GetNumSwitchActives );
		ADD_METHOD( GetCurrentMapIndex );
		ADD_METHOD( GetTotalSectors );		
		ADD_METHOD( GetCurrentSector );
		ADD_METHOD( GetSwitchActive );
	}
};

LUA_REGISTER_DERIVED_CLASS( ScreenWorldMax, ScreenWithMenuElements )

/*
 * (c) 2008 Victor Gajardo Henriquez "v1toko"
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
