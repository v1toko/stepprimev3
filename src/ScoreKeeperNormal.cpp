#include "global.h"
#include "ScoreKeeperNormal.h"
#include "GameState.h"
#include "PrefsManager.h"
#include "GamePreferences.h"
#include "Steps.h"
#include "ScreenManager.h"
#include "GameState.h"
#include "Course.h"
#include "SongManager.h"
#include "NoteDataUtil.h"
#include "NoteData.h"
#include "RageLog.h"
#include "StageStats.h"
#include "ProfileManager.h"
#include "NetworkSyncManager.h"
#include "PlayerState.h"
#include "Game.h"
#include "Style.h"
#include "Song.h"
#include "TimingData.h"
#include "NoteDataWithScoring.h"
#include "Preference.h"

static RString PercentScoreWeightName( size_t i ) { return "PercentScoreWeight" + ScoreEventToString( (ScoreEvent)i ); }
static RString GradeWeightName( size_t i ) { return "GradeWeight" + ScoreEventToString( (ScoreEvent)i ); }

static ThemeMetric1D<int> g_iPercentScoreWeight("ScoreKeeperNormal", PercentScoreWeightName, NUM_ScoreEvent );
static ThemeMetric1D<int> g_iGradeWeight("ScoreKeeperNormal", GradeWeightName, NUM_ScoreEvent );

ScoreKeeperNormal::ScoreKeeperNormal( PlayerState *pPlayerState, PlayerStageStats *pPlayerStageStats ):
	ScoreKeeper(pPlayerState, pPlayerStageStats)
{
}

void ScoreKeeperNormal::Load(
		const vector<Song*>& apSongs,
		const vector<Steps*>& apSteps,
		const vector<AttackArray> &asModifiers )
{
	m_apSteps = apSteps;
	ASSERT( apSongs.size() == apSteps.size() );
	ASSERT( apSongs.size() == asModifiers.size() );

	// True if a jump is one to combo, false if combo is purely based on tap count.
	m_ComboIsPerRow.Load( "Gameplay", "ComboIsPerRow" );
	m_MissComboIsPerRow.Load( "Gameplay", "MissComboIsPerRow" );
	m_MinScoreToContinueCombo.Load( "Gameplay", "MinScoreToContinueCombo" );
	m_MinScoreToMaintainCombo.Load( "Gameplay", "MinScoreToMaintainCombo" );
	m_MaxScoreToIncrementMissCombo.Load( "Gameplay", "MaxScoreToIncrementMissCombo" );
	m_MineHitIncrementsMissCombo.Load( "Gameplay", "MineHitIncrementsMissCombo" );
	m_AvoidMineIncrementsCombo.Load( "Gameplay", "AvoidMineIncrementsCombo" );
	m_UseInternalScoring.Load( "Gameplay", "UseInternalScoring" );

	// Toasty triggers (idea from 3.9+)
	// Multiple toasty support doesn't seem to be working right now.
	// Since it's causing more problems than solutions, I'm going back to
	// the old way of a single toasty trigger for now.
	//m_vToastyTriggers.Load( "Gameplay", "ToastyTriggersAt" );
	m_ToastyTrigger.Load( "Gameplay", "ToastyTriggersAt" );

	// Fill in STATSMAN->m_CurStageStats, calculate multiplier
	int iTotalPossibleDancePoints = 0;
	int iTotalPossibleGradePoints = 0;
	for( unsigned i=0; i<apSteps.size(); i++ )
	{
		Song* pSong = apSongs[i];
		ASSERT( pSong != NULL );
		Steps* pSteps = apSteps[i];
		ASSERT( pSteps != NULL );
		const AttackArray &aa = asModifiers[i];
		NoteData ndTemp;
		pSteps->GetNoteData( ndTemp );

		// We might have been given lots of songs; don't keep them in memory uncompressed.
		pSteps->Compress();

		const Style* pStyle = GAMESTATE->GetCurrentStyle(m_pPlayerState->m_PlayerNumber);
		NoteData ndPre;
		pStyle->GetTransformedNoteDataForStyle( m_pPlayerState->m_PlayerNumber, ndTemp, ndPre );

		/* Compute RadarValues before applying any user-selected mods. Apply
		 * Course mods and count them in the "pre" RadarValues because they're
		 * forced and not chosen by the user. */
		NoteDataUtil::TransformNoteData( ndPre, aa, pSteps->m_StepsType, pSong );

		/* Apply user transforms to find out how the notes will really look.
		 *
		 * XXX: This is brittle: if we end up combining mods for a song differently
		 * than ScreenGameplay, we'll end up with the wrong data.  We should probably
		 * have eg. GAMESTATE->GetOptionsForCourse(po,so,pn) to get options based on
		 * the last call to StoreSelectedOptions and the modifiers list, but that'd
		 * mean moving the queues in ScreenGameplay to GameState ... */
		NoteData ndPost = ndPre;
		NoteDataUtil::TransformNoteData( ndPost, m_pPlayerState->m_PlayerOptions.GetStage(), pSteps->m_StepsType );

		GAMESTATE->SetProcessedTimingData(pSteps->GetTimingData()); // XXX: Not sure why but NoteDataUtil::CalculateRadarValues segfaults without this
		iTotalPossibleDancePoints += this->GetPossibleDancePoints( &ndPre, &ndPost, pSteps->GetTimingData(), pSong->m_fMusicLengthSeconds );
		iTotalPossibleGradePoints += this->GetPossibleGradePoints( &ndPre, &ndPost, pSteps->GetTimingData(), pSong->m_fMusicLengthSeconds );
		GAMESTATE->SetProcessedTimingData(NULL);
	}

	m_pPlayerStageStats->m_iPossibleDancePoints = iTotalPossibleDancePoints;
	m_pPlayerStageStats->m_iPossibleGradePoints = iTotalPossibleGradePoints;

	m_iScoreRemainder = 0;
	m_iCurToastyCombo = 0;
	//m_iCurToastyTrigger = 0;
	//m_iNextToastyAt = 0;
	m_iMaxScoreSoFar = 0;
	m_iPointBonus = 0;
	m_iNumTapsAndHolds = 0;
	m_iNumNotesHitThisRow = 0;
	m_iNumNotesMissThisRow = 0;
	m_bIsLastSongInCourse = false;

	Message msg( "ScoreChanged" );
	msg.SetParam( "PlayerNumber", m_pPlayerState->m_PlayerNumber );
	msg.SetParam( "MultiPlayer", m_pPlayerState->m_mp );
	MESSAGEMAN->Broadcast( msg );

	memset( m_ComboBonusFactor, 0, sizeof(m_ComboBonusFactor) );
	m_iRoundTo = 1;
}

void ScoreKeeperNormal::OnNextSong( int iSongInCourseIndex, const Steps* pSteps, const NoteData* pNoteData )
{
/*
  Note on NONSTOP Mode scoring

  Nonstop mode requires the player to play 4 songs in succession, with the total maximum possible score for
  the four song set being 100,000,000. This comes from the sum of the four stages' maximum possible scores,
  which, regardless of song or difficulty is:

  10,000,000 for the first song
  20,000,000 for the second song
  30,000,000 for the third song
  40,000,000 for the fourth song

  We extend this to work with nonstop courses of any length.

  We also keep track of this scoring type in endless, with 100mil per iteration
  of all songs, though this score isn't actually seen anywhere right now.
*/
	// Calculate the score multiplier
	m_iMaxPossiblePoints = 0;
	if( GAMESTATE->IsCourseMode() )
	{
		const int numSongsInCourse = m_apSteps.size();
		ASSERT( numSongsInCourse != 0 );

		const int iIndex = iSongInCourseIndex % numSongsInCourse;
		m_bIsLastSongInCourse = (iIndex+1 == numSongsInCourse);

		if( numSongsInCourse < 10 )
		{
			const int courseMult = (numSongsInCourse * (numSongsInCourse + 1)) / 2;
			ASSERT(courseMult >= 0);

			m_iMaxPossiblePoints = (100000000 * (iIndex+1)) / courseMult;
		}
		else
		{
			/* When we have lots of songs, the scale above biases too much: in a
			 * course with 50 songs, the first song is worth 80k, the last 4mil, which
			 * is too much of a difference.
			 *
			 * With this, each song in a 50-song course will be worth 2mil. */
			m_iMaxPossiblePoints = 100000000 / numSongsInCourse;
		}
	}
	else
	{
		// long ver and marathon ver songs have higher max possible scores
		int iLengthMultiplier = GameState::GetNumStagesMultiplierForSong( GAMESTATE->m_pCurSong );
		
		/* This is no longer just simple additive/subtractive scoring,
		 * but start with capping the score at the size of the score counter. */
		m_iMaxPossiblePoints = 10 * 10000000 * iLengthMultiplier;
	}
	ASSERT( m_iMaxPossiblePoints >= 0 );
	m_iMaxScoreSoFar += m_iMaxPossiblePoints;

	GAMESTATE->SetProcessedTimingData(const_cast<TimingData *>(pSteps->GetTimingData()));
	
	m_iNumTapsAndHolds = pNoteData->GetNumRowsWithTapOrHoldHead() + pNoteData->GetNumHoldNotes()
		+ pNoteData->GetNumRolls();

	m_iPointBonus = m_iMaxPossiblePoints;
	m_pPlayerStageStats->m_iMaxScore = m_iMaxScoreSoFar;

	/* MercifulBeginner shouldn't clamp weights in course mode, even if a beginner
	 * song is in a course, since that makes PlayerStageStats::GetGrade hard. */
	m_bIsBeginner = pSteps->GetDifficulty() == Difficulty_Beginner && !GAMESTATE->IsCourseMode();

	ASSERT( m_iPointBonus >= 0 );

	m_iTapNotesHit = 0;
	
	GAMESTATE->SetProcessedTimingData(NULL);
}

static int GetScore(int p, int Z, int64_t S, int n)
{
	/* There's a problem with the scoring system described below. Z/S is truncated
	 * to an int. However, in some cases we can end up with very small base scores.
	 * Each song in a 50-song nonstop course will be worth 2mil, which is a base of
	 * 200k; Z/S will end up being zero.
	 *
	 * If we rearrange the equation to (p*Z*n) / S, this problem goes away.
	 * (To do that, we need to either use 64-bit ints or rearrange it a little
	 * more and use floats, since p*Z*n won't fit a 32-bit int.)  However, this
	 * changes the scoring rules slightly.
	 */

#if 0
	// This is the actual method described below.
	return p * (Z / S) * n;
#elif 1
	// This doesn't round down Z/S.
	return int(int64_t(p) * n * Z / S);
#else
	// This also doesn't round down Z/S. Use this if you don't have 64-bit ints.
	return int(p * n * (float(Z) / S));
#endif

}

void ScoreKeeperNormal::AddTapScore( TapNoteScore tns )
{
}

void ScoreKeeperNormal::AddHoldScore( HoldNoteScore hns )
{
	//if( hns == HNS_Held )
		//AddScoreInternal( TNS_W1 );
	//else if ( hns == HNS_LetGo )
		//AddScoreInternal( TNS_W4 ); // required for subtractive score display to work properly.
}

void ScoreKeeperNormal::AddTapRowScore( TapNoteScore score, const NoteData &nd, int iRow )
{
	AddScoreInternal( score );
}

extern ThemeMetric<bool> PENALIZE_TAP_SCORE_NONE;
void ScoreKeeperNormal::HandleTapScoreNone()
{
	if( PENALIZE_TAP_SCORE_NONE )
	{
		m_pPlayerStageStats->m_iCurCombo = 0;

		if( m_pPlayerState->m_PlayerNumber != PLAYER_INVALID )
			MESSAGEMAN->Broadcast( enum_add2(Message_CurrentComboChangedP1,m_pPlayerState->m_PlayerNumber) );
	}

	// TODO: networking code
}

void ScoreKeeperNormal::AddScoreInternal( TapNoteScore score )
{
	if( m_UseInternalScoring )
	{
		int iScore = m_pPlayerStageStats->m_iScore;
		int iCombo = m_pPlayerStageStats->m_iCurCombo;

		float &fExperience = m_pPlayerStageStats->m_fExperience;
		float &fPiuPoints = m_pPlayerStageStats->m_fPiuPoints;

		float p = 0;	// score multiplier 
		float _exp = 0; //experience
		float _pp = 0; //piu points

		/*
			Reference: http://www.ajworld.net/piuscore.html
			Fiesta EX / Fiesta

			O score total Ã© a soma do score de cada passo, mais bÃ´nus ao final da mÃºsica por tirar S (ou SS).

			Score de cada passo: (score base + bÃ´nus de combo + bÃ´nus de seta mÃºltipla) x multiplicador de nÃ­vel x multiplicador de Double

			Score base:

			Perfect: 1000 pontos;
			Great: 500 pontos;
			Good: 100 pontos;
			Bad: -200 pontos;
			Miss: -500 pontos;
			Miss em long note: -300 pontos.

			BÃ´nus de combo:
			Para cada passo com julgamento Perfect ou Great Ã© adicionado um bÃ´nus, dependendo do combo alcanÃ§ado naquele passo:
			AtÃ© 50 de combo: +0;
			A partir de 51 de combo: +1000 pontos.
			Note que o Good nÃ£o recebe esse bÃ´nus.

			BÃ´nus de seta mÃºltipla:
			Para cada passo com julgamento Perfect ou Great que tenha 3 ou mais setas (triplo, quÃ¡druplo etc.), Ã© adicionado um bÃ´nus de (quantidade de setas â€“ 2) * 1000

			Ou seja, para o triplo hÃ¡ um bÃ´nus de 1000, quÃ¡druplo tem bÃ´nus de 2000, quÃ­ntuplo tem bÃ´nus de 3000 etc.

			Multiplicador de nÃ­vel
			Caso o nÃ­vel da mÃºsica seja maior do que 10, a pontuaÃ§Ã£o do passo Ã© multiplicada por (nÃ­vel / 10).
			Ou seja, em uma mÃºsica nÃ­vel 23, o score final de cada passo Ã© multiplicado por 2,3 (inclusive para goods, bads e misses).

			Multiplicador de Double
			Caso a mÃºsica seja um Double, a pontuaÃ§Ã£o de cada passo Ã© multiplicada por 1,2, inclusive para goods, bads e misses.

			BÃ´nus de S e SS
			Ao final da mÃºsica, Ã© adicionado um bÃ´nus Ã  pontuaÃ§Ã£o total da mÃºsica caso o jogador tire S ou SS:
			S prateado: +100000 pontos;
			S dourado: +150000 pontos;
			SS (full perfect): +300000 pontos.
			ObservaÃ§Ãµes:
			A pontuaÃ§Ã£o nÃ£o pode cair abaixo de 0 -- ou seja, caso a pessoa dÃª um miss no primeiro passo da mÃºsica, por exemplo, ela fica com 0 de pontuaÃ§Ã£o apÃ³s aquele passo, e nÃ£o com -500.
			Caso a pontuaÃ§Ã£o final da mÃºsica nÃ£o dÃª um mÃºltiplo de 100, arredondar para baixo atÃ© o mÃºltiplo de 100 mais prÃ³ximo.
		*/

		switch( score )
		{
		case TNS_W1:	
			p = +1000;	
			_exp = 0.04f;		
			_pp = 0.09f;
			break;
		case TNS_W2:	
			p = +1000;			
			_exp = 0.04f;		
			_pp = 0.09f;
			break;
		case TNS_W3:	
			p = +500;	
			_exp = 0.03f;		
			_pp = 0.08f;		
			break;
		case TNS_W4:	
			p = +100;	
			_exp = 0.02f;		
			_pp = 0.07f;		
			break;
		case TNS_W5:	
			p = -200;			
			_exp = 0.01f;		
			_pp = 0.06f;
			break;
		case TNS_Miss:	
			p = -500;
			_exp = 0;		
			_pp = 0;
			break;
		case TNS_CheckpointHit:	
			p = +1000;
			_exp = 0;		
			_pp = 0;
			break;
		case TNS_CheckpointMiss:
			p = -300;	
			_exp = 0;		
			_pp = 0;
			break;
		default:		
			p = 0;	
			_exp = 0;		
			_pp = 0;			
			break;
		}

		m_iTapNotesHit++;

		if( score == TNS_W1 || score == TNS_W2 || score == TNS_W3 || score == TNS_CheckpointHit )
		{
			if( iCombo > 50 ) 
			{
				p += 1000.f;
				_exp += 0.04f;
				_pp += 0.02f;
			}

			if( m_iNumNotesHitThisRow >= 3 )
			{
				p += (((float)m_iNumNotesHitThisRow-2.f)*1000.f);
				_exp += 0.01f * m_iNumNotesHitThisRow;
				_pp += 0.04f * m_iNumNotesHitThisRow;
			}
		}
		int iMeter = GAMESTATE->m_pCurSteps[m_pPlayerState->m_PlayerNumber]->GetMeter();
		if( iMeter > 10 )
		{
			p *= ( (float)iMeter / 10.f );
			_exp += 0.05f * iMeter;
			_pp += 0.01f * iMeter;
		}

		if( GAMESTATE->m_pCurSteps[m_pPlayerState->m_PlayerNumber]->m_StepsType == StepsType_pump_double ) 
		{
			p *= 1.2f;
			_exp += 0.05f * 1.2f;
			_pp += 0.01f * 1.2f;
		}

		//feo pero funcional
		IPreference *pr = IPreference::GetPreferenceByName("TimingWindowScale");
		Preference<float> *pre = (Preference<float>*)pr;
		float fWindowScale = pre->Get();
		
		if( GAMESTATE->IsRankMode() )	// vj modifier
		{
			p *= 1.2556f;
		}
		else if( fWindowScale <= 0.875f ) //hj igual nos da un poco más de puntaje
		{
			p *= 1.1556f;
		}

		iScore += p;		
		fExperience += _exp;
		fPiuPoints += _pp;

		if (iScore < 0)
			iScore = 0;
		
		ASSERT( iScore >= 0 );

		m_pPlayerStageStats->m_iScore = iScore;
	}
}

void ScoreKeeperNormal::HandleTapScore( const TapNote &tn )
{
	TapNoteScore tns = tn.result.tns;

	if( tn.type == TapNoteType_Mine )
	{
		if( tns == TNS_HitMine )
		{
			if( !m_pPlayerStageStats->m_bFailed )
				m_pPlayerStageStats->m_iActualDancePoints += TapNoteScoreToDancePoints( TNS_HitMine );
			m_pPlayerStageStats->m_iTapNoteScores[TNS_HitMine] += 1;
			if( m_MineHitIncrementsMissCombo )
				HandleComboInternal( 0, 0, 1 );
			
		}
		
		if( tns == TNS_AvoidMine && m_AvoidMineIncrementsCombo )
			HandleComboInternal( 1, 0, 0 );

		/*NSMAN->ReportScore(
			m_pPlayerState->m_PlayerNumber,
			tns,
			m_pPlayerStageStats->m_iScore,
			m_pPlayerStageStats->m_iCurCombo,
			tn.result.fTapNoteOffset
		);*/
		Message msg( "ScoreChanged" );
		msg.SetParam( "PlayerNumber", m_pPlayerState->m_PlayerNumber );
		msg.SetParam( "MultiPlayer", m_pPlayerState->m_mp );
		MESSAGEMAN->Broadcast( msg );
	}

	AddTapScore( tns );
}

void ScoreKeeperNormal::HandleHoldCheckpointScore( const NoteData &nd, int iRow, int iNumHoldsHeldThisRow, int iNumHoldsMissedThisRow )
{
	HandleTapNoteScoreInternal(iNumHoldsMissedThisRow == 0 ? TNS_CheckpointHit:TNS_CheckpointMiss, TNS_CheckpointHit, iRow);
	HandleComboInternal( iNumHoldsHeldThisRow, 0, iNumHoldsMissedThisRow, iRow );
	AddScoreInternal(iNumHoldsMissedThisRow == 0 ? TNS_CheckpointHit:TNS_CheckpointMiss);
}

void ScoreKeeperNormal::HandleTapNoteScoreInternal( TapNoteScore tns, TapNoteScore maximum, int row )
{
	// Update dance points.
	if( !m_pPlayerStageStats->m_bFailed )
		m_pPlayerStageStats->m_iActualDancePoints += TapNoteScoreToDancePoints( tns );

	// update judged row totals. Respect Combo segments here.
	TimingData &td = *GAMESTATE->m_pCurSteps[m_pPlayerState->m_PlayerNumber]->GetTimingData();
	ComboSegment *cs = td.GetComboSegmentAtRow(row);

	if (tns == TNS_CheckpointHit || tns >= m_MinScoreToContinueCombo)
	{
		m_pPlayerStageStats->m_iTapNoteScores[tns] += 1 * cs->GetCombo();
	}
	else if (tns == TNS_CheckpointMiss || tns < m_MinScoreToMaintainCombo)
	{
		m_pPlayerStageStats->m_iTapNoteScores[tns] += 1 * cs->GetMissCombo();
	}
	else
	{	
		m_pPlayerStageStats->m_iTapNoteScores[tns] += 1;
	}

	// increment the current total possible dance score
	m_pPlayerStageStats->m_iCurPossibleDancePoints += TapNoteScoreToDancePoints( maximum );
}

void ScoreKeeperNormal::HandleComboInternal( int iNumHitContinueCombo, int iNumHitMaintainCombo, int iNumBreakCombo, int iRow )
{
	// Regular combo
	TimingData &td = *GAMESTATE->m_pCurSteps[m_pPlayerState->m_PlayerNumber]->GetTimingData();
	bool bCountSep = td.GetCountSeparateAtRow(iRow) > 0;	
	if( /*m_ComboIsPerRow ||*/ !bCountSep )
	{
		iNumHitContinueCombo = min( iNumHitContinueCombo, 1 );
		iNumHitMaintainCombo = min( iNumHitMaintainCombo, 1 );
		iNumBreakCombo = min( iNumBreakCombo, 1 );
	}

	/*if( (iNumHitContinueCombo > 0 || iNumHitMaintainCombo > 0) && iNumBreakCombo == 0 )
	{
		m_pPlayerStageStats->m_iCurMissCombo = 0;
	}*/

		//si sacamos bad o good o lo que sea dejamos el misscombo = 0
	if( iNumHitContinueCombo > 0 || iNumHitMaintainCombo > 0 )
	{
		m_pPlayerStageStats->m_iCurMissCombo = 0;
	}

	if( iNumHitContinueCombo > 0 && iNumBreakCombo > 0 )
	{
		m_pPlayerStageStats->m_iCurCombo = 0;
		m_pPlayerStageStats->m_iCurMissCombo = 0;
	}

	if( iNumBreakCombo == 0 )
	{
		int multiplier = ( iRow == -1 ? 1 : td.GetComboSegmentAtRow( iRow )->GetCombo() );
		m_pPlayerStageStats->m_iCurCombo += iNumHitContinueCombo * multiplier;
	}
	else
	{
		m_pPlayerStageStats->m_iCurCombo = 0;
		int multiplier = ( iRow == -1 ? ( bCountSep ? iNumBreakCombo : 1 ) : td.GetComboSegmentAtRow(iRow)->GetMissCombo());
		m_pPlayerStageStats->m_iCurMissCombo += ( !bCountSep ? 1 : iNumBreakCombo ) * multiplier;

		if( m_pPlayerStageStats->m_iCurMissCombo > m_pPlayerStageStats->m_iMaxMissCombo )
			m_pPlayerStageStats->m_iMaxMissCombo = m_pPlayerStageStats->m_iCurMissCombo;
	}
}

void ScoreKeeperNormal::HandleRowComboInternal( TapNoteScore tns, int iNumTapsInRow, int iRow )
{
	TimingData &td = *GAMESTATE->m_pCurSteps[m_pPlayerState->m_PlayerNumber]->GetTimingData();
	bool bCountSep = td.GetCountSeparateAtRow(iRow) > 0;	
	if( /*m_ComboIsPerRow ||*/ !bCountSep )
	{
		iNumTapsInRow = min( iNumTapsInRow, 1);
	}	

	//TimingData &td = *GAMESTATE->m_pCurSteps[m_pPlayerState->m_PlayerNumber]->GetTimingData();
	if ( tns >= m_MinScoreToContinueCombo )
	{
		m_pPlayerStageStats->m_iCurMissCombo = 0;
		int multiplier = ( iRow == -1 ? 1 : td.GetComboSegmentAtRow( iRow )->GetCombo() );
		m_pPlayerStageStats->m_iCurCombo += iNumTapsInRow * multiplier;
	}
	else if ( tns < m_MinScoreToMaintainCombo )
	{
		m_pPlayerStageStats->m_iCurCombo = 0;

		//if( tns <= m_MaxScoreToIncrementMissCombo )
		if( tns == TNS_Miss )
		{
			int multiplier = ( iRow == -1 ? 1 : td.GetComboSegmentAtRow(iRow)->GetMissCombo());
			m_pPlayerStageStats->m_iCurMissCombo += ( !bCountSep ? 1 : iNumTapsInRow ) * multiplier;
			if( m_pPlayerStageStats->m_iCurMissCombo > m_pPlayerStageStats->m_iMaxMissCombo )
				m_pPlayerStageStats->m_iMaxMissCombo = m_pPlayerStageStats->m_iCurMissCombo;
		}
		//BAD case here!!
		else
		{
			m_pPlayerStageStats->m_iCurMissCombo = 0;
			m_pPlayerStageStats->m_iCurCombo = 0;
		}
	}
	else
	{
		m_pPlayerStageStats->m_iCurMissCombo = 0;
	}
}

void ScoreKeeperNormal::GetRowCounts( const NoteData &nd, int iRow,
					  int &iNumHitContinueCombo, int &iNumHitMaintainCombo,
					  int &iNumBreakCombo )
{
	iNumHitContinueCombo = iNumHitMaintainCombo = iNumBreakCombo = 0;
	for( int track = 0; track < nd.GetNumTracks(); ++track )
	{
		const TapNote &tn = nd.GetTapNote( track, iRow );

		if( tn.type != TapNoteType_Tap && tn.type != TapNoteType_HoldHead && tn.type != TapNoteType_Lift )
			continue;
		if( tn.bFakeHold )
			continue;
		TapNoteScore tns = tn.result.tns;
		if( tns >= m_MinScoreToContinueCombo )
			++iNumHitContinueCombo;
		else if( tns >= m_MinScoreToMaintainCombo )
			++iNumHitMaintainCombo;
		else
			++iNumBreakCombo;
	}
}

void ScoreKeeperNormal::HandleTapRowScore( const NoteData &nd, int iRow )
{
	int iNumHitContinueCombo, iNumHitMaintainCombo, iNumBreakCombo;
	GetRowCounts( nd, iRow, iNumHitContinueCombo, iNumHitMaintainCombo, iNumBreakCombo );

	int iNumTapsInRow = iNumHitContinueCombo + iNumHitMaintainCombo + iNumBreakCombo;
	if( iNumTapsInRow <= 0 )
		return;

	m_iNumNotesHitThisRow = iNumHitContinueCombo;
	m_iNumNotesMissThisRow = iNumBreakCombo;

	TimingData &td = *GAMESTATE->m_pCurSteps[m_pPlayerState->m_PlayerNumber]->GetTimingData();
	const bool bCountSep = td.GetCountSeparateAtRow(iRow) > 0;	

	TapNoteScore scoreOfLastTap = NoteDataWithScoring::LastTapNoteWithResult( nd, iRow ).result.tns;
	vector<TapNoteScore> vRowScores = NoteDataWithScoring::GetTapNoteScores(nd, iRow, m_pPlayerState->m_PlayerNumber);
	for( unsigned i = 0; i < vRowScores.size() && bCountSep; i++ )
	{
		LOG->Trace("HandleTapRowScore: scores: %s", TapNoteScoreToString(vRowScores[i]).c_str());
		HandleTapNoteScoreInternal( vRowScores[i], TNS_W1, iRow );
	}

	if( !bCountSep )
		HandleTapNoteScoreInternal( scoreOfLastTap, TNS_W1, iRow );	
	
	if ( /*GAMESTATE->GetCurrentGame()->m_bCountNotesSeparately*/ bCountSep )
	{
		HandleComboInternal( iNumHitContinueCombo, iNumHitMaintainCombo, iNumBreakCombo, iRow );
	}
	else
	{
		HandleRowComboInternal( scoreOfLastTap, iNumTapsInRow, iRow ); //This should work?
	}

	if( m_pPlayerState->m_PlayerNumber != PLAYER_INVALID )
		MESSAGEMAN->Broadcast( enum_add2(Message_CurrentComboChangedP1,m_pPlayerState->m_PlayerNumber) );

	AddTapRowScore( scoreOfLastTap, nd, iRow );		// only score once per row

	// handle combo logic
#ifndef DEBUG
	if( (GamePreferences::m_AutoPlay != PC_HUMAN || m_pPlayerState->m_PlayerOptions.GetCurrent().m_fPlayerAutoPlay != 0)
		&& !GAMESTATE->m_bDemonstrationOrJukebox )	// cheaters always prosper >:D -aj comment edit
	{
		m_iCurToastyCombo = 0;
		return;
	}
#endif //DEBUG

	// Toasty combo
	//vector<int> iToastyMilestones;
	switch( scoreOfLastTap )
	{
	case TNS_W1:
	case TNS_W2:
		m_iCurToastyCombo += iNumTapsInRow;

		/*
		// compile the list of toasty triggers
		{
			Lua *L = LUA->Get();
			m_vToastyTriggers.PushSelf(L);
			LuaHelpers::ReadArrayFromTable(iToastyMilestones, L);
			lua_pop( L, 1 );
			LUA->Release(L);
		}
		// find out which one we're at.
		if(m_iCurToastyTrigger <= int(iToastyMilestones.size()))
		{
			m_iNextToastyAt = iToastyMilestones[m_iCurToastyTrigger];
		}
		else // out of index value? then don't make it toasty!
		{
			m_iNextToastyAt = -1;
		}
		*/

		if( m_iCurToastyCombo >= m_ToastyTrigger &&
			m_iCurToastyCombo - iNumTapsInRow < m_ToastyTrigger &&
			!GAMESTATE->m_bDemonstrationOrJukebox )
		{
			SCREENMAN->PostMessageToTopScreen( SM_PlayToasty, 0 );
			Message msg("ToastyAchieved");
			msg.SetParam( "PlayerNumber", m_pPlayerState->m_PlayerNumber );
			msg.SetParam( "ToastyCombo", m_iCurToastyCombo );
			MESSAGEMAN->Broadcast(msg);

			// TODO: keep a pointer to the Profile.  Don't index with m_PlayerNumber
			PROFILEMAN->IncrementToastiesCount( m_pPlayerState->m_PlayerNumber );

			//m_iCurToastyTrigger++;
		}
		break;
	default:
		m_iCurToastyCombo = 0;
		Message msg("ToastyDropped");
		msg.SetParam( "PlayerNumber", m_pPlayerState->m_PlayerNumber );
		MESSAGEMAN->Broadcast(msg);
		break;
	}

	// TODO: Remove indexing with PlayerNumber
	PlayerNumber pn = m_pPlayerState->m_PlayerNumber;
	float offset = NoteDataWithScoring::LastTapNoteWithResult( nd, iRow ).result.fTapNoteOffset;
	/*NSMAN->ReportScore( pn, scoreOfLastTap,
			m_pPlayerStageStats->m_iScore,
			m_pPlayerStageStats->m_iCurCombo, offset );*/
	Message msg( "ScoreChanged" );
	msg.SetParam( "PlayerNumber", m_pPlayerState->m_PlayerNumber );
	msg.SetParam( "MultiPlayer", m_pPlayerState->m_mp );
	msg.SetParam( "ToastyCombo", m_iCurToastyCombo );
	MESSAGEMAN->Broadcast( msg );
}


void ScoreKeeperNormal::HandleHoldScore( const TapNote &tn )
{
	HoldNoteScore holdScore = tn.HoldResult.hns;

	// update dance points totals
	if( !m_pPlayerStageStats->m_bFailed )
		m_pPlayerStageStats->m_iActualDancePoints += HoldNoteScoreToDancePoints( holdScore );
	// increment the current total possible dance score
	m_pPlayerStageStats->m_iCurPossibleDancePoints += HoldNoteScoreToDancePoints( HNS_Held );
	m_pPlayerStageStats->m_iHoldNoteScores[holdScore] ++;

	AddHoldScore( holdScore );

	// TODO: Remove indexing with PlayerNumber
	PlayerNumber pn = m_pPlayerState->m_PlayerNumber;
	/*NSMAN->ReportScore(
		pn,
		holdScore+TapNoteScore_Invalid,
		m_pPlayerStageStats->m_iScore,
		m_pPlayerStageStats->m_iCurCombo,
		tn.result.fTapNoteOffset );*/
	Message msg( "ScoreChanged" );
	msg.SetParam( "PlayerNumber", m_pPlayerState->m_PlayerNumber );
	msg.SetParam( "MultiPlayer", m_pPlayerState->m_mp );
	MESSAGEMAN->Broadcast( msg );
}


int ScoreKeeperNormal::GetPossibleDancePoints( NoteData* nd, const TimingData* td, float fSongSeconds )
{
	/* Note: If W1 timing is disabled or not active (not course mode),
	 * W2 will be used instead. */
	// XXX: That's not actually implemented!
	RadarValues radars;
	NoteDataUtil::CalculateRadarValues( *nd, fSongSeconds, radars );
	
	int ret = 0;
	 
	ret += int(radars[RadarCategory_TapsAndHolds]) * TapNoteScoreToDancePoints(TNS_W1, false);
	//if( GAMESTATE->GetCurrentGame()->m_bTickHolds ) ret += NoteDataUtil::GetTotalHoldTicks( nd, td ) * g_iPercentScoreWeight.GetValue(SE_CheckpointHit);
	ret += int(radars[RadarCategory_Holds]) * HoldNoteScoreToDancePoints(HNS_Held, false);	
	ret += int(radars[RadarCategory_Rolls]) * HoldNoteScoreToDancePoints(HNS_Held, false);
	
	return ret;
}

int ScoreKeeperNormal::GetPossibleDancePoints( NoteData* ndPre, NoteData* ndPost, const TimingData* td, float fSongSeconds )
{
	/* The logic here is that if you use a modifier that adds notes, you should
	 * have to hit the new notes to get a high grade. However, if you use one
	 * that removes notes, they should simply be counted as misses. */
	return max(
		GetPossibleDancePoints(ndPre, td, fSongSeconds),
		GetPossibleDancePoints(ndPost, td, fSongSeconds) );
}

int ScoreKeeperNormal::GetPossibleGradePoints( NoteData* nd, const TimingData* td, float fSongSeconds )
{
	/* Note: if W1 timing is disabled or not active (not course mode),
	 * W2 will be used instead. */
	// XXX: That's not actually implemented!
	RadarValues radars;
	NoteDataUtil::CalculateRadarValues( *nd, fSongSeconds, radars );

	int ret = 0;
	
	ret += int(radars[RadarCategory_TapsAndHolds]) * TapNoteScoreToGradePoints(TNS_W1, false);
	//if( GAMESTATE->GetCurrentGame()->m_bTickHolds ) ret += NoteDataUtil::GetTotalHoldTicks( nd, td ) * g_iGradeWeight.GetValue(SE_CheckpointHit);
	ret += int(radars[RadarCategory_Holds]) * HoldNoteScoreToGradePoints(HNS_Held, false);
	ret += int(radars[RadarCategory_Rolls]) * HoldNoteScoreToGradePoints(HNS_Held, false);
	
	return ret;
}

int ScoreKeeperNormal::GetPossibleGradePoints( NoteData* ndPre, NoteData* ndPost, const TimingData* td, float fSongSeconds )
{
	/* The logic here is that if you use a modifier that adds notes, you should
	 * have to hit the new notes to get a high grade. However, if you use one
	 * that removes notes, they should simply be counted as misses. */
	return max(
		GetPossibleGradePoints( ndPre, td, fSongSeconds ),
		GetPossibleGradePoints( ndPost, td, fSongSeconds ) );
}

int ScoreKeeperNormal::TapNoteScoreToDancePoints( TapNoteScore tns ) const
{
	return TapNoteScoreToDancePoints( tns, m_bIsBeginner );
}

int ScoreKeeperNormal::HoldNoteScoreToDancePoints( HoldNoteScore hns ) const
{
	return HoldNoteScoreToDancePoints( hns, m_bIsBeginner );
}

int ScoreKeeperNormal::TapNoteScoreToGradePoints( TapNoteScore tns ) const
{
	return TapNoteScoreToGradePoints( tns, m_bIsBeginner );
}
int ScoreKeeperNormal::HoldNoteScoreToGradePoints( HoldNoteScore hns ) const
{
	return HoldNoteScoreToGradePoints( hns, m_bIsBeginner );
}

int ScoreKeeperNormal::TapNoteScoreToDancePoints( TapNoteScore tns, bool bBeginner )
{
	if( !GAMESTATE->ShowW1() && tns == TNS_W1 )
		tns = TNS_W2;

	/* This is used for Oni percentage displays. Grading values are currently in
	 * StageStats::GetGrade. */
	int iWeight = 0;
	switch( tns )
	{
	DEFAULT_FAIL( tns );
	case TNS_None:		iWeight = 0;														break;
	case TNS_HitMine:	iWeight = g_iPercentScoreWeight.GetValue(SE_HitMine);				break;
	case TNS_Miss:		iWeight = g_iPercentScoreWeight.GetValue(SE_Miss);					break;
	case TNS_W5:		iWeight = g_iPercentScoreWeight.GetValue(SE_W5);					break;
	case TNS_W4:		iWeight = g_iPercentScoreWeight.GetValue(SE_W4);					break;
	case TNS_W3:		iWeight = g_iPercentScoreWeight.GetValue(SE_W3);					break;
	case TNS_W2:		iWeight = g_iPercentScoreWeight.GetValue(SE_W2);					break;
	case TNS_W1:		iWeight = g_iPercentScoreWeight.GetValue(SE_W1);					break;
	case TNS_CheckpointHit:	iWeight = g_iPercentScoreWeight.GetValue(SE_CheckpointHit);		break;
	case TNS_CheckpointMiss:iWeight = g_iPercentScoreWeight.GetValue(SE_CheckpointMiss);	break;
	}
	if( bBeginner && PREFSMAN->m_bMercifulBeginner )
		iWeight = max( 0, iWeight );
	return iWeight;
}

int ScoreKeeperNormal::HoldNoteScoreToDancePoints( HoldNoteScore hns, bool bBeginner )
{
	int iWeight = 0;
	switch( hns )
	{
	DEFAULT_FAIL( hns );
	case HNS_None:	iWeight = 0;										break;
	case HNS_LetGo:	iWeight = g_iPercentScoreWeight.GetValue(SE_LetGo);	break;
	case HNS_Held:	iWeight = g_iPercentScoreWeight.GetValue(SE_Held);	break;
	case HNS_Missed:	iWeight = g_iPercentScoreWeight.GetValue(SE_Missed);	break;
	}
	if( bBeginner && PREFSMAN->m_bMercifulBeginner )
		iWeight = max( 0, iWeight );
	return iWeight;
}

int ScoreKeeperNormal::TapNoteScoreToGradePoints( TapNoteScore tns, bool bBeginner )
{
	if( !GAMESTATE->ShowW1() && tns == TNS_W1 )
		tns = TNS_W2;

	/* This is used for Oni percentage displays. Grading values are currently in
	 * StageStats::GetGrade. */
	int iWeight = 0;
	switch( tns )
	{
	DEFAULT_FAIL( tns );
	case TNS_None:		iWeight = 0;												break;
	case TNS_AvoidMine:	iWeight = 0;												break;
	case TNS_HitMine:	iWeight = g_iGradeWeight.GetValue(SE_HitMine);				break;
	case TNS_Miss:		iWeight = g_iGradeWeight.GetValue(SE_Miss);					break;
	case TNS_W5:		iWeight = g_iGradeWeight.GetValue(SE_W5);					break;
	case TNS_W4:		iWeight = g_iGradeWeight.GetValue(SE_W4);					break;
	case TNS_W3:		iWeight = g_iGradeWeight.GetValue(SE_W3);					break;
	case TNS_W2:		iWeight = g_iGradeWeight.GetValue(SE_W2);					break;
	case TNS_W1:		iWeight = g_iGradeWeight.GetValue(SE_W1);					break;
	case TNS_CheckpointHit:	iWeight = g_iGradeWeight.GetValue(SE_CheckpointHit);	break;
	case TNS_CheckpointMiss:iWeight = g_iGradeWeight.GetValue(SE_CheckpointMiss);	break;
	}
	if( bBeginner && PREFSMAN->m_bMercifulBeginner )
		iWeight = max( 0, iWeight );
	return iWeight;
}

int ScoreKeeperNormal::HoldNoteScoreToGradePoints( HoldNoteScore hns, bool bBeginner )
{
	int iWeight = 0;
	switch( hns )
	{
	DEFAULT_FAIL( hns );
	case HNS_None:	iWeight = 0;									break;
	case HNS_LetGo:	iWeight = g_iGradeWeight.GetValue(SE_LetGo);	break;
	case HNS_Held:	iWeight = g_iGradeWeight.GetValue(SE_Held);		break;
	case HNS_Missed:	iWeight = g_iGradeWeight.GetValue(SE_Missed);		break;
	}
	if( bBeginner && PREFSMAN->m_bMercifulBeginner )
		iWeight = max( 0, iWeight );
	return iWeight;
}

/*
 * (c) 2001-2004 Chris Danford, Glenn Maynard
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
