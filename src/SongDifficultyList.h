#ifndef SongDifficultyList_H
#define SongDifficultyList_H

#include "ActorFrame.h"
#include "Sprite.h"
#include "Steps.h"
#include "BitmapText.h"
#include "GameConstantsAndTypes.h"	// for TapNoteScore
#include "RageTexturePreloader.h"

class PlayerState;
class DifficultyListBullet;

/** @brief A graphical display for bullet in SongDifficultyList. */
class DifficultyListBullet : public ActorFrame
{
public:

	void Load();

	// level for bullets, meter for string ex: "04", type for font
	/* state: 0 = none; 1 = ucs; 2 = new; 3 = another, la logica dice: 3 -> 1 -> 2 */
	void SetFromGameState(RString sLevel, RString sMeter, int iType, float x, int iState);

protected:
	ThemeMetric<float> BULLETS_ZOOM;
	ThemeMetric<float> TEXT_ZOOM;
	ThemeMetric<float> BULLETS_Y;
	ThemeMetric<float> TEXT_Y;

	ThemeMetric<float> NEWSTEP_ZOOM;
	ThemeMetric<float> NEWSTEP_Y;

	// por cada bullet tiene 7 fuentes precargadas, pero solo muestra 1
	BitmapText 	m_vlevelText[7];
	BitmapText 	m_vbullets;
	Sprite 		m_newStep;
};

/** @brief A graphical display for difficulties in selectmusic screens. */
class SongDifficultyList : public ActorFrame
{
public:
	SongDifficultyList();
	~SongDifficultyList();
	void Load(const RString &sType);

	void SetFromGameState();	

	//virtual void Update( float fDelta );
	virtual void HandleMessage( const Message &msg );

	virtual SongDifficultyList *Copy() const;

	// Lua
	virtual void PushSelf( lua_State *L );

protected:
	
	Sprite m_backgroundNormal;
	Sprite m_backgroundMission;
	Sprite m_playerSelection[NUM_PLAYERS];

	ActorFrame m_bulletsSection[2];

	ThemeMetric<float> QUESTSTEP_Y;
	ThemeMetric<float> QUESTSTEPCOMPLETE_Y;
	ThemeMetric<float> TRAINZOOM;
	ThemeMetric<float> TRAIN_Y;	

	ThemeMetric<float> BULLETBACK_Y;
	ThemeMetric<float> BULLETBACK_ZOOM;

	ThemeMetric<float> BULLETSPIN_Y;
	ThemeMetric<float> BULLETSPIN_ZOOM;
	ThemeMetric<float> BULLETSPIN_ALPHA;

	//se carga por cancion, clear y volver a mostrar
	//si ponemos visible, false, no gastamos proce en dibujar
	DifficultyListBullet m_vbullets[26];
	//DifficultyListBullet m_vlevelText[26];

	Sprite m_singleTrainPos[13];
	Sprite m_doubleTrainPos[13];

	Sprite m_stepsNumbers[4];
	Sprite m_stepsNumbersDisabled[4];

	Sprite m_stepsComplete[4];
	Sprite m_bulletBack[13];
	Sprite m_bulletSpinner[13];

	//BitmapText *m_pDiffBullet;
private:
	int GetActiveIndex(vector<Steps*> vSteps, PlayerNumber pn);
};

#endif

/**
 * @file
 * @author Victor Gajardo Henriquez "v1toko" (c) 2015
 * @section LICENSE
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */