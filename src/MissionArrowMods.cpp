#include "global.h"
#include "MissionArrowMods.h"
#include "ThemeManager.h"
#include "GameState.h"
#include "MessageManager.h"
#include "PlayerState.h"
#include "ActorUtil.h"
#include "Mission.h"
#include "RageLog.h"

static const char *ArrowModsTypeNames[] = {
	"Velocity",
	"Life",
	"Lottery",
	"Generic",
};
XToString( ArrowModsType );
StringToX( ArrowModsType );

MissionArrowMods::MissionArrowMods()
{
}

MissionArrowMods::~MissionArrowMods()
{
	//for( unsigned i = 0; i < m_vModsSprites.size(); i++ )
	//	SAFE_DELETE( m_vModsSprites[i] );
	//m_vModsSprites.clear();

	m_vsMods.clear();
}

void MissionArrowMods::Iniciar( RString sType, vector<RString> vsMods, int iMileageCost, Profile* pProfile )
{
	m_iCurIndex = 0;
	m_vsMods.clear();
	//m_vModsSprites.clear();

	m_vsMods = vsMods;

	m_Type = StringToArrowModsType(sType);
	ASSERT( m_Type != ArrowModsType_Invalid );

	//conservamos los mods reales en una variable
	m_poRealMods = GAMESTATE->m_pPlayerState[GAMESTATE->GetFirstHumanPlayer()]->m_PlayerOptions.GetStage();
	//cambiamos solo la velocidad
	m_fScrollSpeed = m_poRealMods.m_fScrollSpeed;

	m_iMileageCost = m_iOriginalMileageCost = iMileageCost;

	//si es que está usado
	m_bIsSet = false;

	m_pProfile = pProfile;

	m_iCurrentMileage = 0;

	//m_soundChange.Load( THEME->GetPathS( "WorldMax", "changemods" ), true );
}

void MissionArrowMods::ChangeNextIndex()
{
	++m_iCurIndex;

	if( m_iCurIndex > (m_vsMods.size()-1) )//clamp :P
		m_iCurIndex = 0;
}

void MissionArrowMods::Actualizar( PlayerNumber pn )
{	
	//if( m_iCurrentMileage > GAMESTATE->GetPlayerMileage( pn ) )
	{
		ResetState(pn);
	}

	//usamos originalmil.. para que el mileage no se pierda
	{
		unsigned iIndex = GetIndex();
		if( iIndex == 0 )
			m_iMileageCost = 0;
		else
		{
			if( m_Type == ArrowModsType_Velocity )
				m_iMileageCost = m_iOriginalMileageCost;
			else
				m_iMileageCost = m_iOriginalMileageCost*iIndex;
		}
	}
	
	//hack: si no tenemos mileage para poner los mods, esto hace volver
	//los actores y el indice como en el principio
	//if( GAMESTATE->GetPlayerMileage( pn ) < m_iMileageCost )
	{
		ResetState( pn );
		return;
	}

	//primero ocultamos, despues mostramos el que corresponde!
	/*for( unsigned i = 0; i < m_vModsSprites.size(); i++ )
		m_vModsSprites[i]->PlayCommand( "Hide" );
	m_vModsSprites[GetIndex()]->PlayCommand( "Pulse" );*/

	MESSAGEMAN->Broadcast( "ModsChanged" );

	if( GetIndex() == 0 )//asignamos los mods reales!
	{
		if( m_Type == ArrowModsType_Velocity )
		{
			PlayerOptions po = GAMESTATE->m_pPlayerState[pn]->m_PlayerOptions.GetStage();
			po.m_fScrollSpeed = m_fScrollSpeed;
			GAMESTATE->m_pPlayerState[pn]->m_PlayerOptions.Assign( ModsLevel_Preferred, po );
			MESSAGEMAN->Broadcast( "OptionsChanged" );
		}
		else if( m_Type == ArrowModsType_Life )
			GAMESTATE->m_pCurMission->m_MissionGoals.fLifeDifficulty = 1.0f;
		else if( m_Type == ArrowModsType_Generic )
		{
			m_poRealMods.m_fScrollSpeed = m_fScrollSpeed;
			GAMESTATE->m_pPlayerState[pn]->m_PlayerOptions.Assign( ModsLevel_Preferred, m_poRealMods );
			MESSAGEMAN->Broadcast( "OptionsChanged" );
		}

		m_bIsSet = false;//no estamos usando modificadores!
		return;
	}

	m_bIsSet = true;

	if( m_Type == ArrowModsType_Velocity )//son distintos los handles!
	{
		PlayerOptions po = GAMESTATE->m_pPlayerState[pn]->m_PlayerOptions.GetStage();

		po.m_fScrollSpeed = atof( m_vsMods[GetIndex()] );
		GAMESTATE->m_pPlayerState[pn]->m_PlayerOptions.Assign( ModsLevel_Preferred, po );
		MESSAGEMAN->Broadcast( "OptionsChanged" );

		{//esto es solo para compatibilidad con el _generic.
			Message msg("SpeedChanged");
			msg.SetParam( "NewSpeed", po.m_fScrollSpeed );
			MESSAGEMAN->Broadcast( msg );
		}
	}
	else if( m_Type == ArrowModsType_Life )
	{
		float fLife = atof( m_vsMods[GetIndex()] );
		GAMESTATE->m_pCurMission->m_MissionGoals.fLifeDifficulty = fLife;
	}
	else if( m_Type == ArrowModsType_Generic )
	{
		PlayerOptions po;
		GAMESTATE->GetDefaultPlayerOptions( po );
		po.m_fScrollSpeed = m_fScrollSpeed;
		GAMESTATE->m_pPlayerState[pn]->m_PlayerOptions.Assign( ModsLevel_Preferred, po );
		MESSAGEMAN->Broadcast( "OptionsChanged" );
	}
}

void MissionArrowMods::Rotate()
{
	//if( CanRotate() )
	//	m_AllMods.PlayCommand( "Rotation" );

	//m_soundChange.Play();
}

void MissionArrowMods::ResetState( PlayerNumber pn )
{
	m_iCurIndex = 0;

	m_iCurrentMileage = 0;
	this->Sleep( 0.5 );

	//if( CanRotate() )//rotamos a 0 grados!
		//m_AllMods.SetRotationZ( 0 );

	Actualizar( pn );
}

void MissionArrowMods::HandleMessage( const Message &msg )
{
	if( msg.GetName().CompareNoCase( "SpeedChanged" ) == 0 )
	{
		float fSpeed = 0.0f;
		if( msg.GetParam( "NewSpeed", fSpeed ) )
		{
			m_fScrollSpeed = fSpeed;
		}
	}

	ActorFrame::HandleMessage( msg );
}

/*
 * (c) 2008 Victor Gajardo Henriquez "v1toko"
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */