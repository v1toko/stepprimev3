#include "global.h"
#include "GameState.h"
#include "RageLog.h"
#include "ThemeManager.h"
#include "ScreenDimensions.h"
#include "MissionBullet.h"
#include "SongManager.h"
#include "LuaManager.h"
#include "ActorUtil.h"

REGISTER_ACTOR_CLASS( MissionBullet );

MissionBullet::MissionBullet()  
{ 
	m_posX = SCREEN_CENTER_X;
	m_posY = SCREEN_CENTER_Y;

	m_pMission = NULL;
}

MissionBullet::~MissionBullet()
{
	m_pMission = NULL;
}

MissionBullet::MissionBullet( const MissionBullet &cpy ):
	ActorFrame(cpy)
{
}

void MissionBullet::Load( Mission *pMission )
{
	m_posX = pMission->m_MissionGoals.fposX;
	m_posY = pMission->m_MissionGoals.fposY;

	RString sName = pMission->GetMissionName();

	m_pMission = pMission;

	switch( pMission->GetMissionType() )
	{
	DEFAULT_FAIL( pMission->GetMissionType() );
	case MissionType_Switch:
		m_Active.SetName( "SwitchOn" );
		m_Active.Load( THEME->GetPathG( "WorldMax/Mission", "switchon" ) );
		this->AddChild( &m_Active );

		m_Inactive.SetName( "SwitchOff" );
		m_Inactive.Load( THEME->GetPathG( "WorldMax/Mission", "switchoff" ) );
		this->AddChild( &m_Inactive );

		m_Active.SetXY( m_posX, m_posY );
		m_Inactive.SetXY( m_posX, m_posY );

		if( pMission->GetSwitchState() )
			m_Inactive.SetVisible( false );
		else
			m_Active.SetVisible( false );

		break;
	case MissionType_Boss://el boss los cargamos con el nombre de la mission!
		m_Active.SetName( "Boss" );
		m_Active.Load( THEME->GetPathG( "WorldMax/Mission", ssprintf( "boss%s", sName.MakeLower().c_str() ) ) );
		this->AddChild( &m_Active );

		m_Inactive.SetName( "Boss" );
		m_Inactive.Load( THEME->GetPathG( "WorldMax/Mission", ssprintf( "inactiveboss%s", sName.MakeLower().c_str() ) ) );
		this->AddChild( &m_Inactive );

		m_Active.SetXY( m_posX, m_posY );
		m_Inactive.SetXY( m_posX, m_posY );

		if( !pMission->IsPassed() )
			m_Inactive.SetVisible( false );		
		else
			m_Active.SetVisible( false );

		break;
	case MissionType_Mission:
		m_Active.SetName( "BulletEnabled" );
		m_Active.Load( THEME->GetPathG( "WorldMax/Mission", "bullet enabled" ) );
		this->AddChild( &m_Active );

		m_Inactive.SetName( "BulletDisabled" );
		m_Inactive.Load( THEME->GetPathG( "WorldMax/Mission", "bullet disabled" ) );
		this->AddChild( &m_Inactive );

		m_Active.SetXY( m_posX, m_posY );
		m_Inactive.SetXY( m_posX, m_posY );

		if( pMission->IsPassed() )
			m_Active.SetVisible( false );
		else
			m_Inactive.SetVisible( false );

		break;
	case MissionType_Warp:
	case MissionType_WarpToWarp:

		m_Active.SetName( "Warp" );
		m_Inactive.SetName( "Warp" );

		if( !pMission->m_bNeededCodes )
		{
			m_Active.Load( THEME->GetPathG( "WorldMax/Mission", "warp" ) );
		}
		else
		{
			
			m_Active.Load( THEME->GetPathG( "WorldMax/Mission", "warpactive" ) );
			m_Inactive.Load( THEME->GetPathG( "WorldMax/Mission", "warpinactive" ) );

			if( pMission->m_MissionGoals.iNumCodes > 0 )
				m_Active.SetVisible( false );
			else
				m_Inactive.SetVisible( true );
		}

		this->AddChild( &m_Active );
		m_Active.SetXY( m_posX, m_posY );
		this->AddChild( &m_Inactive );
		m_Inactive.SetXY( m_posX, m_posY );

		break;
	case MissionType_SpaceWarp: //XXX:

		m_Active.SetName( "SpaceWarpActive" );
		m_Inactive.SetName( "SpaceWarpInactive" );
			
		m_Active.Load( THEME->GetPathG( "WorldMax/Mission", "spacewarpactive" ) );
		m_Inactive.Load( THEME->GetPathG( "WorldMax/Mission", "spacewarpinactive" ) );

		m_Active.SetVisible( false );
		m_Inactive.SetVisible( true );

		this->AddChild( &m_Active );
		m_Active.SetXY( m_posX, m_posY );
		this->AddChild( &m_Inactive );
		m_Inactive.SetXY( m_posX, m_posY );

		break;
	case MissionType_WarpSector:
		m_Active.SetName( "WarpSector" );
		m_Active.Load( THEME->GetPathG( "WorldMax/Mission", "warpsector" ) );
		this->AddChild( &m_Active );

		m_Active.SetXY( m_posX, m_posY );

		break;
	case MissionType_DualWarp:
		m_Active.SetName( "BulletEnabled" );
		m_Active.Load( THEME->GetPathG( "WorldMax/Mission", "bullet enabled" ) );
		this->AddChild( &m_Active );

		m_Inactive.SetName( "Warp" );
		m_Inactive.Load( THEME->GetPathG( "WorldMax/Mission", "warp" ) );
		this->AddChild( &m_Inactive );

		//m_sprWarp.SetXY( m_posX, m_posY );

		m_Active.SetXY( m_posX, m_posY );
		m_Inactive.SetXY( m_posX, m_posY );

		if( pMission->IsPassed() )
			m_Active.SetVisible( false );
		else
			m_Inactive.SetVisible( false );
		break;
	case MissionType_HiddenLine:
		break;
	}

	//override all other graphics
	if( pMission->m_MissionGoals.sNeedGraphics.CompareNoCase("YES") == 0 )
	{
		//ocultamos todos los otros!
		//m_sprBullet.SetVisible( false );
		//m_sprBulletDisabled.SetVisible( false );
		//m_sprWarp.SetVisible( false );
		//m_sprWarpSector.SetVisible( false );
		//m_sprBoss.SetVisible( false );

		m_Active.SetName( "SpecialGraphic" );
		m_Active.Load( THEME->GetPathG( "WorldMax/Mission", ssprintf( "graphic%s", pMission->GetMissionName().MakeLower().c_str() ) ) );
		this->AddChild( &m_Active );

		m_Active.SetXY( m_posX, m_posY );
	}

	if( pMission->HasBarrier() && !pMission->IsValid() )
	{
		m_Barrier.SetName( "Barrier" );
		m_Barrier.Load( THEME->GetPathG( "WorldMax/Mission", ssprintf("barrier%s",MissionBarrierToString(pMission->GetBarrierType()).c_str() ) ) );

		m_Barrier.SetXY( m_posX, m_posY+20 );
		//m_Barrier.SetZoom( 2.0f );
		this->AddChild( &m_Barrier );
	}
}

void MissionBullet::UpdateSwitchState( Mission *pMission )
{
	if( !m_pMission->IsSwitch() )//comprobamos la copia, no pMission, por que ese es el switch que cambió.
		return;

	//si no es el mismo switch
	if( m_pMission->GetMissionName() != pMission->GetMissionName() )
		return;

	if( pMission->GetSwitchState() )
	{
		m_Active.SetVisible( true );
		m_Inactive.SetVisible( false );
	}
	else
	{
		m_Active.SetVisible( false );
		m_Inactive.SetVisible( true );
	}
}

void MissionBullet::HandleMessage(const Message &msg)
{
	if( msg.GetName().CompareNoCase("SwitchStateChanged")==0 )
	{
		RString sMissionName;
		if( msg.GetParam( "SwitchName", sMissionName ) )
			UpdateSwitchState( SONGMAN->FindMission( sMissionName ) );
	}
	else if( msg.GetName().CompareNoCase("WMCodeEntered")==0 )
	{
		RString sMissionName;
		if( msg.GetParam( "Name", sMissionName ) )
		{
			//LOG->Trace( "Mission: %s, Warp: %s", m_pMission->GetMissionName().c_str(), sMissionName.c_str() );
			if( m_pMission->GetMissionName() == sMissionName )
			{
				m_Active.SetVisible( true );
				m_Inactive.SetVisible( false );
			}
		}
	}

	ActorFrame::HandleMessage( msg );
}

// lua start
#include "LuaBinding.h"

class LunaMissionBullet: public Luna<MissionBullet>
{
public:
	/*static int GetAllBullets( T* p, lua_State *L )
	{
		vector<MissionBullet> addTo;
		p->GetBullets( addTo );
		LuaHelpers::CreateTableFromArray( addTo, L );
		return 1;
	}*/

	LunaMissionBullet()
	{
		//ADD_METHOD( GetAllBullets );
	}
};

LUA_REGISTER_DERIVED_CLASS( MissionBullet, ActorFrame )
// lua end

/*
 * (c) 2008 Victor Manuel Vicente Gajardo Henríquez "v1toko"
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
