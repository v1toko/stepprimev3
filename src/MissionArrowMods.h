#ifndef MISSION_ARROW_MODS_H
#define MISSION_ARROW_MODS_H

#include "ActorFrame.h"
#include "Sprite.h"
#include "PlayerNumber.h"
#include "RageSound.h"
#include "PlayerOptions.h"
#include "Profile.h"

enum ArrowModsType { 
	ArrowModsType_Velocity,
	ArrowModsType_Life,
	ArrowModsType_Lottery,
	ArrowModsType_Generic,//por mientras que me falta 1
	NUM_ArrowModsType,
	ArrowModsType_Invalid,
};
const RString& ArrowModsTypeToString( ArrowModsType type );
ArrowModsType StringToArrowModsType( const RString& t );

class MissionArrowMods : public ActorFrame
{
public:
	MissionArrowMods();
	virtual ~MissionArrowMods();

	virtual void Iniciar( RString sType, vector<RString> vsMods, int iMileageCost, Profile* pProfile );
	virtual void Actualizar( PlayerNumber pn );
	virtual void ChangeNextIndex();//el indice siempre crece de 0-4
	virtual void Rotate();
	virtual void ResetState( PlayerNumber pn );
	virtual void HandleMessage( const Message &msg );

	virtual int GetIndex() const { return m_iCurIndex; };
	virtual bool IsMoving() const { return GetTweenTimeLeft() > 0; };
	virtual bool CanRotate() const { return (m_Type != ArrowModsType_Lottery) && (m_Type != ArrowModsType_Generic); };
	virtual bool IsUsed() const { return m_bIsSet; };
	virtual int GetMileageCost() const { return m_iMileageCost; };
	virtual void InformarMileage( int iMileage ) { m_iCurrentMileage = iMileage; };
protected:
	//vector<Sprite*> m_vModsSprites;
	vector<RString> m_vsMods;

	//Sprite m_AllMods;//los mods que giran
	//Sprite m_AllModsOverlay;//las flechas que indican!

	//RageSound m_soundChange;

	PlayerOptions m_poRealMods;//guarda los modificadores reales de una cancion!
	Profile* m_pProfile;//guarda la profile que usamos!

	float m_fScrollSpeed;//guarda la velocidad real!
	bool m_bIsSet;//si es que lo estamos usando!
	int m_iMileageCost;//cuanto cuesta una cancion!
	int m_iOriginalMileageCost;//respaldo del mileage original!
	unsigned int m_iCurIndex;

	//aqui, en screenstage informamos el mileage que tenemos usado
	int m_iCurrentMileage;

	ArrowModsType m_Type;
};

#endif

/*
 * (c) 2008 Victor Gajardo Henriquez "v1toko"
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
