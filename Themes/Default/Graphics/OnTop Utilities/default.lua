local t = Def.ActorFrame{}

		t[#t+1] = LoadActor ("NXAbsolute" ) .. {
		InitCommand=cmd(FullScreen);

	};
	t[#t+1] =LoadActor( "black" )..{
		BeginCommand=cmd(StretchTo,0,0,SCREEN_WIDTH,SCREEN_HEIGHT);
		OnCommand=cmd();
		OffCommand=cmd();
	};

	t[#t+1] =LoadActor( "bg" )..{
		BeginCommand=cmd(StretchTo,0,0,SCREEN_WIDTH,SCREEN_HEIGHT);
		OnCommand=cmd(zoom,0.5;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y);
		OffCommand=cmd(sleep,0.5;linear,0.2;zoom,1.5);
	};

t[#t+1] =	LoadActor( "timer" )..{
		BeginCommand=cmd(StretchTo,0,0,SCREEN_WIDTH,SCREEN_HEIGHT);
		OnCommand=cmd(zoom,1;x,SCREEN_CENTER_X+180;y,SCREEN_CENTER_Y-100;linear,0.2;zoom,0.5);
		OffCommand=cmd(sleep,0.5;linear,0.2;zoom,0);
	};

t[#t+1] =	LoadActor( "centro" )..{
		BeginCommand=cmd(StretchTo,0,0,SCREEN_WIDTH,SCREEN_HEIGHT);
		OnCommand=cmd(zoom,1;x,SCREEN_CENTER_X-180;y,SCREEN_CENTER_Y-100;linear,0.2;zoom,0.5);
		OffCommand=cmd(sleep,0.5;linear,0.2;zoom,0);
	};

t[#t+1] =	LoadActor( "centro" )..{
		BeginCommand=cmd(StretchTo,0,0,SCREEN_WIDTH,SCREEN_HEIGHT);
		OnCommand=cmd(zoom,1;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;linear,0.2;zoom,0.5);
		OffCommand=cmd(sleep,0.5;linear,0.2;zoom,0);
		
	};
	
	
	
	t[#t+1] =	LoadActor( "centro" )..{
		BeginCommand=cmd(StretchTo,0,0,SCREEN_WIDTH,SCREEN_HEIGHT);
		OnCommand=cmd(zoom,1;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;linear,0.2;zoom,0.5);
		OffCommand=cmd(sleep,0.5;linear,0.2;zoom,0);
	};

t[#t+1] =	LoadActor( "arcade" )..{
		BeginCommand=cmd(StretchTo,0,0,SCREEN_WIDTH,SCREEN_HEIGHT;draworder,-9);
		OnCommand=cmd(zoom,1;x,SCREEN_CENTER_X-180;y,SCREEN_CENTER_Y+100;linear,0.2;zoom,0.5;draworder,-9);
		OffCommand=cmd(sleep,0.5;linear,0.2;zoom,0);
	};

t[#t+1] =	LoadActor( "battle" )..{
		BeginCommand=cmd(StretchTo,0,0,SCREEN_WIDTH,SCREEN_HEIGHT;draworder,-9);
		OnCommand=cmd(zoom,1;x,SCREEN_CENTER_X+180;y,SCREEN_CENTER_Y+100;linear,0.2;zoom,0.5;draworder,-9);
		OffCommand=cmd(sleep,0.5;linear,0.2;zoom,0);
	};
	
	
	
t[#t+1] =	LoadActor( "hex" )..{
		InitCommand=cmd(StretchTo,0,0,SCREEN_WIDTH,SCREEN_HEIGHT;draworder,-9);
		OnCommand=cmd(zoom,1;x,SCREEN_CENTER_X-180;y,SCREEN_CENTER_Y+100;linear,0.2;zoom,0.5;visible,false;diffusealpha,0.4;draworder,-9);
		OffCommand=cmd(sleep,0.5;linear,0.2;zoom,0);
		CodeMessageCommand=function(self,params)
		local code = params.Name
		local player = params.PlayerNumber
		local file = THEME:GetCurrentThemeDirectory() .. "Scripts/twoPlayersMod.txt"
		local read2 = File.Read( file )
		local buff = "Arcade"
		
		if not GAMESTATE:IsHumanPlayer(player) then return end;
		
		
		if code == "Prev" then
		self:stoptweening();
			self:visible(true);
			self:diffusealpha(0.75);
			self:linear(0.2);
			self:diffusealpha(0.4);
			File.Write(file,buff)
		elseif code == "Next" then
			self:visible(true);
			end
	end	
	};	
	
	
	
	
	t[#t+1] =	LoadActor( "_light" )..{
		InitCommand=cmd(visible,true;draworder,5);
		OnCommand=cmd(zoom,1;x,SCREEN_CENTER_X+180;y,SCREEN_CENTER_Y+100;linear,0.2;zoom,0.5;visible,false;draworder,5);
		OffCommand=cmd(sleep,0.5;linear,0.2;zoom,0);
		CodeMessageCommand=function(self,params)
		local code = params.Name
		local player = params.PlayerNumber
		
		if not GAMESTATE:IsHumanPlayer(player) then return end;
		
		
		if code == "Prev" then
			self:visible(false);
		elseif code == "Next" then
			self:visible(true);
			self:diffusealpha(0.3);
			end
	end	
	};
	t[#t+1] =	LoadActor( "hex" )..{
		InitCommand=cmd(StretchTo,0,0,SCREEN_WIDTH,SCREEN_HEIGHT;draworder,-9);
		OnCommand=cmd(zoom,1;x,SCREEN_CENTER_X+180;y,SCREEN_CENTER_Y+100;linear,0.2;zoom,0.5;visible,false;diffusealpha,0.4;draworder,-9);
		OffCommand=cmd(sleep,0.5;linear,0.2;zoom,0);
		CodeMessageCommand=function(self,params)
		local code = params.Name
		local player = params.PlayerNumber
		local file = THEME:GetCurrentThemeDirectory() .. "Scripts/twoPlayersMod.txt"
		local read1 = File.Read( file )
		local buff = "Battle"
		if not GAMESTATE:IsHumanPlayer(player) then return end;
		
		
		if code == "Prev" then
			self:visible(true);
		elseif code == "Next" then
			self:stoptweening();
			self:visible(true);
			self:diffusealpha(0.75);
			self:linear(0.2);
			self:diffusealpha(0.4);
			File.Write(file,buff)
			--GetPlayMode(   )
			end
	end	
	};	
		
	t[#t+1] =	LoadActor( "_light" )..{
		InitCommand=cmd(visible,true;draworder,0);
		OnCommand=cmd(zoom,1;x,SCREEN_CENTER_X-180;y,SCREEN_CENTER_Y+100;linear,0.2;zoom,0.5;visible,false;draworder,0);
		OffCommand=cmd(sleep,0.5;linear,0.2;zoom,0);
		CodeMessageCommand=function(self,params)
		local code = params.Name
		local player = params.PlayerNumber
		
		if not GAMESTATE:IsHumanPlayer(player) then return end;
		
		
		if code == "Prev" then
			self:visible(true);
			self:diffusealpha(0.3);
		elseif code == "Next" then
			self:visible(false);
			end
	end	
	};
t[#t+1] =	LoadActor( "azul_izq" )..{
		InitCommand=cmd(StretchTo,0,0,SCREEN_WIDTH,SCREEN_HEIGHT;visible,false;draworder,10);
		OnCommand=cmd(zoom,1;x,SCREEN_CENTER_X-180;y,SCREEN_CENTER_Y+100;linear,0.2;zoom,0.5;visible,false;draworder,10);
		OffCommand=cmd(sleep,0.5;linear,0.2;zoom,0);
		CodeMessageCommand=function(self,params)
		local code = params.Name
		local player = params.PlayerNumber
		
		if not GAMESTATE:IsHumanPlayer(player) then return end;
		
		
		if code == "Prev" then
			self:visible(true);
		elseif code == "Next" then
			self:visible(true);
			end
	end	
	};
	
	t[#t+1] =	LoadActor( "derecha" )..{
		InitCommand=cmd(visible,false;draworder,9);
		OnCommand=cmd(zoom,1;x,SCREEN_CENTER_X+180;y,SCREEN_CENTER_Y+100;linear,0.2;zoom,0.5;visible,false;draworder,9);
		OffCommand=cmd(sleep,0.5;linear,0.2;zoom,0);
		CodeMessageCommand=function(self,params)
		local code = params.Name
		local player = params.PlayerNumber
		
		if not GAMESTATE:IsHumanPlayer(player) then return end;
		
		
		if code == "Prev" then
			self:visible(true);
		elseif code == "Next" then
			self:visible(true);
			end
	end	
	};
		
		
	

	
	t[#t+1] =	LoadActor( "_borde" )..{
		InitCommand=cmd(visible,true;draworder,9);
		OnCommand=cmd(x,SCREEN_CENTER_X+180;y,SCREEN_CENTER_Y+100;pulse;effectmagnitude,0.5,0.46,1;effectperiod,1;visible,false;draworder,9);
		OffCommand=cmd(sleep,0.5;linear,0.2;zoom,0);
		CodeMessageCommand=function(self,params)
		local code = params.Name
		local player = params.PlayerNumber
		
		if not GAMESTATE:IsHumanPlayer(player) then return end;
		
		
		if code == "Prev" then
			self:visible(false);
		elseif code == "Next" then
			self:visible(true);
			self:diffusealpha(0.4);
			end
	end	
	};
	
	t[#t+1] =	LoadActor( "_borde" )..{
		InitCommand=cmd(visible,true;draworder,9);
		OnCommand=cmd(x,SCREEN_CENTER_X-180;y,SCREEN_CENTER_Y+100;pulse;effectmagnitude,0.5,0.46,1;effectperiod,1;visible,false;draworder,9);
		OffCommand=cmd(sleep,0.5;linear,0.2;zoom,0);
		CodeMessageCommand=function(self,params)
		local code = params.Name
		local player = params.PlayerNumber
		
		if not GAMESTATE:IsHumanPlayer(player) then return end;
		
		
		if code == "Prev" then
			self:visible(true);
		elseif code == "Next" then
			self:visible(false);
			self:diffusealpha(0.4);
			end
	end	
	};
	
	
	--azul_izq
	--   -.- como funciona esta wea?
	t[#t+1] =	Def.ActorFrame  {
	CodeMessageCommand=function(self,params)
		local code = params.Name
		local player = params.PlayerNumber
		local movement = "none"
		local modscleared = false
		
		if not GAMESTATE:IsHumanPlayer(player) then return end;
		
		
		if code == "Toggle" then
			--SCREENMAN:GetTopScreen():Cancel();
			SCREENMAN:SetNewScreen("ScreenRandomWall");
		end
	end	
};
	return t;
