--S�, ando usando judge premiere para el themekit �Y qu�?
local player = Var "Player"
-- blah
--if not getenv then getenv = function() return false end end

--frame correspondiente de cada judgment
local TNSframe = {
	TapNoteScore_CheckpointHit = 0;
	TapNoteScore_W1 = 0;
	TapNoteScore_W2 = 0;
	TapNoteScore_W3 = 1;
	TapNoteScore_W4 = 2;
	TapNoteScore_W5 = 3;
	TapNoteScore_Miss = 4;
	TapNoteScore_CheckpointMiss = 4;
}

--frames para RG
local TNSframeReversed = {
	TapNoteScore_CheckpointHit = 4;
	TapNoteScore_W1 = 4;
	TapNoteScore_W2 = 4;
	TapNoteScore_W3 = 3;
	TapNoteScore_W4 = 2;
	TapNoteScore_W5 = 1;
	TapNoteScore_Miss = 0;
	TapNoteScore_CheckpointMiss = 0;
}

return Def.ActorFrame {
	--init
	InitCommand=function(self)
		local this = self:GetChildren()
		this.judgm:pause();
		
		--this.judgm:y(-35);
		--this.combo:y(42);
		
		this.combo:vertalign(top);
		this.label:vertalign(top);
		
		this.judgm:visible(false);
		this.combo:visible(false);
		this.label:visible(false);
		
		--self:runcommandsonleaves(cmd( SetTextureFiltering,false ))
	end;

	--judges
	LoadActor("judge")..{
		Name="judgm";
		InitCommand=cmd(vertalign,'VertAlign_Bottom');
		NormalCommand=cmd(diffusealpha,1;zoom,1.37;y,1;decelerate,.1;zoom,.85;y,5;linear,.3;diffusealpha,.7;decelerate,.9;diffusealpha,0;zoomx,1.5;zoomy,0);
	};
	--label
	LoadActor("label")..{
		Name="label";
		NormalCommand=cmd(vertalign,'VertAlign_Middle';diffusealpha,1;zoom,1.32;y,19;decelerate,.1;zoom,.8;linear,.3;diffusealpha,.7;decelerate,.9;diffusealpha,0;zoomx,1.3;zoomy,0);
	};
	--combo
	LoadFont("combo")..{
		Name="combo";
		InitCommand=cmd(vertalign,'VertAlign_Top');
		NormalCommand=cmd(diffusealpha,1;zoom,1.13;y,38;decelerate,.1;zoom,.92;y,29;linear,.3;diffusealpha,.7;decelerate,.9;diffusealpha,0);
	};
	
	--"PERFECT"!
	JudgmentMessageCommand=function(self,param)
		local this = self:GetChildren()
		local iTns = TNSframe[param.TapNoteScore]
		
		local player_options_string = GAMESTATE:GetPlayerState(player):GetPlayerOptionsString('ModsLevel_Preferred');
		if ( string.find(player_options_string,"ReverseGrade") ~= nil ) then
			iTns = TNSframeReversed[param.TapNoteScore];
		end;

		--if getenv("ReverseGrade"..player) then
		--	iTns = TNSframeReversed[param.TapNoteScore]
		--end
		
		--no player, no job
		if param.Player ~= player then return end
		if param.HoldNoteScore then return end
		
		this.judgm:visible(true);
		this.judgm:setstate(iTns);
		--this.combo:visible(true);
		--this.label:visible(true);
		
		this.judgm:stoptweening();
		this.judgm:queuecommand("Normal");
	end;

	ComboCommand=function(self,param)
		local this = self:GetChildren()
		local combo = param.Misses or param.Combo;

		local rg = false
		local player_options_string = GAMESTATE:GetPlayerState(player):GetPlayerOptionsString('ModsLevel_Preferred');
		if ( string.find(player_options_string,"ReverseGrade") ~= nil ) then
			rg = true
		end;
		
		--color misses o RG
		local ccolor
		if param.Misses then
			ccolor = color("1,0,0,1");
			--GradeReverse, combo misses no rojo
			--puedes cambiar userprefs por getenv
			if rg then
				ccolor = color("1,1,1,1");
			end
		else
			ccolor = color("1,1,1,1");
			--GradeReverse, combo rojo
			if rg then
				ccolor = color("1,0,0,1");
			end
		end;

		if self == nil then return end
		if combo == nil then combo = 0 end
		
		--visibilidad
		this.combo:visible(combo >= 4 or GAMESTATE:IsEditing());
		this.combo:stoptweening();
		this.combo:settextf("%03i",combo);
		this.combo:diffuse(ccolor);
		this.combo:queuecommand("Normal");
		
		this.label:visible(combo >= 4 or GAMESTATE:IsEditing());
		this.label:stoptweening();
		this.label:diffuse(ccolor);
		this.label:queuecommand("Normal");
	end;
}