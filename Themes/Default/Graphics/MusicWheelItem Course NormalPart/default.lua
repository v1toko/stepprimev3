return Def.ActorFrame {
	OnCommand=cmd(diffusealpha,0;zoom,1.15;linear,0.3;diffusealpha,1;zoom,1;);

	Def.Banner {
		Name="CourseBanner";
		InitCommand=cmd(draworder,10;);
		SetMessageCommand=function(self,params)
			if not params.Course then return end;
			
			local path = params.Course:GetBannerPath()
			if not path then path = THEME:GetPathG("Common","fallback banner") end
			
			self:LoadFromCachedBanner(path);
			self:scaletoclipped(520,300);
		end;
	};
}
