return Def.ActorFrame {
	--OnCommand=cmd(diffusealpha,0;zoom,1.15;linear,0.3;diffusealpha,1;zoom,1;);

	--[[
	Def.Banner {
		Name="SongBanner";
		InitCommand=cmd(draworder,10;);
		SetMessageCommand=function(self,params)
			if not params.Song then return end;
			
			local path = params.Song:GetBannerPath()
			local bSprite = false;
			if not path then 
				path = THEME:GetPathG("Common","fallback banner") 
				bSprite = true;
			end

			--carga los titles desde los songfolder directamente
			--self:LoadFromSong(params.Song);

			--carga desde el cache, pero igualmente es raro
			if not bSprite then
				self:LoadFromCachedBanner(path);
			else
				self:Load(path);
			end

			self:scaletoclipped(520,300);
		end;
	};
	]]
}
