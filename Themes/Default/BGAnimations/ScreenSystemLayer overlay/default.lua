local t = Def.ActorFrame {}

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Revisa en los metrics de la screen si se deben o no mostrar los creditos
function ShowCredits()
	local screen = SCREENMAN:GetTopScreen();
	local bShow = true;
	if screen then
		local sClass = screen:GetName();
		bShow = THEME:GetMetric( sClass, "ShowCreditDisplay" );
	end
	return( bShow );
end;

function Actor:ShowIfEvent()
	if (GAMESTATE:IsEventMode()) and ShowCredits() then self:visible( true ); else self:visible( false ); end;
end;

function Actor:ShowIfNotEvent()
	if not (GAMESTATE:IsEventMode()) and ShowCredits() then self:visible( true ); else self:visible( false ); end;
end;

t[#t+1] = LoadActor("Event")..{
	InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_HEIGHT-15;zoomy,1.1);
	ScreenChangedMessageCommand=cmd(ShowIfEvent);
};

t[#t+1] = LoadFont("credits") .. {
	InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_HEIGHT-17;zoomy,.63;zoomx,1.04;queuecommand,'Refresh');
	RefreshCommand=function(self)
		local gMode = GAMESTATE:GetCoinMode();
		if gMode == 'CoinMode_Home' then
			self:settext('');
			self:visible(true);
		elseif gMode == 'CoinMode_Free' then
			self:visible(false);
		elseif gMode == 'CoinMode_Pay' then
			self:visible(false);
		end;
	end;
	RefreshCreditTextMessageCommand = cmd(playcommand,'Refresh');
	CoinInsertedMessageCommand = cmd(playcommand,'Refresh');
	CoinModeChangedMessageCommand = cmd(playcommand,'Refresh');
	PlayerJoinedMessageCommand = cmd(playcommand,'Refresh');
	ScreenChangedMessageCommand = cmd(ShowIfNotEvent;playcommand,'Refresh');
};

t[#t+1] = LoadActor(THEME:GetPathG("","Messages/credittext")) .. {
	InitCommand=cmd(x,SCREEN_CENTER_X-40;y,SCREEN_HEIGHT-17;zoom,.75;playcommand,'Refresh');
	RefreshCommand=function(self)
		local gMode = GAMESTATE:GetCoinMode();
		if gMode == 'CoinMode_Home' then
			self:visible(false);
		elseif gMode == 'CoinMode_Free' then
			self:visible(false);
		elseif gMode == 'CoinMode_Pay' then
			self:visible(true);
		end;
	end;
	RefreshCreditTextMessageCommand = cmd(playcommand,'Refresh');
	CoinInsertedMessageCommand = cmd(playcommand,'Refresh');
	CoinModeChangedMessageCommand = cmd(playcommand,'Refresh');
	PlayerJoinedMessageCommand = cmd(playcommand,'Refresh');
	ScreenChangedMessageCommand = cmd(ShowIfNotEvent;playcommand,'Refresh');
};

t[#t+1] = LoadActor(THEME:GetPathG("","Messages/coincount")) .. {
	InitCommand=cmd(x,SCREEN_CENTER_X+66;y,SCREEN_HEIGHT-18;zoom,.75;playcommand,'Refresh');
	RefreshCommand=function(self)
		local gMode = GAMESTATE:GetCoinMode();
		if gMode == 'CoinMode_Home' then
			self:visible(false);
		elseif gMode == 'CoinMode_Free' then
			self:visible(false);
		elseif gMode == 'CoinMode_Pay' then
			self:visible(true);
		end;
	end;
	RefreshCreditTextMessageCommand = cmd(playcommand,'Refresh');
	CoinInsertedMessageCommand = cmd(playcommand,'Refresh');
	CoinModeChangedMessageCommand = cmd(playcommand,'Refresh');
	PlayerJoinedMessageCommand = cmd(playcommand,'Refresh');
	ScreenChangedMessageCommand = cmd(ShowIfNotEvent;playcommand,'Refresh');
};

t[#t+1] = LoadFont("credits_numbers") .. {
	InitCommand=cmd(x,SCREEN_CENTER_X+50;y,SCREEN_HEIGHT-18;zoom,.75;playcommand,'Refresh');
	RefreshCommand=function(self)
		local gMode = GAMESTATE:GetCoinMode();
		if gMode == 'CoinMode_Home' then
			self:visible(false);
		elseif gMode == 'CoinMode_Free' then
			self:visible(false);
		elseif gMode == 'CoinMode_Pay' then
			local CoinstoJoin = GAMESTATE:GetCoinsNeededToJoin();
			local Coins = GAMESTATE:GetCoins();
			local Credits = math.floor(Coins/CoinstoJoin);
			local CoinsLeft = math.floor(Coins - Credits*CoinstoJoin);
			self:settext(tostring(CoinsLeft));
			self:visible(true);
		end;
	end;
	RefreshCreditTextMessageCommand = cmd(playcommand,'Refresh');
	CoinInsertedMessageCommand = cmd(playcommand,'Refresh');
	CoinModeChangedMessageCommand = cmd(playcommand,'Refresh');
	PlayerJoinedMessageCommand = cmd(playcommand,'Refresh');
	ScreenChangedMessageCommand = cmd(ShowIfNotEvent;playcommand,'Refresh');
};

t[#t+1] = LoadFont("credits_numbers") .. {
	InitCommand=cmd(x,SCREEN_CENTER_X+79;y,SCREEN_HEIGHT-18;zoom,.75;playcommand,'Refresh');
	RefreshCommand=function(self)
		local gMode = GAMESTATE:GetCoinMode();
		if gMode == 'CoinMode_Home' then
			self:visible(false);
		elseif gMode == 'CoinMode_Free' then
			self:visible(false);
		elseif gMode == 'CoinMode_Pay' then
			local CoinstoJoin = GAMESTATE:GetCoinsNeededToJoin();
			local Coins = GAMESTATE:GetCoins();
			local Credits = math.floor(Coins/CoinstoJoin);
			local CoinsLeft = math.floor(Coins - Credits*CoinstoJoin);
			self:settext(string.format("%d", CoinstoJoin));
			self:visible(true);
		end;
	end;
	RefreshCreditTextMessageCommand = cmd(playcommand,'Refresh');
	CoinInsertedMessageCommand = cmd(playcommand,'Refresh');
	CoinModeChangedMessageCommand = cmd(playcommand,'Refresh');
	PlayerJoinedMessageCommand = cmd(playcommand,'Refresh');
	ScreenChangedMessageCommand = cmd(ShowIfNotEvent;playcommand,'Refresh');
};

t[#t+1] = LoadFont("credits_numbers") .. {
	InitCommand=cmd(x,SCREEN_CENTER_X+20;y,SCREEN_HEIGHT-18;zoom,.75;playcommand,'Refresh');
	RefreshCommand=function(self)
		local gMode = GAMESTATE:GetCoinMode();
		if gMode == 'CoinMode_Home' then
			self:visible(false);
		elseif gMode == 'CoinMode_Free' then
			self:visible(false);
		elseif gMode == 'CoinMode_Pay' then
			local CoinstoJoin = GAMESTATE:GetCoinsNeededToJoin();
			local Coins = GAMESTATE:GetCoins();
			local Credits = math.floor(Coins/CoinstoJoin);
			local CoinsLeft = math.floor(Coins - Credits*CoinstoJoin);
			self:settext(string.format("%d", Credits));
			self:visible(true);
		end;
	end;
	RefreshCreditTextMessageCommand = cmd(playcommand,'Refresh');
	CoinInsertedMessageCommand = cmd(playcommand,'Refresh');
	CoinModeChangedMessageCommand = cmd(playcommand,'Refresh');
	PlayerJoinedMessageCommand = cmd(playcommand,'Refresh');
	ScreenChangedMessageCommand = cmd(ShowIfNotEvent;playcommand,'Refresh');
};

t[#t+1] = LoadActor(THEME:GetPathG("","Messages/freeplay")) .. {
	InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_HEIGHT-17;zoom,.75;playcommand,'Refresh');
	RefreshCommand=function(self)
		local gMode = GAMESTATE:GetCoinMode();
		if gMode == 'CoinMode_Home' then
			self:visible(false);
		elseif gMode == 'CoinMode_Free' then
			self:visible(true);
		elseif gMode == 'CoinMode_Pay' then
			self:visible(false);
		end;
	end;
	RefreshCreditTextMessageCommand = cmd(playcommand,'Refresh');
	CoinInsertedMessageCommand = cmd(playcommand,'Refresh');
	CoinModeChangedMessageCommand = cmd(playcommand,'Refresh');
	PlayerJoinedMessageCommand = cmd(playcommand,'Refresh');
	ScreenChangedMessageCommand = cmd(ShowIfNotEvent;playcommand,'Refresh');
};

t[#t+1] = Def.ActorFrame {
	Def.Quad {
		InitCommand=cmd(zoomtowidth,SCREEN_WIDTH;zoomtoheight,30;horizalign,left;vertalign,top;y,SCREEN_TOP;diffuse,color("0,0,0,0"));
		OnCommand=cmd(finishtweening;diffusealpha,0.85;);
		OffCommand=cmd(sleep,3;linear,0.5;diffusealpha,0;);
	};
	LoadFont("Common","Normal") .. {
		Name="Text";
		InitCommand=cmd(maxwidth,750;horizalign,left;vertalign,top;y,SCREEN_TOP+10;x,SCREEN_LEFT+10;shadowlength,1;diffusealpha,0;);
		OnCommand=cmd(finishtweening;diffusealpha,1;zoom,0.5);
		OffCommand=cmd(sleep,3;linear,0.5;diffusealpha,0;);
	};
	SystemMessageMessageCommand = function(self, params)
		self:GetChild("Text"):settext( params.Message );
		self:playcommand( "On" );
		if params.NoAnimate then
			self:finishtweening();
		end
		self:playcommand( "Off" );
	end;
	HideSystemMessageMessageCommand = cmd(finishtweening);
};

return t;
