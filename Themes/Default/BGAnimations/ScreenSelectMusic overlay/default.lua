local t = Def.ActorFrame{
	PlayerJoinedMessageCommand=function(self)
		--if GAMESTATE:IsAnyHumanPlayerUsingMemoryCard() then
			SOUND:DimMusic(0,1);
			SOUND:PlayOnce(THEME:GetPathS('','ActionSounds/action1.mp3'));
			(cmd(sleep,.5;queuecommand,'News'))(self);
			SCREENMAN:GetTopScreen():lockinput(1);
		--end;
	end;
	NewsCommand=function(self)
		SCREENMAN:SetNewScreen("ScreenSelectPlayMode");
	end;
	GoFullModeMessageCommand=function(self)
		SCREENMAN:GetTopScreen():lockinput(1);
		SCREENMAN:GetTopScreen():GetMusicWheel():Move(0);
	end;
}

t[#t+1] =Def.Quad {
	InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;zoomto,SCREEN_WIDTH,SCREEN_HEIGHT;diffuse,0,0,0,1;diffusealpha,0;visible,false);
	OnCommand=cmd(stoptweening;diffusealpha,0);
	PlayerJoinedMessageCommand=function(self)
		--if GAMESTATE:IsAnyHumanPlayerUsingMemoryCard() then
			(cmd(stoptweening;visible,true;linear,.8;diffusealpha,1))(self);
		--end;
	end;
};

t[#t+1] = LoadActor("enter_if_you_want") .. {
	Conditions=GAMESTATE:PlayersCanJoin();
	InitCommand=cmd(x,SCREEN_CENTER_X*0.6;y,SCREEN_CENTER_Y*1.6;zoom,.75;draworder,4;);
	OnCommand=function(self)
	self:visible(not GAMESTATE:IsHumanPlayer(PLAYER_1));
	(cmd(diffusealpha,1;sleep,3;linear,.2;addx,-470;sleep,1;queuecommand,'Visible'))(self); --linear,0.1;visible,false;
	end;
	VisibleCommand=cmd(visible,false);
};

t[#t+1] = LoadActor("enter_if_you_want") .. {
	Conditions=GAMESTATE:PlayersCanJoin();
	InitCommand=cmd(x,SCREEN_CENTER_X*1.6;y,SCREEN_CENTER_Y*1.6;zoom,.75;draworder,4;);
	OnCommand=function(self)
	self:visible(not GAMESTATE:IsHumanPlayer(PLAYER_2));
	(cmd(diffusealpha,1;sleep,3;linear,.2;addx,470;sleep,1;queuecommand,'Visible'))(self); --linear,0.1;visible,false;
	end;
	VisibleCommand=cmd(visible,false);
};
---------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
--battle
--[[
t[#t+1] =LoadActor( THEME:GetPathG("","NewPlayer/un_battle") )..{
	InitCommand=cmd(x,SCREEN_CENTER_X+180;y,SCREEN_CENTER_Y+100);
	OnCommand=cmd(stoptweening;diffusealpha,0);
	PlayerJoinedMessageCommand=base;
};

t[#t+1] =LoadActor( THEME:GetPathG("","NewPlayer/shine") )..{
	InitCommand=cmd(x,SCREEN_CENTER_X+180;y,SCREEN_CENTER_Y+100);
	OnCommand=cmd(stoptweening;diffusealpha,0);
	PlayerJoinedMessageCommand=shine2;
};

t[#t+1] =LoadActor( THEME:GetPathG("","NewPlayer/un_battle") )..{
	InitCommand=cmd(x,SCREEN_CENTER_X+180;y,SCREEN_CENTER_Y+100;blend,'BlendMode_Add');
	OnCommand=cmd(stoptweening;diffusealpha,0);
	PlayerJoinedMessageCommand=glow;
};

---------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
-- timer
t[#t+1] =LoadActor( THEME:GetPathG("","NewPlayer/timer") )..{
	InitCommand=cmd(x,SCREEN_CENTER_X+180;y,SCREEN_CENTER_Y-100);
	OnCommand=cmd(stoptweening;diffusealpha,0);
	PlayerJoinedMessageCommand=base;
};

t[#t+1] =LoadActor( THEME:GetPathG("","NewPlayer/shine") )..{
	InitCommand=cmd(x,SCREEN_CENTER_X+180;y,SCREEN_CENTER_Y-100);
	OnCommand=cmd(stoptweening;diffusealpha,0);
	PlayerJoinedMessageCommand=shine;
};

t[#t+1] =LoadActor( THEME:GetPathG("","NewPlayer/timer") )..{
	InitCommand=cmd(x,SCREEN_CENTER_X+180;y,SCREEN_CENTER_Y-100;blend,'BlendMode_Add');
	OnCommand=cmd(stoptweening;diffusealpha,0);
	PlayerJoinedMessageCommand=glow;
};

---------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
--upleft
t[#t+1] =LoadActor( THEME:GetPathG("","NewPlayer/un_mission") )..{
	InitCommand=cmd(x,SCREEN_CENTER_X-180;y,SCREEN_CENTER_Y-100);
	OnCommand=cmd(stoptweening;diffusealpha,0);
	PlayerJoinedMessageCommand=base;
};

t[#t+1] =LoadActor( THEME:GetPathG("","NewPlayer/shine") )..{
	InitCommand=cmd(x,SCREEN_CENTER_X-180;y,SCREEN_CENTER_Y-100);
	OnCommand=cmd(stoptweening;diffusealpha,0);
	PlayerJoinedMessageCommand=shine2;
};

t[#t+1] =LoadActor( THEME:GetPathG("","NewPlayer/un_mission") )..{
	InitCommand=cmd(x,SCREEN_CENTER_X-180;y,SCREEN_CENTER_Y-100;blend,'BlendMode_Add');
	OnCommand=cmd(stoptweening;diffusealpha,0);
	PlayerJoinedMessageCommand=glow;
};
---------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
]]
t[#t+1] = LoadActor("_GoRankMode/RANKMODE")..{
	InitCommand=cmd(stop;pause);
	GoRankModeMessageCommand=cmd(play);
};

t[#t+1] = LoadActor("_GoRankMode/RANKMODEVOICE")..{
	InitCommand=cmd(stop;pause);
	GoRankModeMessageCommand=cmd(play);
};

t[#t+1] = LoadActor("_GoRankMode/rayo1rank")..{
	--Frames = Sprite.LinearFrames(2,.1);
	OnCommand=cmd(stoptweening;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-70;blend,'BlendMode_Add';zoom,0;visible,false);
	--GoRankModeMessageCommand=cmd(stoptweening;diffusealpha,1;zoom,0;sleep,.3;linear,.1;zoomx,2.2;zoomy,1;sleep,.3;linear,.2;zoomy,.2;diffusealpha,0);
	GoRankModeMessageCommand=cmd(stoptweening;visible,true;diffusealpha,1;zoom,0;sleep,.25;linear,.1;zoomto,SCREEN_WIDTH,1;zoomy,1;sleep,.3;linear,.2;zoomy,.2;diffusealpha,0);
};

t[#t+1] = LoadActor("_GoRankMode/rayo2rank")..{
	--Frames = Sprite.LinearFrames(2,.1);
	OnCommand=cmd(stoptweening;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-70;blend,'BlendMode_Add';zoom,0;visible,false);
	--GoRankModeMessageCommand=cmd(stoptweening;diffusealpha,1;zoom,0;sleep,.3;linear,.1;zoomx,2.2;zoomy,1;sleep,.3;linear,.2;zoomy,.2;diffusealpha,0);
	GoRankModeMessageCommand=cmd(stoptweening;visible,true;sleep,.1;diffusealpha,1;zoom,0;sleep,.25;linear,.1;zoomto,SCREEN_WIDTH,1;zoomy,1;sleep,.3;linear,.2;zoomy,.2;diffusealpha,0);
};

t[#t+1] = Def.Quad {
	InitCommand=cmd(stoptweening;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;zoomto,SCREEN_WIDTH,SCREEN_HEIGHT;diffuse,color("0,0,0,0.5");diffusealpha,0;visible,false);
	GoRankModeMessageCommand=cmd(stoptweening;visible,true;diffusealpha,0;sleep,.1;diffusealpha,1;linear,.1;diffusealpha,0;sleep,.05;diffusealpha,1;linear,.8;diffusealpha,0);
};

t[#t+1] = LoadActor("_GoRankMode/brillo_rm")..{
	InitCommand=cmd(stoptweening;blend,'BlendMode_Add';x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-70;zoom,.8;zoomx,.8;zoomy,0;visible,false);
	GoRankModeMessageCommand=cmd(stoptweening;visible,true;diffusealpha,1;zoomy,0;sleep,.25;linear,.05;zoomy,.8;sleep,.6;linear,.1;diffusealpha,0);
};

t[#t+1] = LoadActor("_GoRankMode/rm")..{
	InitCommand=cmd(stoptweening;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-70;zoom,.8;zoomx,.8;zoomy,0;visible,false);
	GoRankModeMessageCommand=cmd(stoptweening;visible,true;diffusealpha,1;zoomy,0;sleep,.25;linear,.05;zoomy,.8;sleep,.6;linear,.1;diffusealpha,0);
};

t[#t+1] = LoadActor("_GoFullMode/rayo1full")..{
	--Frames = Sprite.LinearFrames(2,.1);
	OnCommand=cmd(stoptweening;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-70;blend,'BlendMode_Add';zoom,0;visible,false);
	--GoFullModeMessageCommand=cmd(stoptweening;diffusealpha,1;zoom,0;sleep,.3;linear,.1;zoomx,2.2;zoomy,1;sleep,.3;linear,.2;zoomy,.2;diffusealpha,0);
	GoFullModeMessageCommand=cmd(stoptweening;visible,true;diffusealpha,1;zoom,0;sleep,.25;linear,.1;zoomto,SCREEN_WIDTH,1;zoomy,1;sleep,.3;linear,.2;zoomy,.2;diffusealpha,0);
};

t[#t+1] = LoadActor("_GoFullMode/rayo2full")..{
	--Frames = Sprite.LinearFrames(2,.1);
	OnCommand=cmd(stoptweening;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-70;blend,'BlendMode_Add';zoom,0;visible,false);
	--GoFullModeMessageCommand=cmd(stoptweening;diffusealpha,1;zoom,0;sleep,.3;linear,.1;zoomx,2.2;zoomy,1;sleep,.3;linear,.2;zoomy,.2;diffusealpha,0);
	GoFullModeMessageCommand=cmd(stoptweening;visible,true;sleep,.1;diffusealpha,1;zoom,0;sleep,.25;linear,.1;zoomto,SCREEN_WIDTH,1;zoomy,1;sleep,.3;linear,.2;zoomy,.2;diffusealpha,0);
};

t[#t+1] = Def.Quad {
	InitCommand=cmd(stoptweening;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;zoomto,SCREEN_WIDTH,SCREEN_HEIGHT;diffuse,color("0,0,0,0.5");diffusealpha,0;visible,false);
	GoFullModeMessageCommand=cmd(stoptweening;visible,true;diffusealpha,0;sleep,.1;diffusealpha,1;linear,.1;diffusealpha,0;sleep,.05;diffusealpha,1;linear,.8;diffusealpha,0);
};

t[#t+1] = LoadActor("_GoFullMode/brillo_fm")..{
	InitCommand=cmd(stoptweening;blend,'BlendMode_Add';x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-70;zoom,.8;zoomx,.8;zoomy,0;visible,false);
	GoFullModeMessageCommand=cmd(stoptweening;visible,true;diffusealpha,1;zoomy,0;sleep,.25;linear,.05;zoomy,.8;sleep,.6;linear,.1;diffusealpha,0);
};

t[#t+1] = LoadActor("_GoFullMode/fm")..{
	InitCommand=cmd(stoptweening;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-70;zoom,.8;zoomx,.8;zoomy,0;visible,false);
	GoFullModeMessageCommand=cmd(stoptweening;visible,true;diffusealpha,1;zoomy,0;sleep,.25;linear,.05;zoomy,.8;sleep,.6;linear,.1;diffusealpha,0);
};

t[#t+1] = LoadActor("_GoFullMode/FULLMODE")..{
	InitCommand=cmd(stop;pause);
	GoFullModeMessageCommand=cmd(play);
};

t[#t+1] = LoadActor("_GoFullMode/FULLMODEVOICE")..{
	InitCommand=cmd(stop;pause);
	GoFullModeMessageCommand=cmd(play);
};

--[[
---------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
t[#t+1] = LoadActor("bar_glow")..{
	OnCommand=cmd(stoptweening;x,SCREEN_CENTER_X;y,281;zoom,1.1;blend,'BlendMode_Add';diffusealpha,0);
	GoFullModeMessageCommand=cmd(stoptweening;diffusealpha,0;sleep,.25;linear,.1;diffusealpha,1;sleep,.2;linear,.1;diffusealpha,0);
}

--p1
t[#t+1] = LoadActor("stage_glow")..{
	OnCommand=cmd(stoptweening;x,SCREEN_CENTER_X-150;y,22;blend,'BlendMode_Add';diffusealpha,0);
	GoFullModeMessageCommand=cmd(stoptweening;diffusealpha,0;sleep,.25;linear,.1;diffusealpha,1;sleep,.2;linear,.1;diffusealpha,0);
}

if GAMESTATE:IsSideJoined(PLAYER_1) then
t[#t+1] = LoadActor("life_glow")..{
	OnCommand=cmd(stoptweening;x,SCREEN_CENTER_X-144;y,54;blend,'BlendMode_Add';diffusealpha,0);
	GoFullModeMessageCommand=cmd(stoptweening;diffusealpha,0;sleep,.25;linear,.1;diffusealpha,1;sleep,.2;linear,.1;diffusealpha,0);
}
end;

--p2
t[#t+1] = LoadActor("stage_glow")..{
	OnCommand=cmd(stoptweening;rotationy,180;x,SCREEN_CENTER_X+150;y,22;blend,'BlendMode_Add';diffusealpha,0);
	GoFullModeMessageCommand=cmd(stoptweening;diffusealpha,0;sleep,.25;linear,.1;diffusealpha,1;sleep,.2;linear,.1;diffusealpha,0);
}

if GAMESTATE:IsSideJoined(PLAYER_2) then
t[#t+1] = LoadActor("life_glow")..{
	OnCommand=cmd(stoptweening;rotationy,180;x,SCREEN_CENTER_X+144;y,54;blend,'BlendMode_Add';diffusealpha,0);
	GoFullModeMessageCommand=cmd(stoptweening;diffusealpha,0;sleep,.25;linear,.1;diffusealpha,1;sleep,.2;linear,.1;diffusealpha,0);
}
end;
]]

return t;
