--  Da prioridad a la carga de title's con el nombre
-- title.png o title.jpg. Si no existen alguno de estos, carga el background 
function GetPath()
	local song = GAMESTATE:GetCurrentSong();
	return song:GetBackgroundPath();
end;

return Def.ActorFrame {
	Def.Sprite {
		Texture = "_blank";

		InitCommand=cmd(Center);
		ChangeCourseSongOutMessageCommand=function(self)
			self:diffusealpha(1);
			self:Load(GetPath());
			self:scale_or_crop_background();
		end;
		FinishCommand=cmd(sleep,2;diffusealpha,0);
	};
};
