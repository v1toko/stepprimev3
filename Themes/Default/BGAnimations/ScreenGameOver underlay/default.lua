return Def.ActorFrame {
	LoadActor(THEME:GetPathG("","background/gameover")) .. {
		InitCommand = cmd(Center;zoomto,SCREEN_WIDTH,SCREEN_HEIGHT);
		OnCommand = cmd(stoptweening;sleep,4);
	};
	LoadActor(THEME:GetPathS("","ActionSounds/gameover")) .. {
		OnCommand=cmd(play);
	};
}