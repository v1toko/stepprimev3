return Def.ActorFrame {	
	InitCommand=function(self)
		local fns = THEME:GetMetric('Common','DefaultNoteSkinName');	
		GAMESTATE:ApplyPreferredModifiers(PLAYER_1,fns);
		GAMESTATE:ApplyPreferredModifiers(PLAYER_2,fns);
	end;
	LoadActor( THEME:GetPathB("ScreenGamePlay","overlay") );
	--[[
	Def.Quad {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_BOTTOM-45;zoomto,SCREEN_WIDTH,30;diffuse,0,0,0,.8);
	};
	LoadFont("BPM")..{
		OnCommand=function(self)		
		local title = GAMESTATE:GetCurrentSong():GetDisplayMainTitle();
		local artist = GAMESTATE:GetCurrentSong():GetDisplayArtist();
		self:settext(title.." - "..artist);
		(cmd(stoptweening;zoom,.47;zoomy,.44;shadowlength,0;x,SCREEN_CENTER_X;y,SCREEN_BOTTOM-45))(self);
		end;
	};
	]]
};