local t = Def.ActorFrame {}

local Player_1 = 'PlayerNumber_P1';
local Player_2 = 'PlayerNumber_P2';

local function PlayerMessage( pn )
	local xpos = {122, 122, 210, 242};	-- [back, text, lim, timer] positions 
	local horz;
	local factor; 
	
	if pn == Player_1 then
		horz = 'HorizAlign_Left';
		factor = -3;
	elseif pn == Player_2 then
		xpos = { SCREEN_WIDTH-xpos[1],SCREEN_WIDTH-xpos[2],SCREEN_WIDTH-xpos[3], SCREEN_WIDTH-xpos[4] }; 
		horz = 'HorizAlign_Right';
		factor = 3;
	else return;
	end;
		
	local t = Def.ActorFrame {
		LoadActor(THEME:GetPathG("","Messages/spinner"))..{
			InitCommand=cmd(x,xpos[2];y,SCREEN_HEIGHT-64;zoom,.75;diffusealpha,0;pause;animate,0);
			OnCommand=cmd(sleep,.1;linear,.1;diffusealpha,0); 
			ProfileAlreadyLoadedMessageCommand=function(self,params)
				if params.Player == pn then
					(cmd(diffusealpha,0))(self);
				end;
			end;
			StorageDevicesChangedMessageCommand=function(self)

				if MEMCARDMAN:GetCardState(pn) == 'MemoryCardState_checking' then
					(cmd(play;animate,1;setstate,0;diffusealpha,1))(self);
				end;

				if MEMCARDMAN:GetCardState(pn) == 'MemoryCardState_ready' then
					(cmd(pause;animate,0;setstate,0;diffusealpha,0))(self);					
				end;
			end;
		};

		LoadActor(THEME:GetPathG("","Messages/back"))..{
			InitCommand=cmd(x,xpos[1]*factor;y,SCREEN_HEIGHT-64);
			OnCommand=cmd(linear,.4;x,xpos[1];zoom,.75);
			ProfileAlreadyLoadedMessageCommand=function(self,params)
				if params.Player == pn then
					(cmd(diffusealpha,0))(self);
				end;
			end;
			StorageDevicesChangedMessageCommand=function(self)

				if MEMCARDMAN:GetCardState(pn) == 'MemoryCardState_checking' then
					(cmd(diffusealpha,0))(self);
				end;			
			end;
		};

		LoadActor(THEME:GetPathG("","Messages/text"))..{
			InitCommand=cmd(x,xpos[1]*factor;y,SCREEN_HEIGHT-64);
			OnCommand=cmd(linear,.4;x,xpos[1];zoom,.75);
			ProfileAlreadyLoadedMessageCommand=function(self,params)
				if params.Player == pn then
					(cmd(diffusealpha,0))(self);
				end;
			end;
			StorageDevicesChangedMessageCommand=function(self)

				if MEMCARDMAN:GetCardState(pn) == 'MemoryCardState_checking' then
					(cmd(diffusealpha,0))(self);
				end;				
			end;
		};
	}; 
	return t; 
end;


t[#t+1] = Def.ActorFrame{
	-- Si inicia el juego, y las memorias estan colocadas, se unen los jugadores
	--[[
	BeginCommand=function(self)
		if MEMCARDMAN:GetCardState('PlayerNumber_P1') == 'MemoryCardState_ready' then
			--GAMESTATE:JoinPlayer('PlayerNumber_P1'); 
			--SCREENMAN:GetTopScreen():SetProfileIndex('PlayerNumber_P1', 0);			
		end;

		if MEMCARDMAN:GetCardState('PlayerNumber_P2') == 'MemoryCardState_ready' then
			--GAMESTATE:JoinPlayer('PlayerNumber_P2'); 
			--SCREENMAN:GetTopScreen():SetProfileIndex('PlayerNumber_P2', 0);
		end;
	end;
	]]

	-- Mensajes de que jugador se unio al juego
	PlayerJoinedMessageCommand=function( self, params )
		if (params.Player == 'PlayerNumber_P1') then
			MESSAGEMAN:Broadcast('Player1Joined'); end;
		if (params.Player == 'PlayerNumber_P2') then
			MESSAGEMAN:Broadcast('Player2Joined'); end;
	end;

	-- USB conected !
	--[[
	StorageDevicesChangedMessageCommand=function(self)

		-- Si se conecto la memory y esta lista para el 'PlayerNumber_P1', se inicia el juego
		if MEMCARDMAN:GetCardState('PlayerNumber_P1') == 'MemoryCardState_ready' then
			--GAMESTATE:JoinPlayer('PlayerNumber_P1');
			--SCREENMAN:GetTopScreen():SetProfileIndex('PlayerNumber_P1', 0); -- para usar USB
			--self:sleep(2); --self:queuecommand('Continue'); --delay
		end;
		if MEMCARDMAN:GetCardState('PlayerNumber_P2') == 'MemoryCardState_ready' then
			--GAMESTATE:JoinPlayer('PlayerNumber_P2');
			--SCREENMAN:GetTopScreen():SetProfileIndex('PlayerNumber_P1', 0); -- para usar USB
			--self:sleep(0.2); --self:queuecommand('Continue'); --delay
		end;
	end;
	]]

	ContinueCommand = function(self)
		SCREENMAN:GetTopScreen():Finish();
	end;
}

t[#t+1] = LoadActor( THEME:GetPathS("","titlescreen_start") )..{
	FinishSelectProfileMessageCommand=function (self) 
		self:play();		
		self:queuecommand('Continue'); --delay
	end;

	ContinueCommand = function(self)
		SCREENMAN:GetTopScreen():Finish();
	end;
};

t[#t+1] = LoadActor( THEME:GetPathG("", "online") ) .. {
		InitCommand=cmd(animate,0;pause;x,SCREEN_CENTER_X;y,SCREEN_HEIGHT-80;setstate,0);
		OnCommand=function(self)
			if IsNetSMOnline() then self:setstate(1); end;
		end;
};

--1st Player
t[#t+1] = PlayerMessage(Player_1)..{ FinishSelectProfileMessageCommand=cmd(zoom,0); };	--Si se une al juego el Player1, no mostrar mas 
t[#t+1] = PlatPiu(SCREEN_LEFT+120,SCREEN_HEIGHT-133, 0.8) .. { FinishSelectProfileMessageCommand=cmd(zoom,0); };	--ThemeUtils

--2nd Player
t[#t+1] = PlayerMessage(Player_2)..{ FinishSelectProfileMessageCommand=cmd(zoom,0); };	--Si se une al juego el Player1, no mostrar mas
t[#t+1] = PlatPiu(SCREEN_WIDTH-120,SCREEN_HEIGHT-133, 0.8) .. { FinishSelectProfileMessageCommand=cmd(zoom,0); };	--ThemeUtils

t[#t+1] = LoadActor("profiles.lua");

return t