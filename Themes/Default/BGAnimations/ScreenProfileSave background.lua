return Def.ActorFrame {
	LoadActor(THEME:GetPathG("","background".."/USB_BACK"))..{
		InitCommand=cmd(FullScreen);
		IncomingLogoMessageCommand=cmd(stoptweening;decelerate,.3;zoomto,SCREEN_WIDTH+50,SCREEN_HEIGHT+30);
	};
	Def.Quad {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;zoomto,SCREEN_WIDTH,SCREEN_HEIGHT;diffuse,0,0,0,1;diffusealpha,.9);
	};
};
