local t = Def.ActorFrame {};

if GAMESTATE:IsPlayerEnabled(PLAYER_1) then
	t[#t+1] = Def.ActorFrame {
		InitCommand=cmd(x,200;y,SCREEN_CENTER_Y);
		--InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y);
		BeginCommand=function(self)
			self:sleep(10);
			self:visible(true);	
			if SCREENMAN:GetTopScreen():HaveProfileToSave() then
				self:visible(true);
				self:sleep(10);
			end;
			self:queuecommand("Save");
		end;
		SaveCommand=function(self)
			SCREENMAN:GetTopScreen():Continue();
		end;
		children = {
			LoadActor("usb_back")..{
				InitCommand=cmd(zoom,.75);
			};     

			LoadFont("_karnivore lite 20px") .. {
				InitCommand=cmd(horizalign,'HorizAlign_Right';addx,120;addy,-95;);
				OnCommand=function(self)
					local p_exp = PROFILEMAN:GetProfile( PLAYER_1 ):GetExperience();
					local g_exp = STATSMAN:GetAccumPlayedStageStats():GetPlayerStageStats(PLAYER_1):GetExperience();
					self:settext(string.format( "%03d + %05d", g_exp, p_exp-g_exp ));
				end;
			};

			LoadFont("_karnivore lite 20px") .. {
				InitCommand=cmd(horizalign,'HorizAlign_Right';addx,120;addy,-72;);
				OnCommand=function(self)
					local p_pp = PROFILEMAN:GetProfile( PLAYER_1 ):GetPiuPoints();
					local g_pp = STATSMAN:GetAccumPlayedStageStats():GetPlayerStageStats(PLAYER_1):GetPiuPoints();
					self:settext(string.format( "%03d + %05d", g_pp, p_pp-g_pp));
				end;
			};

			LoadFont("_karnivore lite 20px") .. {
				InitCommand=cmd(settext,string.format( "%07d", PROFILEMAN:GetProfile( PLAYER_1 ):GetSongsActualScore("StepsType_Pump_Single") );horizalign,'HorizAlign_Right';addx,120;addy,-18;);
			};

			LoadFont("_karnivore lite 20px") .. {
				InitCommand=cmd(settext,string.format( "%07d", PROFILEMAN:GetProfile( PLAYER_1 ):GetSongsActualScore("StepsType_Pump_Double") );horizalign,'HorizAlign_Right';addx,120;addy,5;);
			};

			LoadFont("_karnivore lite 20px") .. {
				InitCommand=cmd(settext,string.format( "%07d", PROFILEMAN:GetProfile( PLAYER_1 ):GetSongsActualScore("StepsType_Pump_Single")+PROFILEMAN:GetProfile( PLAYER_1 ):GetSongsActualScore("StepsType_Pump_Double") );horizalign,'HorizAlign_Right';addx,120;addy,28;);
			};

			LoadFont("_karnivore lite 20px") .. {
				InitCommand=cmd(settext,string.format( "%07d", PROFILEMAN:GetProfile( PLAYER_1 ):GetTotalTapsAndHolds() );horizalign,'HorizAlign_Right';addx,120;addy,63;);
			};

			LoadFont("_karnivore lite 20px") .. {
				InitCommand=cmd(settext,string.format( "%.3f", PROFILEMAN:GetProfile( PLAYER_1 ):GetTotalCaloriesBurned() );horizalign,'HorizAlign_Right';addx,120;addy,86;);
			};

			LoadFont("_karnivore lite 20px") .. {
				InitCommand=cmd(settext, string.format( "%05d", PROFILEMAN:GetProfile( PLAYER_1 ):GetTotalSessions() );horizalign,'HorizAlign_Right';addx,120;addy,108;);
			};
		};
	};
end

if GAMESTATE:IsPlayerEnabled(PLAYER_2) then
	t[#t+1] = Def.ActorFrame {
		InitCommand=cmd(x,SCREEN_RIGHT-200;y,SCREEN_CENTER_Y);
		--InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y);
		BeginCommand=function(self)
			self:sleep(10);
			self:visible(true);	
			if SCREENMAN:GetTopScreen():HaveProfileToSave() then
				self:visible(true);
				self:sleep(10);
			end;
			self:queuecommand("Save");
		end;
		SaveCommand=function(self)
			SCREENMAN:GetTopScreen():Continue();
		end;
		children = {
			LoadActor("usb_back")..{
				InitCommand=cmd(zoom,.75);
			};     

			LoadFont("_karnivore lite 20px") .. {
				InitCommand=cmd(horizalign,'HorizAlign_Right';addx,120;addy,-95;);
				OnCommand=function(self)
					local p_exp = PROFILEMAN:GetProfile( PLAYER_2 ):GetExperience();
					local g_exp = STATSMAN:GetAccumPlayedStageStats():GetPlayerStageStats(PLAYER_2):GetExperience();
					self:settext(string.format( "%03d + %05d", g_exp, p_exp-g_exp ));
				end;
			};

			LoadFont("_karnivore lite 20px") .. {
				InitCommand=cmd(horizalign,'HorizAlign_Right';addx,120;addy,-72;);
				OnCommand=function(self)
					local p_pp = PROFILEMAN:GetProfile( PLAYER_2 ):GetPiuPoints();
					local g_pp = STATSMAN:GetAccumPlayedStageStats():GetPlayerStageStats(PLAYER_2):GetPiuPoints();
					self:settext(string.format( "%03d + %05d", g_pp, p_pp-g_pp));
				end;
			};

			LoadFont("_karnivore lite 20px") .. {
				InitCommand=cmd(settext,string.format( "%07d", PROFILEMAN:GetProfile( PLAYER_2 ):GetSongsActualScore("StepsType_Pump_Single") );horizalign,'HorizAlign_Right';addx,120;addy,-18;);
			};

			LoadFont("_karnivore lite 20px") .. {
				InitCommand=cmd(settext,string.format( "%07d", PROFILEMAN:GetProfile( PLAYER_2 ):GetSongsActualScore("StepsType_Pump_Double") );horizalign,'HorizAlign_Right';addx,120;addy,5;);
			};

			LoadFont("_karnivore lite 20px") .. {
				InitCommand=cmd(settext,string.format( "%07d", PROFILEMAN:GetProfile( PLAYER_2 ):GetSongsActualScore("StepsType_Pump_Single")+PROFILEMAN:GetProfile( PLAYER_2 ):GetSongsActualScore("StepsType_Pump_Double") );horizalign,'HorizAlign_Right';addx,120;addy,28;);
			};

			LoadFont("_karnivore lite 20px") .. {
				InitCommand=cmd(settext,string.format( "%07d", PROFILEMAN:GetProfile( PLAYER_2 ):GetTotalTapsAndHolds() );horizalign,'HorizAlign_Right';addx,120;addy,63;);
			};

			LoadFont("_karnivore lite 20px") .. {
				InitCommand=cmd(settext,string.format( "%.3f", PROFILEMAN:GetProfile( PLAYER_2 ):GetTotalCaloriesBurned() );horizalign,'HorizAlign_Right';addx,120;addy,86;);
			};

			LoadFont("_karnivore lite 20px") .. {
				InitCommand=cmd(settext, string.format( "%05d", PROFILEMAN:GetProfile( PLAYER_2 ):GetTotalSessions() );horizalign,'HorizAlign_Right';addx,120;addy,108;);
			};
		};
	};
end

return t;