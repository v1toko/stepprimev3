
local s = Def.ActorFrame {};

local missions = SONGMAN:GetSectorNames()

for i = 1, #missions do
	local name = missions[i]
	name = string.lower( name )
	s[#s+1] = Def.ActorFrame {
		LoadActor( name ) .. {
			Name=name;
			SetCommand=function(self)
				local nameCurMap = SONGMAN:GetSectorNames()
				local index = SCREENMAN:GetTopScreen():GetCurrentMapIndex()
				local mapName = self:GetName()
				
				Warn( "nameCurMap: " .. nameCurMap[index+1] .. " MapName: " .. mapName .. " index: " .. tostring(index+1))
				
				if string.lower(nameCurMap[index+1]) == mapName then
					self:visible( true )
				else
					self:visible( false )
				end
			end;
			
			OnCommand = cmd(diffusealpha,1;zoom,0.5;addx,500;sleep,0.3;queuecommand,"Set";accelerate,0.3;addx,-500);
			OffCommand=cmd(linear,0.3;addx,500);
			CurrentMissionChangedMessageCommand=cmd(queuecommand,"Set");
			MapMovedMessageCommand=cmd(queuecommand,"Set");
			SwitchStateChangedMessageCommand=cmd(queuecommand,"Set");
		};
	};
end

--minimap con switch
s[#s+1] = Def.ActorFrame {
	LoadActor( "earthSwitch" ) .. {
		Name="earth";
		SetCommand=function(self)
			local nameCurMap = SONGMAN:GetSectorNames()
			local index = SCREENMAN:GetTopScreen():GetCurrentMapIndex()
			local mapName = self:GetName()
			
			--preguntamos si el switch "Switch 1" est� activo,
			--modificar dependiendo de lo que requeramos
			local bSwitchAct = SCREENMAN:GetTopScreen():GetSwitchActive( "Switchearth" )
			
			--Trace( "nameCurMap: " .. nameCurMap[index+1] .. " MapName: " .. mapName .. " index: " .. tostring(index+1))
			
			if string.lower(nameCurMap[index+1]) == mapName and bSwitchAct then
				self:visible( true )
			else
				self:visible( false )
			end
		end;
		
		OnCommand = cmd(diffusealpha,1;zoom,0.5;addx,500;sleep,0.3;queuecommand,"Set";accelerate,0.3;addx,-500);
		OffCommand=cmd(linear,0.3;addx,500);
		CurrentMissionChangedMessageCommand=cmd(queuecommand,"Set");
		MapMovedMessageCommand=cmd(queuecommand,"Set");
		SwitchStateChangedMessageCommand=cmd(queuecommand,"Set");
	};
};
			
return s