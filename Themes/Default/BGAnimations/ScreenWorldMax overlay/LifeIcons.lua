local bNoEnoughStages = false

return Def.ActorFrame {
	--TODO: terminar el lifeicon!
	LoadActor ("LifeIcon" ) .. {
		
		SetCommand=function(self)
			local song = GAMESTATE:GetCurrentSong()
			local long = GAMESTATE:GetCurrentSong():IsLong()
			local marathon = GAMESTATE:GetCurrentSong():IsMarathon()
			local normal = not long and not marathon
			
			if not song then
				self:hidden(1)
				return
			end
			
			local sStage1 = "" --GAMESTATE:GetFirstStageString()
			local sStage2 = "" --GAMESTATE:GetSecondStageString()
			local sStage3 = "" --GAMESTATE:GetThirdStageString()
			
			self:horizalign('HorizAlign_Left')						
			self:hidden(0)
			
			if sStage1 == "" then --primera stage!
				if normal then
					self:cropright( 0.33 )
				elseif long then
					self:cropright( 0.66 )
				elseif marathon then
					self:cropright( 1 )
				end
			end
			
			if sStage2 == "" and sStage1 == "Normal" then
				if normal then
					self:cropright( 0.66 )
				elseif long then					
					self:cropright( 1 )
				elseif marathon then
					self:cropright( 1 )
				end
			elseif sStage2 == "" and sStage1 == "Remix" then
				if normal then
					self:cropright( 1 )
				elseif long then
					self:cropright( 1 )
				elseif marathon then
					self:cropright( 1 )
				end
			elseif sStage2 == "" and sStage1 == "Full" then
				self:cropright( 1 )
			end
			
			if sStage3 == "" and sStage2 == "Normal" then
				if normal then
					self:cropright( 1 )
				elseif long then
					self:cropright( 1 )
				elseif marathon then
					self:cropright( 1 )
				end
			elseif sStage3 == "" and sStage2 == "Remix" then
				self:cropright( 1 )
			elseif sStage3 == "" and sStage2 == "Full" then
				self:cropright( 1 )
			end
		end;

 		OnCommand=cmd(y,267;x,93;addx,-500;sleep,0.3;accelerate,0.3;addx,500);
		OffCommand=cmd(linear,0.3;addx,-500);	
		ShowCommand=cmd(diffuseshift,1;effectperiod,1;effectcolor1,1,1,1,0;effectcolor2,1,1,1,1);

		CurrentSongChangedMessageCommand=cmd(playcommand,"Set");
 	};
	
 	LoadActor ("LifeIcon" ) .. {
		
		SetCommand=function(self)
			local song = GAMESTATE:GetCurrentSong()
			local long = GAMESTATE:GetCurrentSong():IsLong()
			local marathon = GAMESTATE:GetCurrentSong():IsMarathon()
			local normal = not long and not marathon
			
			if not song then
				self:hidden(1)
				return
			end
			
			local sStage1 = "" --GAMESTATE:GetFirstStageString()
			local sStage2 = "" --GAMESTATE:GetSecondStageString()
			local sStage3 = "" --GAMESTATE:GetThirdStageString()
			
			self:horizalign('HorizAlign_Left')						
			self:playcommand("Show")
			self:hidden(0)
			bNoEnoughStages = false
			
			if sStage1 == "" then --primera stage!
				if normal then
					self:cropleft( 0.66 )
				elseif long then
					self:cropleft( 0.33 )
				elseif marathon then
					self:cropleft( 0 )
				end
			end
			
			if sStage2 == "" and sStage1 == "Normal" then
				if normal then
					self:cropleft( 0.33 )
					self:cropright( 0.33 )
				elseif long then
					self:cropleft( 0 )
					self:cropright( 0.33 )
				elseif marathon then
					self:hidden( 1 )
					bNoEnoughStages = true
				end
			elseif sStage2 == "" and sStage1 == "Remix" then
				if normal then
					self:cropleft( 0 )
					self:cropright( 0.66 )
				elseif long then
					self:hidden( 1 )
					bNoEnoughStages = true
				elseif marathon then
					self:hidden( 1 )
					bNoEnoughStages = true
				end
			elseif sStage2 == "" and sStage1 == "Full" then
				self:hidden( 1 )
				bNoEnoughStages = true
			end
			
			if sStage3 == "" and sStage2 == "Normal" then
				if normal then
					self:cropleft( 0 )
					self:cropright( 0.66 )
				elseif long then
					self:hidden( 1 )
					bNoEnoughStages = true
				elseif marathon then
					self:hidden( 1 )
					bNoEnoughStages = true
				end
			elseif sStage3 == "" and sStage2 == "Remix" then
				self:hidden( 1 )
				bNoEnoughStages = true
			elseif sStage3 == "" and sStage2 == "Full" then
				self:hidden( 1 )
				bNoEnoughStages = true
			end
			
			MESSAGEMAN:Broadcast( "SetLifeIcons" )
			
			--Trace( "Stages 1:" .. sStage1 .. " 2:" .. sStage2 )
		end;

 		OnCommand=cmd(y,267;x,93;addx,-500;sleep,0.3;accelerate,0.3;addx,500);
		OffCommand=cmd(linear,0.3;addx,-500);	
		ShowCommand=cmd(diffuseshift,1;effectperiod,1;effectcolor1,1,1,1,0;effectcolor2,1,1,1,1);

		CurrentSongChangedMessageCommand=cmd(playcommand,"Set");
 	};
	
	LoadActor ("LifeIconNoSongs" ) .. {
		SetLifeIconsMessageCommand=function(self)
			self:hidden( 1 )
			if bNoEnoughStages then
				self:hidden( 0 )
			end
		end;

 		OnCommand=cmd(y,267;x,130;addx,-500;sleep,0.3;accelerate,0.3;addx,500);
		OffCommand=cmd(linear,0.3;addx,-500);	
 	};

};