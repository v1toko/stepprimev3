return Def.ActorFrame {
	LoadActor( "part" ) .. {
		OnCommand=cmd();
		ItemUsedMessageCommand=cmd(play);
	};
	
	LoadActor( "Portal" ) .. {
		OnCommand=cmd(zoom,1;diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y);
		EnteringCodeMessageCommand=cmd(diffusealpha,1;addy,-400;linear,0.3;addy,400);
		FinishedCodeMessageCommand=cmd(sleep,1;diffusealpha,0;);
	};
	
	LoadActor( "UpLeft" ) .. {
		OnCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X-72;y,SCREEN_CENTER_Y-72;zoom,1.2);
		UpLeftItemUsedMessageCommand=cmd(sleep,0;diffusealpha,1;linear,0.2;zoom,0.8);
		FinishedCodeMessageCommand=cmd(linear,0.5;diffusealpha,0);
	};
	
	LoadActor( "ball" ) .. {
		OnCommand=cmd(blend,"BlendMode_Add";diffusealpha,0;x,SCREEN_CENTER_X-72;y,SCREEN_CENTER_Y-72;zoom,1);
		UpLeftItemUsedMessageCommand=cmd(sleep,0.2;diffusealpha,0.5;linear,0.1;zoom,1.2;diffusealpha,0);
	};
	
	--
	
	LoadActor( "UpRight" ) .. {
		OnCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X+73;y,SCREEN_CENTER_Y-72;zoom,1.2);
		UpRightItemUsedMessageCommand=cmd(sleep,0;diffusealpha,1;linear,0.2;zoom,0.8);
		FinishedCodeMessageCommand=cmd(linear,0.5;diffusealpha,0);
	};
	
	LoadActor( "ball" ) .. {
		OnCommand=cmd(blend,"BlendMode_Add";diffusealpha,0;x,SCREEN_CENTER_X+73;y,SCREEN_CENTER_Y-72;zoom,1);
		UpRightItemUsedMessageCommand=cmd(sleep,0.2;diffusealpha,0.5;linear,0.1;zoom,1.2;diffusealpha,0);
	};
	
	--
	
	LoadActor( "DownLeft" ) .. {
		OnCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X-72;y,SCREEN_CENTER_Y+71;zoom,1.2);
		DownLeftItemUsedMessageCommand=cmd(sleep,0;diffusealpha,1;linear,0.2;zoom,0.8);
		FinishedCodeMessageCommand=cmd(linear,0.5;diffusealpha,0);
	};
	
		LoadActor( "ball" ) .. {
		OnCommand=cmd(blend,"BlendMode_Add";diffusealpha,0;x,SCREEN_CENTER_X-72;y,SCREEN_CENTER_Y+71;zoom,1);
		DownLeftItemUsedMessageCommand=cmd(sleep,0.2;diffusealpha,0.5;linear,0.1;zoom,1.2;diffusealpha,0);
	};
	
	--
		
	LoadActor( "DownRight" ) .. {
		OnCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X+73;y,SCREEN_CENTER_Y+71;zoom,1.2);
		DownRightItemUsedMessageCommand=cmd(sleep,0;diffusealpha,1;linear,0.2;zoom,0.8);
		FinishedCodeMessageCommand=cmd(linear,0.5;diffusealpha,0);
	};
	
	LoadActor( "ball" ) .. {
		OnCommand=cmd(blend,"BlendMode_Add";diffusealpha,0;x,SCREEN_CENTER_X+73;y,SCREEN_CENTER_Y+71;zoom,1);
		DownRightItemUsedMessageCommand=cmd(sleep,0.2;diffusealpha,0.5;linear,0.1;zoom,1.2;diffusealpha,0);
	};
	
	--
	
	LoadActor( "Center" ) .. {
		OnCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X+1;y,SCREEN_CENTER_Y;zoom,1.3);
		CenterItemUsedMessageCommand=cmd(sleep,0;diffusealpha,1;linear,0.2;zoom,0.9);
		FinishedCodeMessageCommand=cmd(linear,0.5;diffusealpha,0);
	};
	
	LoadActor( "ball" ) .. {
		OnCommand=cmd(blend,"BlendMode_Add";diffusealpha,0;x,SCREEN_CENTER_X+1;y,SCREEN_CENTER_Y;zoom,1);
		CenterItemUsedMessageCommand=cmd(sleep,0.2;diffusealpha,0.5;linear,0.1;zoom,1.2;diffusealpha,0);
	};
	
	--	
	
 	LoadActor( "PortalDone" ) .. {
		OnCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;diffusealpha,0);
		FinishedCodeMessageCommand=cmd(blend,"BlendMode_Add";diffusealpha,0;zoom,1;linear,1;diffusealpha,1;zoom,1.1;linear,0.1;diffusealpha,0;zoom,1);
	};
	
	LoadActor( "PortalLight" ) .. {
		OnCommand=cmd(diffusealpha,0;zoom,0.75;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y);
		FinishedCodeMessageCommand=cmd(blend,"BlendMode_Add";diffusealpha,0;sleep,0.5;linear,0.5;diffusealpha,1;zoom,1.5;linear,0.2;diffusealpha,0;zoom,0.7);
	};
	

};