local t = Def.ActorFrame {

	LoadActor( "Code" ) .. {
		OnCommand=cmd();
		WMCodeEnteredMessageCommand=cmd( play );
	};
	
	Def.Sprite {
		OnCommand=cmd(pause;diffusealpha,0;x,SCREEN_CENTER_X;y,170;zoom,2);
		MineActivatedMessageCommand=cmd(sleep,1.5;play;diffusealpha,1;sleep,0.5;queuecommand,"Stop");
		PathFailedMessageCommand=cmd(play;diffusealpha,1;sleep,0.5;queuecommand,"Stop");
		StopCommand=cmd(pause;playcommand,"On");
		Texture="HitMine Explosion";
		Frames=Sprite.LinearFrames( 64, 1 );
	};
	
	
	LoadActor ( "Banner" ) .. {
		OnCommand=cmd(x,SCREEN_CENTER_X;y,19;addy,-150;linear,0.4;addy,150);
		OffCommand=cmd(linear,1.5;addy,-150);
	};

	LoadActor ( "Time" ) .. {
		OnCommand=cmd(x,SCREEN_CENTER_X;y,20;addy,-150;linear,0.4;addy,150);
		OffCommand=cmd(linear,1.5;addy,-150);
	};
	
	LoadActor( "Items.lua" ) .. {
		OnCommand=cmd();
	};

 	LoadActor ("ControlPanel" ) .. {
 		OnCommand=cmd(x,208;y,387;addx,-500;sleep,0.3;accelerate,0.3;addx,500;zoom,1);
		OffCommand=cmd(linear,0.3;addx,-500);		
 	};
	
	LoadActor ("Info" ) .. {
 		OnCommand=cmd(x,344;y,399;diffusealpha,0;zoom,1;sleep,0.7;linear,0.3;diffusealpha,1;);
		OffCommand=cmd(linear,0.3;addx,-500);		
 	};
	
	LoadActor ("SongPanel" ) .. {
 		OnCommand=cmd(x,SCREEN_RIGHT-110;y,383;addx,500;sleep,0.3;accelerate,0.3;addx,-500;zoom,1);
		OffCommand=cmd(linear,0.3;addx,150);	
 	};
	
	LoadActor("Credit.png") .. {
 		OnCommand=cmd(y,465;x,322;addy,100;sleep,0.2;accelerate,0.4;addy,-100;zoom,1);
		OffCommand=cmd(linear,0.5;addy,100);		
 	};

 	LoadActor ("Level.png" ) .. {
		ColorCommand=cmd( diffuse,missionmetercolor(); cropright, missionmetercropright() );
 		OnCommand=cmd(y,386;x,374;diffusealpha,0;zoom,1;sleep,0.7;linear,0.3;diffusealpha,1;playcommand, "Color");
		OffCommand=cmd(linear,0.3;addx,-500);
		CurrentMissionChangedMessageCommand=cmd(playcommand,"Color");
 	};
	
	LoadActor("Levelblank.png") .. {
 		OnCommand=cmd(y,386;x,374;diffusealpha,0;zoom,1;sleep,0.7;linear,0.3;diffusealpha,1;);
		OffCommand=cmd(linear,0.3;addx,-500);		
 	};

 	LoadActor ("LifePanel" ) .. {
 		OnCommand=cmd(y,268;x,90;addx,-500;sleep,0.3;accelerate,0.3;addx,500);
		OffCommand=cmd(linear,0.3;addx,-500);
 	};

	LoadActor( "LifeIcons.lua" ) .. {
		OnCommand=cmd();
	};
	
	LoadActor("Minimaps/Minimaps.lua") .. {
		OnCommand=cmd(x,SCREEN_RIGHT-111;y,369;);
	};
	
	LoadActor ("minimaparrow" ) .. {
		MapMovedMessageCommand=function(self,param)
			local posX = param.PosX;
			local posY = param.PosY;
			
			posX = scale(posX,0,4608*1,0,196);
			posY = scale(posY,0,3328*1,0,142);
			
			self:finishtweening( 1 );
			self:linear( 0.3 );
			if posX ~= 0 then self:x( SCREEN_RIGHT-110-posX ); end;
			if posY ~= 0 then self:y( SCREEN_CENTER_Y+118-posY ); end;
		end;
 		InitCommand=cmd(x,SCREEN_RIGHT-110;y,SCREEN_CENTER_Y+150;bounce;effectperiod,0.5;effectmagnitude,0,-10,0);
		OffCommand=cmd(linear,0.3;addx,150);
 	};

	LoadFont("Special/_karnivore lite 20px") .. {
		SetCommand=function(self)
			local mission = GAMESTATE:GetCurrentMission():GetMissionLand();

			if not mission then
				self:visible(false)
				return
			end

			self:visible(true)
			self:horizalign('HorizAlign_Left')

			mission = string.upper( mission )

			if mission then
				self:settext( mission );
			else
				self:settext( "No Mission" );
			end
		end;

		InitCommand=cmd(shadowlength,0;diffusealpha,0;zoom,0.7;maxwidth,450);
		OnCommand=cmd(x,40;y,320;sleep,0.7;linear,0.3;diffusealpha,1;);
		OffCommand=cmd(linear,0.3;addx,-500);	

		CurrentMissionChangedMessageCommand=cmd(playcommand,"Set");
	};

	LoadFont( "Special/_karnivore lite 20px" ) .. {
		SetCommand=function(self)
			local mission = GAMESTATE:GetCurrentMission():GetMissionName();
			local bwarp = GAMESTATE:GetCurrentMission():IsWarp();
			local bwarptowarp = GAMESTATE:GetCurrentMission():IsWarpToWarp();
			local bswitch = GAMESTATE:GetCurrentMission():IsSwitch();
			--local bvisibleline = GAMESTATE:GetCurrentMission():IsvisibleLine();

			if not mission then
				self:visible(false)
				return
			end
			self:visible(true)
			self:horizalign('HorizAlign_Left')
			
			if bwarp then
				self:settext( "WARP" )
				return
			elseif bwarptowarp then
				self:settext( "Mystic Portal" )
				return
			elseif bswitch then
				self:settext( "SWITCH" )
				return
			--elseif bvisibleline then
			--	self:settext( "-visible Line-" )
			--	return
			end

			if mission then
				self:settext( mission );
			else
				self:settext( "No Mission" );
			end
		end;

		InitCommand=cmd(shadowlength,2;diffusealpha,0;zoom,0.6;maxwidth,900);
		OnCommand=cmd(x,40;y,345;sleep,0.7;linear,0.3;diffusealpha,1;);
		OffCommand=cmd(linear,0.3;addx,-500);		

		CurrentMissionChangedMessageCommand=cmd(sleep,.1;queuecommand,"Set");
	};

	LoadFont( "Special/_karnivore lite 20px" ) .. {
		SetCommand=function(self)
			local mission = GAMESTATE:GetCurrentSong():GetDisplayMainTitle();
			local bwrap = GAMESTATE:GetCurrentMission():IsWarp() or GAMESTATE:GetCurrentMission():IsWarpToWarp() or GAMESTATE:GetCurrentMission():IsSwitch(); -- or GAMESTATE:GetCurrentMission():IsvisibleLine();

			if not mission then
				self:visible(false)
				return
			end

			if bwrap then
				self:visible(false)
				return
			end

			self:visible(true)
			self:horizalign('HorizAlign_Left')
			self:vertalign('VertAlign_Top')

			if mission then
				self:settext( mission );
			else
				self:settext( "No Mission" );
			end
		end;

		InitCommand=cmd(shadowlength,1.4;diffusealpha,0;zoom,0.35;wrapwidthpixels,700;);
		OnCommand=cmd(x,40;y,360;sleep,0.7;linear,0.3;diffusealpha,1;);
		OffCommand=cmd(linear,0.3;addx,-500);	

		CurrentSongChangedMessageCommand=cmd(sleep,.1;queuecommand,"Set");
		CurrentMissionChangedMessageCommand=cmd(sleep,.1;queuecommand,"Set" );
	};
	
	LoadFont("Special/_karnivore lite 20px") .. {
		SetCommand=function(self)
			local mission = GAMESTATE:GetCurrentMission():GetDescription() or GAMESTATE:GetCurrentMission():IsWarpToWarp();
			local bwrap = GAMESTATE:GetCurrentMission():IsWarp() or GAMESTATE:GetCurrentMission():IsSwitch(); -- or GAMESTATE:GetCurrentMission():IsvisibleLine();
			if not mission then
				self:visible(false)
				return
			end
			
			if bwrap then
				self:visible(false)
				return
			end
			
			self:visible(true)
			self:horizalign('HorizAlign_Left')
			self:vertalign('VertAlign_Top')
			
			if mission then
				self:settext( mission );
			else
				self:settext( "No Mission" );
			end
		end;

		InitCommand=cmd(shadowlength,1.4;diffusealpha,0;zoom,0.35;wrapwidthpixels,700;);
		OnCommand=cmd(x,40;y,382;sleep,0.7;linear,0.3;diffusealpha,1;);
		OffCommand=cmd(linear,0.3;addx,-500);
		CurrentMissionChangedMessageCommand=cmd(playcommand,"Set" );
	};

	LoadFont( "Special/_karnivore lite 20px" ) .. {
		SetCommand=function(self)
			if not GAMESTATE:GetMasterPlayerNumber() then self:visible(false); return; end;
			if not GAMESTATE:GetCurrentSteps(GAMESTATE:GetMasterPlayerNumber()):GetStepsType() then self:visible(false); return; end;

			local mission = GAMESTATE:GetCurrentSteps(GAMESTATE:GetMasterPlayerNumber()):GetStepsType();
			local bwrap = GAMESTATE:GetCurrentMission():IsWarp() or GAMESTATE:GetCurrentMission():IsWarpToWarp() or GAMESTATE:GetCurrentMission():IsSwitch(); -- or GAMESTATE:GetCurrentMission():IsvisibleLine();

			--SCREENMAN:SystemMessage("asdf ");

			if not mission then
				self:visible(false)
				return
			end

			if bwrap then
				self:visible(false)
				return
			end

			self:visible(true)
			self:horizalign('HorizAlign_Left')
			self:vertalign('VertAlign_Top')			

			if mission == 'StepsType_Pump_Double' then
				self:settext( "Double" );
			else
				if mission == 'StepsType_Pump_Single' then
					self:settext( "Single" );
				else
					self:settext( "" );
				end
			end
		end;

		InitCommand=cmd(shadowlength,1.4;diffusealpha,0;zoom,0.35;wrapwidthpixels,700;);
		OnCommand=cmd(x,40;y,404;sleep,0.7;linear,0.3;diffusealpha,1;);
		OffCommand=cmd(linear,0.3;addx,-500);	

		CurrentSongChangedMessageCommand=cmd(sleep,0.1;queuecommand,"Set");
		CurrentMissionChangedMessageCommand=cmd(sleep,0.1;queuecommand,"Set" );
	};
	
	LoadFont("Special/_karnivore lite 20px") .. {
		SetCommand=function(self)
			local mission = GAMESTATE:GetCurrentMission():GetAuthor();
			local bwrap = GAMESTATE:GetCurrentMission():IsWarp() or GAMESTATE:GetCurrentMission():IsWarpToWarp() or GAMESTATE:GetCurrentMission():IsSwitch(); --or GAMESTATE:GetCurrentMission():IsvisibleLine();

			if not mission then
				self:visible(false)
				return
			end

			if bwrap then
				self:visible(false)
				return
			end

			self:visible(true)
			self:horizalign('HorizAlign_Left')

			if mission then
				self:settext( mission );
			else
				self:settext( "No Mission" );
			end
		end;

		InitCommand=cmd(shadowlength,1.4;diffusealpha,0;zoom,0.4;wrapwidthpixels,700;);
		OnCommand=cmd(x,40;y,428;sleep,0.7;linear,0.3;diffusealpha,1;);
		OffCommand=cmd(linear,0.3;addx,-500);	
		
		CurrentMissionChangedMessageCommand=cmd(playcommand,"Set" );
	};
	
	LoadFont("Common","normal") .. {
		SetCommand=function(self)
			local player = GAMESTATE:GetCurrentMission():GetRequiredPlayers()

			if not player then
				self:visible(false)
				return
			end

			self:visible(true)
			player = string.upper( player )

			if player then
				self:settext( player );
			else
				self:settext( "No Player" );
			end
		end;
		OnCommand=cmd(y,386;x,314;zoom,0.5;shadowlength,0;diffuse,0.2,0.6,1.0,1;diffusealpha,0;sleep,0.7;linear,0.3;diffusealpha,1;);
		OffCommand=cmd(linear,0.3;addx,-500);	
		CurrentMissionChangedMessageCommand=cmd(playcommand,"Set");
	};
	
	LoadActor ( "Light.png" ) .. {
 		OnCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;diffusealpha,0;blend,"BlendMode_Add";sleep,0.6;accelerate,0.1;diffusealpha,1;linear,0.3;diffusealpha,0);
 	};
	
	LoadFont("Common","normal") .. {
		SetCommand=function(self)
			local bOn = GAMESTATE:GetCurrentMission():StageBreak()
			
			if bOn then
				self:settext( "ON" );
			else
				self:settext( "OFF" );
			end
		end;
		OnCommand=cmd(y,426;x,314;zoom,0.5;shadowlength,0;diffuse,0.2,0.6,1.0,1;diffusealpha,0;sleep,0.7;linear,0.3;diffusealpha,1;playcommand,"Set");
		OffCommand=cmd(linear,0.3;addx,-500);	
		CurrentMissionChangedMessageCommand=cmd(playcommand,"Set");
	};
	
	Def.Quad {
		InitCommand=cmd(diffusealpha,0.5;diffuse,color("#000000"));
		OnCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;diffusealpha,0.5);
		EnteringCodeMessageCommand=cmd(linear,0.3;zoomx,640;zoomy,480);
		FinishedCodeMessageCommand=cmd(linear,0;zoom,0);
	};
	
	LoadActor( "EnterCodes" ) .. {
		OnCommand=cmd();
	};
	
	LoadActor( "OPS" ) .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-100;diffusealpha,0;zoom,1);
		CodeEnteringDoneMessageCommand=cmd(sleep,1;diffusealpha,1;sleep,3;linear,0;diffusealpha,0);
	};
	
	LoadActor( "OPF" ) .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-100;diffusealpha,0;zoom,1);
		CodeEnteringFailedMessageCommand=cmd(sleep,1;diffusealpha,1;sleep,3;linear,0;diffusealpha,0);
	};
	
	LoadFont( "NoItem" ) .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-100;diffusealpha,1;zoom,1);
		CannotEnterCodeMessageCommand=cmd(diffusealpha,1;sleep,3;linear,0;diffusealpha,0);
	};
	
	LoadActor( "GeneralMap/default.lua" ) .. {
		OnCommand=cmd();
	};
};

return t;
