
return Def.ActorFrame {
	--GetTotalItems
	Def.ActorFrame {
		--Condition=GetPlayerProfile( 0; --GAMESTATE:GetMasterPlayerNumber() ):GetTotalItems() == 1;
		LoadActor( "Items/DownLeft.png" ) .. {
			OnCommand=cmd(x,67;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,67;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		SetCommand=function( self )
			local numItems = 0; --GetPlayerProfile( GAMESTATE:GetMasterPlayerNumber() ):GetTotalItems();
			
			if numItems ~= 1 then
				self:visible( false )
				return
			end
			
			self:visible( true )
		end;
		OnCommand=cmd(playcommand,"Set");
		ItemUsedMessageCommand=cmd(playcommand,"Set");
	};
	
	Def.ActorFrame {
		--Condition=GetPlayerProfile( 0; --GAMESTATE:GetMasterPlayerNumber() ):GetTotalItems() == 2;
		LoadActor( "Items/DownLeft.png" ) .. {
			OnCommand=cmd(x,67;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,67;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/DownRight.png" ) .. {
			OnCommand=cmd(x,105	;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,105;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		SetCommand=function( self )
			local numItems = 0; --GetPlayerProfile( GAMESTATE:GetMasterPlayerNumber() ):GetTotalItems();
			
			if numItems ~= 2 then
				self:visible( false )
				return
			end
			
			self:visible( true )
		end;
		OnCommand=cmd(playcommand,"Set");
		ItemUsedMessageCommand=cmd(playcommand,"Set");
	};
	
	Def.ActorFrame {
		--Condition=GetPlayerProfile( 0; --GAMESTATE:GetMasterPlayerNumber() ):GetTotalItems() == 3;
		LoadActor( "Items/DownLeft.png" ) .. {
			OnCommand=cmd(x,67;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,67;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/DownRight.png" ) .. {
			OnCommand=cmd(x,105	;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,105;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/UpLeft.png" ) .. {
			OnCommand=cmd(x,147;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,147;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		SetCommand=function( self )
			local numItems = 0; --GetPlayerProfile( GAMESTATE:GetMasterPlayerNumber() ):GetTotalItems();
			
			if numItems ~= 3 then
				self:visible( false )
				return
			end
			
			self:visible( true )
		end;
		OnCommand=cmd(playcommand,"Set");
		ItemUsedMessageCommand=cmd(playcommand,"Set");
	};
	
	Def.ActorFrame {
		--Condition=GetPlayerProfile( 0; --GAMESTATE:GetMasterPlayerNumber() ):GetTotalItems() == 4;
		LoadActor( "Items/DownLeft.png" ) .. {
			OnCommand=cmd(x,67;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,67;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/DownRight.png" ) .. {
			OnCommand=cmd(x,105	;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,105;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/UpLeft.png" ) .. {
			OnCommand=cmd(x,147;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,147;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/UpRight.png" ) .. {
			OnCommand=cmd(x,186;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,186;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		SetCommand=function( self )
			local numItems = 0; --GetPlayerProfile( GAMESTATE:GetMasterPlayerNumber() ):GetTotalItems();
			
			if numItems ~= 4 then
				self:visible( false )
				return
			end
			
			self:visible( true )
		end;
		OnCommand=cmd(playcommand,"Set");
		ItemUsedMessageCommand=cmd(playcommand,"Set");
	};
	
	Def.ActorFrame {
		--Condition=GetPlayerProfile( 0; --GAMESTATE:GetMasterPlayerNumber() ):GetTotalItems() == 5;
		LoadActor( "Items/DownLeft.png" ) .. {
			OnCommand=cmd(x,67;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,67;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/DownRight.png" ) .. {
			OnCommand=cmd(x,105	;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,105;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/UpLeft.png" ) .. {
			OnCommand=cmd(x,147;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,147;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/UpRight.png" ) .. {
			OnCommand=cmd(x,186;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,186;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/Center.png" ) .. {
			OnCommand=cmd(x,226;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,226;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		SetCommand=function( self )
			local numItems = 0; --GetPlayerProfile( GAMESTATE:GetMasterPlayerNumber() ):GetTotalItems();
			
			if numItems ~= 5 then
				self:visible( false )
				return
			end
			
			self:visible( true )
		end;
		OnCommand=cmd(playcommand,"Set");
		ItemUsedMessageCommand=cmd(playcommand,"Set");
	};
	
	Def.ActorFrame {
		--Condition=GetPlayerProfile( 0; --GAMESTATE:GetMasterPlayerNumber() ):GetTotalItems() == 5;
		LoadActor( "Items/DownLeft.png" ) .. {
			OnCommand=cmd(x,67;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,67;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/DownRight.png" ) .. {
			OnCommand=cmd(x,105	;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,105;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/UpLeft.png" ) .. {
			OnCommand=cmd(x,147;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,147;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/UpRight.png" ) .. {
			OnCommand=cmd(x,186;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,186;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/Center.png" ) .. {
			OnCommand=cmd(x,226;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,226;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/Center.png" ) .. {
			OnCommand=cmd(x,416;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
	LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,416;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		SetCommand=function( self )
			local numItems = 0; --GetPlayerProfile( GAMESTATE:GetMasterPlayerNumber() ):GetTotalItems();
			
			if numItems ~= 6 then
				self:visible( false )
				return
			end
			
			self:visible( true )
		end;
		OnCommand=cmd(playcommand,"Set");
		ItemUsedMessageCommand=cmd(playcommand,"Set");
	};
	
	Def.ActorFrame {
		--Condition=GetPlayerProfile( 0; --GAMESTATE:GetMasterPlayerNumber() ):GetTotalItems() == 6;
		LoadActor( "Items/DownLeft.png" ) .. {
			OnCommand=cmd(x,67;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,67;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/DownRight.png" ) .. {
			OnCommand=cmd(x,105	;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,105;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/UpLeft.png" ) .. {
			OnCommand=cmd(x,147;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,147;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/UpRight.png" ) .. {
			OnCommand=cmd(x,186;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,186;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/Center.png" ) .. {
			OnCommand=cmd(x,226;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,226;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/Center.png" ) .. {
			OnCommand=cmd(x,416;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
	LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,416;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/UpLeft.png" ) .. {
			OnCommand=cmd(x,456;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,456;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		SetCommand=function( self )
			local numItems = 0; --GetPlayerProfile( GAMESTATE:GetMasterPlayerNumber() ):GetTotalItems();
			
			if numItems ~= 7 then
				self:visible( false )
				return
			end
			
			self:visible( true )
		end;
		OnCommand=cmd(playcommand,"Set");
		ItemUsedMessageCommand=cmd(playcommand,"Set");
	};
	
	Def.ActorFrame {
		--Condition=GetPlayerProfile( 0; --GAMESTATE:GetMasterPlayerNumber() ):GetTotalItems() == 8;
		LoadActor( "Items/DownLeft.png" ) .. {
			OnCommand=cmd(x,67;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,67;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/DownRight.png" ) .. {
			OnCommand=cmd(x,105	;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,105;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/UpLeft.png" ) .. {
			OnCommand=cmd(x,147;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,147;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/UpRight.png" ) .. {
			OnCommand=cmd(x,186;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,186;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/Center.png" ) .. {
			OnCommand=cmd(x,226;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,226;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/Center.png" ) .. {
			OnCommand=cmd(x,416;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
	LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,416;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/UpLeft.png" ) .. {
			OnCommand=cmd(x,456;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,456;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		
		LoadActor( "Items/UpRight.png" ) .. {
			OnCommand=cmd(x,495;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,495;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		SetCommand=function( self )
			local numItems = 0; --GetPlayerProfile( GAMESTATE:GetMasterPlayerNumber() ):GetTotalItems();
			
			if numItems ~= 8 then
				self:visible( false )
				return
			end
			
			self:visible( true )
		end;
		OnCommand=cmd(playcommand,"Set");
		ItemUsedMessageCommand=cmd(playcommand,"Set");
	};
	
	Def.ActorFrame {
		--Condition=GetPlayerProfile( 0; --GAMESTATE:GetMasterPlayerNumber() ):GetTotalItems() == 9;
		LoadActor( "Items/DownLeft.png" ) .. {
			OnCommand=cmd(x,67;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,67;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/DownRight.png" ) .. {
			OnCommand=cmd(x,105	;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,105;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/UpLeft.png" ) .. {
			OnCommand=cmd(x,147;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,147;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/UpRight.png" ) .. {
			OnCommand=cmd(x,186;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,186;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/Center.png" ) .. {
			OnCommand=cmd(x,226;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,226;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/Center.png" ) .. {
			OnCommand=cmd(x,416;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
	LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,416;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/UpLeft.png" ) .. {
			OnCommand=cmd(x,456;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,456;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		
		LoadActor( "Items/UpRight.png" ) .. {
			OnCommand=cmd(x,495;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,495;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/DownLeft.png" ) .. {
			OnCommand=cmd(x,536;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,536;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		SetCommand=function( self )
			local numItems = 0; --GetPlayerProfile( GAMESTATE:GetMasterPlayerNumber() ):GetTotalItems();
			
			if numItems ~= 9 then
				self:visible( false )
				return
			end
			
			self:visible( true )
		end;
		OnCommand=cmd(playcommand,"Set");
		ItemUsedMessageCommand=cmd(playcommand,"Set");
	};
	
	Def.ActorFrame {
		--Condition=GetPlayerProfile( 0; --GAMESTATE:GetMasterPlayerNumber() ):GetTotalItems() == 10;
		LoadActor( "Items/DownLeft.png" ) .. {
			OnCommand=cmd(x,67;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,67;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/DownRight.png" ) .. {
			OnCommand=cmd(x,105	;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,105;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/UpLeft.png" ) .. {
			OnCommand=cmd(x,147;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,147;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/UpRight.png" ) .. {
			OnCommand=cmd(x,186;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,186;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/Center.png" ) .. {
			OnCommand=cmd(x,226;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,226;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/Center.png" ) .. {
			OnCommand=cmd(x,416;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
	LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,416;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/UpLeft.png" ) .. {
			OnCommand=cmd(x,456;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,456;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		
		LoadActor( "Items/UpRight.png" ) .. {
			OnCommand=cmd(x,495;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,495;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/DownLeft.png" ) .. {
			OnCommand=cmd(x,536;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,536;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		LoadActor( "Items/DownRight.png" ) .. {
			OnCommand=cmd(x,574;y,17;diffusealpha,0;sleep,0.8;diffusealpha,1;zoom,1.5;accelerate,0.4;zoom,0.5);
			OffCommand=cmd(linear,1.5;addy,-150);
		};
		LoadActor( "Items/Itemlight.png" ) .. {
			OnCommand=cmd(x,574;y,17;blend,"BlendMode_Add";diffusealpha,0;sleep,1.2;diffusealpha,1;zoom,1;linear,0.3;zoom,1.5;diffusealpha,0);
		};
		
		SetCommand=function( self )
			local numItems = 0; --GetPlayerProfile( GAMESTATE:GetMasterPlayerNumber() ):GetTotalItems();
			
			if numItems ~= 10 then
				self:visible( false )
				return
			end
			
			self:visible( true )
		end;
		OnCommand=cmd(playcommand,"Set");
		ItemUsedMessageCommand=cmd(playcommand,"Set");
	};
};