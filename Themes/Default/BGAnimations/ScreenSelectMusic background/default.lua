return Def.ActorFrame {
	LoadActor(THEME:GetPathG("","background".."/ARCADE_BG")) .. {
		InitCommand=cmd(visible,not GAMESTATE:IsRankMode() and GAMESTATE:GetCurrentGroup() ~= "quest zone";stretchto,0,0,SCREEN_WIDTH,SCREEN_HEIGHT;loop,true;queuecommand,'UpdateBga');
		GoFullModeMessageCommand=cmd(visible,GAMESTATE:GetCurrentGroup() ~= "quest zone";diffusealpha,0;sleep,.2;linear,.1;diffusealpha,1;queuecommand,'UpdateBga');
		GoRankModeMessageCommand=cmd(visible,false;queuecommand,'UpdateBga');
		StartSelectingSongMessageCommand=cmd(sleep,.1;queuecommand,'UpdateBga');
		UpdateBgaCommand=function(self)
			if GAMESTATE:GetCurrentGroup() ~= "quest zone" and not GAMESTATE:IsRankMode() then
				self:visible( true )
				self:play()
			else
				self:visible( false );
				self:pause()
			end
		end;
	};

	LoadActor(THEME:GetPathG("","background".."/RANK_BG")) .. {
		InitCommand=cmd(visible,GAMESTATE:IsRankMode();stretchto,0,0,SCREEN_WIDTH,SCREEN_HEIGHT;loop,true;queuecommand,'UpdateBga');
		GoFullModeMessageCommand=cmd(visible,false;queuecommand,'UpdateBga');
		GoRankModeMessageCommand=cmd(visible,true;diffusealpha,0;sleep,.2;linear,.1;diffusealpha,1;queuecommand,'UpdateBga');
		StartSelectingSongMessageCommand=cmd(sleep,.1;queuecommand,'UpdateBga');
		UpdateBgaCommand=function(self)
			if GAMESTATE:IsRankMode() then
				self:visible( true )
				self:play()
			else
				self:visible( false );
				self:pause()
			end
		end;
	};

	LoadActor(THEME:GetPathG("","background".."/QUEST_BG")) .. {
		InitCommand=cmd(visible,GAMESTATE:GetCurrentGroup() == "quest zone";stretchto,0,0,SCREEN_WIDTH,SCREEN_HEIGHT;loop,true;queuecommand,'UpdateBga');
		GoFullModeMessageCommand=cmd(visible,GAMESTATE:GetCurrentGroup() == "quest zone";diffusealpha,0;sleep,.2;linear,.1;diffusealpha,1;queuecommand,'UpdateBga');
		GoRankModeMessageCommand=cmd(visible,false;queuecommand,'UpdateBga');
		StartSelectingSongMessageCommand=cmd(sleep,.1;queuecommand,'UpdateBga');
		UpdateBgaCommand=function(self,params)
			if GAMESTATE:GetCurrentGroup() == "quest zone" then
				self:visible( true )
				self:play()
			else
				self:visible( false );
				self:pause()
			end
		end;
	};
}
