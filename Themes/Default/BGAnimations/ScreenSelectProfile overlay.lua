local function OtherPlayer( player )
	if player == 'PlayerNumber_P1' then return 'PlayerNumber_P2'; end;
	if player == 'PlayerNumber_P2' then return 'PlayerNumber_P1'; end;
end;

-- Comandos y mensajes para utilizar en las decoraciones y en el screensystemlayer.
local t = Def.ActorFrame {
	CodeMessageCommand = function(self, params)
----------------------------------------------------------------------------------------------------------------
	if params.Name == 'Center' then
		--[[if not GAMESTATE:IsHumanPlayer(params.PlayerNumber) then
			SCREENMAN:GetTopScreen():SetProfileIndex(params.PlayerNumber, -1); -- Con -1, joinplayer y startsound
		
			if PROFILEMAN:GetNumLocalProfiles()>0 then	--hay por lo menos 1 localprofile
				if PROFILEMAN:GetNumLocalProfiles()==1 then --solo hay 1 localprofile		
					if not GAMESTATE:IsHumanPlayer(OtherPlayer(params.PlayerNumber)) then -- la unica prof. esta disponible, se setea al jugador
						SCREENMAN:GetTopScreen():SetProfileIndex(params.PlayerNumber, 1 );
						local names={ PROFILEMAN:GetLocalProfileFromIndex(0):GetDisplayName() , params.PlayerNumber };
						MESSAGEMAN:Broadcast("LocalProfileChange",names);
					else
						SCREENMAN:GetTopScreen():Finish();	-- Ak llega el jugador que entro segundo, y no tiene una LP para cargar
					end;
					return;	-- no revisa mas posibilidades
				end;
				
				-- hay mas de 1 localprof, se carga la primera al jugador
				SCREENMAN:GetTopScreen():SetProfileIndex(params.PlayerNumber, 1 );
				local names={ PROFILEMAN:GetLocalProfileFromIndex(0):GetDisplayName() , params.PlayerNumber };
				MESSAGEMAN:Broadcast("LocalProfileChange",names);
			end;
		else]]

		-- Si ya esta cargado el jugador, sale
		GAMESTATE:JoinPlayer(params.PlayerNumber); 
		--GAMESTATE:LoadProfiles(params.PlayerNumber);
		MESSAGEMAN:Broadcast("FinishSelectProfile");	

		--end;
	end;
--[[ ----------------------------------------------------------------------------------------------------------------
	if params.Name == 'DownLeft' then
		if GAMESTATE:IsHumanPlayer(params.PlayerNumber) then
			local ind = SCREENMAN:GetTopScreen():GetProfileIndex(params.PlayerNumber);
			if ind > 1 then
				if SCREENMAN:GetTopScreen():SetProfileIndex(params.PlayerNumber, ind - 1 ) then
					ind = SCREENMAN:GetTopScreen():GetProfileIndex(params.PlayerNumber);
					local names={ PROFILEMAN:GetLocalProfileFromIndex(ind-1):GetDisplayName() , params.PlayerNumber };
					MESSAGEMAN:Broadcast("LocalProfileChange",names);
				end;
			end;
		end;
	end;
----------------------------------------------------------------------------------------------------------------
	if params.Name == 'DownRight' then
		if GAMESTATE:IsHumanPlayer(params.PlayerNumber) then
			local ind = SCREENMAN:GetTopScreen():GetProfileIndex(params.PlayerNumber);
			if ind > 0 then
				if SCREENMAN:GetTopScreen():SetProfileIndex(params.PlayerNumber, ind + 1 ) then
					ind = SCREENMAN:GetTopScreen():GetProfileIndex(params.PlayerNumber);
					local names={ PROFILEMAN:GetLocalProfileFromIndex(ind-1):GetDisplayName() , params.PlayerNumber };
					MESSAGEMAN:Broadcast("LocalProfileChange",names);
				end;
			end;
		end;
	end;
----------------------------------------------------------------------------------------------------------------
	if params.Name == 'Back' then
		if GAMESTATE:GetNumPlayersEnabled()==0 then
			SCREENMAN:GetTopScreen():Cancel();
		else
			MESSAGEMAN:Broadcast("BackButton");
			SCREENMAN:GetTopScreen():SetProfileIndex(params.PlayerNumber, -2);
		end;
	end;
end;

};

return t;
]]
end;
};

--return Def.ActorFrame {};
return t;