return Def.ActorFrame {	

	LoadActor(THEME:GetPathG("","background".."/title_prime"))..{
		InitCommand=cmd(FullScreen);
		IncomingLogoMessageCommand=cmd(stoptweening;decelerate,.3;zoomto,SCREEN_WIDTH+50,SCREEN_HEIGHT+30);
	};

	LoadActor(THEME:GetPathG("","scroller back"))..{
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_HEIGHT-80;zoom,.8;zoomx,1);
		OffCommand=cmd(stoptweening;diffusealpha,0);
	};
	
	Def.Quad {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;zoomto,SCREEN_WIDTH,SCREEN_HEIGHT;diffuse,0,0,0,1;diffusealpha,0);
		IncomingLogoMessageCommand=cmd(stoptweening;linear,.2;diffusealpha,1;sleep,.4);
	};
};
