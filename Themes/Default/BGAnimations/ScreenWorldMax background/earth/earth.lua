

return Def.ActorFrame { 	
	
	LoadActor( "1" ) .. {
		OnCommand=cmd(addx,1152*0);
	};
	
	LoadActor( "2" ) .. {
		OnCommand=cmd(addx,1152*1);
	};
	
	LoadActor( "3" ) .. {
		OnCommand=cmd(addx,1152*2);
	};
	
	LoadActor( "4" ) .. {
		OnCommand=cmd(addx,1152*3);
	};
	
	LoadActor( "5" ) .. {
		OnCommand=cmd(addx,1152*0;addy,999);
	};
	
	LoadActor( "6" ) .. {
		OnCommand=cmd(addx,1152*1;addy,999);
	};
	
	LoadActor( "7" ) .. {
		OnCommand=cmd(addx,1152*2;addy,999);
	};
	
	LoadActor( "8" ) .. {
		OnCommand=cmd(addx,1152*3;addy,999);
	};
	
	LoadActor( "9" ) .. {
		OnCommand=cmd(addx,1152*0;addy,2160);
	};
	
	LoadActor( "10" ) .. {
		OnCommand=cmd(addx,1152*1;addy,2160);
	};
	
	LoadActor( "11" ) .. {
		OnCommand=cmd(addx,1152*2;addy,2160);
	};
	
	LoadActor( "12" ) .. {
		OnCommand=cmd(addx,1152*3;addy,2160);
	};
	
	LoadActor( "dark" ) .. {
		OnCommand=cmd(addx,2344;addy,2088);
		SwitchStateChangedMessageCommand=cmd(playcommand,"Set");
		SetCommand=function(self)
			--preguntamos si el switch "Switch 1" est� activo,
			--modificar dependiendo de lo que requeramos
			local bSwitchAct = false; --SCREENMAN:GetTopScreen():GetSwitchActive( "SWITCHEARTH" )
			
			if bSwitchAct then
				self:visible( true )
			else
				self:visible( false )
			end
		end;
	};
	
	MapMovedMessageCommand=function(self,param)
	
		if param.PosX == 0 or param.PosY == 0 then return end
		
		--HACK: sabemos como actuar
		--dependiendo de donde salga el mensaje
		--ajustamos las posicion inicial de mapa
		if param.From ~= "ScreenWorldMax" then		
			self:x( param.PosX-1410 )
			self:y( param.PosY-920 )
		else
			self:stoptweening( 0 )
			self:linear( 0.3 )
			self:x( param.PosX-1410 )
			self:y( param.PosY-920 )
		end
		
		--SCREENMAN:SystemMessage( param.From .. " POSX:" .. tostring( param.PosX ) .. " POSY:" .. tostring( param.PosY ) )
	end;
	
	CurrentMissionChangedMessageCommand=cmd(sleep,.001;queuecommand,"Set");
	OnCommand=cmd(sleep,.001;queuecommand,"Set");
	
	SetCommand=function(self)		
		local sectorname = SCREENMAN:GetTopScreen():GetCurrentSector()
		
		--SCREENMAN:SystemMessage( "SectorName: " .. sectorname )
		
		if string.lower(sectorname) ~= "earth" then 
			self:visible( false ) 
		else 
			self:visible( true ) 
		end	
	end;
	
};
