
local t = Def.ActorFrame {};

--song:GetChapter
t[#t+1] = LoadActor("chapter_name") .. {
	InitCommand=cmd(zoom, .8);
};

t[#t+1] = LoadFont("_youth 20px")..{
	InitCommand=function(self)
		(cmd(zoom,.65))(self);
		if GAMESTATE:GetCurrentSong() then
			self:settext(GAMESTATE:GetCurrentSong():GetChapter());
		end;
	end;
	CurrentSongChangedMessageCommand=function(self)
		if GAMESTATE:GetCurrentSong() then
			self:settext(GAMESTATE:GetCurrentSong():GetChapter());
		end;
	end;
};

return t;