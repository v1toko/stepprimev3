local t = Def.ActorFrame {
	OnCommand=cmd(playcommand,'StartSelectingSong');
}

t[#t+1] = Def.ActorProxy {
	BeginCommand=function(self) 
		local Timer = SCREENMAN:GetTopScreen():GetChild('Timer'); 
		self:SetTarget(Timer); 
		end;
	OnCommand=cmd(stoptweening;x,1;y,10;);
}

t[#t+1] = LoadActor("moon") .. {
	InitCommand=cmd(y,41;diffusealpha,0;visible,not GAMESTATE:IsRankMode());
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;decelerate,.1;diffusealpha,1;decelerate,.2;diffusealpha,0);
	GoBackSelectingSongMessageCommand=cmd(stoptweening;decelerate,.1;diffusealpha,1);
	StartSelectingSongMessageCommand=cmd(stoptweening;decelerate,.1;diffusealpha,1);
	GoFullModeMessageCommand=cmd(sleep,0.4;visible,true);
	GoRankModeMessageCommand=cmd(sleep,0.4;visible,false);
	GroupNameUpdateMessageCommand=function(self,params)
		self:visible( params.Group ~= "quest zone" and not GAMESTATE:IsRankMode() )
	end;
}; 

t[#t+1] = LoadActor("moon_rank") .. {
	InitCommand=cmd(y,40;diffusealpha,0;visible,GAMESTATE:IsRankMode());
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;decelerate,.1;diffusealpha,1;decelerate,.2;diffusealpha,0);
	GoBackSelectingSongMessageCommand=cmd(stoptweening;decelerate,.1;diffusealpha,1);
	StartSelectingSongMessageCommand=cmd(stoptweening;decelerate,.1;diffusealpha,1);
	GoFullModeMessageCommand=cmd(sleep,0.4;visible,false);
	GoRankModeMessageCommand=cmd(sleep,0.4;visible,true);
	GroupNameUpdateMessageCommand=function(self,params)
		self:visible( params.Group ~= "quest zone" and GAMESTATE:IsRankMode() )
	end;
}; 

t[#t+1] = LoadActor("moon_quest") .. {
	InitCommand=cmd(y,43;visible,GAMESTATE:GetCurrentGroup() == "quest zone");
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;decelerate,.1;diffusealpha,1;decelerate,.2;diffusealpha,0);
	GoBackSelectingSongMessageCommand=cmd(stoptweening;decelerate,.1;diffusealpha,1);
	StartSelectingSongMessageCommand=cmd(stoptweening;decelerate,.1;diffusealpha,1);
	--GoFullModeMessageCommand=cmd(sleep,0.4;visible,false);
	--GoRankModeMessageCommand=cmd(sleep,0.4;visible,true);
	GroupNameUpdateMessageCommand=function(self,params)
		self:visible( params.Group == "quest zone" )
	end;
};

for k=1,2 do
	t[#t+1] = LoadActor("channel") .. {
		InitCommand=cmd(y,60;diffusealpha,0);
		OnCommand==function(self)
			if k==1 then 
				self:diffusealpha(1);
			elseif k==2 then 
				self:diffusealpha(0); 
			end;
		end;
		StartSelectingSongMessageCommand=cmd(stoptweening;decelerate,.1;diffusealpha,0);
		GoBackSelectingGroupMessageCommand=function(self)
			if k==1 then 
				(cmd(stoptweening;decelerate,.1;diffusealpha,1))(self);
			elseif k==2 then 
				(cmd(stoptweening;blend,'BlendMode_Add';decelerate,.1;diffusealpha,1;sleep,.02;linear,.2;diffusealpha,0))(self);
			end;
		end;
	};
end;

for j=1,2 do
	t[#t+1] = LoadActor("music") .. {
		InitCommand=cmd(y,60);
		OnCommand==function(self)
			if j==1 then 
				self:diffusealpha(1);
			elseif j==2 then 
				self:diffusealpha(0); 
			end;
		end;
		StartSelectingStepsMessageCommand=cmd(stoptweening;decelerate,.1;diffusealpha,0);
		GoBackSelectingGroupMessageCommand=cmd(stoptweening;decelerate,.1;diffusealpha,0);
		StartSelectingSongMessageCommand=cmd(stoptweening;playcommand,'GoBackSelectingSong');
		GoBackSelectingSongMessageCommand=function(self)
			if j==1 then 
				(cmd(stoptweening;decelerate,.1;diffusealpha,1))(self);
			elseif j==2 then 
				(cmd(stoptweening;blend,'BlendMode_Add';decelerate,.1;diffusealpha,1;sleep,.02;linear,.2;diffusealpha,0))(self);
			end;
		end;
	};
end;

for i=1,2 do
	t[#t+1] = LoadActor("step") .. {
		InitCommand=cmd(y,60;diffusealpha,0);	--nunca empieza seleccionando los pasos
		StartSelectingStepsMessageCommand=function(self)
			if i==1 then 
				(cmd(stoptweening;decelerate,.1;diffusealpha,1))(self);
			elseif i==2 then 
				(cmd(stoptweening;blend,'BlendMode_Add';decelerate,.1;diffusealpha,1;sleep,.02;linear,.2;diffusealpha,0))(self);
			end;
		end;
		GoBackSelectingSongMessageCommand=cmd(stoptweening;decelerate,.1;diffusealpha,0);
	};
end


return t