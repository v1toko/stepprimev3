local t = Def.ActorFrame {}

--side= "Left" or "Right"

local sleeps = {.4, .35, .3};

function GetBallons(side)
	local t = Def.ActorFrame {};
	
	-- Texto 1
	t[#t+1] = Def.ActorFrame {
			LoadActor(side.."_back.png")..{ 
				InitCommand=cmd(diffusealpha,0;zoom,1;sleep,sleeps[1];linear,.2;zoom,.8;diffusealpha,.9); 
				StartSelectingStepsMessageCommand=cmd(stoptweening;diffusealpha,0);
				GoBackSelectingSongMessageCommand=cmd(stoptweening;sleep,.5;diffusealpha,1);
				OffCommand=cmd(stoptweening);
			};
			LoadActor("texto_1.png")..{ 
				InitCommand=cmd(diffusealpha,0;zoom,1;sleep,sleeps[1];linear,.2;zoom,.8;diffusealpha,1);
				StartSelectingStepsMessageCommand=cmd(stoptweening;diffusealpha,0);
				GoBackSelectingSongMessageCommand=cmd(stoptweening;sleep,.5;diffusealpha,1);
				OffCommand=cmd(stoptweening);
			};
		};
	for i=2,3 do
		t[#t+1] = Def.ActorFrame {
			LoadActor(side.."_back.png")..{ 
				InitCommand=cmd(diffusealpha,0;zoom,1;sleep,sleeps[i];linear,.2;zoom,.8;diffusealpha,.8;sleep,.05;linear,.2;zoom,.95;diffusealpha,0); 
				StartSelectingStepsMessageCommand=cmd(stoptweening;visible,false);
				OffCommand=cmd(stoptweening);
			};
			LoadActor("texto_1.png")..{ 
				InitCommand=cmd(diffusealpha,0;zoom,1;sleep,sleeps[i];linear,.2;zoom,.8;diffusealpha,.8;sleep,.05;linear,.2;zoom,.95;diffusealpha,0); 
				StartSelectingStepsMessageCommand=cmd(stoptweening;visible,false);
				OffCommand=cmd(stoptweening);
			};	
		};
	end;
	
	-- Texto 2
	t[#t+1] = Def.ActorFrame {
			LoadActor(side.."_back.png")..{ 
				InitCommand=cmd(diffusealpha,0;zoom,1); 
				StartSelectingStepsMessageCommand=cmd(stoptweening;sleep,sleeps[1];linear,.2;zoom,.8;diffusealpha,.9);
				GoBackSelectingSongMessageCommand=cmd(stoptweening;diffusealpha,0);
				StepsChosenMessageCommand=function(self,params) if params.Player == SideToPlayer(side) then self:diffusealpha(0); end; end;
				StepsPreselectedCancelledMessageCommand=function(self,params) if params.Player == SideToPlayer(side) then self:playcommand('StartSelectingSteps'); end; end;
				OffCommand=cmd(stoptweening);
			};
			LoadActor("texto_2.png")..{ 
				InitCommand=cmd(diffusealpha,0;zoom,1); 
				StartSelectingStepsMessageCommand=cmd(stoptweening;sleep,sleeps[1];linear,.2;zoom,.8;diffusealpha,.9);
				GoBackSelectingSongMessageCommand=cmd(stoptweening;diffusealpha,0);
				StepsChosenMessageCommand=function(self,params) if params.Player == SideToPlayer(side) then self:diffusealpha(0); end; end;
				StepsPreselectedCancelledMessageCommand=function(self,params) if params.Player == SideToPlayer(side) then self:playcommand('StartSelectingSteps'); end; end;
				OffCommand=cmd(stoptweening);
			};
		};
	for i=2,3 do
		t[#t+1] = Def.ActorFrame {
			LoadActor(side.."_back.png")..{ 
				InitCommand=cmd(diffusealpha,0;zoom,1); 
				StartSelectingStepsMessageCommand=cmd(stoptweening;sleep,sleeps[i];linear,.2;zoom,.8;diffusealpha,.8;sleep,.05;linear,.2;zoom,.95;diffusealpha,0);
				GoBackSelectingSongMessageCommand=cmd(stoptweening;visible,false);
				StepsChosenMessageCommand=function(self,params) if params.Player == SideToPlayer(side) then self:stoptweening(0); self:visible(false); end; end;
				StepsPreselectedCancelledMessageCommand=function(self,params) if params.Player == SideToPlayer(side) then self:playcommand('StartSelectingSteps'); end; end;
				OffCommand=cmd(stoptweening);
			};
			LoadActor("texto_2.png")..{ 
				InitCommand=cmd(diffusealpha,0;zoom,1); 
				StartSelectingStepsMessageCommand=cmd(stoptweening;sleep,sleeps[i];linear,.2;zoom,.8;diffusealpha,.8;sleep,.05;linear,.2;zoom,.95;diffusealpha,0);
				GoBackSelectingSongMessageCommand=cmd(stoptweening;visible,false);
				StepsChosenMessageCommand=function(self,params) if params.Player == SideToPlayer(side) then self:stoptweening(0); self:visible(false); end; end;
				StepsPreselectedCancelledMessageCommand=function(self,params) if params.Player == SideToPlayer(side) then self:playcommand('StartSelectingSteps'); end; end;
				OffCommand=cmd(stoptweening);
			};	
		};
	end;
	
	-- Texto 3
	function SideToPlayer(side)
		local player;
		if( side == "Left"  ) then player = PLAYER_1; end;
		if( side == "Right" ) then player = PLAYER_2; end;
		
		return player;
	end;
	
	function Actor:StepsChosenActor(side,pn,simple,i)
		player = SideToPlayer(side);
		
		if( player == pn ) then
			if simple then
			return (cmd(stoptweening;sleep,sleeps[i];linear,.2;zoom,.8;diffusealpha,.9))(self);
			else
			return (cmd(stoptweening;sleep,sleeps[i];linear,.2;zoom,.8;diffusealpha,.8;sleep,.05;linear,.2;zoom,.95;diffusealpha,0))(self);
			end;
		end;
	end;
	
	t[#t+1] = Def.ActorFrame {
			LoadActor(side.."_back.png")..{ 
				InitCommand=cmd(diffusealpha,0;zoom,1); 
				StepsChosenMessageCommand=function(self,params) self:StepsChosenActor(side,params.Player,true,1); end;
				GoBackSelectingSongMessageCommand=cmd(stoptweening;diffusealpha,0);
				StepsPreselectedCancelledMessageCommand=function(self,params) if params.Player == SideToPlayer(side) then self:diffusealpha(0); end; end;
				OffCommand=cmd(stoptweening);
			};
			LoadActor("texto_3.png")..{ 
				InitCommand=cmd(diffusealpha,0;zoom,1);
				GoBackSelectingSongMessageCommand=cmd(stoptweening;diffusealpha,0);
				StepsChosenMessageCommand=function(self,params) self:StepsChosenActor(side,params.Player,true,1); end;
				StepsPreselectedCancelledMessageCommand=function(self,params) if params.Player == SideToPlayer(side) then self:diffusealpha(0); end; end;
				OffCommand=cmd(stoptweening);
			};
		};
	for i=2,3 do
		t[#t+1] = Def.ActorFrame {
			LoadActor(side.."_back.png")..{ 
				InitCommand=cmd(diffusealpha,0;zoom,1); 
				StepsChosenMessageCommand=function(self,params) self:StepsChosenActor(side,params.Player,false,i); end;
				GoBackSelectingSongMessageCommand=cmd(stoptweening;visible,false);
				StepsPreselectedCancelledMessageCommand=function(self,params) if params.Player == SideToPlayer(side) then self:stoptweening(); self:visible(false); end; end;
				GoBackSelectingSongMessageCommand=cmd(stoptweening;visible,false);
				OffCommand=cmd(stoptweening);
			};
			LoadActor("texto_3.png")..{ 
				InitCommand=cmd(diffusealpha,0;zoom,1); 
				StepsChosenMessageCommand=function(self,params) self:StepsChosenActor(side,params.Player,false,i); end;
				StepsPreselectedCancelledMessageCommand=function(self,params) if params.Player == SideToPlayer(side) then self:stoptweening(); self:visible(false); end; end;
				GoBackSelectingSongMessageCommand=cmd(stoptweening;visible,false);
				OffCommand=cmd(stoptweening);
			};	
		};
	end;
	return t;
end;

-- Carga de actoress
if GAMESTATE:IsSideJoined(PLAYER_1) then
t[#t+1] = GetBallons("Left")..{
	InitCommand=cmd(x,133;y,224);
	OffCommand=cmd(visible,false);
};
end;

if GAMESTATE:IsSideJoined(PLAYER_2) then
t[#t+1] = GetBallons("Right")..{
	InitCommand=cmd(x,SCREEN_RIGHT-133;y,224);
	OffCommand=cmd(visible,false);
};
end;

return t;