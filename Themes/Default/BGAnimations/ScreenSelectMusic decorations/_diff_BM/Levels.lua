local pn = Player; -- = PLAYER_1;	--algo asi como los parámetros privados
local name = Name; --  = '1P';
local ActiveBlocks = AB; -- = 4;

local t = Def.ActorFrame {
	SetPlayer1Command=function(self) pn = PLAYER_1; name = '1P'; end;
	SetPlayer2Command=function(self) pn = PLAYER_2; name = '2P'; end;
	Set4BlocksCommand=function(self) ActiveBlocks = 4; end;
	Set3BlocksCommand=function(self) ActiveBlocks = 3; end;
}

--///////////////////////////////////////////////////////////////////////////////////////////
--///////////////////////////////////////////////////////////////////////////////////////////
--[[ Clasificacion de pasos
		0: Single, Easy
		1: Single, Medium
		2: Single, Hard
		3: Double, AllTypes
]]

--Regresa la difficultad segun el estilo y la difucultad de los pasos seleccionados
local function DifficultyAndStyleToInt(steps)
	if not steps or steps=='' then return (4); end;
	--style = GAMESTATE:GetCurrentSteps(pn):GetStepsType();
	style = steps:GetStepsType();
	--diff = GAMESTATE:GetCurrentSteps(pn):GetDifficulty();
	diff = steps:GetDifficulty();
	
	if (style=='StepsType_Pump_Single') then 
		if (diff == 'Difficulty_Beginner') or (diff == 'Difficulty_Easy') then return (0); end;
		if (diff == 'Difficulty_Medium') then return (1); end;
		if (diff == 'Difficulty_Hard') or (diff == 'Difficulty_Challenge') or (diff == 'Difficulty_Edit') then return (2); end;
	elseif (style=='StepsType_Pump_Double') then return (3);
	end;
	return (4);	-- black block, por las dudas
end;

-- Regresa la posicion de los pasos seleccionados en el arreglo de pasos disponibles
local function GetCurStepsPosition(arr)
	local cur_steps = GAMESTATE:GetCurrentSteps(pn);
	
	for i=1,#arr do
		if arr[i]==cur_steps then return i; end;
	end;
	return 1;	-- no tendria que llegar a este punto
end;

--Regresa el bloque activo, de 1 a 4 (ActiveBlocks)
local function GetActiveBlock()	--pos=posicion en el arreglo
	local steps = SongUtil.GetPlayableSteps(GAMESTATE:GetCurrentSong());
	local pos = GetCurStepsPosition( steps );

	if( pos <= ActiveBlocks) then return pos; else 
		if math.mod(pos,ActiveBlocks) == 0 then return (ActiveBlocks); else return math.mod(pos,ActiveBlocks); end;
	end;
end;

--Regresa los steps en la posion i, desde 1 a 4
local function GetStepsInItem(i)
	local steps = SongUtil.GetPlayableSteps(GAMESTATE:GetCurrentSong());
	local num_steps = #(SongUtil.GetPlayableSteps(GAMESTATE:GetCurrentSong()));
	
	if (i > num_steps) then 
		if (i <= ActiveBlocks) then return ''; else return ''; end;
	end;
	
	local limInf = 1;
	local limSup = ActiveBlocks;

	local pos = GetCurStepsPosition( steps );	--falta mandar el num. de jugador como parámetro
	
	while (limInf>pos) or (limSup<pos)  do
		limInf = limInf + ActiveBlocks;
		limSup = limSup + ActiveBlocks;
	end;
	
	local arrToRet = {};
	for j=1,ActiveBlocks do
		arrToRet[j] = steps[j+limInf-1];
		
	end;
	
	return ( arrToRet[i] );
end;

--Regresa el texto segun los pasos seleccionados y su posicion. Compilacion de las anteriores
local function SetBackLevelFromDifficulty(i)
	return DifficultyAndStyleToInt( GetStepsInItem(i) );
end;

--Regresa las posiciones, segun la cantidad de bloques activos.
local function GetPositions()
	if ActiveBlocks == 3 then return { -170, 0 , 170 };
	elseif ActiveBlocks == 4 then return { -225,-75,75,225 }; end;
end;

-- Posiciones de los bloques
local XPos = GetPositions();

--///////////////////////////////////////////////////////////////////////////////////////////
local function SetTextFromItem(i)
	local steps = GetStepsInItem(i);
	if not steps or steps == '' then return ''; end;
	local meter = steps:GetMeter();
	if meter<10 then return (':x.0'..meter); else return (':x.'..meter); end;
end;
--///////////////////////////////////////////////////////////////////////////////////////////
--///////////////////////////////////////////////////////////////////////////////////////////
local function GetLong()
	if ActiveBlocks == 1 then	return 400; else return 600; end;
end;

t[#t+1] = LoadActor("ovalo.png").. {
	OnCommand=cmd(zoomto,GetLong(),155;diffuse,0,0,0,.8;zoom,.66;zoomx,.78);
};

local fadein  = cmd(stoptweening;diffusealpha,0;linear,.3;diffusealpha,1);
local fadeout = cmd(stoptweening;diffusealpha,1;linear,.2;diffusealpha,0);

-- BackLevels
for i=1,ActiveBlocks do
t[#t+1] = Def.ActorFrame {
	InitCommand=cmd(x,XPos[i];zoom,.5;diffusealpha,1);
	CurrentSongChangedMessageCommand=cmd(stoptweening;queuecommand,'Init');
	ChangeStepsMessageCommand=function(self,params)
		if( params.Player ~= pn ) then return; end;
		(cmd(stoptweening;queuecommand,'Init'))(self);
	end;
	StartSelectingStepsMessageCommand=cmd(stoptweening;diffuse,1,1,1,1);
	StepsChosenMessageCommand=function(self,params)
		if( params.Player ~= pn ) then return; end;
		if not ( i==GetActiveBlock() ) then self:stoptweening();self:diffuse(.3,.3,.3,1); end; --Pasa a gris los bloques.
	end;
	StepsPreselectedCancelledMessageCommand=function(self,params)
		if( params.Player ~= pn ) then return;	end;
		(cmd(stoptweening;diffuse,1,1,1,1))(self);
	end;
	children = {
		LoadFont("Levels")..{
			InitCommand=cmd(settext,SetBackLevelFromDifficulty(i));
			StartSelectingStepsMessageCommand=fadein;
			GoBackSelectingSongMessageCommand=fadeout;
		};
		LoadFont("BMfont")..{
			InitCommand=cmd(settext,SetTextFromItem(i);zoom,0.9;y,-9);
			StartSelectingStepsMessageCommand=fadein;
			GoBackSelectingSongMessageCommand=fadeout;
		};
	};
};
end;

-- Glow y demas
t[#t+1] = Def.ActorFrame {
	InitCommand=cmd(x,XPos[GetActiveBlock()];zoom,.5);
	CurrentSongChangedMessageCommand=cmd(stoptweening;sleep,.05;queuecommand,'Init');
	ChangeStepsMessageCommand=function(self,params)
		if( params.Player ~= pn ) then return; end;
		(cmd(stoptweening;sleep,.05;playcommand,'Refresh';queuecommand,'Init'))(self);
	end;
	StepsChosenMessageCommand=function(self,params)
		if( params.Player ~= pn ) then return; end;
		self:playcommand('Blink');
	end;
	StartSelectingStepsMessageCommand=cmd(stoptweening;zoom,0;sleep,.3;queuecommand,'Refresh';zoom,.49;linear,.02;zoom,.5);
	GoBackSelectingSongMessageCommand=cmd(stoptweening;zoom,0);
	children = {
		LoadActor("white.png")..{
			InitCommand=cmd(diffusealpha,0);
			RefreshCommand=cmd(stoptweening;diffusealpha,0;fadetop,0;sleep,.05;linear,.05;diffusealpha,1;linear,.05;fadetop,1;diffusealpha,0);
			OffCommand=cmd(playcommand,'Refresh');
		};
		LoadActor(name.."_frame.png")..{
			InitCommand=cmd();
			RefreshCommand=cmd(stoptweening;diffusealpha,1;linear,.05;diffusealpha,0;linear,.05;diffusealpha,1;queuecommand,'Loop');
			LoopCommand=cmd(stoptweening;zoom,1;linear,.4;zoom,1.03;linear,.4;zoom,1;queuecommand,'Loop');
			BlinkCommand=cmd(stoptweening;zoom,1;linear,.1;zoom,1.03;linear,.1;zoom,1;queuecommand,'Blink');
			OffCommand=cmd(stoptweening;zoom,1;diffusealpha,1;decelerate,.3;zoom,1.3;diffusealpha,0);
		};
		LoadActor(name.."_name.png")..{
			InitCommand=cmd(y,-120,'BlendMode_Add');
			RefreshCommand=cmd(stoptweening;diffusealpha,1;linear,.05;diffusealpha,0;linear,.05;diffusealpha,1);
			OffCommand=cmd(stoptweening;diffusealpha,1;linear,.2;diffusealpha,0);
		};
		LoadActor(name.."_Left_arrow.png")..{
			InitCommand=cmd(x,-110;horizalign,'HorizAlign_Right','BlendMode_Add');
			
		};
		LoadActor(name.."_Left_arrow.png")..{
			InitCommand=cmd(x,106;horizalign,'HorizAlign_Right';rotationz,180,'BlendMode_Add');
		};
	};
};

local i;

for i=1,2 do
t[#t+1] = LoadActor('READY.png')..{
	OnCommand=function(self)
		(cmd(stoptweening;x,XPos[GetActiveBlock()];y,35;zoomy,.6;zoomx,1;diffusealpha,0))(self);
		if i==2 then self:blend('BlendMode_Add'); end;
	end;
	ChangeStepsMessageCommand=function(self,params)
		if( params.Player ~= pn ) then return; end;
		self:x(XPos[GetActiveBlock()]);
		(cmd(stoptweening;stopeffect;diffusealpha,0))(self);
	end;
	StartSelectingStepsMessageCommand=cmd(stoptweening;diffusealpha,0);
	GoBackSelectingSongMessageCommand=cmd(stoptweening;diffusealpha,0);
	StepsChosenMessageCommand=function(self,params)
		if( params.Player ~= pn ) then return; end;
		self:diffusealpha(1);
		if i==2 then (cmd(diffuseshift;effectcolor2,color("1,1,1,.8");effectcolor1,color("1,1,1,.3");effectperiod,.2))(self); end;
	end;
	OffCommand=cmd(stoptweening;stopeffect;diffusealpha,0);
};
end;

return t;