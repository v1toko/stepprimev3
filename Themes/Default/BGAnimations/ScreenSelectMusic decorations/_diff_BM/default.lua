local t = Def.ActorFrame {};

local p1 = GAMESTATE:IsSideJoined(PLAYER_1);
local p2 = GAMESTATE:IsSideJoined(PLAYER_2);

if p1 and not p2 then
	Player = PLAYER_1;	Name = '1P';  AB = 4;
	t[#t+1] = LoadActor('Levels.lua')..{ InitCommand=cmd(zoomx,.8;zoomy,.93); };
elseif not p1 and p2 then
	Player = PLAYER_2;	Name = '2P';  AB = 4;
	t[#t+1] = LoadActor('Levels.lua')..{ InitCommand=cmd(zoomx,.8;zoomy,.93); };
elseif p1 and p2 then
	Player = PLAYER_1;	Name = '1P';  AB = 3;
	t[#t+1] = LoadActor('Levels2.lua')..{ InitCommand=cmd(x,-161;zoomx,.7;zoomy,.87); };
	Player = PLAYER_2;	Name = '2P';  AB = 3;
	t[#t+1] = LoadActor('Levels2.lua')..{ InitCommand=cmd(x,161;zoomx,.7;zoomy,.87); };
end;

return t;