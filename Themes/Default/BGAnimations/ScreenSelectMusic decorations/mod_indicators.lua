function GetCodesColumn( CUR_PLAYER )
local t = Def.ActorFrame {};
--x,24;

---------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------

local blank = THEME:GetPathG("","CWUtils").."_blank.png";
local flash = THEME:GetPathG("","CWUtils").."_flash.png";

---------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------
-- Sprite para la velocidad
local speed_codes = {};
local cur_speed_item;
t[#t+1] = Def.ActorFrame {
	InitCommand=cmd(stoptweening;y,110;zoom,.38);
	SpeedCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;zoom,.38;decelerate,.1;zoom,.35;decelerate,.2;zoom,.38))(self);
	end;
	children = {
		Def.Sprite {
			InitCommand=cmd(stoptweening;playcommand,'UpDate');
			SpeedCodeMessageCommand=function(self,params)
				if (params.Player ~= CUR_PLAYER) then return; end;
				(cmd(stoptweening;playcommand,'UpDate'))(self);
			end;
			UpDateCommand=function(self)
				speed_codes = nil;
				speed_codes = {};
				for i=1,#(speed[1]) do
					if IsCodeOn(CUR_PLAYER,"Speed",i) then
						speed_codes[#speed_codes+1] = i;
					end;
				end;
				if speed_codes ~= nil then cur_speed_item = 1; else cur_speed_item = 0; end;
				self:stoptweening();
				self:queuecommand('Loop');
			end;
			LoopCommand=function(self)
				self:stoptweening();
				if cur_speed_item == 0 then self:Load(""); return; end;
				--self:Load("Themes/FIESTA EX2/BGAnimations/ScreenSelectMusic decorations/_CM/_mods/Speed/"..sp[speed_codes[cur_speed_item]]..".png");
				self:Load(THEME:GetPathG("","Mods").."/Speed/"..speed[1][speed_codes[cur_speed_item]]..".png");
				self:sleep(3);
				if #speed_codes == cur_speed_item then cur_speed_item = 1; else cur_speed_item = cur_speed_item+1; end;
				self:queuecommand('Loop');
			end;
		};
};
};
t[#t+1] = LoadActor(flash)..{
	InitCommand=cmd(stoptweening;y,110;blend,'BlendMode_Add';diffusealpha,0);
	SpeedCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;diffusealpha,0;zoom,.8;linear,.05;diffusealpha,1;zoom,.85;sleep,.05;decelerate,.6;zoomx,.9;diffusealpha,0))(self);
	end;
};
---------------------------------------------------------------------
---------------------------------------------------------------------
-- Sprite para el display
local display_codes = {};
local cur_display_item;
t[#t+1] = Def.ActorFrame {
	InitCommand=cmd(stoptweening;y,150;zoom,.38);
	DisplayCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;zoom,.38;decelerate,.1;zoom,.35;decelerate,.2;zoom,.38))(self);
	end;
	children = {
		Def.Sprite {
			InitCommand=cmd(stoptweening;playcommand,'UpDate');
			DisplayCodeMessageCommand=function(self,params)
				if (params.Player ~= CUR_PLAYER) then return; end;
				(cmd(stoptweening;playcommand,'UpDate'))(self);
			end;
			UpDateCommand=function(self)
				display_codes = nil;
				display_codes = {};
				for i=1,#(display[1]) do
					if IsCodeOn(CUR_PLAYER,"Display",i) then
						display_codes[#display_codes+1] = i;
					end;
				end;
				if #display_codes ~= 0 then cur_display_item = 1; else cur_display_item = 0; end;
				self:stoptweening();
				self:queuecommand('Loop');
			end;
			LoopCommand=function(self)
				self:stoptweening();
				if cur_display_item == 0 then self:Load(blank); MESSAGEMAN:Broadcast("NoOneDisplayCode",{ Player = CUR_PLAYER }); return; end;
				self:Load(THEME:GetPathG("","Mods").."/Display/"..display[1][display_codes[cur_display_item]]..".png");
				self:sleep(3);
				if #display_codes == cur_display_item then cur_display_item = 1; else cur_display_item = cur_display_item+1; end;
				self:queuecommand('Loop');
			end;
		};
};
};
t[#t+1] = LoadActor(flash)..{
	InitCommand=cmd(stoptweening;y,150;blend,'BlendMode_Add';diffusealpha,0);
	DisplayCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;diffusealpha,0;zoom,.8;linear,.05;diffusealpha,1;zoom,.85;sleep,.05;decelerate,.6;zoomx,.9;diffusealpha,0))(self);
	end;
	NoOneDisplayCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;diffusealpha,0;zoom,.8))(self);
	end;
};
---------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------
-- Sprite para los noteskin
local noteskin_codes = {};
local cur_noteskin_item;
t[#t+1] = Def.ActorFrame {
	InitCommand=cmd(stoptweening;y,190;zoom,.38);
	NoteSkinCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;zoom,.38;decelerate,.1;zoom,.35;decelerate,.2;zoom,.38))(self);
	end;
	children = {
		Def.Sprite {
			InitCommand=cmd(stoptweening;playcommand,'UpDate');
			NoteSkinCodeMessageCommand=function(self,params)
				if (params.Player ~= CUR_PLAYER) then return; end;
				(cmd(stoptweening;playcommand,'UpDate'))(self);
			end;
			DefaultNoteSkinCodeMessageCommand=function(self,params)
				if (params.Player ~= CUR_PLAYER) then return; end;
				(cmd(stoptweening;Load,blank))(self);
			end;
			UpDateCommand=function(self)
				noteskin_codes = nil;
				noteskin_codes = {};
				for i=1,#noteskin do
					if IsCodeOn(CUR_PLAYER,"Noteskin",i) then
						noteskin_codes[#noteskin_codes+1] = i;
					end;
				end;
				if #noteskin_codes ~= 0 then cur_noteskin_item = 1; else cur_noteskin_item = 0; end;
				self:stoptweening();
				self:queuecommand('Loop');
			end;
			LoopCommand=function(self)
				self:stoptweening();
				if cur_noteskin_item == 0 then self:Load(blank); return; end;
				--self:Load(THEME:GetPathG("","Mods").."/Noteskin/"..noteskin[noteskin_codes[cur_noteskin_item]]..".png");
				self:Load("NoteSkins/Labels/"..noteskin[noteskin_codes[cur_noteskin_item]]..".png");
				self:sleep(3);
				if #noteskin_codes == cur_noteskin_item then cur_noteskin_item = 1; else cur_noteskin_item = cur_noteskin_item+1; end;
				self:queuecommand('Loop');
			end;
		};
};
};
t[#t+1] = LoadActor(flash)..{
	InitCommand=cmd(stoptweening;y,190;blend,'BlendMode_Add';diffusealpha,0);
	NoteSkinCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;diffusealpha,0;zoom,.8;linear,.05;diffusealpha,1;zoom,.85;sleep,.05;decelerate,.6;zoomx,.9;diffusealpha,0))(self);
	end;
	DefaultNoteSkinCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;diffusealpha,0;zoom,.8))(self);
	end;
};
---------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------
-- Sprite para el path
local path_codes = {};
local cur_path_item;
t[#t+1] = Def.ActorFrame {
	InitCommand=cmd(stoptweening;y,230;zoom,.38);
	PathCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;zoom,.38;decelerate,.1;zoom,.35;decelerate,.2;zoom,.38))(self);
	end;
	children = {
		Def.Sprite {
			InitCommand=cmd(stoptweening;playcommand,'UpDate');
			PathCodeMessageCommand=function(self,params)
				if (params.Player ~= CUR_PLAYER) then return; end;
				(cmd(stoptweening;playcommand,'UpDate'))(self);
			end;
			UpDateCommand=function(self)
				path_codes = nil;
				path_codes = {};
				for i=1,#(path[1]) do
					if IsCodeOn(CUR_PLAYER,"Path",i) then
						path_codes[#path_codes+1] = i;
					end;
				end;
				if #path_codes ~= 0 then cur_path_item = 1; else cur_path_item = 0; end;
				self:stoptweening();
				self:queuecommand('Loop');
			end;
			LoopCommand=function(self)
				self:stoptweening();
				if cur_path_item == 0 then self:Load(blank); MESSAGEMAN:Broadcast("NoOnePathCode",{Player=CUR_PLAYER}); return; end;
				self:Load(THEME:GetPathG("","Mods").."/Path/"..path[1][path_codes[cur_path_item]]..".png");
				self:sleep(3);
				if #path_codes == cur_path_item then cur_path_item = 1; else cur_path_item = cur_path_item+1; end;
				self:queuecommand('Loop');
			end;
		};
};
};
t[#t+1] = LoadActor(flash)..{
	InitCommand=cmd(stoptweening;y,230;blend,'BlendMode_Add';diffusealpha,0);
	PathCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;diffusealpha,0;zoom,.8;linear,.05;diffusealpha,1;zoom,.85;sleep,.05;decelerate,.6;zoomx,.9;diffusealpha,0))(self);
	end;
	NoOnePathCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;diffusealpha,0;zoom,.8))(self);
	end;
};
---------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------
-- Sprite para el alternate
local alternate_codes = {};
local cur_alternate_item;
t[#t+1] = Def.ActorFrame {
	InitCommand=cmd(stoptweening;y,270;zoom,.38);
	AlternateCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;zoom,.38;decelerate,.1;zoom,.35;decelerate,.2;zoom,.38))(self);
	end;
	children = {
		Def.Sprite {
			InitCommand=cmd(stoptweening;playcommand,'UpDate');
			AlternateCodeMessageCommand=function(self,params)
				if (params.Player ~= CUR_PLAYER) then return; end;
				(cmd(stoptweening;playcommand,'UpDate'))(self);
			end;
			UpDateCommand=function(self)
				alternate_codes = nil;
				alternate_codes = {};
				for i=1,#(alternate[1]) do
					if IsCodeOn(CUR_PLAYER,"Alternate",i) then
						alternate_codes[#alternate_codes+1] = i;
					end;
				end;
				if #alternate_codes ~= 0 then cur_alternate_item = 1; else cur_alternate_item = 0; end;
				self:stoptweening();
				self:queuecommand('Loop');
			end;
			LoopCommand=function(self)
				self:stoptweening();
				if cur_alternate_item == 0 then self:Load(blank); MESSAGEMAN:Broadcast("NoOneAlternateCode",{Player=CUR_PLAYER}); return; end;
				self:Load(THEME:GetPathG("","Mods").."/Alternate/"..alternate[1][alternate_codes[cur_alternate_item]]..".png");
				self:sleep(3);
				if #alternate_codes == cur_alternate_item then cur_alternate_item = 1; else cur_alternate_item = cur_alternate_item+1; end;
				self:queuecommand('Loop');
			end;
		};
};
};
t[#t+1] = LoadActor(flash)..{
	InitCommand=cmd(stoptweening;y,270;blend,'BlendMode_Add';diffusealpha,0);
	AlternateCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;diffusealpha,0;zoom,.8;linear,.05;diffusealpha,1;zoom,.85;sleep,.05;decelerate,.6;zoomx,.9;diffusealpha,0))(self);
	end;
	NoOneAlternateCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;diffusealpha,0;zoom,.8))(self);
	end;
};
---------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------
-- Sprite para el alternate
local rush_codes = {};
local cur_rush_item;
t[#t+1] = Def.ActorFrame {
	InitCommand=cmd(stoptweening;y,310;zoom,.38);
	RushCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;zoom,.38;decelerate,.1;zoom,.35;decelerate,.2;zoom,.38))(self);
	end;
	children = {
		Def.Sprite {
			InitCommand=cmd(stoptweening;playcommand,'UpDate');
			RushCodeMessageCommand=function(self,params)
				if (params.Player ~= CUR_PLAYER) then return; end;
				(cmd(stoptweening;playcommand,'UpDate'))(self);
			end;
			UpDateCommand=function(self)
				rush_codes = nil;
				rush_codes = {};
				for i=1,#(rush[1]) do
					if IsCodeOn(CUR_PLAYER,"Rush",i) then
						rush_codes[#rush_codes+1] = i;
					end;
				end;
				if #rush_codes ~= 0 then cur_rush_item = 1; else cur_rush_item = 0; end;
				self:stoptweening();
				self:queuecommand('Loop');
			end;
			LoopCommand=function(self)
				self:stoptweening();
				if cur_rush_item == 0 then self:Load(blank); MESSAGEMAN:Broadcast("NoOneRushCode",{Player=CUR_PLAYER}); return; end;
				self:Load(THEME:GetPathG("","Mods").."/Rush/"..rush[1][rush_codes[cur_rush_item]]..".png");
				self:sleep(3);
				if #rush_codes == cur_rush_item then cur_rush_item = 1; else cur_rush_item = cur_rush_item+1; end;
				self:queuecommand('Loop');
			end;
		};
};
};
t[#t+1] = LoadActor(flash)..{
	InitCommand=cmd(stoptweening;y,310;blend,'BlendMode_Add';diffusealpha,0);
	RushCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;diffusealpha,0;zoom,.8;linear,.05;diffusealpha,1;zoom,.85;sleep,.05;decelerate,.6;zoomx,.9;diffusealpha,0))(self);
	end;
	NoOneRushCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;diffusealpha,0;zoom,.8))(self);
	end;
};
---------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------
return t;
end;