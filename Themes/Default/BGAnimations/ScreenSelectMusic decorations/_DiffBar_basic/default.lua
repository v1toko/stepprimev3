local t = Def.ActorFrame {}

--[[
for i=1,12 do
--	Black star
	t[#t+1] = LoadActor("b_star")..{
		InitCommand=function(self)
		self:zoom(.25);
		self:diffuse(.3,.3,.3,1);
			self:x(28*i-180);
		end;
	}
end;
]]--

function GetMaxMeter()
	if GAMESTATE:IsSideJoined(PLAYER_1) and GAMESTATE:IsSideJoined(PLAYER_2) then
		local meter1 = GAMESTATE:GetCurrentSteps(PLAYER_1):GetMeter();
		local meter2 = GAMESTATE:GetCurrentSteps(PLAYER_2):GetMeter();
		if meter1 >= meter2 then return meter1; else return meter2; end;
	else
		return GAMESTATE:GetCurrentSteps(GAMESTATE:GetMasterPlayerNumber()):GetMeter();
	end;
end;

for i=1,1 do
	t[#t+1] = LoadActor("estrellas")..{
		InitCommand=cmd(x,0;y,2;zoom,.800;diffuse,.7);
        };
end;
for i=1,12 do
--	Stars
	t[#t+1] = LoadActor("diff_stars 5x1")..{
		InitCommand=function(self)
			self:pause();
			self:setstate(0);
			--self:zoom(.25);
			self:x(27.8*i-180);
			self:zoom(1.35);
			(cmd(diffuseshift;effectcolor1,1,1,1,1;effectcolor2,1,1,1,.7;effectperiod,.5))(self);
		end;
		OnCommand=function(self)
		local meter = GetMaxMeter();
		if (meter <= 12) then
			if( i<= meter ) then
			self:diffusealpha(1);
			else
			self:diffusealpha(0);
			end;
		else
		self:diffusealpha (1);
		end;
		end;
		ChangeStepsMessageCommand=cmd(playcommand,'On');
		CurrentSongChangeMessageCommand=cmd(playcommand,'On');
	}
end;
return t;