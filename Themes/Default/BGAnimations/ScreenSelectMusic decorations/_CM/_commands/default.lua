function GetCommands( CUR_PLAYER )
	local t = Def.ActorFrame {}

	local ActorsPos = {
		(cmd(horizalign,'HorizAlign_Left';x,-115;zoomx,0;diffusealpha,0)),
		(cmd(horizalign,'HorizAlign_Left';x,-115;zoomx,.65;diffusealpha,.5)),
		(cmd(horizalign,'HorizAlign_Center';x,0;zoomx,1;diffusealpha,1;linear,.1;zoom,.9;decelerate,.12;zoom,1)),
		(cmd(horizalign,'HorizAlign_Right';x,115;zoomx,.65;diffusealpha,.5)),
		(cmd(horizalign,'HorizAlign_Right';x,115;zoomx,0;diffusealpha,0)),
	};
	--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	local size = #(GetOptionsNames());

	local function GetActors()
		local co = GetCurrentOption(CUR_PLAYER);
		if co == 1 then
			return { size-1, size, 1, 2, 3 };
		elseif co == 2 then
			return { size, 1, 2, 3, 4 };
		elseif co == size then
			return { size-2, size-1, size, 1, 2 };
		elseif co == size-1 then
			return { size-3, size-2, size-1, size, 1 };
		else
			return { co-2,co-1, co, co+1, co+2 };
		end;
	end;

	--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	local Actors = {};
	local factor;

	t[#t+1] = LoadActor("black_frame") .. {
		InitCommand=cmd(zoom, 0.7;zoomx, 0.675);
	};

	
	-- Efecto del comand windows, Estatico 100%
	t[#t+1] = LoadActor("../effect_codes")..{
		InitCommand=cmd(blend,'BlendMode_Add';);
		OnCommand=cmd(stoptweening;diffuseshift,1;decelerate,1;effectperiod,2;effectcolor1,1,1,1,0.2;effectcolor2,1,1,1,0);
	};
	
	t[#t+1] = LoadActor("../effect_codes")..{
		InitCommand=cmd(blend,'BlendMode_Add';);
		OnCommand=cmd(stoptweening;diffusealpha,0;zoomx,0.7);
		OptionsListPrevMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER) then return; end;
			(cmd(stoptweening;
			linear, 0.05;
			diffusealpha,1;
			x,-50;
			linear, 0.1;
			x,50;	
			linear, 0.01;			
			diffusealpha,0))(self);
		end;
		OptionsListNextMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER) then return; end;
			(cmd(stoptweening;
			linear, 0.05;
			diffusealpha,1;
			x,50;
			linear, 0.1;
			x,-50;	
			linear, 0.01;			
			diffusealpha,0))(self);
		end;
		OffCommand=cmd(stoptweening;diffusealpha,0);
	};
	
	-- Para cambiar el indice
	t[#t+1] = Def.ActorFrame {
		InitCommand=function(self)
			Actors = GetActors();
		end;
		OptionsListNextMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER) then return; end;
			if not IsSelectingOption(CUR_PLAYER) then return; end;
			if GetCurrentOption(CUR_PLAYER) == size then SetCurrentOption(CUR_PLAYER,1); else SetCurrentOption(CUR_PLAYER,GetCurrentOption(CUR_PLAYER)+1); end;
			Actors = GetActors();
			factor = -1;
			MESSAGEMAN:Broadcast("CMChanged",{ Player = CUR_PLAYER });
		end;
		OptionsListPrevMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER) then return; end;
			if not IsSelectingOption(CUR_PLAYER) then return; end;
			if GetCurrentOption(CUR_PLAYER) == 1 then SetCurrentOption(CUR_PLAYER,size); else SetCurrentOption(CUR_PLAYER,GetCurrentOption(CUR_PLAYER)-1); end;
			Actors = GetActors();
			factor = 1;
			MESSAGEMAN:Broadcast("CMChanged",{ Player = CUR_PLAYER });
		end;
	}
	

	-- Iconoes de las opciones
	local opNames = GetOptionsNames();
	for i=1,#opNames do
	t[#t+1] = LoadActor( "commands" )..{
		InitCommand=function(self)
			Actors = GetActors();
			self:stoptweening(); 
			if 	    i==Actors[1] then (ActorsPos[1])(self);
			elseif 	i==Actors[2] then (ActorsPos[2])(self);
			elseif 	i==Actors[3] then (ActorsPos[3])(self); --center
			elseif 	i==Actors[4] then (ActorsPos[4])(self);
			elseif 	i==Actors[5] then (ActorsPos[5])(self);
			else (cmd(stoptweening;diffusealpha,0))(self);
			end;

			(cmd(animate,0;pause;setstate,GetStateByOptionName(opNames[i])))(self);
		end;
		CMChangedMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
			self:finishtweening(); --detiene todas las animaciones
			if 	    i==Actors[1] then self:linear(.18);(ActorsPos[1])(self);
			elseif 	i==Actors[2] then if factor==-1 then self:x( -(self:GetZoomedWidth())/2 );end; self:linear(.18);(ActorsPos[2])(self);
			elseif 	i==Actors[3] then self:x( factor*(self:GetZoomedWidth())/2 + (self:GetX()));self:linear(.18);(ActorsPos[3])(self);--center
			elseif 	i==Actors[4] then if factor==1 then self:x( (self:GetZoomedWidth())/2 );end; self:linear(.18);(ActorsPos[4])(self);
			elseif 	i==Actors[5] then self:linear(.18);(ActorsPos[5])(self);
			else (cmd(stoptweening;diffusealpha,0))(self);
			end;
		end;
	};
	end;

	return t;
end;
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////