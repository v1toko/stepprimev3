function GetArrowsSteps( CUR_PLAYER )
local t = Def.ActorFrame {}

t[#t+1] = LoadActor("dr")..{
	InitCommand=cmd(x,83;y,130;blend,'BlendMode_Add';diffusealpha,0);
	OptionsListNextMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER ) then return; end;
		(cmd(stoptweening;diffusealpha,1;zoom,1;decelerate,.05;zoom,1.08;linear,.3;zoom,1.3;diffusealpha,0))(self);
	end;
}

t[#t+1] = LoadActor("dl")..{
	InitCommand=cmd(x,-83;y,130;diffusealpha,0);
	OptionsListPrevMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER ) then return; end;
		(cmd(stoptweening;diffusealpha,1;zoom,1;decelerate,.05;zoom,1.08;linear,.3;zoom,1.3;diffusealpha,0))(self);
	end;
}

t[#t+1] = LoadActor("ul")..{
	InitCommand=cmd(x,-83;y,-130;diffusealpha,0);
	OptionsListBackMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER ) then return; end;
		(cmd(stoptweening;diffusealpha,1;zoom,1;decelerate,.05;zoom,1.08;linear,.3;zoom,1.3;diffusealpha,0))(self);
	end;
}

t[#t+1] = LoadActor("ur")..{
	InitCommand=cmd(x,83;y,-130;diffusealpha,0);
	OptionsListBackMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER ) then return; end;
		(cmd(stoptweening;diffusealpha,1;zoom,1;decelerate,.05;zoom,1.08;linear,.3;zoom,1.3;diffusealpha,0))(self);
	end;
}
return t;
end;