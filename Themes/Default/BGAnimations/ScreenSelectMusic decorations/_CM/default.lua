--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-- Options = Speed, Display, etc.
-- Codes = 3x, V, M, Reset, etc.
local current_option = { Player1 = 1 , Player2 = 1 };
local option_names = { "Speed", "Display", "Noteskin", "Path", "Alternate", "Judge", "Rush", "System", "Rank" };
local frames_options = { Speed = 8, Display = 7, Noteskin = 6, Path = 5, Alternate = 4, Rush = 2, System = 0, Rank = 1, Judge = 3 };
local options_desc = { 	
						Speed = "Cambia la velocidad\n de la nota", 
						Display = "Cambia la apariencia\n de juego", 
						Noteskin = "Cambia el skin de la nota", 
						Path = "Cambia el patron\n de la cancion", 
						Alternate = "Cambia la aparicion\n de los steps", 
						Rush = "Cambia la velocidad\n de la cancion", 
						Judge = "Cambia la dificultad",
						Rank = "Configura el Rank Mode",
						System = "Reestablece todos\n los comandos"
					};					
local codes_desc = 	{
						["0.5x"] = "Cambia la velocidad de\n la nota por +0.5X", 
						["1x"] = "Cambia la velocidad de\n la nota por 1X", 
						["2x"] = "Cambia la velocidad de\n la nota por 2X", 
						["3x"] = "Cambia la velocidad de\n la nota por 3X", 
						["4x"] = "Cambia la velocidad de\n la nota por 4X", 
						["5x"] = "Cambia la velocidad de\n la nota por 5X", 
						["6x"] = "Cambia la velocidad de\n la nota por 6X", 
						["RV"] = "La velocidad de las notas\n se cambia al azar", 
						["AC"] = "La velocidad de las notas\n se mueve acelerando", 
						["DC"] = "La velocidad de las notas\n se mueve disminuyendo", 
						["EW"] = "Se mueven las notas\n como gusano", 
						["+100"] = "Aumenta la velocidad\n automatica en 100",
						["-100"] = "Disminuye la velocidad\n automatica en 100",
						["+10"] = "Aumenta la velocidad\n automatica en 10",
						["-10"] = "Disminuye la velocidad\n automatica en 10",
						["+1"] = "Aumenta la velocidad\n automatica en 1",
						["-1"] = "Disminuye la velocidad\n automatica en 1",
						["V"] = "Las notas desaparecen\n en medio de la pantalla",
						["NS"] = "Las notas son\n invisibles", 
						["AP"] = "Las notas aparecen\n en medio de la pantalla", 
						["FD"] = "La zona de\n secuencia desaparece", 
						["FL"] = "Las notas parpadean\n mientras se mueven", 
						["RNS"] = "Las notas tienen\n distintos noteskins",
						["BGA_Off"] = "Desactiva en BGA\n original de la cancion",
						["X"] = "Se mueven las notas\n dibujando la ruta diagonal", 
						["NX"] = "Se mueven las notas\n a la distancia", 
						["UA"] = "Aparece la posicion\n de los steps al reves", 
						["DR"] = "Las notas y la zona\n de secuencia\n se mueven hacia abajo", 
						["SN"] = "Se mueven las notas\n como serpiente",
						["RS"] = "Aparece la posición\n de los steps al azar",
						["M"] = "Cambia el patron\n de la cancion", 
						["JR"] = "Cambia el resultado de\n juicio para que sea al reves", 
						["SI"] = "Se mueven las notas\n dibujando curva hundida",
						["RI"] = "Se mueven las notas\n dibujando curva inflada", 
						["EJ"] = "Cambia por\n dificultad EASY",
						["NJ"] = "Cambia por\n dificultad NORMAL",
						["HJ"] = "Cambia por\n dificultad HARD",
						["VJ"] = "Cambia por\n dificultad VERY HARD",
						["XJ"] = "Cambia por\n dificultad EXTRA HARD",
						["Rank"] = "Aplica el modo\n BGA ON, juicio VJ,\n BREAK ON",
						["80"] = "Cambia la velocidad\n de la cancion por 0.8X", 
						["90"] = "Cambia la velocidad\n de la cancion por 0.9X", 
						["110"] = "Cambia la velocidad\n de la cancion por 1.1X", 
						["120"] = "Cambia la velocidad\n de la cancion por 1.2X", 
						["130"] = "Cambia la velocidad\n de la cancion por 1.3X", 
						["140"] = "Cambia la velocidad\n de la cancion por 1.4X", 
						["150"] = "Cambia la velocidad\n de la cancion por 1.5X"					
					};				

-- referencia para los speed frames 
local mods_frames = { 
			--speed {"0.5x","1x","2x","3x","4x","5x","6x","RV","AC","DC","EW","AV"}
			["0.5x"] = 6, ["1x"] = 0, ["2x"] = 1, ["3x"] = 2, ["4x"] = 3, ["5x"] = 4, ["6x"] = 5, ["RV"] = 8, ["AC"] = 10, ["DC"] = 11, ["EW"] = 7, ["AV"] = 9,
			--velocidad automatica
			["+100"] = 145, ["-100"] = 146, ["+10"] = 147, ["-10"] = 148, ["+1"] = 149, ["-1"] = 150,
			--display ( falta el random skin )
			["V"] = 16,["NS"] = 18, ["AP"] = 17, ["FD"] = 19, ["FL"] = 20, ["BGA_Off"] = 22, ["RNS"] = 21,
			--path {"X", "NX", "UA", "DR", "SN"}
			["X"] = 64, ["NX"] = 65, ["UA"] = 66, ["DR"] = 67, ["SN"] = 71,
			--judgement {"ej", "nj", "hj"}
			["EJ"] = 85, ["NJ"] = 86, ["HJ"] = 87, ["VJ"] = 88, ["XJ"] = 89, 
			--alternate {"RS", "M", "RG", "SI", "RI", "HJ"}
			["RS"] = 82, ["M"] = 80, ["JR"] = 91, ["SI"] = 69, ["RI"] = 70,
			--rank
			["Rank"] = 144,
			--rush {"80", "90", "110", "120", "130", "140", "150"}
			["80"] = 112, ["90"] = 113, ["110"] = 114, ["120"] = 115, ["130"] = 116, ["140"] = 117, ["150"] = 118
					};

local current_option_name = "";
local isSelectingOption = { Player1 = true , Player2 = true };
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function IsSelectingOption( pn )
	if pn == PLAYER_1 then return isSelectingOption.Player1; end;
	if pn == PLAYER_2 then return isSelectingOption.Player2; end;
end;

function SetSelectingOption( pn, bol )
	if pn == PLAYER_1 then isSelectingOption.Player1 = bol; end;
	if pn == PLAYER_2 then isSelectingOption.Player2 = bol; end;
end;

function SetCurrentOption( pn , p)
	if pn == PLAYER_1 then current_option.Player1 = p; end;
	if pn == PLAYER_2 then current_option.Player2 = p; end;
end;

function GetOptionsNames()
	return option_names;
end;	

function GetStateByOptionName(option)
	return frames_options[option];
end;

function GetDescByOptionName(option)
	return options_desc[option];
end;

function GetDescByCodeName(option, code)
	if code == nil then return end;
	if option ~= "Noteskin" then return codes_desc[code]; end;
	return "Cambia el skin\n de las notas por " .. code;
end;

function GetStateByModName(mod)
	return mods_frames[mod];
end;

function GetCurrentOption( pn )
	if pn == PLAYER_1 then return current_option.Player1; end;
	if pn == PLAYER_2 then return current_option.Player2; end;
end;

function GetCurrentOptionName( pn )
	--Warn("GetCurrentOptionName: " .. table.tostring(option_names));
	if pn == PLAYER_1 then return option_names[current_option.Player1]; end;
	if pn == PLAYER_2 then return option_names[current_option.Player2]; end;
end;

--[[ function ResetAllPlayerOptions(pn)
	GAMESTATE:ResetPlayerOptions(pn);
	GAMESTATE:ApplyPreferredModifiers(pn,default_noteskin);
	GAMESTATE:ApplyPreferredModifiers(pn,"1.0xMusic");
	TurnOffCode(pn,"BGAOff");
	TurnOffCode(pn,"HJ");
	MESSAGEMAN:Broadcast("SpeedCode",{Player = pn});
	MESSAGEMAN:Broadcast("NoteSkinCode",{Player = pn});
	MESSAGEMAN:Broadcast("DefaultNoteSkinCode",{Player = pn});
	MESSAGEMAN:Broadcast("DisplayCode",{Player = pn});
	MESSAGEMAN:Broadcast("AlternateCode",{Player = pn});
	MESSAGEMAN:Broadcast("PathCode",{Player = pn});
end; ]]

function GetCommandWindow( CUR_PLAYER )
	--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--[[  * Nuevos mensajes * params.Player
		OptionsListNext
		OptionsListPrev
		OptionsListBack
		OptionsListCenter
	]]--
	--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	local t = Def.ActorFrame {};

	t[#t+1] = Def.ActorFrame {
		StartSelectingStepsMessageCommand=function(self,params)
			--Warn("sonando start1");
			SOUND:PlayOnce(THEME:GetPathS('','ButtonsSounds/start1'));
		end;

		StepsChosenMessageCommand=function(self,params)
			--Warn("sonando start1");
			SOUND:PlayOnce(THEME:GetPathS('','ButtonsSounds/start7'));
		end;

		OffCommand=function(self,params)
			--Warn("sonando start1");
			SOUND:PlayOnce(THEME:GetPathS('','ButtonsSounds/startout'));
		end;

		GoBackSelectingSongMessageCommand=function(self,params)
			--Warn("sonando start1");
			SOUND:PlayOnce(THEME:GetPathS('','ActionSounds/songunchosen'));
		end;

		OptionsListNextMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER) then return; end;
			SOUND:PlayOnce(THEME:GetPathS('','Command Windows/next'));
		end;
		OptionsListPrevMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER) then return; end;
			SOUND:PlayOnce(THEME:GetPathS('','Command Windows/next'));
		end;
		OptionsListCenterMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER) then return; end;
			if( IsSelectingOption(CUR_PLAYER) ) then 				
				--no enviamos el comando si estamos en system?
				if GetCurrentOptionName(CUR_PLAYER) ~= "System" then 
					SetSelectingOption(CUR_PLAYER,false);
					MESSAGEMAN:Broadcast("OptionSelected",{ Player = CUR_PLAYER });
					SOUND:PlayOnce(THEME:GetPathS('','Command Windows/preselect'));
				else
					--el reset llama al sonido de activar code
					SOUND:PlayOnce(THEME:GetPathS('','Command Windows/select'));
					MESSAGEMAN:Broadcast("CodeSeleted",{ Player = CUR_PLAYER });
				end;				
			else
				MESSAGEMAN:Broadcast("CodeSeleted",{ Player = CUR_PLAYER });
				SOUND:PlayOnce(THEME:GetPathS('','Command Windows/select'));
			end;
		end;
		OptionsListBackMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER) then return; end;
			if not( IsSelectingOption(CUR_PLAYER) ) then
				SetSelectingOption(CUR_PLAYER,true);
				MESSAGEMAN:Broadcast("GoBackSelectingOption",{ Player = CUR_PLAYER });
				SOUND:PlayOnce(THEME:GetPathS('','Command Windows/preselect'));
			else
				MESSAGEMAN:Broadcast("ExitOptionsList",{ Player = CUR_PLAYER });
				SOUND:PlayOnce(THEME:GetPathS('','Command Windows/exit'));
			end;
		end;
		OptionsListOpenedMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER) then return; end;
			SOUND:PlayOnce(THEME:GetPathS('','Command Windows/exit'));
		end;
	};
	--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	-- Base
	t[#t+1] = LoadActor("_basic/base");

	t[#t+1] = LoadActor("_basic/base")..{
		InitCommand=cmd(stoptweening;blend,'BlendMode_Add';diffusealpha,0);
		OptionSelectedMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER ) then return; end;
			(cmd(stoptweening;diffusealpha,.7;zoom,1;decelerate,.5;zoom,1.08;diffusealpha,0))(self);
		end;
		CodeSeletedMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER ) then return; end;
			(cmd(stoptweening;diffusealpha,.7;zoom,1;decelerate,.5;zoom,1.08;diffusealpha,0))(self);
		end;
	}
	
	-- Lista de Comandos
	t[#t+1] = LoadActor("_commands");
	
	t[#t+1] = GetCommands( CUR_PLAYER )..{
		InitCommand=cmd(y, -40);
		OptionSelectedMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER ) then return; end;
			--if GetCurrentOptionName(CUR_PLAYER) == "System" then return end;

			(cmd(stoptweening;y,-40;decelerate,.05;y,-10))(self);
		end;
		GoBackSelectingOptionMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER ) then return; end;
			--if GetCurrentOptionName(CUR_PLAYER) == "System" then return end;

			(cmd(stoptweening;y,-10;sleep,.2;linear,.05;y,-40))(self);
		end;
	}
	
	t[#t+1] = LoadActor("_commands/black_frame")..{
		InitCommand=cmd(stoptweening;zoomx,0;y,-75;diffusealpha,1);
		OptionSelectedMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER ) then return; end;
			--if GetCurrentOptionName(CUR_PLAYER) == "System" then return end;

			(cmd(stoptweening;sleep,.05;linear,.1;zoom, 0.7;zoomx, 0.675))(self);
		end;
		GoBackSelectingOptionMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER ) then return; end;
			--if GetCurrentOptionName(CUR_PLAYER) == "System" then return end;

			(cmd(stoptweening;sleep,.15;zoom, 0.7;zoomx, 0.675;linear,.05;zoomx,0))(self);
		end;
	}	
	
	-- Efectos de los codigos
	t[#t+1] = LoadActor("effect_codes")..{
		InitCommand=cmd(blend,'BlendMode_Add';);
		OnCommand=cmd(stoptweening;zoom,1;y,-75;diffusealpha,0);
		OptionsListPrevMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER) then return; end;
			if not( IsSelectingOption(CUR_PLAYER) ) then
				(cmd(stoptweening;diffusealpha,1;decelerate,.5;diffusealpha,0))(self);
			else
				self:queuecommand("On");
			end;			
		end;
		OptionsListNextMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER) then return; end;
			if not( IsSelectingOption(CUR_PLAYER) ) then
				(cmd(stoptweening;diffusealpha,1;decelerate,.5;diffusealpha,0))(self);
			else
				self:queuecommand("On");
			end;		
		end;
		OffCommand=cmd(stoptweening;diffusealpha,0);
	};
	
	
	-- Efecto del comand windows, Estatico 100%
	t[#t+1] = LoadActor("effect_option")..{
		InitCommand=cmd(blend,'BlendMode_Add';);
		OnCommand=cmd(stoptweening;zoom,1;y,63;diffuseshift,1;decelerate,1;effectperiod,2;effectcolor1,1,1,1,0.2;effectcolor2,1,1,1,0);
	};
	
	-- Efecto de comand windows, en movimiento 100%
	t[#t+1] = LoadActor("effect_option")..{
		InitCommand=cmd(blend,'BlendMode_Add';);
		OnCommand=cmd(stoptweening;zoom,1;y,63;diffusealpha,0);
		OptionsListPrevMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER) then return; end;
			(cmd(stoptweening;decelerate,.1;diffusealpha,0.8;decelerate,.1;diffusealpha,0))(self);
		end;
		OptionsListNextMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
			(cmd(stoptweening;decelerate,.1;diffusealpha,0.8;decelerate,.1;diffusealpha,0))(self);
		end;
		OffCommand=cmd(stoptweening;diffusealpha,0);
	};


	t[#t+1] = LoadFont("_youth 20px")..{
		InitCommand=cmd(stoptweening;zoom,.75;y,63;diffusealpha,1);
		OptionsListOpenedMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER) then return; end;
			self:settext(GetDescByOptionName(GetCurrentOptionName(CUR_PLAYER)));
		end;
		GoBackSelectingOptionMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER) then return; end;

			if IsSelectingOption(CUR_PLAYER) then
				self:settext(GetDescByOptionName(GetCurrentOptionName(CUR_PLAYER)));
			end;
		end;
		CMChangedMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER) then return; end;
			if IsSelectingOption(CUR_PLAYER) then
				self:settext(GetDescByOptionName(GetCurrentOptionName(CUR_PLAYER)));
			end;
		end;
		ModChangedMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER) then return; end;
			
			if not( IsSelectingOption(CUR_PLAYER) ) then
				local txt = GetDescByCodeName(params.option,params.code);
				if txt ~= nil then
					self:settext(txt);
				end;
			end;
		end;
		OffCommand=cmd(stoptweening;diffusealpha,0);
	};

	-- Lista de Codigos
	t[#t+1] = LoadActor("objeto_base");
	
	
	t[#t+1] = GetCodes( CUR_PLAYER )..{
		InitCommand=cmd(y,-75; zoom, 0.75);
	}
	
	-- Blue Arrows
	t[#t+1] = LoadActor("_basic/blue_arrows")..{
		InitCommand=cmd(stoptweening; y, -40);
		OptionsListOpenedMessageCommand=function(self,params)
			if (params.Player == CUR_PLAYER) then 
				(cmd(stoptweening;queuecommand,'Loop'))(self);
			end;
		end;
		LoopCommand=cmd(stoptweening;zoom,0.75;decelerate,.14;zoom,.79;decelerate,.14;zoom,0.75;sleep,.06;queuecommand,'Loop');
		OptionsListNextMessageCommand=function(self,params)
			if params.Player == CUR_PLAYER and IsSelectingOption(CUR_PLAYER) then
				(cmd(finishtweening;diffusealpha,1;zoom,0.75;decelerate,.18;zoom,.9;diffusealpha,.7;linear,.1;zoom,.69;diffusealpha,1;decelerate,.14;zoom,.79;decelerate,.14;zoom,0.75;queuecommand,'Loop'))(self);
			end;
		end;
		OptionsListPrevMessageCommand=function(self,params)
			if params.Player == CUR_PLAYER and IsSelectingOption(CUR_PLAYER) then
				(cmd(finishtweening;diffusealpha,1;zoom,0.75;decelerate,.18;zoom,.9;diffusealpha,.7;linear,.1;zoom,.69;diffusealpha,1;decelerate,.14;zoom,.79;decelerate,.14;zoom,0.75;queuecommand,'Loop'))(self);
			end;
		end;
		OptionSelectedMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER ) then return; end;
			--if GetCurrentOptionName(CUR_PLAYER) == "System" then return end;

			(cmd(stoptweening;diffusealpha,1;decelerate,.05;zoom,.69;decelerate,.05;zoom,.9;diffusealpha,0))(self);
		end;
		GoBackSelectingOptionMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER ) then return; end;
			--if GetCurrentOptionName(CUR_PLAYER) == "System" then return end;

			(cmd(stoptweening;diffusealpha,0;zoom,.9;sleep,.2;decelerate,.05;zoom,0.75;diffusealpha,1;queuecommand,'Loop'))(self);
		end;
	}
	
	
	
	-- Gold arrows
	t[#t+1] = LoadActor("_basic/gold_arrows")..{
		InitCommand=cmd(stoptweening;y,-75;zoom,.5;diffusealpha,0);
		OptionSelectedMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER ) then return; end;
			--if GetCurrentOptionName(CUR_PLAYER) == "System" then return end;

			(cmd(stoptweening;zoom,.7;diffusealpha,0;sleep,.2;linear,.05;diffusealpha,1;decelerate,.05;zoom,.5;queuecommand,'Loop'))(self);
		end;
		LoopCommand=cmd(stoptweening;zoom,.5;linear,.3;zoom,.55;linear,.3;zoom,.5;queuecommand,'Loop');
		CodeSeletedMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER ) then return; end;
			--if GetCurrentOptionName(CUR_PLAYER) == "System" then return end;

			(cmd(stoptweening;sleep,.05;linear,.05;zoom,.7;decelerate,.4;zoom,.5;queuecommand,'Loop'))(self);
		end;
		GoBackSelectingOptionMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER ) then return; end;
			--if GetCurrentOptionName(CUR_PLAYER) == "System" then return end;

			(cmd(stoptweening;decelerate,.05;zoom,.5;sleep,.05;linear,.05;zoom,1;diffusealpha,0))(self);
		end;
		OptionsListNextMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER) then return; end;
			if IsSelectingOption(CUR_PLAYER) then self:diffusealpha(0); return; end;
			(cmd(finishtweening;zoom,.5;diffusealpha,1;linear,.1;zoom,.55;decelerate,.1;zoom,.5;queuecommand,'Loop'))(self);
		end;
		OptionsListPrevMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER) then return; end;
			if IsSelectingOption(CUR_PLAYER) then self:diffusealpha(0); return; end;
			(cmd(finishtweening;zoom,.5;diffusealpha,1;linear,.1;zoom,.55;decelerate,.1;zoom,.5;queuecommand,'Loop'))(self);
		end;
	}
	
	-- Arrows
	t[#t+1] = LoadActor("_arrows");
	t[#t+1] = GetArrowsSteps( CUR_PLAYER );
	
	return t;
end;

-- TODO FUNK OK :)