local t = Def.ActorFrame {}

--Posicion del wheel, menos 100 pixeles desde la base--
ypos = SCREEN_HEIGHT-100;
per = .8;

--Wheel--
--t[#t+1] = LoadActor("Wheel_FIESTA_EX.lua")


 t[#t+1] = Def.ActorProxy {
	BeginCommand=function(self) 
		local Wheel = SCREENMAN:GetTopScreen():GetChild('MusicWheel'); 
		self:SetTarget(Wheel); 
		end;
} 

--[[ Whithe glow
t[#t+1] = LoadActor("resplandor.png")..{
	InitCommand=cmd(x,cx;y,ypos;zoomto,145,108;diffuse,color("1,1,1,1"));
	OnCommand=cmd(stoptweening;diffusealpha,0);
	StartSelectingStepsMessageCommand=cmd(stoptweening;diffusealpha,0;accelerate,.01;diffusealpha,1;accelerate,.12;diffusealpha,0);
	CurrentSongChangedMessageCommand=cmd(stoptweening;diffusealpha,0;sleep,.03;linear,.04;diffusealpha,1;decelerate,.12;diffusealpha,0);
};

--Frame Fiesta 2--
t[#t+1] = LoadActor("newbox")..{
	OnCommand=cmd(stoptweening;x,cx;y,ypos;zoom,.522);	
};

t[#t+1] = Def.ActorFrame{
	InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y*1.3+6;visible,true);
	SongChosenMessageCommand=cmd(sleep,0.5;visible);
	TwoPartConfirmCanceledMessageCommand=cmd(sleep,0.5;visible,true);
	SongUnchosenMessageCommand=cmd(visible,true);

	LoadFont("_squarefont 40px")..{
		InitCommand=cmd(maxwidth,300;zoomx,0.2;zoomy,0.35;y,15;x,-1;);
		CurrentSongChangedMessageCommand=function(self)
		self:stoptweening();
		local num = SCREENMAN:GetTopScreen():GetChild('MusicWheel'):GetCurrentIndex();
		if not num then num = 0 else num = num + 1 end;
		local numb = num < 1000 and string.format("%.3i", num) or scorecap(num);
		
		local index = SCREENMAN:GetTopScreen():GetChild('MusicWheel'):GetNumItems();
			if not index then index = 0 end;
		local total = index < 1000 and string.format("%.3i", index) or scorecap(index);
			self:settext( numb.." % "..total );
		end;
		CurrentCourseChangedMessageCommand=function(self)
		self:stoptweening();
		local num = SCREENMAN:GetTopScreen():GetChild('MusicWheel'):GetCurrentIndex();
			if not num then num = 0 else num = num + 1 end;
		local numb = num < 1000 and string.format("%.3i", num) or scorecap(num);
		
		local index = SCREENMAN:GetTopScreen():GetChild('MusicWheel'):GetNumItems();
			if not index then index = 0 end;
		local total = index < 1000 and string.format("%.3i", index) or scorecap(index);
			self:settext( numb.." % "..total );
		end;
	};
	-- by INTRMNS
};

--Flechas--
t[#t+1] = LoadActor("ar")..{
	OnCommand=cmd(stoptweening;x,cx;y,ypos;zoom,.5;diffusealpha,0;sleep,.2;decelerate,.2;diffusealpha,1;sleep,.1;queuecommand,'Loop');
	LoopCommand=cmd(stoptweening;linear,.4;zoom,.59;linear,.4;zoom,.53;queuecommand,'Loop');
	StartSelectingStepsMessageCommand=cmd(stoptweening;decelerate,.2;zoom,.53;diffusealpha,0);
	GoBackSelectingSongMessageCommand=cmd(playcommand,'On');
	CurrentSongChangedMessageCommand=cmd(stoptweening;diffusealpha,1;linear,.1;zoom,.6;diffusealpha,0;sleep,.1;zoom,.53;linear,.05;diffusealpha,1;queuecommand,'Loop');
}

function GetCurrentSongNumber()
	local cursong = GAMESTATE:GetCurrentSong();
	
	for i=1,#AllSongs do
		if AllSongs[i] == cursong then
		return i;
		end;
	end;
end;

function GetMeMyIndex(i)
	local temp = SCREENMAN:GetTopScreen():GetMusicWheel():GetCurrentIndex()+1;
	
	local s1 = string.format("%i", temp /100);
	local s2 = string.format("%i", (temp - s1*100)/10 );
	local s3 = string.format("%i", (temp - s1*100 - s2*10));
	
	local num = {};
	num = { s1, s2, s3 };
	return num[i];

end;


t[#t+1] = Def.ActorFrame {
	OnCommand=cmd(playcommand,'Update');
	CurrentSongChangedMessageCommand=cmd(playcommand,'Update');
	children = {
	--1st digit
	LoadFont("SongNumber")..{
		UpdateCommand=cmd(stoptweening;settext,GetMeMyIndex(1);zoomx,.6;zoomy,.35;diffusealpha,1);
		OnCommand=cmd(stoptweening;x,cx-12;y,ypos-58;zoom,0;diffusealpha,0;sleep,.14;decelerate,.02;zoomx,.6;zoomy,.35;diffusealpha,1);
		StartSelectingStepsMessageCommand=cmd(stoptweening;decelerate,.08;zoom,0;diffusealpha,0);
		GoBackSelectingGroupMessageCommand=cmd(stoptweening;decelerate,.08;zoom,0;diffusealpha,0);
		GoBackSelectingSongMessageCommand=cmd(playcommand,'On');
		StartSelectingSongMessageCommand=cmd(playcommand,'On');
		OffCommand=cmd(stoptweening);
	};
	--2nd digit
	LoadFont("SongNumber")..{
		UpdateCommand=cmd(stoptweening;settext,GetMeMyIndex(2);zoomx,.6;zoomy,.35;diffusealpha,1);
		OnCommand=cmd(stoptweening;x,cx;y,ypos-58;zoom,0;diffusealpha,0;sleep,.16;decelerate,.02;zoomx,.6;zoomy,.35;diffusealpha,1);
		StartSelectingStepsMessageCommand=cmd(stoptweening;decelerate,.08;zoom,0;diffusealpha,0);
		GoBackSelectingGroupMessageCommand=cmd(stoptweening;decelerate,.08;zoom,0;diffusealpha,0);
		GoBackSelectingSongMessageCommand=cmd(playcommand,'On');
		StartSelectingSongMessageCommand=cmd(playcommand,'On');
		OffCommand=cmd(stoptweening);
	};
	--3er digit
	LoadFont("SongNumber")..{
		UpdateCommand=cmd(stoptweening;settext,GetMeMyIndex(3);zoomx,.6;zoomy,.35;diffusealpha,1);
		OnCommand=cmd(stoptweening;x,cx+12;y,ypos-58;zoom,0;diffusealpha,0;sleep,.18;decelerate,.02;zoomx,.6;zoomy,.35;diffusealpha,1);
		StartSelectingStepsMessageCommand=cmd(stoptweening;decelerate,.08;zoom,0;diffusealpha,0);
		GoBackSelectingGroupMessageCommand=cmd(stoptweening;decelerate,.08;zoom,0;diffusealpha,0);
		GoBackSelectingSongMessageCommand=cmd(playcommand,'On');
		StartSelectingSongMessageCommand=cmd(playcommand,'On');
		OffCommand=cmd(stoptweening);
	};
};
}; ]]
	
return t;