local t = Def.ActorFrame {
	OnCommand=cmd(fov,60;zbuffer,true;vanishpoint,cx,cy);--SortByDrawOrder
}

-- Regresa la cantidad de REAL grupos
function GetNumGroupsReal()
	local groupsfinal = {};
	local temp = SONGMAN:GetSongGroupNames();
	local genres = SONGMAN:GetSongGenresNames();
	local genresfinal = {};	

	for _,v in ipairs(genres) do
		if SONGMAN:GetIfGenreIsSelectable(v) then
			table.insert(genresfinal,v);
		end
	end

	for _,v in ipairs(temp) do
		if SONGMAN:GetIfGroupIsSelectable(v) then
			table.insert(groupsfinal,v);
		end
	end

	temp = #groupsfinal + #genresfinal;
		
	return temp;
end;

--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-- Regresa la cantidad de grupos, para q sea mayor o igual a 7
function GetNumGroups()
	local groupsfinal = {};
	local temp = SONGMAN:GetSongGroupNames();
	local genres = SONGMAN:GetSongGenresNames();
	local genresfinal = {};	

	for _,v in ipairs(genres) do
		if SONGMAN:GetIfGenreIsSelectable(v) then
			table.insert(genresfinal,v);
		end
	end

	for _,v in ipairs(temp) do
		if SONGMAN:GetIfGroupIsSelectable(v) then
			table.insert(groupsfinal,v);
		end
	end

	temp = #groupsfinal + #genresfinal;

	if temp < 9 then
		while (temp < 9) do
			temp = temp + temp;
		end;
	end;
		
	return temp;
end;

-- Regresa un arreglo con todos los grupos, de tama�o GetNumGrops()
function GetAllGroups()
	local groups = SONGMAN:GetSongGroupNames();
	local genres = SONGMAN:GetSongGenresNames();
	local temp = {};

	if GAMESTATE:GetHighestNumStagesLeftForAnyHumanPlayer() > 1 then
		for _,v in ipairs(genres) do
			if SONGMAN:GetIfGenreIsSelectable(v) then
				if v == "quest zone" then
					local bMission = true;
					local players = GAMESTATE:GetNumSidesJoined();
					if players > 1 then bMission = false end;
					--[[if players == 1 then
						local pn = GAMESTATE:GetMasterPlayerNumber();
						local prf = nil
						if PROFILEMAN:IsPersistentProfile(pn) then
							prf = PROFILEMAN:GetProfile(pn)
							--TODO: excluir machine profiles
							if not prf or prf:IsMachine() then
								bMission = false
							end
						else
							bMission = false
						end
					end]]

					if bMission then
						table.insert(temp,v);
					end
				else
					table.insert(temp,v);
				end				
			end
		end

		for _,v in ipairs(groups) do
			if SONGMAN:GetIfGroupIsSelectable(v) then
				table.insert(temp,v);
			end
		end
	else
		table.insert(temp,"shortcut");
	end

	--SCREENMAN:SystemMessage(" table: " .. table.tostring( temp ) );

	local lengh = #temp;
	--
	local vector = {};
	for i=1,lengh do
		vector[i]= temp[i];	--nombre
	end;
	
	--Falta arreglar!!!!!!!!!
	while #vector < 9 do
		local templengh = #vector;
		for i=1,(#temp) do
			vector[i+templengh] = temp[i];
		end;
	end;
	
	return vector;
end;

--local NumGroups = GetNumGroups();
local AllGroups = GetAllGroups();
local NumGroups = #AllGroups;

--Warn( "numgroups: " .. tostring(NumGroups) .. " groups " .. table.tostring(AllGroups) );

--if NumGroups < 9 then
--	NumGroups = NumGroups - GetNumGroupsReal();
--end

function GetGroupInNumber(curGroup)
	--local curGroup = GAMESTATE:GetCurrentSong():GetGroupName();
	
	for j=1,NumGroups do
		if AllGroups[j]==curGroup then --regresa el primero que encuentre
		return j;
		end;
	end;
	
	return j;
	
end;

--Como minimo, el vector de AllGroups() debe tener 7 lugares
function GetBanners(curGroup)
	local toReturn = {};
	
	if curGroup == 4 then
		toReturn = { NumGroups , 1 , 2 , 3 , 4 , 5 , 6, 7 , 8 };
	elseif curGroup == 3 then
		toReturn = { NumGroups-1 , NumGroups , 1 , 2 , 3 , 4 , 5 , 6, 7 };
	elseif curGroup == 2 then
		toReturn = { NumGroups-2 , NumGroups-1 , NumGroups , 1 , 2 , 3 , 4 , 5 , 6 };
	elseif curGroup == 1 then
		toReturn = { NumGroups-3 , NumGroups-2 , NumGroups-1 , NumGroups , 1 , 2 , 3 , 4 , 5 };
	elseif curGroup == NumGroups then
		toReturn = { NumGroups-4 , NumGroups-3 , NumGroups-2 , NumGroups-1 , NumGroups , 1 , 2 , 3 , 4 };
	elseif curGroup == (NumGroups - 1) then
		toReturn = { NumGroups-5 , NumGroups-4 , NumGroups-3 , NumGroups-2 , NumGroups-1 , NumGroups , 1 , 2 , 3 };
	elseif curGroup == (NumGroups - 2) then
		toReturn = { NumGroups-6 , NumGroups-5 , NumGroups-4 , NumGroups-3 , NumGroups-2 , NumGroups-1 , NumGroups , 1 , 2 };
	elseif curGroup == (NumGroups - 3) then
		toReturn = { NumGroups-7 , NumGroups-6 , NumGroups-5 , NumGroups-4 , NumGroups-3 , NumGroups-2 , NumGroups-1 , NumGroups , 1 };
	else
		toReturn = { curGroup-4 , curGroup-3 , curGroup-2 , curGroup-1 , curGroup , curGroup+1, curGroup+2 , curGroup+3 , curGroup+4 };
	end;
	
	return toReturn;
end;
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--Posiciones iniciales
--self:rotationy( clamp( offsetFromCenter, -1, 1) * 95)
local InitPos = {
	(cmd(x,-450;rotationy,-90;z,-150;zoom,.65;diffusealpha,0)),
	(cmd(x,-400;rotationy,-90;z,-150;zoom,.65;diffusealpha,.2)),
	(cmd(x,-290;rotationy,-90;z,-100;zoom,.65;diffusealpha,.4)),
	(cmd(x,-200;rotationy,-90;z,-50;zoom,.65;diffusealpha,.6)),
	(cmd(x,0;z,0;zoom,.65;rotationy,0;diffusealpha,1)),--center
	(cmd(x,200;rotationy,90;z,-50;zoom,.65;diffusealpha,.6)),
	(cmd(x,290;rotationy,90;z,-100;zoom,.65;diffusealpha,.4)),
	(cmd(x,400;rotationy,90;z,-150;zoom,.65;diffusealpha,.2)),
	(cmd(x,450;rotationy,90;z,-150;zoom,.65;diffusealpha,0)),
};

--Para el cambio del centro
local ChangeCenter = (cmd(x,0;z,0;zoom,.65;rotationy,0;diffusealpha,1;sleep,.2;queuecommand,'Loop'));

--Posiciones de comienzo
local StartPos = {
	(cmd(diffusealpha,0;sleep,.2;linear,.1;diffusealpha,0)),
	(cmd(x,-1200;diffusealpha,0;linear,.3;diffusealpha,.2; x,-390)),
	(cmd(x,-1200;diffusealpha,0;linear,.275;diffusealpha,.4; x,-290)),
	(cmd(x,-1200;diffusealpha,0;linear,.25;diffusealpha,.8; x,-200)),
	(cmd(zoom,0;diffusealpha,0;linear,.2;diffusealpha,1;zoom,.65)),--center
	(cmd(x,1200;diffusealpha,0;linear,.25;diffusealpha,.8; x,200)),
	(cmd(x,1200;diffusealpha,0;linear,.275;diffusealpha,.4; x,290)),
	(cmd(x,1200;diffusealpha,0;linear,.3;diffusealpha,.2; x,390)),
	(cmd(diffusealpha,0;sleep,.2;linear,.1;diffusealpha,0)),
};

--Posiciones de salida
local OutPos = {
	(cmd(diffusealpha,0;sleep,.2;linear,.1;diffusealpha,0)),
	(cmd(x,-350;diffusealpha,0;linear,.25;diffusealpha,.2; addx,-600)),
	(cmd(x,-290;diffusealpha,0;linear,.275;diffusealpha,.4; addx,-600)),
	(cmd(x,-200;diffusealpha,0;linear,.3;diffusealpha,.8; addx,-600)),
	(cmd(zoom,.65;diffusealpha,1;linear,.3;diffusealpha,0;zoom,0)),--center
	(cmd(x,200;diffusealpha,0;linear,.3;diffusealpha,.8; addx,600)),
	(cmd(x,290;diffusealpha,0;linear,.275;diffusealpha,.4; addx,600)),
	(cmd(x,350;diffusealpha,0;linear,.25;diffusealpha,.2; addx,600)),
	(cmd(diffusealpha,0;sleep,.2;linear,.1;diffusealpha,0)),
};
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--[[ Pink glow
t[#t+1] = LoadActor("br")..{
	InitCommand=cmd(blend,'BlendMode_Add';diffusealpha,0);
	StartSelectingSongMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.25;linear,.05;diffusealpha,0);
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;diffusealpha,0);
}; ]]

t[#t+1] = Def.ActorFrame {
	InitCommand=cmd(z,-100;zoom,0);
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;zoom,0;sleep,.3;linear,.1;zoom,.8;linear,.05;zoom,.85);
	StartSelectingSongMessageCommand=cmd(stoptweening;zoom,0);
	ChangeGroupMessageCommand=cmd(stoptweening;linear,.15;rotationx,90;linear,.15;rotationx,0;zoom,.85);
	children = {
		LoadActor("disc_back")..{
			InitCommand=cmd(blend,'BlendMode_Add';zoom,.85);
		};
	};
}
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-- Funciones e Indice de grupo
local CurrentGroupNumber = 0;
local Banners = {};

function ActualGroupNumber()
	return GetGroupInNumber( AllGroups[CurrentGroupNumber] );
end;


-- Para cambiar el indice
t[#t+1] = Def.ActorFrame {
	InitCommand=function(self)
		--local curGroup = GAMESTATE:GetCurrentSong():GetGroupName();
		local curGroup = GAMESTATE:GetCurrentGroup();
		CurrentGroupNumber = GetGroupInNumber(curGroup);
	end;
	GoBackSelectingGroupMessageCommand=function(self)
		--local curGroup = GAMESTATE:GetCurrentSong():GetGroupName();
		local curGroup = GAMESTATE:GetCurrentGroup();
		CurrentGroupNumber = GetGroupInNumber(curGroup);
		Banners = GetBanners( CurrentGroupNumber );
		MESSAGEMAN:Broadcast("RefreshPositions");
		MESSAGEMAN:Broadcast("OpenPositions");
	end;
	ChangeGroupMessageCommand=function(self,params)
		if GAMESTATE:IsRankMode() then return end;

		SOUND:PlayOnce(THEME:GetPathS('MusicWheel','change.wav'));
		local dir = params.Dir;
		if params.Dir == nil then dir = 0 end;		
		CurrentGroupNumber = CurrentGroupNumber + (dir);	-- params.Dir = +- 1, dependiendo de la direccion
		--Warn("groupnumbre: " .. tostring());
		if CurrentGroupNumber > NumGroups then CurrentGroupNumber = 1; end;
		if CurrentGroupNumber < 1 then CurrentGroupNumber = NumGroups; end;
		Banners = GetBanners( CurrentGroupNumber );
		MESSAGEMAN:Broadcast("Move");
		MESSAGEMAN:Broadcast("GroupNameUpdate", { Group = AllGroups[CurrentGroupNumber] });
		MESSAGEMAN:Broadcast("MoveGroup", { Group = AllGroups[CurrentGroupNumber] });
		MESSAGEMAN:Broadcast("ModifyGroupInternal", { Group = AllGroups[CurrentGroupNumber] });

		--Warn( "ChangeGroupMessageCommand" );
	end;
	GroupWasSelectedMessageCommand=function(self,params)
		if GAMESTATE:IsRankMode() then return end;
		SOUND:PlayOnce(THEME:GetPathS('MusicWheel','change.wav'));
		--(cmd(lockinput,0.4))(self);
	end;
}

t[#t+1] = LoadActor(THEME:GetPathS("", "select_group_bgm loop")) .. { 
	--File = ;
	InitCommand=cmd(stop);
	GoBackSelectingGroupMessageCommand=function(self)
		--self:get():SetProperty("Loop",1);
		if GAMESTATE:IsRankMode() then return end;
		self:play();
	end;	
	StartSelectingSongMessageCommand=cmd(stop);
};

t[#t+1] = LoadActor(THEME:GetPathS("", "go to groups")) .. { 
	--File = ;
	InitCommand=cmd(stop);
	GoBackSelectingGroupMessageCommand=function(self)
		--self:get():SetProperty("Loop",1);
		if GAMESTATE:IsRankMode() then return end;
		self:play();
	end;	
	--StartSelectingSongMessageCommand=cmd(stop);
};

function GetGroupSound(group)
	local fir = SONGMAN:GetSongGroupBannerPath( group );
	local ogg = string.gsub(fir,'banner.*','info/sound.ogg');
	local mp3 = string.gsub(fir,'banner.*','info/sound.mp3');
	local wav = string.gsub(fir,'banner.*','info/sound.wav');
	local file = "";
	if (FILEMAN:DoesFileExist(ogg)) then file = ogg;
	elseif (FILEMAN:DoesFileExist(mp3)) then file = mp3;
	elseif (FILEMAN:DoesFileExist(wav)) then file = wav;
	--else return; 
	end;
	
	--SOUND:PlayOnce(file);
	return "/" .. file;
end;

function SoundExists(path)
	return FILEMAN:DoesFileExist(path);
end;

--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-- Banners XD
for i=1,NumGroups do 
t[#t+1] = Def.Sprite {
	InitCommand = function(self)
		(cmd(stoptweening;diffusealpha,0;zbuffer,true))(self);
		local gname = AllGroups[i];
		--self:scaletoclipped(300,190);		
		self:zoom(.6);
		if i then
			self:Load(SONGMAN:GetSongGroupBannerPath(gname));	-- se generan los actores
		end;
	end;
	RefreshPositionsMessageCommand = function(self)
		self:stoptweening(); 
		if 	    i==Banners[1] then (InitPos[1])(self);
		elseif 	i==Banners[2] then (InitPos[2])(self);
		elseif 	i==Banners[3] then (InitPos[3])(self);
		elseif 	i==Banners[4] then (InitPos[4])(self); 
		elseif 	i==Banners[5] then (InitPos[5])(self); --center
		elseif 	i==Banners[6] then (InitPos[6])(self);
		elseif 	i==Banners[7] then (InitPos[7])(self);
		elseif 	i==Banners[8] then (InitPos[8])(self);
		elseif 	i==Banners[9] then (InitPos[9])(self);
		else (cmd(stoptweening;diffusealpha,0))(self);
		end;
		self:diffusealpha(1);
	end;
	MoveMessageCommand=function(self)
		self:stoptweening(); --detiene todas las animaciones
		if 	    i==Banners[1] then self:decelerate(.3);(InitPos[1])(self);
		elseif 	i==Banners[2] then self:decelerate(.3);(InitPos[2])(self);
		elseif 	i==Banners[3] then self:decelerate(.3);(InitPos[3])(self);
		elseif 	i==Banners[4] then self:decelerate(.3);(InitPos[4])(self);
		elseif 	i==Banners[5] then self:decelerate(.3);(ChangeCenter)(self); --center
		elseif 	i==Banners[6] then self:decelerate(.3);(InitPos[6])(self);
		elseif 	i==Banners[7] then self:decelerate(.3);(InitPos[7])(self);
		elseif 	i==Banners[8] then self:decelerate(.3);(InitPos[8])(self);
		elseif 	i==Banners[9] then self:decelerate(.3);(InitPos[9])(self);
		else (cmd(stoptweening;diffusealpha,0))(self);
		end;
	end;
	LoopCommand=cmd(stoptweening;);
	StartSelectingSongMessageCommand=function(self,params)
		self:stoptweening();	
		if 	    i==Banners[1] then (OutPos[1])(self);
		elseif 	i==Banners[2] then (OutPos[2])(self);
		elseif 	i==Banners[3] then (OutPos[3])(self);
		elseif 	i==Banners[4] then (OutPos[4])(self);
		elseif 	i==Banners[5] then (OutPos[5])(self); --center
		elseif 	i==Banners[6] then (OutPos[6])(self);
		elseif 	i==Banners[7] then (OutPos[7])(self);
		elseif 	i==Banners[8] then (OutPos[8])(self);
		elseif 	i==Banners[9] then (OutPos[9])(self);
		else (cmd(stoptweening;diffusealpha,0))(self);
		end;
	end;
	OpenPositionsMessageCommand=function(self)	--comienza la seleccion de grupos
		self:stoptweening();
		if 	    i==Banners[1] then (StartPos[1])(self);
		elseif 	i==Banners[2] then (StartPos[2])(self);
		elseif 	i==Banners[3] then (StartPos[3])(self);
		elseif 	i==Banners[4] then (StartPos[4])(self); 
		elseif 	i==Banners[5] then (StartPos[5])(self); --center
		elseif 	i==Banners[6] then (StartPos[6])(self);
		elseif 	i==Banners[7] then (StartPos[7])(self);
		elseif 	i==Banners[8] then (StartPos[8])(self);
		elseif 	i==Banners[9] then (StartPos[9])(self);
		else (cmd(stoptweening;diffusealpha,0))(self);
		end;
	end;
};

	--cuidado con esto wn !!!
	--esta funci�n esta m�s peligrosa que una pira�a en el bidet
	if SONGMAN:DoesSongGroupExist(AllGroups[i]) then
		if GetGroupSound(AllGroups[i]) ~= "/" then
			t[#t+1] = LoadActor(GetGroupSound(AllGroups[i])) .. {
				Name=AllGroups[i];
				GoBackSelectingGroupMessageCommand=cmd(stoptweening;sleep,.6;queuecommand,'Play');
				MoveMessageCommand=cmd(stoptweening;sleep,.6;queuecommand,'Play');
				PlayCommand=function(self)
					if GAMESTATE:IsRankMode() then return; end;
					if not SoundExists(GetGroupSound(AllGroups[i])) then return; end;

					if AllGroups[i] == AllGroups[CurrentGroupNumber] then self:play(); else self:stop() end;
				end;
			};
		end;
	else 
		t[#t+1] = LoadActor(THEME:GetPathS("","Groups/" .. AllGroups[i] .. ".wav")) .. {
			GoBackSelectingGroupMessageCommand=cmd(stoptweening;sleep,.6;queuecommand,'Play');
			MoveMessageCommand=cmd(stoptweening;sleep,.6;queuecommand,'Play');
			PlayCommand=function(self)
				if GAMESTATE:IsRankMode() then return; end;
				if not SoundExists(THEME:GetPathS("","Groups/" .. AllGroups[i] .. ".wav")) then return; end;
				
				if AllGroups[i] == AllGroups[CurrentGroupNumber] then self:play(); else self:stop() end;
			end;
		};
	end;

end;

--[[
function PlayGroupSound()
	local fir = SONGMAN:GetSongGroupBannerPath( AllGroups[CurrentGroupNumber] );
	local ogg = string.gsub(fir,'banner.*','info/sound.ogg');
	local mp3 = string.gsub(fir,'banner.*','info/sound.mp3');
	local wav = string.gsub(fir,'banner.*','info/sound.wav');
	local file = "";
	if (FILEMAN:DoesFileExist(ogg)) then file = ogg;
	elseif (FILEMAN:DoesFileExist(mp3)) then file = mp3;
	elseif (FILEMAN:DoesFileExist(wav)) then file = wav;
	else return; end;
	
	SOUND:PlayOnce(file);
end;

t[#t+1] = Def.ActorFrame {
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;sleep,.6;queuecommand,'Play');
	MoveMessageCommand=cmd(stoptweening;sleep,.6;queuecommand,'Play');
	PlayCommand=function(self)
		--if GAMESTATE:IsRankMode() then return end;
		
		if SONGMAN:DoesSongGroupExist(AllGroups[CurrentGroupNumber]) then
			SOUND:StopMusic();
			PlayGroupSound();
		else
			SOUND:StopMusic();
			SOUND:PlayOnce( THEME:GetPathS("","Groups/" .. AllGroups[CurrentGroupNumber] .. ".wav") );
		end;
	end;
}
]]

t[#t+1] = LoadFont("Special/_myriad pro 20px") .. {
	InitCommand=function(self)
		(cmd(zoom,.05;y,170;settext,'';shadowlength,1;strokecolor,0,0,1,1;diffuse,0.8,.9,1,1))(self);
	end;
	Move2Command=function(self)
		self:settext('');

		if not AllGroups[CurrentGroupNumber] then return end;

		if SONGMAN:DoesSongGroupExist(AllGroups[CurrentGroupNumber]) then
			local fir = SONGMAN:GetSongGroupBannerPath( AllGroups[CurrentGroupNumber] );
			local dir = string.gsub(fir,'banner.*','info/text.ini');

			local tt = lua.ReadFile(dir);
			self:settext(tt);
		else
			local path = THEME:GetPathO("","") .. "/" .. AllGroups[CurrentGroupNumber] .. ".ini";
			--Warn(path);
			local tt = lua.ReadFile(path);
			self:settext(tt);
		end;

		--self:settext(string.sub(AllGroups[CurrentGroupNumber],4));

		(cmd(stoptweening;zoom,.7;wrapwidthpixels,600/0.75;cropright,1;sleep,.5;linear,0.7;cropright,0))(self);
	end;
	MoveMessageCommand=function(self,params)
		self:settext('');

		if not self then return end;

		if SONGMAN:DoesSongGroupExist(AllGroups[CurrentGroupNumber]) then
			local fir = SONGMAN:GetSongGroupBannerPath( AllGroups[CurrentGroupNumber] );
			local dir = string.gsub(fir,'banner.*','info/text.ini');

			local tt = lua.ReadFile(dir);
			self:settext(tt);
		else
			local path = THEME:GetPathO("","") .. "/" .. AllGroups[CurrentGroupNumber] .. ".ini";
			--Warn(path);
			local tt = lua.ReadFile(path);
			self:settext(tt);
		end;		

		--self:settext(string.sub(AllGroups[CurrentGroupNumber],4));

		(cmd(stoptweening;zoom,.7;wrapwidthpixels,600/0.75;cropright,1;sleep,.5;linear,0.7;cropright,0))(self);
	end;
	GoBackSelectingGroupMessageCommand=cmd(playcommand,'Move2');
	StartSelectingSongMessageCommand=cmd(stoptweening;settext,'');
}

t[#t+1] = LoadActor("numero")..{
	InitCommand=cmd(x,2;y,120;zoom,0);
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;zoom,0;sleep,.3;linear,.1;zoom,0.8;zoomx,0.7;zoomy,0.7;sleep,.05;queuecommand,'Loop');
};

t[#t+1] = LoadFont("group_numbers")..{
	InitCommand=cmd(zoom,.8;y,119;x,20;settext,string.format("%02d",NumGroups);diffusealpha,0);
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;sleep,.3;linear,.1;diffusealpha,1);
	StartSelectingSongMessageCommand=cmd(stoptweening;linear,.1;diffusealpha,0);
}

t[#t+1] = LoadFont("group_numbers")..{
	InitCommand=cmd(zoom,.8;y,119;x,-14;settext,string.format("%02d",CurrentGroupNumber);diffusealpha,0);
	MoveMessageCommand=cmd(settext,string.format("%02d",CurrentGroupNumber));
	-- ****** que pasa si es un genre?? ****
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;settext,string.format("%02d",GetGroupInNumber(AllGroups[CurrentGroupNumber]));sleep,.3;linear,.1;diffusealpha,1);
	StartSelectingSongMessageCommand=cmd(stoptweening;linear,.1;diffusealpha,0);
}

local xmin = -190;
local xmax = 175; 

t[#t+1] = Def.ActorFrame { 
	LoadActor("subraya")..{
		Name="Subraya";
		InitCommand=cmd(y,147;zoom,.75;diffusealpha,0);
		GoBackSelectingGroupMessageCommand=cmd(stoptweening;zoom,0;sleep,.3;linear,.1;diffusealpha,1;zoom,.75);
		StartSelectingSongMessageCommand=cmd(stoptweening;zoom,0);
	};

	LoadActor("dot")..{
		Name="Dot";
		InitCommand=cmd(y,147;zoom,.75;diffusealpha,0;x,xmax);
		GoBackSelectingGroupMessageCommand=cmd(stoptweening;x,scale( GetGroupInNumber(AllGroups[CurrentGroupNumber])/#AllGroups, 0, 1, xmin, xmax );zoom,0;sleep,.3;linear,.1;diffusealpha,1;zoom,.75);
		StartSelectingSongMessageCommand=cmd(stoptweening;zoom,0);
		MoveMessageCommand=function(self)
			local l = GetGroupInNumber(AllGroups[CurrentGroupNumber])
			local mod = CurrentGroupNumber/#AllGroups;
			local x = scale( mod, 0, 1, xmin, xmax );
			self:x(x);
		end;
	};
}

--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-- der
t[#t+1] = LoadActor("arrow.png")..{
	InitCommand=cmd(x,155;rotationz,180;zoom,0; z, 100; bob; effectmagnitude, 3,0,0; effectperiod, .9);
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;zoom,0;sleep,.3;linear,.1;zoom,.55;sleep,.05;queuecommand,'Loop');
	StartSelectingSongMessageCommand=cmd(stoptweening;zoom,0);
	NextGroupMessageCommand=cmd(stoptweening;linear,.05;x,155;decelerate,.2;x,150;decelerate,.1;x,155;queuecommand,'Loop');	
};

--izq
t[#t+1] = LoadActor("arrow.png")..{
	InitCommand=cmd(x,-155;zoom,0; z, 100;bob; effectmagnitude, -3,0,0; effectperiod, .9);
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;zoom,0;sleep,.3;linear,.1;zoom,.55;sleep,.05;queuecommand,'Loop');
	StartSelectingSongMessageCommand=cmd(stoptweening;zoom,0);
	PrevGroupMessageCommand=cmd(stoptweening;linear,.05;x,-155;decelerate,.2;x,-150;decelerate,.1;x,-155;queuecommand,'Loop');
	
};
return t;