local lastGenre = "";

local t = Def.ActorFrame {
	OnCommand=function(self)
		if GAMESTATE:GetCurrentSong() then
			local curGenre = GAMESTATE:GetCurrentSong():GetSongCategory();
			lastGenre = GAMESTATE:GetCurrentSong():GetSongCategory();
		end
		local curGroup = GAMESTATE:GetCurrentGroup();		
		--MESSAGEMAN:Broadcast("ModifyGroupInternal", { Group = nGroup });
		--MESSAGEMAN:Broadcast("GroupNameUpdate", { Group = nGroup });
	end;
	CurrentSongChangedMessageCommand=function()
		if GAMESTATE:GetCurrentSong() then 
			local nGenre = GAMESTATE:GetCurrentSong():GetSongCategory();
			if nGenre ~= lastGenre then
				MESSAGEMAN:Broadcast("ChangeCategory");
				MESSAGEMAN:Broadcast("SoundCategory", { Genre = string.lower(nGenre) });
				lastGenre = nGenre;
			end;
		end

		local nGroup = GAMESTATE:GetCurrentGroup();
		if nGroup ~= curGroup then
			MESSAGEMAN:Broadcast("GroupNameUpdate", { Group = nGroup });
			MESSAGEMAN:Broadcast("ModifyGroupInternal", { Group = nGroup });
			curGroup = nGroup;
		end;
	end;
	CurrentCourseChangedMessageCommand=function()
		local nGenre = "";
		if nGenre ~= curGenre then
			MESSAGEMAN:Broadcast("ChangeCategory");
			curGenre = nGenre;
		end;
		local nGroup = GAMESTATE:GetCurrentGroup();
		if nGroup ~= curGroup then
			MESSAGEMAN:Broadcast("GroupNameUpdate", { Group = nGroup });
			MESSAGEMAN:Broadcast("ModifyGroupInternal", { Group = nGroup });
			curGroup = nGroup;
		end;
	end;
};

--Comandos
local in_stg = cmd(stoptweening;diffusealpha,0;sleep,.2;decelerate,.2;diffusealpha,1);
local out_stg = cmd(stoptweening;linear,.1;diffusealpha,0;sleep,.02;linear,.08;diffusealpha,1;decelerate,.1;diffusealpha,0);

--CHANNEL TEXT
t[#t+1] = Def.ActorFrame {
	GoFullModeMessageCommand=cmd(playcommand,'Init');
	InitCommand=cmd(stoptweening;x,cx-70);	--Movimiento
	OnCommand=cmd(stoptweening;diffusealpha,0;sleep,.2;decelerate,.1;diffusealpha,1);
	OffCommand=cmd(stoptweening;decelerate,.1;y,-60);	--Mov
	children = {
		LoadFont("_karnivore lite Bold 20px")..{
			InitCommand=function(self)
				(cmd(y,25;zoom,.8;sleep,.2))(self);
				--if GAMESTATE:GetCurrentSong() == nil then return end;
				local Group = GAMESTATE:GetCurrentGroup();
				--local Group = THEME:GetMetric("ScreenSelectMusic","FullModeInitGroup");
				if SONGMAN:DoesSongGroupExist(Group) then
					self:settext(string.upper(string.sub(Group,4)));
				else
					if Group == "fav" then Group = "favorites" end;
					if Group == "full" then Group = "full song" end;
					if Group == "co-op" then Group = "co-op play" end;
					self:settext(string.upper(Group));
				end;
			end;
			OnCommand=in_stg;
			OffCommand=out_stg;	--Fade
			ChangeGroupMessageCommand=function(self, params)
				if params.Group == nil then return end;

				local Group = params.Group;

				if SONGMAN:DoesSongGroupExist(Group) then
					self:settext(string.upper(string.sub(Group,4)));
				else
					if Group == "fav" then Group = "favorites" end;
					if Group == "full" then Group = "full song" end;
					if Group == "co-op" then Group = "co-op play" end;
					self:settext(string.upper(Group));
				end;
			end;
			GroupNameUpdateMessageCommand=function(self, params)
				if params.Group == nil then return end;

				local Group = params.Group;

				if SONGMAN:DoesSongGroupExist(Group) then
					self:settext(string.upper(string.sub(Group,4)));
				else
					if Group == "fav" then Group = "favorites" end;
					if Group == "full" then Group = "full song" end;
					if Group == "co-op" then Group = "co-op play" end;
					self:settext(string.upper(Group));
				end;
			end;
		};
		LoadActor("channel")..{
			InitCommand=cmd(y,10;zoom,.75);
			OnCommand=in_stg;
			OffCommand=out_stg;
		};
	};
};

local bPlayable = false;

--CATEGORY TEXT
t[#t+1] = Def.ActorFrame {
	InitCommand=cmd(stoptweening;x,cx+220;queuecommand,'Adjust');	--Movimiento
	OnCommand=cmd(stoptweening;diffusealpha,0;sleep,.2;decelerate,.1;diffusealpha,1);
	OffCommand=cmd(stoptweening;decelerate,.1;y,-60);	--Mov
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;zoom,0);

	--con el delay para el command 'adjust' evitamos el bug de GAMESTATE:GetCurrentGroup()
	StartSelectingSongMessageCommand=cmd(sleep,.05;queuecommand,'Adjust');
	AdjustCommand=function(self)
		if GAMESTATE:GetCurrentGroup() ~= "music train" and GAMESTATE:GetCurrentGroup() ~= "random" then
			(cmd(stoptweening;zoom,1))(self);
		else
			(cmd(stoptweening;zoom,0))(self);
		end;
	end;

	children = {
		LoadFont("_karnivore lite Bold 20px")..{
			InitCommand=function(self)
				self:y(25);
				self:zoom(.8);
				if GAMESTATE:GetCurrentGroup() == "random" then self:zoom(0); return; end;
				if not GAMESTATE:GetCurrentSong() then return end;
				if GAMESTATE:GetCurrentSong():GetSongCategory() == nil then return end;
				local genre = GAMESTATE:GetCurrentSong():GetSongCategory();
				if genre == nil then return end;
				self:settext(string.upper(genre));
			end;
			OnCommand=in_stg;
			OffCommand=out_stg;	--Fade
			CurrentSongChangedMessageCommand=cmd(playcommand,'Init');
			CurrentCourseChangedMessageCommand=cmd(playcommand,'Init');
		};

		LoadActor("categor")..{
			InitCommand=cmd(y,10;zoom,.75;visible,GAMESTATE:GetCurrentGroup() ~= "random");
			OnCommand=in_stg;
			OffCommand=out_stg;
		};

		--[[
		LoadActor("cat_sounds/01_NEW_TUNES.WAV") .. {
			SoundCategoryMessageCommand=function(self, params)
				self:stop();
				if params.Genre == "new tunes" and bPlayable then
					self:play();
				end;	
			end;
			StartSelectingSongMessageCommand=function(self) bPlayable = false; end;
			CurrentSongChangedMessageCommand=function(self) bPlayable = true; end;
		};

		LoadActor("cat_sounds/02_ORIGINAL_TUNES.WAV") .. {
			SoundCategoryMessageCommand=function(self, params)
				self:stop();
				if params.Genre == "original" and bPlayable  then
					self:play();
				end;	
			end;
			StartSelectingSongMessageCommand=function(self) bPlayable = false; end;
			CurrentSongChangedMessageCommand=function(self) bPlayable = true; end;
		};

		LoadActor("cat_sounds/03_K-POP.WAV") .. {
			SoundCategoryMessageCommand=function(self, params)
				self:stop();
				if params.Genre == "k-pop" and bPlayable  then
					self:play();
				end;	
			end;
			StartSelectingSongMessageCommand=function(self) bPlayable = false; end;
			CurrentSongChangedMessageCommand=function(self) bPlayable = true; end;
		};

		LoadActor("cat_sounds/04_WORLD_MUSIC.WAV") .. {
			SoundCategoryMessageCommand=function(self, params)
				self:stop();
				if params.Genre == "world music" and bPlayable  then
					self:play();
				end;	
			end;
			StartSelectingSongMessageCommand=function(self) bPlayable = false; end;
			CurrentSongChangedMessageCommand=function(self) bPlayable = true; end;
		};

		LoadActor("cat_sounds/j-music.WAV") .. {
			SoundCategoryMessageCommand=function(self, params)
				self:stop();
				if params.Genre == "j-music" and bPlayable  then
					self:play();
				end;	
			end;
			StartSelectingSongMessageCommand=function(self) bPlayable = false; end;
			CurrentSongChangedMessageCommand=function(self) bPlayable = true; end;
		};

		]]
	};
};


return t