local t = Def.ActorFrame {}

-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
local xBase = {172,-173};
local xSC = {172,-173};
local InitDelay = .2;

-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
--Base lights

t[#t+1] = LoadActor("song_bright_left")..{
	InitCommand=function(self)
		self:y(31); self:zoom(.645); self:zoomx(0.9); self:x(-182);
	end;
	OnCommand=cmd(stoptweening;diffusealpha,0;blend,'BlendMode_Add');
	LoopCommand=cmd(diffusealpha,.7;decelerate,.4;diffusealpha,1;decelerate,.4;diffusealpha,.7;queuecommand,'Loop');
	StepsChosenMessageCommand=cmd(stoptweening;linear,.1;diffusealpha,1;diffuseshift;effectcolor2,color("1,1,1,1");effectcolor1,color("1,1,1,.72");effectperiod,.1);
	StartSelectingStepsMessageCommand=cmd(stoptweening;linear,.1;diffusealpha,1;diffuseshift;effectcolor2,color("1,1,1,1");effectcolor1,color("1,1,1,.72");effectperiod,0.6);
	StepsPreselectedCancelledMessageCommand=cmd(stoptweening;stopeffect;stoptweening;linear,.1;diffusealpha,1;diffuseshift;effectcolor2,color("1,1,1,1");effectcolor1,color("1,1,1,.72");effectperiod,0.6);
	GoBackSelectingSongMessageCommand=cmd(stopeffect;linear,.05;diffusealpha,0);
	OffCommand=cmd(stoptweening;stopeffect;decelerate,.05;diffusealpha,0);
};

t[#t+1] = LoadActor("song_bright_right")..{
	InitCommand=function(self)
		self:y(29); self:zoom(.645); self:zoomx(0.9);self:x(182);
	end;
	OnCommand=cmd(stoptweening;diffusealpha,0;blend,'BlendMode_Add');
	LoopCommand=cmd(diffusealpha,.7;decelerate,.4;diffusealpha,1;decelerate,.4;diffusealpha,.7;queuecommand,'Loop');
	StepsChosenMessageCommand=cmd(stoptweening;linear,.1;diffusealpha,1;diffuseshift;effectcolor2,color("1,1,1,1");effectcolor1,color("1,1,1,.72");effectperiod,.1);
	StartSelectingStepsMessageCommand=cmd(stoptweening;linear,.1;diffusealpha,1;diffuseshift;effectcolor2,color("1,1,1,1");effectcolor1,color("1,1,1,.72");effectperiod,0.6);
	StepsPreselectedCancelledMessageCommand=cmd(stoptweening;stopeffect;stoptweening;linear,.1;diffusealpha,1;diffuseshift;effectcolor2,color("1,1,1,1");effectcolor1,color("1,1,1,.72");effectperiod,0.6);
	GoBackSelectingSongMessageCommand=cmd(stopeffect;linear,.05;diffusealpha,0);
	OffCommand=cmd(stoptweening;stopeffect;decelerate,.05;diffusealpha,0);
};

-----------------------------------------------------------------------
-----------------------------------------------------------------------
--Glow light
t[#t+1] = LoadActor("chevron_left")..{
	InitCommand=function(self)
		self:x(-174); 	--Left glow		
	end;
	OnCommand=cmd(stoptweening;y,24;zoom,0;sleep,0.5;diffusealpha,0;decelerate,.2;zoom,.65;diffusealpha,1;blend,'BlendMode_Add';queuecommand,'Loop');
	PreviousSongMessageCommand=function(self)
		(cmd(stoptweening;decelerate,.05;addx, -10; decelerate,.05; addx, 10;queuecommand,'Loop'))(self);
	end;
	NextSongMessageCommand=function(self)
		(cmd(stoptweening;queuecommand,'Loop'))(self);
	end;
	GoBackSelectingSongMessageCommand=cmd(decelerate,.2;diffusealpha,1;zoom,.65;queuecommand,'Loop');
	LoopCommand=cmd(stoptweening;diffusealpha,1;linear,0.7;diffusealpha,0.5;linear,0.7;diffusealpha,1;queuecommand,'Loop');
	--NextStepMessageCommand=cmd(playcommand,'NextSong');
	--PreviousStepMessageCommand=cmd(playcommand,'PreviousSong');
	StartSelectingStepsMessageCommand=cmd(stoptweening;stopeffect;decelerate,.2;diffusealpha,0;zoom,0);
	FadeOutCommand=cmd(stoptweening;stopeffect;decelerate,.05;diffusealpha,0);
	FadeInHelperMessageCommand=cmd(stoptweening;queuecommand,'On');
};

t[#t+1] = LoadActor("chevron_right")..{
	InitCommand=function(self)
		self:x(174); 	--Left glow
	end;
	OnCommand=cmd(stoptweening;y,24;zoom,0;sleep,0.5;diffusealpha,0;decelerate,.2;zoom,.65;diffusealpha,1;blend,'BlendMode_Add';queuecommand,'Loop');
	NextSongMessageCommand=function(self)
		(cmd(stoptweening;decelerate,.05;addx, 10; decelerate,.05; addx, -10;queuecommand,'Loop'))(self);
	end;
	PreviousSongMessageCommand=function(self)
		(cmd(stoptweening;queuecommand,'Loop'))(self);
	end;
	GoBackSelectingSongMessageCommand=cmd(decelerate,.2;diffusealpha,1;zoom,.65;queuecommand,'Loop');
	LoopCommand=cmd(stoptweening;diffusealpha,1;linear,0.7;diffusealpha,0.5;linear,0.7;diffusealpha,1;queuecommand,'Loop');
	--NextStepMessageCommand=cmd(playcommand,'NextSong');
	--PreviousStepMessageCommand=cmd(playcommand,'PreviousSong');
	StartSelectingStepsMessageCommand=cmd(stoptweening;stopeffect;decelerate,.2;diffusealpha,0;zoom,0);
	FadeOutCommand=cmd(stoptweening;stopeffect;decelerate,.05;diffusealpha,0);
	FadeInHelperMessageCommand=cmd(stoptweening;queuecommand,'On');
};

--[[ t[#t+1] = LoadActor("subraya")..{
	InitCommand=cmd(x,0);
	OnCommand=cmd(stoptweening;y,120;diffusealpha,0;sleep,0.5;decelerate,.2;zoomx,.625;diffusealpha,1;blend,'BlendMode_Add');
	GoBackSelectingSongMessageCommand=cmd(decelerate,.2;diffusealpha,1);
	FadeOutCommand=cmd(stoptweening;stopeffect;decelerate,.05;diffusealpha,0);
	FadeInHelperMessageCommand=cmd(stoptweening;queuecommand,'On');
}; ]]

local xmin = -150;
local xmax = 145; 

t[#t+1] = Def.ActorFrame { 
	LoadActor("subraya")..{
		Name="Subraya";
		InitCommand=cmd(y,120;zoom,.75;diffusealpha,0);
		--GoBackSelectingGroupMessageCommand=cmd(stoptweening;zoom,0;sleep,.3;linear,.1;diffusealpha,1;zoom,.75);
		--StartSelectingSongMessageCommand=cmd(stoptweening;zoom,0);

		OnCommand=cmd(stoptweening;y,120;diffusealpha,0;sleep,0.5;decelerate,.2;zoomx,.625;diffusealpha,1;blend,'BlendMode_Add');
		GoBackSelectingSongMessageCommand=cmd(decelerate,.2;diffusealpha,1);
		FadeOutCommand=cmd(stoptweening;stopeffect;decelerate,.05;diffusealpha,0);
		FadeInHelperMessageCommand=cmd(stoptweening;queuecommand,'On');
	};

	LoadActor("dot")..{
		Name="Dot";
		InitCommand=cmd(y,120;zoom,.75;diffusealpha,1;x,xmax);
		--GoBackSelectingSongMessageCommand=cmd(stoptweening;x,scale( SCREENMAN:GetTopScreen():GetMusicWheel():GetCurrentIndex()+1/SCREENMAN:GetTopScreen():GetMusicWheel():GetNumItems(), 0, 1, xmin, xmax );zoom,0;sleep,.3;linear,.1;diffusealpha,1;zoom,.75);
		RestoreCommand=cmd(stoptweening;x,scale( SCREENMAN:GetTopScreen():GetMusicWheel():GetCurrentIndex()+1/SCREENMAN:GetTopScreen():GetMusicWheel():GetNumItems(), 0, 1, xmin, xmax );zoom,0;sleep,.3;linear,.1;diffusealpha,1;zoom,.75);
		CurrentSongChangedMessageCommand=function(self)
			local l = SCREENMAN:GetTopScreen():GetMusicWheel():GetCurrentIndex()+1;
			local mod = l/SCREENMAN:GetTopScreen():GetMusicWheel():GetNumItems();
			local x = scale( mod, 0, 1, xmin, xmax );
			self:x(x);
		end;
		FadeOutCommand=cmd(stoptweening;stopeffect;decelerate,.05;diffusealpha,0);
		FadeInHelperMessageCommand=cmd(stoptweening;queuecommand,'Restore');
	};
}

-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
--Preview (CDTitle)
local textt;

-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
--TITLE
function Actor:blinky()
	--(cmd(diffuse,0,0,0,0;linear,.4;diffuse,1,1,1,1;linear,.06;diffuse,0,0,0,0;decelerate,.2;diffuse,1,1,1,1;sleep,.05;linear,.25;diffuse,0,0,0,0))(self);
	(cmd(diffuse,1,1,1,1;fadeleft,1;faderight,1;linear,.1;fadeleft,0;linear,.1;faderight,0;sleep,.1;linear,.1;fadeleft,1;linear,.1;faderight,1;sleep,.1;
	linear,.1;faderight,0;linear,.1;fadeleft,0;sleep,.1;linear,.1;faderight,1;linear,.1;fadeleft,1;sleep,.1;
	))(self);
end;

function GetCurrentSongNumber()
	local cursong = GAMESTATE:GetCurrentSong();
	
	for i=1,#AllSongs do
		if AllSongs[i] == cursong then
		return i;
		end;
	end;
end;

function GetMeMyIndex(i)
	local temp = SCREENMAN:GetTopScreen():GetMusicWheel():GetCurrentIndex()+1;
	
	local s1 = string.format("%i", temp /100);
	local s2 = string.format("%i", (temp - s1*100)/10 );
	local s3 = string.format("%i", (temp - s1*100 - s2*10));
	
	local num = {};
	num = { s1, s2, s3 };
	return num[i];

end;

function GetMissionGoalStep()
	local stepsp1 = GAMESTATE:GetCurrentSteps('PlayerNumber_P1')
	local stepsp2 = GAMESTATE:GetCurrentSteps('PlayerNumber_P2')
	if not stepsp1 and not stepsp2 then return "" end;

	steps = stepsp1 or stepsp2;

	return steps:GetMissionGoal();
end;

local title;
local bpm;
local art;
local cota;
local bpmText;
t[#t+1] = Def.ActorFrame {
	OnCommand=function(self)
		(cmd(stoptweening;y,97;zoom,1;playcommand,'Refresh'))(self);
	end;
	RefreshCommand=function(self)
		if GAMESTATE:GetCurrentGroup() ~= "music train" and GAMESTATE:GetCurrentGroup() ~= "random" and GAMESTATE:GetCurrentGroup() ~= "quest zone" then
			song = GAMESTATE:GetCurrentSong();
			title = song:GetDisplayMainTitle();
			art = song:GetDisplayArtist(); 
			bpmText = "bpm"
			if song:IsDisplayBpmConstant() and song:GetDisplayBpms()[1] == 0 then
				bpm = "???"
			elseif not song:IsDisplayBpmConstant() then
				numbpm = song:GetDisplayBpms()[1];
				numbpm2 = song:GetDisplayBpms()[2];

				str1 = "%i"
				str2 = "%i"

				if numbpm == math.floor(numbpm) then str1 = "%i" else str1 = "%.2f" end;
				if numbpm2 == math.floor(numbpm2) then str2 = "%i" else str2 = "%.2f" end;
				
				bpm = string.format(str1 .. "-" .. str2,song:GetDisplayBpms()[1],song:GetDisplayBpms()[2]);
			elseif song:IsDisplayBpmConstant() then
				numbpm = song:GetDisplayBpms()[1];
				--if numbpm == math.floor(numbpm) then
					bpm = string.format("%d",math.floor(numbpm));
				--else
				--	bpm = string.format("%.2f",song:GetDisplayBpms()[1]);
				--end			
			end
		elseif GAMESTATE:GetCurrentGroup() == "quest zone" then
			title = ""
			art = ""
			bpm = ""
			bpmText = ""
		elseif GAMESTATE:GetCurrentGroup() == "random" then
			title = "???"
			art = "???"
			bpm = "???"
			bpmText = "bpm"
		else
			if GAMESTATE:GetCurrentCourse() then
				title = GAMESTATE:GetCurrentCourse():GetDisplayFullTitle();
				art = ""
				bpm = ""
				bpmText = ""
			end;
		end;
	end;
	PreviewChangedCommand=cmd(stoptweening;playcommand,'Refresh');
	FadeInHelperMessageCommand=cmd(stoptweening;linear,.5;zoom,1;diffusealpha,1;playcommand,'On');
	FadeOutCommand=cmd(stoptweening;zoom,0);
	children = {
		LoadActor("mission_overlay")..{
			InitCommand=cmd(y,-69);
			OnCommand=cmd(stoptweening;zoomx,.6375;zoomy,.73;diffusealpha,0;sleep,0.3;decelerate,.2;diffusealpha,0);
			PreviewChangedCommand=cmd(stoptweening;visible,GAMESTATE:GetCurrentGroup() == "quest zone";diffusealpha,0);
			FadeOutCommand=cmd(stoptweening;zoom,0);
			StartSelectingStepsMessageCommand=cmd(stoptweening;diffusealpha,1);
			GoBackSelectingSongMessageCommand=cmd(stoptweening;diffusealpha,0);
		};

		--mostrar si está completo solo si corresponde
		LoadActor("complete_song")..{
			InitCommand=cmd(y,-30.25; visible, false);
			OnCommand=cmd(stoptweening;zoom,.65;diffusealpha,0;sleep,0.3;decelerate,.2;diffusealpha,0);
			PreviewChangedCommand=cmd(stoptweening;visible,GAMESTATE:GetCurrentGroup() == "quest zone";diffusealpha,0);
			FadeOutCommand=cmd(stoptweening;zoom,0);
			StepPositionMessageCommand=function(self, params)
				if params.Valid == 0 then self:visible(false); return; end;
				
				if params.LevelCompleted >= 4 then
					self:visible(true);
				else
					self:visible(false);
				end
			end;
			StartSelectingStepsMessageCommand=cmd(stoptweening;diffusealpha,1);
			GoBackSelectingSongMessageCommand=cmd(stoptweening;diffusealpha,0);
		};

		LoadFont("BPM")..{
			InitCommand=cmd(y,-80;wrapwidthpixels,600);
			OnCommand=cmd(stoptweening;diffuse,1,1,1,1;visible,GAMESTATE:GetCurrentGroup() == "quest zone";shadowlength,0;settext,GetMissionGoalStep();zoom,0;sleep,0;diffusealpha,0;decelerate,.2;diffusealpha,0;zoom,.42);
			PreviewChangedCommand=cmd(stoptweening;settext,GetMissionGoalStep());
			StartSelectingStepsMessageCommand=cmd(stoptweening;diffusealpha,1);
			GoBackSelectingSongMessageCommand=cmd(stoptweening;diffusealpha,0);
			CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;settext,GetMissionGoalStep());
			CurrentStepsP2ChangedMessageCommand=cmd(stoptweening;settext,GetMissionGoalStep());
			PositionCommand=cmd(stoptweening;diffusealpha,0;zoom,.42);
			FadeOutCommand=cmd(stoptweening;zoom,0);
		};

		LoadActor("top_bright")..{
			InitCommand=cmd(blend,'BlendMode_Add';y,-155);
			OnCommand=cmd(stoptweening;stopeffect;zoom,0;sleep,0.5;diffusealpha,0;decelerate,.2;zoom,0.63;diffusealpha,0.3;queuecommand,"SelectingSong");
			RestoreCommand=cmd(stoptweening;stopeffect;x,0;decelerate,.2;fadeleft,0;cropright,0;cropleft,0;faderight,0;zoom,0.63;diffusealpha,0.3;queuecommand,'SelectingSong');
			NextSongMessageCommand=cmd(stoptweening;stopeffect;finishtweening;diffusealpha,1;x,0;addx,-70;fadeleft,0.1;cropright,0.2;cropleft,0.2;faderight,0.1;linear,0.15;addx,120;linear,0.1;diffusealpha,0;queuecommand,'Restore');
			PreviousSongMessageCommand=cmd(stoptweening;stopeffect;finishtweening;diffusealpha,1;x,0;addx,70;fadeleft,0.1;cropright,0.2;cropleft,0.2;faderight,0.1;linear,0.15;addx,-120;linear,0.1;diffusealpha,0;queuecommand,'Restore');
			FadeOutCommand=cmd(stoptweening;stopeffect;zoom,0);
			StepsPreselectedCancelledMessageCommand=cmd(stoptweening;stopeffect;cropright,0;cropleft,0;faderight,0;fadeleft,0;linear,.1;diffusealpha,0.3;diffuseshift;effectcolor2,color("1,1,1,1");effectcolor1,color("1,1,1,.72");effectperiod,0.6;queuecommand,'SelectingSong');
			GoBackSelectingSongMessageCommand=cmd(stoptweening;stopeffect;playcommand,'SelectingSong');
			--StepsChosenMessageCommand=cmd(stoptweening;linear,.1;diffusealpha,1;diffuseshift;effectcolor2,color("1,1,1,1");effectcolor1,color("1,1,1,.72");effectperiod,.1);
			StepsChosenMessageCommand=cmd(playcommand,"Loop");
			LoopCommand=cmd(stoptweening;diffusealpha,1;cropright,0;cropleft,0;faderight,0;fadeleft,0;linear,0.2;cropright,0.45;cropleft,0.45;faderight,0.25;fadeleft,0.25;linear,0.1;queuecommand,'Loop');
			StartSelectingStepsMessageCommand=cmd(stoptweening;linear,.1;diffusealpha,0.3;diffuseshift;effectcolor2,color("1,1,1,1");effectcolor1,color("1,1,1,.72");effectperiod,0.6);
			SelectingSongCommand=cmd(stoptweening;diffusealpha,0.3;linear,0.7;diffusealpha,0.5;linear,0.7;diffusealpha,0.3;queuecommand,'SelectingSong');
		};

		--bg
		LoadActor("song_number")..{
			InitCommand=cmd(y,-142);
			OnCommand=cmd(stoptweening;zoom,0;sleep,0.5;diffusealpha,0;decelerate,.2;zoom,0.63;diffusealpha,1;);
			--PreviewChangedCommand=cmd(stoptweening;);
			FadeOutCommand=cmd(stoptweening;zoom,0);
		};

	--bg
		LoadActor("song_panel")..{
			OnCommand=cmd(stoptweening;zoom,0;sleep,0.5;diffusealpha,0;decelerate,.2;zoom,0.63;diffusealpha,1;);
			--PreviewChangedCommand=cmd(stoptweening;);
			FadeOutCommand=cmd(stoptweening;zoom,0);
		};

	--Title de la song
		LoadFont("Special/_myriad pro 20px") .. {
			InitCommand=cmd(x,-140;y,-8);
			OnCommand=cmd(stoptweening;diffuse,1,1,1,1;horizalign,left;shadowlength,0;settext,title;zoom,0;sleep,0.5;diffusealpha,0;decelerate,.2;diffusealpha,1;zoom,.6);
			PreviewChangedCommand=cmd(stoptweening;settext,title;queuecommand,'Position');
			PositionCommand=cmd(stoptweening;diffusealpha,1;zoom,.6);
			FadeOutCommand=cmd(stoptweening;zoom,0);
		};

	--BPM's
		LoadFont("Special/_karnivore lite 20px") .. {
			Name="bpms";
			InitCommand=cmd(x,135;y,5);
			OnCommand=cmd(stoptweening;diffuse,0.35,0.89,0.98,1;horizalign,right;shadowlength,0;settext,bpm;zoom,0;sleep,0.5;diffusealpha,0;decelerate,.2;diffusealpha,1;zoom,.65);
			--FadeInHelperMessageCommand=cmd(stoptweening;diffuse,0.35,0.89,0.98,1;horizalign,left;shadowlength,0;settext,tostring('BPM '..bpm);zoom,0;sleep,0.5;diffusealpha,0;decelerate,.2;diffusealpha,1;zoom,.65);
			PreviewChangedCommand=cmd(stoptweening;settext,bpm;queuecommand,'Position');
			PositionCommand=cmd(stoptweening;diffusealpha,1;zoom,.65);
			FadeOutCommand=cmd(stoptweening;zoom,0);
		}; 

		LoadFont("Special/_karnivore lite 20px") .. {
			InitCommand=function(self) 
				x1 = self:GetParent():GetChild('bpms'):GetX();
				h = self:GetParent():GetChild('bpms'):GetZoomedWidth();
				(cmd(x,x1-h-26;y,5))(self);
			end;
			OnCommand=cmd(stoptweening;diffuse,0.35,0.89,0.98,1;horizalign,right;shadowlength,0;settext,bpmText;zoom,0;sleep,0.5;diffusealpha,0;decelerate,.2;diffusealpha,1;zoom,.5;queuecommand,'Position');
			--FadeInHelperMessageCommand=cmd(stoptweening;diffuse,0.35,0.89,0.98,1;horizalign,left;shadowlength,0;settext,tostring('"bpm" '.."bpm");zoom,0;sleep,0.5;diffusealpha,0;decelerate,.2;diffusealpha,1;zoom,.5);
			PreviewChangedCommand=cmd(stoptweening;settext,bpmText;sleep,.001;queuecommand,'Position');
			PositionCommand=cmd(stoptweening;diffusealpha,1;zoom,.5;x,self:GetParent():GetChild('bpms'):GetX()-self:GetParent():GetChild('bpms'):GetZoomedWidth()-6);
			FadeOutCommand=cmd(stoptweening;zoom,0);
		}; 
	--Artista
		LoadFont("Special/_myriad pro 20px") .. {
			InitCommand=cmd(x,-140;y,5);
			OnCommand=cmd(stoptweening;diffuse,0.35,0.89,0.98,1;horizalign,left;shadowlength,0;settext,art;zoom,0;sleep,0.5;diffusealpha,0;decelerate,.2;diffusealpha,1;zoom,.6);
			PositionCommand=cmd(stoptweening;diffusealpha,1;zoom,.6);
			PreviewChangedCommand=cmd(stoptweening;settext,art;queuecommand,'Position');
			FadeOutCommand=cmd(stoptweening;zoom,0);
		}; 		

		Def.ActorFrame {
			OnCommand=cmd(diffusealpha,0;sleep,.3;decelerate,.02;diffusealpha,1;queuecommand,'Update');
			CurrentSongChangedMessageCommand=cmd(playcommand,'Update');
			children = {
				--1st digit
				LoadFont("songnumbers")..{
					UpdateCommand=cmd(stoptweening;settext,string.format("%03d", SCREENMAN:GetTopScreen():GetMusicWheel():GetCurrentIndex()+1);zoom,0.75;zoomx,0.6;diffusealpha,1);
					OnCommand=cmd(stoptweening;x,-18;y,-144;zoom,0;diffusealpha,0;sleep,.5;decelerate,.02;zoom,0.75;zoomx,0.6;diffusealpha,1);
					--StartSelectingStepsMessageCommand=cmd(stoptweening;decelerate,.08;zoom,0;diffusealpha,0);
					GoBackSelectingGroupMessageCommand=cmd(stoptweening;decelerate,.08;zoom,0;diffusealpha,0);
					--GoBackSelectingSongMessageCommand=cmd(playcommand,'On');
					StartSelectingSongMessageCommand=cmd(stoptweening;queuecommand,'On');
					OffCommand=cmd(stoptweening);
				};

				LoadFont("songnumbers")..{
					UpdateCommand=cmd(stoptweening;settext,string.format("%03d", SCREENMAN:GetTopScreen():GetMusicWheel():GetNumItems());zoom,0.75;zoomx,0.6;diffusealpha,1);
					OnCommand=cmd(stoptweening;x,18;y,-144;zoom,0;diffusealpha,0;sleep,.5;decelerate,.02;zoom,0.75;zoomx,0.6;diffusealpha,1);
					--StartSelectingStepsMessageCommand=cmd(stoptweening;decelerate,.08;zoom,0;diffusealpha,0);
					GoBackSelectingGroupMessageCommand=cmd(stoptweening;decelerate,.08;zoom,0;diffusealpha,0);
					--GoBackSelectingSongMessageCommand=cmd(playcommand,'On');
					StartSelectingSongMessageCommand=cmd(stoptweening;queuecommand,'On');
					OffCommand=cmd(stoptweening);
				};
			};
		};
	};
};

return t
