local t = Def.ActorFrame {
	OnCommand=cmd(fov,60;zbuffer,true;vanishpoint,cx,cy);--SortByDrawOrder
}

-- Regresa la cantidad de REAL grupos
function GetNumGroupsRealRank()
	local groupsfinal = {};
	local temp = SONGMAN:GetSongGroupNames();
	local genres = SONGMAN:GetSongGenresNames();
	local genresfinal = {};

	for _,v in ipairs(genres) do
		if SONGMAN:GetIfGenreIsSelectable(v) then
			if v == "original" or v == "world music" or v == "all tunes" or v == "k-pop" or v == "j-music" then
				table.insert(genresfinal,v);
			end
		end
	end

	for _,v in ipairs(temp) do
		if SONGMAN:GetIfGroupIsSelectableForRank(v) then
			table.insert(groupsfinal,v);
		end
	end	

	temp = #groupsfinal + #genresfinal;
		
	return temp;
end;

--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-- Regresa la cantidad de grupos, para q sea mayor o igual a 7
function GetNumGroupsRank()
	local groupsfinal = {};
	local temp = SONGMAN:GetSongGroupNames();
	local genres = SONGMAN:GetSongGenresNames();
	local genresfinal = {};

	for _,v in ipairs(genres) do
		if SONGMAN:GetIfGenreIsSelectable(v) then
			if v == "original" or v == "world music" or v == "all tunes" or v == "k-pop" or v == "j-music" then
				table.insert(genresfinal,v);
			end
		end
	end

	for _,v in ipairs(temp) do
		if SONGMAN:GetIfGroupIsSelectableForRank(v) then
			table.insert(groupsfinal,v);
		end
	end

	temp = #groupsfinal + #genresfinal;

	if temp < 9 then
		while (temp < 9) do
			temp = temp + temp;
		end;
	end;
		
	return temp;
end;

-- Regresa un arreglo con todos los grupos, de tama�o GetNumGrops()
function GetAllGroupsRank()
	local groups = SONGMAN:GetSongGroupNames();
	local genres = SONGMAN:GetSongGenresNames();
	local temp = {};

	if GAMESTATE:GetHighestNumStagesLeftForAnyHumanPlayer() > 1 then
		for _,v in ipairs(genres) do
			if SONGMAN:GetIfGenreIsSelectable(v) then
				if v == "original" or v == "world music" or v == "all tunes" or v == "k-pop" or v == "j-music" then
					table.insert(temp,v);
				end
			end
		end

		for _,v in ipairs(groups) do
			if SONGMAN:GetIfGroupIsSelectableForRank(v) then
				table.insert(temp,v);
			end
		end
	else
		table.insert(temp,"shortcut");
	end

	--SCREENMAN:SystemMessage(" table: " .. table.tostring( temp ) );

	local lengh = #temp;
	--
	local vector = {};
	for i=1,lengh do
		vector[i]= temp[i];	--nombre
	end;
	
	--Falta arreglar!!!!!!!!!
	while #vector < 9 do
		local templengh = #vector;
		for i=1,(#temp) do
			vector[i+templengh] = temp[i];
		end;
	end;
	
	return vector;
end;

--local NumGroups = GetNumGroupsRank();
local AllGroupsRank = GetAllGroupsRank();
local NumGroupsRank = #AllGroupsRank;

--Warn( "numgroupsRank: " .. tostring(NumGroupsRank) .. " groups " .. table.tostring(AllGroupsRank) );

--if NumGroupsRank < 9 then
--	NumGroupsRank = NumGroupsRank - GetNumGroupsRankRealRank();
--end

function GetGroupInNumberRank(curGroup)
	--local curGroup = GAMESTATE:GetCurrentSong():GetGroupName();
	local j = 1;
	for j=1,NumGroupsRank do
		if AllGroupsRank[j]==curGroup then --regresa el primero que encuentre
		return j;
		end;
	end;
	
	return j;
	
end;

function ExistInRankMode(curGroup)
	--local curGroup = GAMESTATE:GetCurrentSong():GetGroupName();
	local j = 1;
	for j=1,NumGroupsRank do
		if AllGroupsRank[j]==curGroup then --regresa el primero que encuentre
		return curGroup;
		end;
	end;
	
	return "all tunes";
	
end;

function ExistInRankModeBool(curGroup)
	--local curGroup = GAMESTATE:GetCurrentSong():GetGroupName();
	local j = 1;
	for j=1,NumGroupsRank do
		if AllGroupsRank[j]==curGroup then --regresa el primero que encuentre
			return true;
		end;
	end;
	
	return false;
	
end;

--Como minimo, el vector de AllGroupsRank() debe tener 7 lugares
function GetBannersRank(curGroup)
	local toReturn = {};
	
	if curGroup == 4 then
		toReturn = { NumGroupsRank , 1 , 2 , 3 , 4 , 5 , 6, 7 , 8 };
	elseif curGroup == 3 then
		toReturn = { NumGroupsRank-1 , NumGroupsRank , 1 , 2 , 3 , 4 , 5 , 6, 7 };
	elseif curGroup == 2 then
		toReturn = { NumGroupsRank-2 , NumGroupsRank-1 , NumGroupsRank , 1 , 2 , 3 , 4 , 5 , 6 };
	elseif curGroup == 1 then
		toReturn = { NumGroupsRank-3 , NumGroupsRank-2 , NumGroupsRank-1 , NumGroupsRank , 1 , 2 , 3 , 4 , 5 };
	elseif curGroup == NumGroupsRank then
		toReturn = { NumGroupsRank-4 , NumGroupsRank-3 , NumGroupsRank-2 , NumGroupsRank-1 , NumGroupsRank , 1 , 2 , 3 , 4 };
	elseif curGroup == (NumGroupsRank - 1) then
		toReturn = { NumGroupsRank-5 , NumGroupsRank-4 , NumGroupsRank-3 , NumGroupsRank-2 , NumGroupsRank-1 , NumGroupsRank , 1 , 2 , 3 };
	elseif curGroup == (NumGroupsRank - 2) then
		toReturn = { NumGroupsRank-6 , NumGroupsRank-5 , NumGroupsRank-4 , NumGroupsRank-3 , NumGroupsRank-2 , NumGroupsRank-1 , NumGroupsRank , 1 , 2 };
	elseif curGroup == (NumGroupsRank - 3) then
		toReturn = { NumGroupsRank-7 , NumGroupsRank-6 , NumGroupsRank-5 , NumGroupsRank-4 , NumGroupsRank-3 , NumGroupsRank-2 , NumGroupsRank-1 , NumGroupsRank , 1 };
	else
		toReturn = { curGroup-4 , curGroup-3 , curGroup-2 , curGroup-1 , curGroup , curGroup+1, curGroup+2 , curGroup+3 , curGroup+4 };
	end;
	
	return toReturn;
end;
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--Posiciones iniciales
--self:rotationy( clamp( offsetFromCenter, -1, 1) * 95)
local InitPosRank = {
	(cmd(x,-450;rotationy,-90;z,-150;zoom,.65;diffusealpha,0)),
	(cmd(x,-400;rotationy,-90;z,-150;zoom,.65;diffusealpha,.2)),
	(cmd(x,-290;rotationy,-90;z,-100;zoom,.65;diffusealpha,.4)),
	(cmd(x,-200;rotationy,-90;z,-50;zoom,.65;diffusealpha,.6)),
	(cmd(x,0;z,0;zoom,.65;rotationy,0;diffusealpha,1)),--center
	(cmd(x,200;rotationy,90;z,-50;zoom,.65;diffusealpha,.6)),
	(cmd(x,290;rotationy,90;z,-100;zoom,.65;diffusealpha,.4)),
	(cmd(x,400;rotationy,90;z,-150;zoom,.65;diffusealpha,.2)),
	(cmd(x,450;rotationy,90;z,-150;zoom,.65;diffusealpha,0)),
};

--Para el cambio del centro
local ChangeCenterRank = (cmd(x,0;z,0;zoom,.65;rotationy,0;diffusealpha,1;sleep,.2;queuecommand,'Loop'));

--Posiciones de comienzo
local StartPosRank = {
	(cmd(diffusealpha,0;sleep,.2;linear,.1;diffusealpha,0)),
	(cmd(x,-1200;diffusealpha,0;linear,.3;diffusealpha,.2; x,-390)),
	(cmd(x,-1200;diffusealpha,0;linear,.275;diffusealpha,.4; x,-290)),
	(cmd(x,-1200;diffusealpha,0;linear,.25;diffusealpha,.8; x,-200)),
	(cmd(zoom,0;diffusealpha,0;linear,.2;diffusealpha,1;zoom,.65)),--center
	(cmd(x,1200;diffusealpha,0;linear,.25;diffusealpha,.8; x,200)),
	(cmd(x,1200;diffusealpha,0;linear,.275;diffusealpha,.4; x,290)),
	(cmd(x,1200;diffusealpha,0;linear,.3;diffusealpha,.2; x,390)),
	(cmd(diffusealpha,0;sleep,.2;linear,.1;diffusealpha,0)),
};

--Posiciones de salida
local OutPosRank = {
	(cmd(diffusealpha,0;sleep,.2;linear,.1;diffusealpha,0)),
	(cmd(x,-350;diffusealpha,0;linear,.25;diffusealpha,.2; addx,-600)),
	(cmd(x,-290;diffusealpha,0;linear,.275;diffusealpha,.4; addx,-600)),
	(cmd(x,-200;diffusealpha,0;linear,.3;diffusealpha,.8; addx,-600)),
	(cmd(zoom,.65;diffusealpha,1;linear,.3;diffusealpha,0;zoom,0)),--center
	(cmd(x,200;diffusealpha,0;linear,.3;diffusealpha,.8; addx,600)),
	(cmd(x,290;diffusealpha,0;linear,.275;diffusealpha,.4; addx,600)),
	(cmd(x,350;diffusealpha,0;linear,.25;diffusealpha,.2; addx,600)),
	(cmd(diffusealpha,0;sleep,.2;linear,.1;diffusealpha,0)),
};
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--[[ Pink glow
t[#t+1] = LoadActor("br")..{
	InitCommand=cmd(blend,'BlendMode_Add';diffusealpha,0);
	StartSelectingSongMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.25;linear,.05;diffusealpha,0);
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;diffusealpha,0);
}; ]]

t[#t+1] = Def.ActorFrame {
	InitCommand=cmd(z,-100;zoom,0);
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;zoom,0;sleep,.3;linear,.1;zoom,.8;linear,.05;zoom,.85);
	StartSelectingSongMessageCommand=cmd(stoptweening;zoom,0);
	ChangeGroupMessageCommand=cmd(stoptweening;linear,.15;rotationx,90;linear,.15;rotationx,0;zoom,.85);
	children = {
		LoadActor("disc_back")..{
			InitCommand=cmd(blend,'BlendMode_Add';zoom,.85);
		};
	};
}
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-- Funciones e Indice de grupo
local CurrentGroupNumberRank = 0;
local BannersRank = {};

function ActualGroupNumberRank()
	return GetGroupInNumberRank( AllGroupsRank[CurrentGroupNumberRank] );
end;


-- Para cambiar el indice
t[#t+1] = Def.ActorFrame {
	InitCommand=function(self)
		--local curGroup = GAMESTATE:GetCurrentSong():GetGroupName();
		local curGroup = GAMESTATE:GetCurrentGroup();
		CurrentGroupNumberRank = GetGroupInNumberRank(curGroup);
	end;
	GoBackSelectingGroupMessageCommand=function(self)
		--local curGroup = GAMESTATE:GetCurrentSong():GetGroupName();
		local curGroup = GAMESTATE:GetCurrentGroup();

		curGroup = ExistInRankMode(curGroup);

		CurrentGroupNumberRank = GetGroupInNumberRank(curGroup);

		BannersRank = GetBannersRank( CurrentGroupNumberRank );
		MESSAGEMAN:Broadcast("RefreshPositions");
		MESSAGEMAN:Broadcast("OpenPositions");
	end;
	ChangeGroupMessageCommand=function(self,params)
		if not GAMESTATE:IsRankMode() then return end;

		SOUND:PlayOnce(THEME:GetPathS('MusicWheel','change.wav'));
		local dir = params.Dir;
		if params.Dir == nil then dir = 0 end;		
		CurrentGroupNumberRank = CurrentGroupNumberRank + (dir);	-- params.Dir = +- 1, dependiendo de la direccion
		--Warn("groupnumbre: " .. tostring());
		if CurrentGroupNumberRank > NumGroupsRank then CurrentGroupNumberRank = 1; end;
		if CurrentGroupNumberRank < 1 then CurrentGroupNumberRank = NumGroupsRank; end;
		BannersRank = GetBannersRank( CurrentGroupNumberRank );
		MESSAGEMAN:Broadcast("Move");
		MESSAGEMAN:Broadcast("GroupNameUpdate", { Group = AllGroupsRank[CurrentGroupNumberRank] });
		MESSAGEMAN:Broadcast("MoveGroup", { Group = AllGroupsRank[CurrentGroupNumberRank] });
		MESSAGEMAN:Broadcast("ModifyGroupInternal", { Group = AllGroupsRank[CurrentGroupNumberRank] });
	end;
	GroupWasSelectedMessageCommand=function(self,params)
		if not GAMESTATE:IsRankMode() then return end;

		SOUND:PlayOnce(THEME:GetPathS('MusicWheel','change.wav'));
		--(cmd(lockinput,0.4))(self);
	end;
	GoRankModeMessageCommand=function(self)
		local curGroup = GAMESTATE:GetCurrentGroup();
		if not ExistInRankModeBool(curGroup) then
			MESSAGEMAN:Broadcast("Move");
			MESSAGEMAN:Broadcast("GroupNameUpdate", { Group = "all tunes" });
			MESSAGEMAN:Broadcast("ModifyGroupInternal", { Group = "all tunes" });			
			MESSAGEMAN:Broadcast("MoveGroup", { Group = "all tunes" });
			MESSAGEMAN:Broadcast("UpdateWheelSongsAndSteps");
		end
	end;
}

t[#t+1] = LoadActor(THEME:GetPathS("", "select_group_bgm loop")) .. { 
	--File = ;
	InitCommand=cmd(stop);
	GoBackSelectingGroupMessageCommand=function(self)
		--self:get():SetProperty("Loop",1);
		if not GAMESTATE:IsRankMode() then return end;
		self:play();
	end;	
	StartSelectingSongMessageCommand=cmd(stop);
};

t[#t+1] = LoadActor(THEME:GetPathS("", "go to groups")) .. { 
	--File = ;
	InitCommand=cmd(stop);
	GoBackSelectingGroupMessageCommand=function(self)
		if not GAMESTATE:IsRankMode() then return end;
		--self:get():SetProperty("Loop",1);
		self:play();
	end;	
	--StartSelectingSongMessageCommand=cmd(stop);
};

function GetGroupSoundRank(group)
	local fir = SONGMAN:GetSongGroupBannerPath( group );
	local ogg = string.gsub(fir,'banner.*','info/sound.ogg');
	local mp3 = string.gsub(fir,'banner.*','info/sound.mp3');
	local wav = string.gsub(fir,'banner.*','info/sound.wav');
	local file = "";
	if (FILEMAN:DoesFileExist(ogg)) then file = ogg;
	elseif (FILEMAN:DoesFileExist(mp3)) then file = mp3;
	elseif (FILEMAN:DoesFileExist(wav)) then file = wav;
	--else return; 
	end;
	
	--SOUND:PlayOnce(file);
	return "/" .. file;
end;

function SoundExists(path)
	return FILEMAN:DoesFileExist(path);
end;

--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-- BannersRank XD
for i=1,NumGroupsRank do 
t[#t+1] = Def.Sprite {
	InitCommand = function(self)
		(cmd(stoptweening;diffusealpha,0;zbuffer,true))(self);
		local gname = AllGroupsRank[i];
		--self:scaletoclipped(300,190);		
		self:zoom(.6);
		if i then
			self:Load(SONGMAN:GetSongGroupBannerPath(gname));	-- se generan los actores
		end;
	end;
	RefreshPositionsMessageCommand = function(self)
		self:stoptweening(); 
		if 	    i==BannersRank[1] then (InitPosRank[1])(self);
		elseif 	i==BannersRank[2] then (InitPosRank[2])(self);
		elseif 	i==BannersRank[3] then (InitPosRank[3])(self);
		elseif 	i==BannersRank[4] then (InitPosRank[4])(self); 
		elseif 	i==BannersRank[5] then (InitPosRank[5])(self); --center
		elseif 	i==BannersRank[6] then (InitPosRank[6])(self);
		elseif 	i==BannersRank[7] then (InitPosRank[7])(self);
		elseif 	i==BannersRank[8] then (InitPosRank[8])(self);
		elseif 	i==BannersRank[9] then (InitPosRank[9])(self);
		else (cmd(stoptweening;diffusealpha,0))(self);
		end;
		self:diffusealpha(1);
	end;
	MoveMessageCommand=function(self)
		self:stoptweening(); --detiene todas las animaciones
		if 	    i==BannersRank[1] then self:decelerate(.3);(InitPosRank[1])(self);
		elseif 	i==BannersRank[2] then self:decelerate(.3);(InitPosRank[2])(self);
		elseif 	i==BannersRank[3] then self:decelerate(.3);(InitPosRank[3])(self);
		elseif 	i==BannersRank[4] then self:decelerate(.3);(InitPosRank[4])(self);
		elseif 	i==BannersRank[5] then self:decelerate(.3);(ChangeCenterRank)(self); --center
		elseif 	i==BannersRank[6] then self:decelerate(.3);(InitPosRank[6])(self);
		elseif 	i==BannersRank[7] then self:decelerate(.3);(InitPosRank[7])(self);
		elseif 	i==BannersRank[8] then self:decelerate(.3);(InitPosRank[8])(self);
		elseif 	i==BannersRank[9] then self:decelerate(.3);(InitPosRank[9])(self);
		else (cmd(stoptweening;diffusealpha,0))(self);
		end;
	end;
	LoopCommand=cmd(stoptweening;);
	StartSelectingSongMessageCommand=function(self,params)
		self:stoptweening();	
		if 	    i==BannersRank[1] then (OutPosRank[1])(self);
		elseif 	i==BannersRank[2] then (OutPosRank[2])(self);
		elseif 	i==BannersRank[3] then (OutPosRank[3])(self);
		elseif 	i==BannersRank[4] then (OutPosRank[4])(self);
		elseif 	i==BannersRank[5] then (OutPosRank[5])(self); --center
		elseif 	i==BannersRank[6] then (OutPosRank[6])(self);
		elseif 	i==BannersRank[7] then (OutPosRank[7])(self);
		elseif 	i==BannersRank[8] then (OutPosRank[8])(self);
		elseif 	i==BannersRank[9] then (OutPosRank[9])(self);
		else (cmd(stoptweening;diffusealpha,0))(self);
		end;
	end;
	OpenPositionsMessageCommand=function(self)	--comienza la seleccion de grupos
		self:stoptweening();
		if 	    i==BannersRank[1] then (StartPosRank[1])(self);
		elseif 	i==BannersRank[2] then (StartPosRank[2])(self);
		elseif 	i==BannersRank[3] then (StartPosRank[3])(self);
		elseif 	i==BannersRank[4] then (StartPosRank[4])(self); 
		elseif 	i==BannersRank[5] then (StartPosRank[5])(self); --center
		elseif 	i==BannersRank[6] then (StartPosRank[6])(self);
		elseif 	i==BannersRank[7] then (StartPosRank[7])(self);
		elseif 	i==BannersRank[8] then (StartPosRank[8])(self);
		elseif 	i==BannersRank[9] then (StartPosRank[9])(self);
		else (cmd(stoptweening;diffusealpha,0))(self);
		end;
	end;
};

	--cuidado con esto wn !!!
	--esta funci�n esta m�s peligrosa que una pira�a en el bidet
	if SONGMAN:DoesSongGroupExist(AllGroupsRank[i]) then
		if GetGroupSoundRank(AllGroupsRank[i]) ~= "/" then
			t[#t+1] = LoadActor(GetGroupSoundRank(AllGroupsRank[i])) .. {
				GoBackSelectingGroupMessageCommand=cmd(stoptweening;sleep,.6;queuecommand,'Play');
				MoveMessageCommand=cmd(stoptweening;sleep,.6;queuecommand,'Play');
				PlayCommand=function(self)
					if not GAMESTATE:IsRankMode() then return; end;
					if not SoundExists(GetGroupSound(AllGroupsRank[i])) then return; end;

					if AllGroupsRank[i] == AllGroupsRank[CurrentGroupNumberRank] then self:play(); else self:stop() end;
				end;
			};
		end;
	else
		t[#t+1] = LoadActor(THEME:GetPathS("","Groups/" .. AllGroupsRank[i] .. ".wav")) .. {
			GoBackSelectingGroupMessageCommand=cmd(stoptweening;sleep,.6;queuecommand,'Play');
			MoveMessageCommand=cmd(stoptweening;sleep,.6;queuecommand,'Play');
			PlayCommand=function(self)
				if not GAMESTATE:IsRankMode() then return; end;
				if not SoundExists(THEME:GetPathS("","Groups/" .. AllGroupsRank[i] .. ".wav")) then return; end;
				
				if AllGroupsRank[i] == AllGroupsRank[CurrentGroupNumberRank] then self:play(); else self:stop() end;
			end;
		};
	end;
end;

--[[
function PlayGroupSoundRank()
	local fir = SONGMAN:GetSongGroupBannerPath( AllGroupsRank[CurrentGroupNumberRank] );
	local ogg = string.gsub(fir,'banner.*','info/sound.ogg');
	local mp3 = string.gsub(fir,'banner.*','info/sound.mp3');
	local wav = string.gsub(fir,'banner.*','info/sound.wav');
	local file = "";
	if (FILEMAN:DoesFileExist(ogg)) then file = ogg;
	elseif (FILEMAN:DoesFileExist(mp3)) then file = mp3;
	elseif (FILEMAN:DoesFileExist(wav)) then file = wav;
	else return; end;
	
	SOUND:PlayOnce(file);
end;

t[#t+1] = Def.ActorFrame {
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;sleep,.5;queuecommand,'Play');
	MoveMessageCommand=cmd(stoptweening;sleep,.5;queuecommand,'Play');
	PlayCommand=function(self)
		if not GAMESTATE:IsRankMode() then return end;
		if not AllGroupsRank[CurrentGroupNumberRank] then return end;
		if SONGMAN:DoesSongGroupExist(AllGroupsRank[CurrentGroupNumberRank]) then
			PlayGroupSoundRank();
		else
			SOUND:PlayOnce( THEME:GetPathS("","Groups/" .. AllGroupsRank[CurrentGroupNumberRank] .. ".wav") );
		end;
	end;
}
]]

t[#t+1] = LoadFont("Special/_myriad pro 20px") .. {
	InitCommand=function(self)
		(cmd(zoom,.05;y,170;settext,'';shadowlength,1;strokecolor,0,0,1,1;diffuse,0.8,.9,1,1))(self);
	end;
	Move2Command=function(self)
		self:settext('');

		if not AllGroupsRank[CurrentGroupNumberRank] then return end;

		if SONGMAN:DoesSongGroupExist(AllGroupsRank[CurrentGroupNumberRank]) then
			local fir = SONGMAN:GetSongGroupBannerPath( AllGroupsRank[CurrentGroupNumberRank] );
			local dir = string.gsub(fir,'banner.*','info/text.ini');

			local tt = lua.ReadFile(dir);
			self:settext(tt);
		else
			local path = THEME:GetPathO("","") .. "/" .. AllGroupsRank[CurrentGroupNumberRank] .. ".ini";
			--Warn(path);
			local tt = lua.ReadFile(path);
			self:settext(tt);
		end;

		--self:settext(string.sub(AllGroupsRank[CurrentGroupNumberRank],4));

		(cmd(stoptweening;zoom,.7;shadowlength,0;wrapwidthpixels,600/0.75;cropright,1;sleep,.5;linear,0.7;cropright,0))(self);
	end;
	MoveMessageCommand=function(self,params)
		self:settext('');

		if not AllGroupsRank[CurrentGroupNumberRank] then return end;

		if SONGMAN:DoesSongGroupExist(AllGroupsRank[CurrentGroupNumberRank]) then
			local fir = SONGMAN:GetSongGroupBannerPath( AllGroupsRank[CurrentGroupNumberRank] );
			local dir = string.gsub(fir,'banner.*','info/text.ini');

			local tt = lua.ReadFile(dir);
			self:settext(tt);
		else
			local path = THEME:GetPathO("","") .. "/" .. AllGroupsRank[CurrentGroupNumberRank] .. ".ini";
			--Warn(path);
			local tt = lua.ReadFile(path);
			self:settext(tt);
		end;

		--self:settext(string.sub(AllGroupsRank[CurrentGroupNumberRank],4));

		(cmd(stoptweening;zoom,.7;shadowlength,0;wrapwidthpixels,600/0.75;cropright,1;sleep,.5;linear,0.7;cropright,0))(self);
	end;
	GoBackSelectingGroupMessageCommand=cmd(playcommand,'Move2');
	StartSelectingSongMessageCommand=cmd(stoptweening;settext,'');
}

t[#t+1] = LoadActor("numero")..{
	InitCommand=cmd(x,2;y,120;zoom,0);
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;zoom,0;sleep,.3;linear,.1;zoom,0.8;zoomx,0.7;zoomy,0.7;sleep,.05;queuecommand,'Loop');
};

t[#t+1] = LoadFont("group_numbers")..{
	InitCommand=cmd(zoom,.8;y,119;x,20;settext,string.format("%02d",NumGroupsRank);diffusealpha,0);
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;sleep,.3;linear,.1;diffusealpha,1);
	StartSelectingSongMessageCommand=cmd(stoptweening;linear,.1;diffusealpha,0);
}

t[#t+1] = LoadFont("group_numbers")..{
	InitCommand=cmd(zoom,.8;y,119;x,-14;settext,string.format("%02d",CurrentGroupNumberRank);diffusealpha,0);
	MoveMessageCommand=cmd(settext,string.format("%02d",CurrentGroupNumberRank));
	-- ****** que pasa si es un genre?? ****
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;settext,string.format("%02d",GetGroupInNumberRank(AllGroupsRank[CurrentGroupNumberRank]));sleep,.3;linear,.1;diffusealpha,1);
	StartSelectingSongMessageCommand=cmd(stoptweening;linear,.1;diffusealpha,0);
}

local xminRank = -190;
local xmaxRank = 175; 

t[#t+1] = Def.ActorFrame { 
	LoadActor("subraya")..{
		Name="Subraya";
		InitCommand=cmd(y,147;zoom,.75;diffusealpha,0);
		GoBackSelectingGroupMessageCommand=cmd(stoptweening;zoom,0;sleep,.3;linear,.1;diffusealpha,1;zoom,.75);
		StartSelectingSongMessageCommand=cmd(stoptweening;zoom,0);
	};

	LoadActor("dot")..{
		Name="Dot";
		InitCommand=cmd(y,147;zoom,.75;diffusealpha,0;x,xmaxRank);
		GoBackSelectingGroupMessageCommand=cmd(stoptweening;x,scale( GetGroupInNumberRank(AllGroupsRank[CurrentGroupNumberRank])/#AllGroupsRank, 0, 1, xminRank, xmaxRank );zoom,0;sleep,.3;linear,.1;diffusealpha,1;zoom,.75);
		StartSelectingSongMessageCommand=cmd(stoptweening;zoom,0);
		MoveMessageCommand=function(self)
			local l = GetGroupInNumberRank(AllGroupsRank[CurrentGroupNumberRank])
			local mod = CurrentGroupNumberRank/#AllGroupsRank;
			local x = scale( mod, 0, 1, xminRank, xmaxRank );
			self:x(x);
		end;
	};
}

--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-- der
t[#t+1] = LoadActor("arrow.png")..{
	InitCommand=cmd(x,155;rotationz,180;zoom,0; z, 100; bob; effectmagnitude, 3,0,0; effectperiod, .9);
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;zoom,0;sleep,.3;linear,.1;zoom,.55;sleep,.05;queuecommand,'Loop');
	StartSelectingSongMessageCommand=cmd(stoptweening;zoom,0);
	NextGroupMessageCommand=cmd(stoptweening;linear,.05;x,155;decelerate,.2;x,150;decelerate,.1;x,155;queuecommand,'Loop');	
};

--izq
t[#t+1] = LoadActor("arrow.png")..{
	InitCommand=cmd(x,-155;zoom,0; z, 100;bob; effectmagnitude, -3,0,0; effectperiod, .9);
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;zoom,0;sleep,.3;linear,.1;zoom,.55;sleep,.05;queuecommand,'Loop');
	StartSelectingSongMessageCommand=cmd(stoptweening;zoom,0);
	PrevGroupMessageCommand=cmd(stoptweening;linear,.05;x,-155;decelerate,.2;x,-150;decelerate,.1;x,-155;queuecommand,'Loop');
	
};
return t;