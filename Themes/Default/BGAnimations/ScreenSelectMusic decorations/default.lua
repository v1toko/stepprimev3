local t = Def.ActorFrame {
	ChangeStepsMessageCommand=function(self, params)
		--Para cambios generales de steps
		if params.Direction == 1 then
		MESSAGEMAN:Broadcast("NextStep",{Player = params.Player});
		elseif params.Direction == -1 then
		MESSAGEMAN:Broadcast("PreviousStep",{Player = params.Player});
		end;
	end;
}

-- Manejo de C�digos para el BasicMode
t[#t+1] =  Def.ActorFrame {
	CodeMessageCommand=function(self,params)
		if params.Name == "SpeedUp" then
			local cur_speed = GetPlayerSpeed( params.PlayerNumber );
			--if cur_speed >= 6 then return; end;
			GAMESTATE:ApplyPreferredModifiers(params.PlayerNumber,tostring(cur_speed+1).."x");
			MESSAGEMAN:Broadcast("SpeedCode",{Player = params.PlayerNumber});
			SOUND:PlayOnce(THEME:GetPathS('','Command Windows/select'));
		end;
		if params.Name == "SpeedDown" then
			local cur_speed = GetPlayerSpeed( params.PlayerNumber );
			--if cur_speed <= 1 then return; end;
			GAMESTATE:ApplyPreferredModifiers(params.PlayerNumber,tostring(cur_speed-1).."x");
			MESSAGEMAN:Broadcast("SpeedCode",{Player = params.PlayerNumber});
			SOUND:PlayOnce(THEME:GetPathS('','Command Windows/select'));
		end;
	end;
};


-------------------------------------GENERAL------------------------------------------------------

cx = (SCREEN_WIDTH/2);
cy = (SCREEN_HEIGHT/2);

--------------------------------------------------------------------------------------------------
t[#t+1] = LoadActor("_timer")..{
	InitCommand=cmd(zoom,0.7;x,cx;y,-10;decelerate,.15;y,15;decelerate,.05;y,15);
	OffCommand=cmd(x,cx;y,0;decelerate,.15;y,15;decelerate,.05;y,-58);
};

t[#t+1] = LoadActor("_diff_FM/2player_inactive")..{
	InitCommand=cmd(x,cx+87;y,387;zoom,.75);
	OnCommand=cmd(diffusealpha,0;linear,0.3;diffusealpha,1);
	StartSelectingSongMessageCommand=cmd(diffusealpha,0;linear,0.3;diffusealpha,1);
	GoBackSelectingGroupMessageCommand=cmd(diffusealpha,1;linear,0.3;diffusealpha,0);
	OffCommand=cmd(stoptweening;linear,.2;diffusealpha,0);
};

t[#t+1] = LoadActor("_diff_FM/1player_inactive")..{
	InitCommand=cmd(x,cx-89;zoom,.75;y,386);
	OnCommand=cmd(diffusealpha,0;linear,0.3;diffusealpha,1);
	StartSelectingSongMessageCommand=cmd(diffusealpha,0;linear,0.3;diffusealpha,1);
	GoBackSelectingGroupMessageCommand=cmd(diffusealpha,1;linear,0.3;diffusealpha,0);
	OffCommand=cmd(stoptweening;linear,.2;diffusealpha,0);
};

t[#t+1] = LoadActor("_diff_FM")..{
	InitCommand=function(self)
		if GAMESTATE:IsBasicMode() then self:visible(false); end;		
	end;
	OnCommand=cmd(stoptweening;y,580);
	StartSelectingStepsMessageCommand=cmd(stoptweening;y,580;decelerate,.1;y,385);
	GoBackSelectingSongMessageCommand=cmd(stoptweening;sleep,.2;y,385;decelerate,.1;y,580);
	OffCommand=cmd(stoptweening;linear,.2;diffusealpha,0;y,400);
}

t[#t+1] = LoadActor("_rankmode_records");

--MUSIC WHEEL
t[#t+1] = LoadActor("_wheel")..{
	InitCommand=cmd(addy,-15; draworder, -2);
	--aparece el banner con difuse (o zoom) y llegan los discos desde fuera
	OnCommand=cmd();
	--desaparecen los discos
	StartSelectingStepsMessageCommand=cmd();
	--salen los discos fuera y el banner desaparece 
	GoBackSelectingGroupMessageCommand=function(self)
		(cmd(stoptweening;addy,0;accelerate,0.2;diffusealpha,0))(self);
		SCREENMAN:GetTopScreen():GetChild('MusicWheel'):MoveItemsToSides(true);		
		SCREENMAN:GetTopScreen():GetChild('CDTitle'):diffusealpha(0);
	end;		
	--aparece el banner con difuse (o zoom) y llegan los discos desde fuera
	StartSelectingSongMessageCommand=function(self)
		(cmd(stoptweening;accelerate,0.2;diffusealpha,1))(self);
		SCREENMAN:GetTopScreen():GetChild('MusicWheel'):MoveItemsToScreen(true);
		SCREENMAN:GetTopScreen():GetChild('CDTitle'):diffusealpha(0);
		SCREENMAN:GetTopScreen():GetChild('CDTitle'):sleep(.3);
		SCREENMAN:GetTopScreen():GetChild('CDTitle'):linear(.05);
		SCREENMAN:GetTopScreen():GetChild('CDTitle'):diffusealpha(1);
	end;
	--llegan los discos desde fuera
	GoBackSelectingSongMessageCommand=cmd();
	--deberia ocultarse la wheel y mostrar en fade el preview.
	OffCommand=function(self)
		(cmd(stoptweening;addy,0;accelerate,0.2;diffusealpha,0))(self);
		SCREENMAN:GetTopScreen():GetChild('MusicWheel'):MoveItemsToSides(true);
	end; 
} 

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

function Actor:moveout()
	return (cmd())(self);
end;

t[#t+1] = LoadActor("_preview")..{
	InitCommand=cmd(y,152;x,cx-100);
	OnCommand=cmd(stoptweening;x,cx);
	StartSelectingSongMessageCommand=function(self)
		(cmd(queuecommand,'On'))(self);	
		MESSAGEMAN:Broadcast("FadeInHelper");
	end;
	--GoBackSelectingSongMessageCommand=cmd(playcommand,'On');
	GoBackSelectingGroupMessageCommand=cmd(playcommand,'FadeOut');
	CurrentSongChangedMessageCommand=cmd(playcommand,'PreviewChanged');
	CurrentCourseChangedMessageCommand=cmd(playcommand,'PreviewChanged');
	OffCommand=cmd(stoptweening;sleep,.07;queuecommand,'FadeOut';moveout); --con un peque�o delay
}  

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

t[#t+1] = Def.ActorProxy {
	BeginCommand=function(self) 
		local diffbar = SCREENMAN:GetTopScreen():GetChild('SongDifficultyList'); 		
		self:SetTarget(diffbar); 
		--diffbar:fov(45);
		--local bgNormal = diffbar:GetChild('BackgroundNormal');
		--(cmd(y,11;zoom,0.64;stoptweening;diffusealpha,0;sleep,.2;decelerate,.2;diffusealpha,1))(bgNormal);

		--local bgMission = diffbar:GetChild('BackgroundMission');
		--(cmd(y,11;zoom,.4;stoptweening;diffusealpha,0;sleep,.2;decelerate,.2;diffusealpha,1))(bgMission);

		local CursorP1 = diffbar:GetChild('CursorP1');
		local CursorP2 = diffbar:GetChild('CursorP2');
		(cmd(zoom,1.25;y,6;pulse;effectmagnitude,.7,.66,.66;effectperiod,.8;diffuse,0,0,0,0))(CursorP1);
		(cmd(zoom,1.25;y,20;pulse;effectmagnitude,.7,.66,.66;effectperiod,.8;diffuse,0,0,0,0))(CursorP2);

		--lo bueno que esto lo recorremos 1 sola vez
		for j=1,13 do 
			local CursorTrainSingle = diffbar:GetChild('TrainCursorSingleN' .. j);
			--CursorTrainSingle:addcommand("Repeat", (cmd(stoptweening;sleep,1.5;diffusealpha,1;linear,.5;diffusealpha,0;linear,.5;diffusealpha,1;queuecommand,'Repeat')));
			--(cmd(playcommand,'Repeat'))(CursorTrainSingle);
			(cmd(diffuseshift;effecttiming,0,1,0,1;effectperiod,2;effectcolor1,1,1,1,1;effectcolor2,1,1,1,0))(CursorTrainSingle);

			local CursorTrainDouble = diffbar:GetChild('TrainCursorDoubleN' .. j);
			(cmd(diffuseshift;effecttiming,0,1,0,1;effectperiod,2;effectcolor1,1,1,1,1;effectcolor2,1,1,1,0))(CursorTrainDouble);

			local spinnerMini = diffbar:GetChild('SpinnerMini' .. j);
			spinnerMini:addcommand("Repeat", (cmd(stoptweening;diffusealpha,0;rotationz,-180;linear,.9;addrotationz,-70;diffusealpha,.2;linear,.6;addrotationz,-90;diffusealpha,0;linear,.5;addrotationz,-20;sleep,1;queuecommand,'Repeat')) );
			(cmd(queuecommand,'Repeat'))(spinnerMini);
		end;
	end;
	OnCommand=function(self)
		(cmd(stoptweening;x,cx;y,350;diffusealpha,0;zoom,0.7;sleep,.2;accelerate,.1;diffusealpha,1;y,290))(self);
	end;
	StartSelectingSongMessageCommand=cmd(sleep,.05;queuecommand,'On');
	StartSelectingStepsMessageCommand=function(self)		
		local diffbar = SCREENMAN:GetTopScreen():GetChild('SongDifficultyList'); 

		(cmd(stoptweening;diffusealpha,1;decelerate,.2;zoom,0.85;y,295))(self);

		local CursorP1 = diffbar:GetChild('CursorP1');
		local CursorP2 = diffbar:GetChild('CursorP2');
		(cmd(stoptweening;diffuse,1,1,1,1))(CursorP1);
		(cmd(stoptweening;diffuse,1,1,1,1))(CursorP2);	
	end;
	StepsChosenMessageCommand=function(self,params)
		local diffbar = SCREENMAN:GetTopScreen():GetChild('SongDifficultyList'); 
		
		if params.Player == PLAYER_1 then
			local CursorP1 = diffbar:GetChild('CursorP1');
			(cmd(stoptweening;effectmagnitude,.7,.66,.66;effectperiod,.2))(CursorP1);
		end;
		if params.Player == PLAYER_2 then
			local CursorP2 = diffbar:GetChild('CursorP2');
			(cmd(stoptweening;effectmagnitude,.7,.66,.66;effectperiod,.2))(CursorP2);
		end;
	end;
	StepsPreselectedCancelledMessageCommand=function(self,params)
		local diffbar = SCREENMAN:GetTopScreen():GetChild('SongDifficultyList'); 
				
		if params.Player == PLAYER_1 then
			local CursorP1 = diffbar:GetChild('CursorP1');
			(cmd(stoptweening;effectmagnitude,.7,.66,.66;effectperiod,.8))(CursorP1);
		end;
		if params.Player == PLAYER_2 then
			local CursorP2 = diffbar:GetChild('CursorP2');
			(cmd(stoptweening;effectmagnitude,.7,.66,.66;effectperiod,.8))(CursorP2);
		end;
	end;

	GoBackSelectingGroupMessageCommand=cmd(playcommand,'Off');
	GoBackSelectingSongMessageCommand=function(self)
		(cmd(stoptweening;diffusealpha,1;decelerate,.2;zoom,0.7;y,290))(self);

		local diffbar = SCREENMAN:GetTopScreen():GetChild('SongDifficultyList'); 
		local CursorP1 = diffbar:GetChild('CursorP1');
		(cmd(stoptweening;diffuse,0,0,0,0;effectmagnitude,.7,.66,.66;effectperiod,.8))(CursorP1);
		local CursorP2 = diffbar:GetChild('CursorP2');
		(cmd(stoptweening;diffuse,0,0,0,0;effectmagnitude,.7,.66,.66;effectperiod,.8))(CursorP2);
	end;
	OffCommand=function(self)
		(cmd(stoptweening;sleep,.05;accelerate,.2;y,350;diffusealpha,0))(self);
	end;
}

-- 1st player
t[#t+1] = SimplePlatPiu(cx*1.42,cy*1.8)..{ 
	OnCommand=cmd(zoom,.7;zoom,0);
	BothStepsConfirmedMessageCommand=cmd(stoptweening;zoom,.7);
	StepsPreselectedCancelledMessageCommand=cmd(stoptweening;zoom,0);
	GoBackSelectingSongMessageCommand=cmd(stoptweening;zoom,0);
	OffCommand=cmd(stoptweening;zoom,0);
}

--GROUP WHEEL
t[#t+1] = LoadActor("_groupwheel")..{
	OnCommand=cmd(stoptweening;y,cy-20;x,cx;queuecommand,"Invisible");
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;queuecommand,"Visible";linear,.3;diffusealpha,1);
	StartSelectingSongMessageCommand=cmd(stoptweening;linear,.3;diffusealpha,0;queuecommand,"Invisible");
	GoRankModeMessageCommand=cmd(visible,false);
	GoFullModeMessageCommand=cmd(visible,true);
	VisibleCommand=cmd(visible,not GAMESTATE:IsRankMode());
	InvisibleCommand=cmd(visible,false);
}

t[#t+1] = LoadActor("_groupwheelrank")..{
	OnCommand=cmd(stoptweening;y,cy-20;x,cx;queuecommand,"Invisible");
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;queuecommand,"Visible";linear,.3;diffusealpha,1);
	StartSelectingSongMessageCommand=cmd(stoptweening;linear,.3;diffusealpha,0;queuecommand,"Invisible");
	VisibleCommand=cmd(visible,GAMESTATE:IsRankMode());
	GoRankModeMessageCommand=cmd(visible,true);
	GoFullModeMessageCommand=cmd(visible,false);
	InvisibleCommand=cmd(visible,false);
}

t[#t+1] = LoadActor(THEME:GetPathS("", "ActionSounds/no_back_in_groups.wav")) .. {
	CannotGoBackInSelectingGroupsMessageCommand=cmd(play);
}

t[#t+1] = LoadActor("_stages")..{
	OnCommand=cmd(BMinvisible;y,10;zoom,.85);
} 

t[#t+1] = LoadActor("_arrows");

t[#t+1] = LoadActor("_chapters") .. {
	InitCommand=cmd(visible,false;y,500;x,SCREEN_CENTER_X;diffusealpha,0);
	SetVisibleCommand=cmd(visible,GAMESTATE:GetCurrentGroup() == "quest zone");
	InCommand=cmd(y,500; diffusealpha,0;decelerate,.3;diffusealpha,1;y,385);
	OutCommand=cmd(y,385; diffusealpha,1;decelerate,.3;diffusealpha,0;y,500);
	StartSelectingSongMessageCommand=cmd(stoptweening;queuecommand,'SetVisible';queuecommand,'In');
	GoBackSelectingSongMessageCommand=cmd(stoptweening;queuecommand,'SetVisible';queuecommand,'In');
	StartSelectingStepsMessageCommand=cmd(stoptweening;queuecommand,'SetVisible';queuecommand,'Out');
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;queuecommand,'SetVisible';queuecommand,'Out');
	OnCommand=cmd(stoptweening;queuecommand,'SetVisible');
};

t[#t+1] = LoadActor(THEME:GetPathG("", "new")) .. {
	InitCommand=cmd(x,SCREEN_CENTER_X - 130; y, SCREEN_CENTER_Y - 130; diffusealpha, 0; zoom, 0; queuecommand, 'Check');
	OnCommand=cmd(sleep, .2; decelerate, .2; diffusealpha, 1; zoom, .5);
	GoBackSelectingSongMessageCommand=cmd(sleep, .2; decelerate, .2; diffusealpha, 1; zoom, .5);
	CurrentSongChangedMessageCommand=cmd(sleep,.01; queuecommand, 'Check');
	GoBackSelectingGroupMessageCommand=cmd(linear,.1;zoom,0;diffusealpha,0);
	OffCommand=cmd(linear,.5;zoom,0;diffusealpha,0);
	CheckCommand=function(self)
		if GAMESTATE:GetCurrentGroup() == 'quest zone' or GAMESTATE:GetCurrentGroup() == 'random' or GAMESTATE:GetCurrentGroup() == 'music train' then self:visible(false); return end;  			

		local song = GAMESTATE:GetCurrentSong();
		--Warn(tostring(song:IsNew()));
		if song:IsNew() then
			(cmd(visible,true;linear,.2;zoom,.5;diffusealpha,1))(self);
		else
			(cmd(linear,.2;zoom,0;diffusealpha,0;sleep,.1;visible,false;))(self);
		end
	end;
};


t[#t+1] = LoadActor('_CM/default.lua');

-- Columna de C�digos P1
if GAMESTATE:IsSideJoined(PLAYER_1) then
t[#t+1] = GetCodesColumn( PLAYER_1 )..{
	InitCommand=cmd(x,24; addy, 30);
	OnCommand=function(self)
		self:visible( GAMESTATE:GetCurrentGroup() ~= "quest zone" )
	end;
	OffCommand=cmd(visible,false);
	GroupNameUpdateMessageCommand=function(self,params)
		self:visible( params.Group ~= "quest zone" )
	end;
};
end;

-- Columna de C�digos P2
if GAMESTATE:IsSideJoined(PLAYER_2) then
t[#t+1] = GetCodesColumn( PLAYER_2 )..{
	InitCommand=cmd(x,SCREEN_RIGHT-24; addy, 30);
	OffCommand=cmd(visible,false);
	OnCommand=function(self)
		self:visible( GAMESTATE:GetCurrentGroup() ~= "quest zone" )
	end;
	GroupNameUpdateMessageCommand=function(self,params)
		self:visible( params.Group ~= "quest zone" )
	end;
};
end;


if GAMESTATE:IsSideJoined(PLAYER_1) then
t[#t+1] = GetCommandWindow( PLAYER_1 )..{
	InitCommand=cmd(x,-120;y,cy-35;zoom,.65;visible,false);
	OptionsListOpenedMessageCommand=function(self,params)
		if params.Player == PLAYER_1 then
			(cmd(finishtweening;playcommand,'Visible';x,-120;decelerate,.1;x,140;decelerate,.2;x,130))(self);
		end;
	end;
	OptionsListClosedMessageCommand=function(self,params)
		if params.Player == PLAYER_1 then
			(cmd(stoptweening;x,130;decelerate,.1;x,140;decelerate,.1;x,-120;sleep,.2;queuecommand,'Invisible'))(self);
		end;
	end;
	VisibleCommand=cmd(visible,true);
	InvisibleCommand=cmd(visible,false);
	CodeMessageCommand=function(self,params)
	if GAMESTATE:IsBasicMode() or GAMESTATE:GetCurrentGroup() == "quest zone" then return; end;
		if  params.Name == 'OptionList' and params.PlayerNumber == PLAYER_1 then
			SCREENMAN:GetTopScreen():GetMusicWheel():Move(0);
			SCREENMAN:GetTopScreen():OpenOptionsList(PLAYER_1);
		end;
	end;
}
end;

if GAMESTATE:IsSideJoined(PLAYER_2) then
t[#t+1] = GetCommandWindow( PLAYER_2 )..{
	InitCommand=cmd(x,SCREEN_RIGHT+120;y,cy-35;zoom,.65;visible,false);
	OptionsListOpenedMessageCommand=function(self,params)
		if params.Player == PLAYER_2 then
			(cmd(finishtweening;playcommand,'Visible';x,SCREEN_RIGHT+120;decelerate,.1;x,SCREEN_RIGHT-140;decelerate,.2;x,SCREEN_RIGHT-130))(self);
		end;
	end;
	OptionsListClosedMessageCommand=function(self,params)
		if params.Player == PLAYER_2 then
			(cmd(stoptweening;x,SCREEN_RIGHT-130;decelerate,.1;x,SCREEN_RIGHT-140;decelerate,.1;x,SCREEN_RIGHT+120;sleep,.2;queuecommand,'Invisible'))(self);
		end;
	end;
	VisibleCommand=cmd(visible,true);
	InvisibleCommand=cmd(visible,false);
	CodeMessageCommand=function(self,params)
	if GAMESTATE:IsBasicMode() or GAMESTATE:GetCurrentGroup() == "quest zone" then return; end;
		if  params.Name == 'OptionList' and params.PlayerNumber == PLAYER_2 then
			SCREENMAN:GetTopScreen():GetMusicWheel():Move(0);
			SCREENMAN:GetTopScreen():OpenOptionsList(PLAYER_2);
		end;
	end;
}
end;

t[#t+1] = LoadActor("profiles.lua");

return t
--code by xMAx (modificaciones por v1toko)
