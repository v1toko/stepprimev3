local t = Def.ActorFrame {}

-- Pantallas en las que se muestra el nombre de los jugadores.
-- en screenevaluation mostramos una propia.
local Screens = {
	("ScreenSelectMusic"), 
};

--actor para mostrar o no el nombre del jugador
function Actor:ShowPlayerName(pn)
	local screen = SCREENMAN:GetTopScreen():GetName();
	local show = false;
	for i=1,#Screens do
		if Screens[i] == screen then show = true; end;
	end;
	 
	--if not show then
	self:visible( show );
	self:zoom( 1 );
	
	-- Se resetea la posicion en x, ademas de mostrar los nombres.
	if screen == "ScreenSelectMusic" then
		if pn==1 then 
			self:x(100); 
			--self:y(SCREEN_HEIGHT-20); 
		end;
		if pn==2 then 
			self:x(SCREEN_WIDTH-100); 
			--self:y(SCREEN_HEIGHT-20);
		end;
	end;
end;

--para mostrar el corazón dependiendo de las stages que nos quedan
--o hayamos jugado.
function Actor:ShowHeartOnStage(heartIndex, pn)

	--  5[SPP] - ( 3[FULL] - 5[SPP] ) == 3
	--  si el index es menor a 2, ocultalo

	heartsPlayed = GAMESTATE:GetNumStagesLeft(pn);
	heartsLeft = PREFSMAN:GetPreference('SongsPerPlay') + (heartsPlayed - PREFSMAN:GetPreference('SongsPerPlay'));

	if heartsLeft-1 < heartIndex then
		self:visible(false);
	end;
end;

-- determina si el corazón debe mostrarse con el efecto fadein/fadeout
function ShowHeartOnStageEffect(heartIndex, pn)
	heartsPlayed = GAMESTATE:GetNumStagesLeft(pn);
	heartsLeft = PREFSMAN:GetPreference('SongsPerPlay') + (heartsPlayed - PREFSMAN:GetPreference('SongsPerPlay'));
	stagesForThisSong = GAMESTATE:GetNumStagesForCurrentSongAndStepsOrCourse();

	if (heartsLeft - stagesForThisSong)-1 < heartIndex then
		return true;
	end;

	return false;
end;

--frame que define el background y font para el nombre
local function PlayerName( Player )
	local xpos;
	local ypos;
	local horz; 
	local adx;
	local ady;
	local hdx;
	local hdy;
	local avdy;
	local avdx;
	local adjustx;
	local pn = (Player == PLAYER_1) and 1 or 2;	--pn = 1 o 2, dependiendo del nº del jugador
	
	if Player == PLAYER_1 then
		horz = 'HorizAlign_Left'; 
		xpos = 40;
		ypos = -5;
		adx = 13;
		ady = 12;
		hdx = 49.5;
		hdy = -1;
		avdy = 5;
		avdx = -35;
		adjustx = -3;
	elseif Player == PLAYER_2 then
		horz = 'HorizAlign_Right'; 
		xpos = -90;
		ypos = -5;
		adx = 7;
		ady = 12;
		hdx = 22;
		hdy = -1;
		avdy = 5;
		avdx = -60;
		adjustx = 85;
	else 
		return;
	end;

---	--- --- --- --- --- --- ---	--- --- --- --- --- --- ---	--- --- --- --- --- ---
	local t = Def.ActorFrame {		 
		LoadActor(THEME:GetPathG("","Messages/avatars"))..{
			Name = "Avatars";
			InitCommand=cmd(horizalign,horz; zoom,.75; x, xpos+avdx; y, ypos+avdy; setstate, 0; animate, 0; pause);	
		};

		LoadActor(THEME:GetPathG("","Messages/player_name"))..{
			InitCommand=cmd(horizalign,horz; zoom,.75;);
		};

		LoadFont("","_youth 20px") .. {
			Name = "Name";
			InitCommand=cmd(horizalign,horz;x,xpos+adjustx;y,ypos;zoom,0.6);
		}; 

		LoadFont("","lvl_numbers") .. {
			Name = "Level";
			InitCommand=cmd(horizalign,horz;x,xpos+adx;y,ypos+ady;zoom,0.65;settext,"00000");
		};		
	};

	local songs = PREFSMAN:GetPreference('SongsPerPlay');
	for i=0,songs-1 do
		t[#t+1] = LoadActor(THEME:GetPathG("","Messages/heart")) .. {
			InitCommand=cmd(horizalign,horz;x,xpos+adx+hdx+(14.3*i);y,ypos+ady+hdy;zoom,0.75);
			LoopCommand=cmd(stoptweening;diffusealpha,1;linear,0.5;diffusealpha,0;linear,0.5;diffusealpha,1;queuecommand,'Loop');
			CurrentSongChangedMessageCommand=function(self, params)				
				if not SCREENMAN:GetTopScreen() then return end;

				(cmd(ShowHeartOnStage,i,Player))(self);

				if ShowHeartOnStageEffect(i, Player) then
					(cmd(stoptweening;queuecommand,'Loop'))(self);
				else
					(cmd(stoptweening;diffusealpha,1))(self);
				end;
			end;
		};
	end;
	
	return t;
end;

-- Player 1 Name
t[#t+1] = PlayerName( PLAYER_1 )..{
	InitCommand=function(self)
		(cmd(x,SCREEN_CENTER_X-180;y,SCREEN_HEIGHT-20))(self);		
	end;
	ScreenChangedMessageCommand=function(self)
		(cmd(stoptweening;ShowPlayerName,1))(self);		

		if not GAMESTATE:IsSideJoined(PLAYER_1) then
			self:visible(false);
		end;

		if MEMCARDMAN:GetCardState('PlayerNumber_P1') == 'MemoryCardState_ready' then				
			(cmd(stoptweening;sleep,0.5;decelerate,.3;y,SCREEN_HEIGHT-20))(self); -- para el frame en si
			MESSAGEMAN:Broadcast("ProfileAlreadyLoaded", {Player = "PlayerNumber_P1"});

			local name = self:GetChild("Name");		-- si la memoria ya esta lista, carga el nombre
			local temp = MEMCARDMAN:GetName('PlayerNumber_P1');
			if temp ~= nil then
				name:settext( string.upper(string.sub(temp,1,10)) ); --corta el nombre hasta 10 digitos
			end;

			local avatar = self:GetChild("Avatars");
			local index = MEMCARDMAN:GetAvatarIndex('PlayerNumber_P1');
			avatar:setstate(index);

			local level = self:GetChild("Level");
			local index = MEMCARDMAN:GetLevel('PlayerNumber_P1');
			level:settext(string.format("%05d",index));

			if IsNetSMOnline() then
				SendLogin('PlayerNumber_P1');
			end

			(cmd(stoptweening;decelerate,.3;y,SCREEN_HEIGHT-20))(self); -- para el frame en si
		elseif GAMESTATE:IsPlayerEnabled('PlayerNumber_P1') and MEMCARDMAN:GetCardState('PlayerNumber_P1') == 'MemoryCardState_none' then	
			(cmd(stoptweening;sleep,0.5;decelerate,.3;y,SCREEN_HEIGHT-20))(self); -- para el frame en si

			local name = self:GetChild("Name");		-- si la memoria ya esta lista, carga el nombre
			local temp = "PUMPITUP"
			name:settext( string.upper(string.sub(temp,1,10)) ); --corta el nombre hasta 10 digitos

			local avatar = self:GetChild("Avatars");
			local index = 0
			avatar:setstate(index);

			local level = self:GetChild("Level");
			local index = 0
			level:settext(string.format("%05d",index));

			(cmd(stoptweening;y,SCREEN_HEIGHT-20))(self); -- para el frame en si
		end;
	end;
	PlayerJoinedMessageCommand=function(self)
		--(cmd(stoptweening;ShowPlayerName,1))(self);		

		--if GAMESTATE:IsAnyHumanPlayerUsingMemoryCard() then
		--	self:visible(false);
		--	return;
		--end;

		--SCREENMAN:SetNewScreen("ScreenSelectMusic");
	end;
	FinishSelectProfileMessageCommand=cmd(zoom,0);
};
	
-- Player 2 Name
t[#t+1] = PlayerName( PLAYER_2 )..{
	InitCommand=function(self)
		(cmd(x,SCREEN_CENTER_X+180;y,SCREEN_HEIGHT-20))(self);		
	end;
	ScreenChangedMessageCommand=function(self)			
		(cmd(stoptweening;ShowPlayerName,2))(self);		

		if not GAMESTATE:IsSideJoined(PLAYER_2) then
			self:visible(false);
		end;

		if MEMCARDMAN:GetCardState('PlayerNumber_P2') == 'MemoryCardState_ready' then				
			(cmd(stoptweening;sleep,0.5;decelerate,.3;y,SCREEN_HEIGHT-20))(self); -- para el frame en si
			MESSAGEMAN:Broadcast("ProfileAlreadyLoaded", {Player = "PlayerNumber_P2"});

			local name = self:GetChild("Name");		-- si la memoria ya esta lista, carga el nombre
			local temp = MEMCARDMAN:GetName('PlayerNumber_P2');
			if temp ~= nil then
				name:settext( string.upper(string.sub(temp,1,10)) ); --corta el nombre hasta 10 digitos
			end;

			local avatar = self:GetChild("Avatars");
			local index = MEMCARDMAN:GetAvatarIndex('PlayerNumber_P2');
			avatar:setstate(index);

			local level = self:GetChild("Level");
			local index = MEMCARDMAN:GetLevel('PlayerNumber_P2');
			level:settext(string.format("%05d",index));

			if IsNetSMOnline() then
				SendLogin('PlayerNumber_P2');
			end
		elseif MEMCARDMAN:GetCardState('PlayerNumber_P2') == 'MemoryCardState_none' then	
			(cmd(stoptweening;sleep,0.5;decelerate,.3;y,SCREEN_HEIGHT-20))(self); -- para el frame en si

			local name = self:GetChild("Name");		-- si la memoria ya esta lista, carga el nombre
			local temp = "PUMPITUP"
			name:settext( string.upper(string.sub(temp,1,10)) ); --corta el nombre hasta 10 digitos

			local avatar = self:GetChild("Avatars");
			local index = 0
			avatar:setstate(index);

			local level = self:GetChild("Level");
			local index = 0
			level:settext(string.format("%05d",index));

			(cmd(stoptweening;y,SCREEN_HEIGHT-20))(self); -- para el frame en si
		end;
	end; 
	PlayerJoinedMessageCommand=function(self)
		--(cmd(stoptweening;ShowPlayerName,2))(self);		

		--if GAMESTATE:IsAnyHumanPlayerUsingMemoryCard() then
		--	self:visible(false);
		--	return;
		--end;

		--SCREENMAN:SetNewScreen("ScreenSelectMusic");		
	end; 
	FinishSelectProfileMessageCommand=cmd(zoom,0);
};

return t;