--local bIsVisible = false;

function IsVisible()
	--ssm
	screen = SCREENMAN:GetTopScreen()
	state = screen:GetSelectionState()

	if GAMESTATE:GetCurrentGroup() == "quest zone" then return false end;

	if state == "SelectingSong" then
		return true
	end

	return false
end

local t = Def.ActorFrame {
	InitCommand=function(self)
		if GAMESTATE:IsFullMode() or not GAMESTATE:IsRankMode() then 			
			self:visible(false); 
		else
			self:visible(true); 
		end;	
		self:zoom(.75);
		self:x(cx);
	end;

	--net verify stuff
	CurrentSongChangedMessageCommand=function(self)		
		if GAMESTATE:IsRankMode() and IsNetSMOnline() then
			AskSongScore();
		end;
	end;

	GoFullModeMessageCommand=function(self)
		(cmd(stoptweening;y,580;visible,false;diffusealpha,1))(self);
	end;

	GoRankModeMessageCommand=function(self)		
		if IsVisible() then
			(cmd(stoptweening;y,385;visible,true;diffusealpha,1))(self);
		else
			(cmd(stoptweening;y,580;visible,false;diffusealpha,1))(self);
		end;
	end;

	OnCommand=function(self)
		if GAMESTATE:IsRankMode() and GAMESTATE:GetCurrentGroup() ~= "quest zone" then
			(cmd(stoptweening;visible,true;diffusealpha,1;y,580;decelerate,.1;y,385))(self);
		else
			(cmd(stoptweening;diffusealpha,1;visible,false))(self);
		end
	end;

	StartSelectingStepsMessageCommand=function(self)
		if GAMESTATE:IsRankMode() and GAMESTATE:GetCurrentGroup() ~= "quest zone" then
			(cmd(stoptweening;;visible,true;y,385;decelerate,.1;y,580;diffusealpha,0))(self);
		else
			(cmd(stoptweening;diffusealpha,1;visible,false))(self);
		end
	end;

	--aqui no va la comprobación de si es quest zone, porque no sirve de nada.
	StartSelectingSongMessageCommand=function(self)		
		if GAMESTATE:IsRankMode() and SCREENMAN:GetTopScreen():GetLastGroup() ~= "quest zone" then
			(cmd(stoptweening;visible,true;y,580;decelerate,.1;y,385;diffusealpha,1))(self);
		else
			(cmd(stoptweening;diffusealpha,1;visible,false))(self);
		end		
	end;

	GoBackSelectingSongMessageCommand=function(self)
		if GAMESTATE:IsRankMode() and GAMESTATE:GetCurrentGroup() ~= "quest zone" then
			(cmd(stoptweening;visible,true;diffusealpha,1;y,580;decelerate,.1;y,385))(self);
		else
			(cmd(stoptweening;diffusealpha,1;visible,false))(self);
		end
	end;

	GoBackSelectingGroupMessageCommand=function(self)
		if GAMESTATE:IsRankMode() and GAMESTATE:GetCurrentGroup() ~= "quest zone" then			
			(cmd(stoptweening;visible,true;y,385;decelerate,.1;y,580;diffusealpha,0))(self);
		else
			(cmd(stoptweening;diffusealpha,1;visible,false))(self);
		end
	end;

	OffCommand=cmd(stoptweening;linear,.2;diffusealpha,0;y,580);
};

t[#t+1] = LoadActor("record_online.png");

--1st
t[#t+1] = LoadFont("_youth 20px") .. {
	InitCommand=cmd(stoptweening;addy,-40;addx,20;settext,"");
	UpdateRankNamesMessageCommand=function(self)
		if Get1StScore() == "(none)" then self:settext(""); return; end;
		self:settext(string.upper(Get1StScore()));
	end;
	OffCommand=cmd(stoptweening;visible,false);
};

--2nd
t[#t+1] = LoadFont("_youth 20px") .. {
	InitCommand=cmd(stoptweening;addy,0;addx,20;settext,"";);
	UpdateRankNamesMessageCommand=function(self)
		if Get2NdScore() == "(none)" then self:settext(""); return; end;
		self:settext(string.upper(Get2NdScore()));
	end;
	OffCommand=cmd(stoptweening;visible,false);
};

--3rd
t[#t+1] = LoadFont("_youth 20px") .. {
	InitCommand=cmd(stoptweening;addy,40;addx,20;settext,"";);
	UpdateRankNamesMessageCommand=function(self)
		if Get3RdScore() == "(none)" then self:settext(""); return; end;
		self:settext(string.upper(Get3RdScore()));
	end;
	OffCommand=cmd(stoptweening;visible,false);
};

return t;