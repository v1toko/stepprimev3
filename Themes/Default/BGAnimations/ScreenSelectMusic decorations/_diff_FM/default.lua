local t = Def.ActorFrame {}

--///////////////////////////////////////////////////////////////////////////////////////////
--///////////////////////////////////////////////////////////////////////////////////////////
local backSelectSteps = cmd(stoptweening;diffusealpha,0;decelerate,.3;diffusealpha,.3);
local ballSelectSteps = cmd(stoptweening;diffusealpha,0;decelerate,.3;diffusealpha,1);
--///////////////////////////////////////////////////////////////////////////////////////////
--///////////////////////////////////////////////////////////////////////////////////////////

function GetDiffNumberBalls(pn)

	if GAMESTATE:GetCurrentGroup() ~= "music train"  and GAMESTATE:GetCurrentGroup() ~= "random" then
		--Clasificacion de pasos x estilo, single, double, couple o halfdouble
		--todo "StepsType_Pump_Couple"
		
		--Cada estilo posee, beginner, easy, medium, hard, challenge, y edit.
		dif = GAMESTATE:GetCurrentSteps(pn):GetDifficulty();
		style = GAMESTATE:GetCurrentSteps(pn):GetStepsType();
		meter = GAMESTATE:GetCurrentSteps(pn):GetMeter();
		desc = GAMESTATE:GetCurrentSteps(pn):GetDescription();

		if string.find(desc, "SP") ~= nil then return 1 end;
		if string.find(desc, "DP") ~= nil then 
			if meter < 50 then return 3 end;
			if meter >= 50 then return 4 end;
		end;

		--Cada estilo posee, beginner, easy, medium, hard, challenge, y edit.
		if style=='StepsType_Pump_Single' then return (0);
		--si tenemos un edit (performance > 10) es comun y corriente
		--elseif style=='StepsType_Pump_Single' and dif=='Difficulty_Edit' and meter > 10 then return(0);
		elseif style=='StepsType_Pump_Double' then return (2);
		--elseif style=='StepsType_Pump_Single' and dif=='Difficulty_Edit' then return(1); 
		elseif style=='StepsType_Pump_Routine' then return(3); 
		end;
	else
		local crs = GAMESTATE:GetCurrentCourse();
		if crs == nil then return ""; end;

		if crs:GetCourseStyle()[1] == "DOUBLE" then
			return 2;
		else
			return 0; 
		end
	end;
	
	return 0;
end;

function GetDiffNumber(pn)

	if GAMESTATE:GetCurrentGroup() ~= "music train"  and GAMESTATE:GetCurrentGroup() ~= "random" and GAMESTATE:GetCurrentGroup() ~= "quest zone" then
		--Clasificacion de pasos x estilo, single, double, couple o halfdouble
		--todo "StepsType_Pump_Couple"
		
		--Cada estilo posee, beginner, easy, medium, hard, challenge, y edit.
		dif = GAMESTATE:GetCurrentSteps(pn):GetDifficulty();
		style = GAMESTATE:GetCurrentSteps(pn):GetStepsType();
		meter = GAMESTATE:GetCurrentSteps(pn):GetMeter();
		desc = GAMESTATE:GetCurrentSteps(pn):GetDescription();

		if string.find(desc, "SP") ~= nil then return 1 end;
		if string.find(desc, "DP") ~= nil and meter < 50 then return 3 end;
		if string.find(desc, "DP") ~= nil and meter >= 50 then return 5 end;

		--Cada estilo posee, beginner, easy, medium, hard, challenge, y edit.
		if style=='StepsType_Pump_Single' then return (0);
		--si tenemos un edit (performance > 10) es comun y corriente
		--elseif style=='StepsType_Pump_Single' and dif=='Difficulty_Edit' and meter > 10 then return(0);
		elseif style=='StepsType_Pump_Double' then return (2);
		--elseif style=='StepsType_Pump_Single' and dif=='Difficulty_Edit' then return(1); 
		elseif style=='StepsType_Pump_Routine' then return(3); 
		end;
	elseif GAMESTATE:GetCurrentGroup() == "quest zone" then
		return 4;
	else
		local crs = GAMESTATE:GetCurrentCourse();
		if crs == nil then return ""; end;

		if crs:GetCourseStyle()[1] == "DOUBLE" then
			return 2;
		else
			return 0; 
		end
	end;
	
	return 0;
end;

-- Regresa booleano si ya fue jugado
function GetIfUnplayedStepDiff(pn)
	--Carga de Steps "disponibles" en la cancion actual
	if GAMESTATE:GetCurrentGroup() == "quest zone" or GAMESTATE:GetCurrentGroup() == "random" or GAMESTATE:GetCurrentGroup() == "music train" then return 0 end;

	local step = GAMESTATE:GetCurrentSteps(pn);

	if not step then return 0 end;
	
	desc = step:GetDescription();
	desc = string.lower(desc);

	if string.find(desc, "ucs") ~= nil then return 1 end;
	if string.find(desc, "new") ~= nil then return 2 end;
	if string.find(desc, "another") ~= nil then return 3 end;

	return 0;
end;

function Actor:settextbymeter(pn)
	if GAMESTATE:GetCurrentGroup() == 'music train' or GAMESTATE:GetCurrentGroup() == 'random' then self:settext('??'); return; end;
	
	local meter = GAMESTATE:GetCurrentSteps(pn):GetMeter();

	desc = GAMESTATE:GetCurrentSteps(pn):GetDescription();
	desc = string.upper(desc);

	local bDP = false;
	if string.find(desc, "DP") ~= nil then bDP = true end;

	if meter<=9 then 
		self:settext('0'..meter); 
	else 
		if meter < 50 then
			self:settext(meter); 
		else
			if bDP then				
				--sweet magic
				self:settext('02');
				for v in string.gmatch(desc, "OPX[0-9]") do
				    for a in string.gmatch(v, "[^OPX,]+") do
			            local n = tonumber(a)
			            self:settext('0'..n);
		            end
				end
			else
				self:settext("??"); 
			end			
		end
	end;
end;

function Actor:ifsteps(player,style)
	if GetDiffNumber(player)~=style then
		self:visible(false);
	else
		self:visible(true);
	end;
end;

adjustp1x = 35;

--///////////////////////////////////////////////////////////////////////////////////////////
--///////////////////////////////////////////////////////////////////////////////////////////
--///////////////////////////////////////////////////////////////////////////////////////////
--Player 1
if GAMESTATE:IsSideJoined('PlayerNumber_P1') then

for i=1,2 do
	xRight = cx-187+adjustp1x;
	xLeft = cx-60+adjustp1x;
	t[#t+1] = LoadActor("1p_arrow")..{
		InitCommand=function(self)
			if i==1 then 
				self:x(xRight); 
				self:rotationz(180); 
			elseif i==2 then 
				self:x(xLeft); 
			end;
		end;
		OnCommand=function(self)
			if i == 1 then
				(cmd(stoptweening;blend,'BlendMode_Add';diffusealpha,0;zoom,0.75;bob; effectmagnitude, 2,0,0; effectperiod, .9))(self);
			elseif i==2 then 
				(cmd(stoptweening;blend,'BlendMode_Add';diffusealpha,0;zoom,0.75;bob; effectmagnitude, -2,0,0; effectperiod, .9))(self);
			end;
		end;
		--
		StartSelectingStepsMessageCommand=cmd(stoptweening;diffusealpha,0;sleep,.2;decelerate,.2;diffusealpha,1;queuecommand,'Loop');
		ChangeStepsMessageCommand=function(self, params)
			--Para cambios generales de steps
			if params.Player == PLAYER_1 then
				if params.Direction == 1 then	--der
					if i==1 then 
						(cmd(stoptweening;diffusealpha,1;linear,.5;x,xRight; queuecommand, 'Loop'))(self);
					elseif i==2 then 
						(cmd(stoptweening;diffusealpha,1;linear,.05;x,xLeft;decelerate,.05;x,xLeft+10;decelerate,.4;x,xLeft; queuecommand, 'Loop'))(self);
					end;
				elseif params.Direction == -1 then	--izq
					if i==2 then 
						(cmd(stoptweening;diffusealpha,1;linear,.5;x,xLeft; queuecommand, 'Loop'))(self);
					elseif i==1 then 
						(cmd(stoptweening;diffusealpha,1;linear,.05;x,xRight;decelerate,.05;x,xRight-10;decelerate,.4;x,xRight; queuecommand, 'Loop'))(self);
					end;
				end;
			end;
		end;
		--CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;linear,.05;zoom,.52;linear,.05;zoom,.55;decelerate,.1;zoom,.5;sleep,.14;decelerate,.4;zoom,.54);
		--[[LoopCommand=function(self)
			if i==1 then 
				(cmd(stoptweening;x,xRight;decelerate,.4;x,xRight-3;decelerate,.4;x,xRight; queuecommand, "Loop"))(self);
			elseif i==2 then 
				(cmd(stoptweening;x,xLeft;decelerate,.4;x,xLeft+3;decelerate,.4;x,xLeft; queuecommand, "Loop"))(self);
			end;
		end;]]
		LoopCommand=cmd();
		--
		SongUnchosenMessageCommand=cmd(stoptweening;diffusealpha,1;decelerate,.1;diffusealpha,0);
		--
		StepsChosenMessageCommand=function(self,params)
			if params.Player == PLAYER_1 then 
				--self:stoptweening();
				--self:diffusealpha(1);
				self:playcommand('Loop'); 
			end;
		end;
		--FastCommand=cmd(stoptweening;decelerate,.1;decelerate,.1;);
		StepsPreselectedCancelledMessageCommand=function(self,params)
			self:diffusealpha(1);
			if params.Player == PLAYER_1 then self:playcommand('Loop'); end;
		end;
		--
		OffCommand=function(self)
			if i==1 then 
				(cmd(stoptweening;decelerate,.1;diffusealpha,0;x,xLeft+5))(self);
			elseif i==2 then 
				(cmd(stoptweening;decelerate,.1;diffusealpha,0;x,xRight-5))(self);
			end;
		end;
	};
	
end;

--Back
t[#t+1] = LoadActor("diff_ball_bg")..{
	InitCommand=cmd(x,adjustp1x+cx-123;zoom,.75;diffusealpha,0);
	--InitCommand=cmd(x,cx-123;zoom,.75;diffusealpha,0);
	SetBallDiffCommand=cmd(stoptweening;);
	StartSelectingStepsMessageCommand=cmd(stoptweening;diffusealpha,0;decelerate,.3;diffusealpha,1);
	--CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'SetBallDiff');
	OffCommand=cmd(stoptweening;diffusealpha,1;sleep,.4;decelerate,.4;diffusealpha,0);
}

t[#t+1] = LoadActor("diff_balls")..{
	InitCommand=cmd(x,adjustp1x+cx-122;zoom,.75;diffusealpha,0);
	--InitCommand=cmd(x,cx-122;zoom,.75;diffusealpha,0);
	SetBallDiffCommand=cmd(stoptweening;animate,0;pause;setstate,GetDiffNumberBalls('PlayerNumber_P1'));
	StartSelectingStepsMessageCommand=cmd(stoptweening;diffusealpha,0;decelerate,.3;diffusealpha,1);
	CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'SetBallDiff');
	OffCommand=cmd(stoptweening;diffusealpha,1;sleep,.4;decelerate,.4;diffusealpha,0);
};

t[#t+1] = LoadActor("efx1")..{
	OnCommand=cmd(x,adjustp1x+cx-122;zoom,.75;diffusealpha,0;blend,'BlendMode_Add';diffusealpha,0);
	ChangeStepsMessageCommand=function(self,params)
		if params.Player == "PlayerNumber_P1" and params.Direction == -1 then
			(cmd(stoptweening;diffusealpha,.7;fadeleft,.5;decelerate,.3;fadeleft,0;diffusealpha,0))(self);
		elseif params.Player == "PlayerNumber_P1" and params.Direction == 1 then
			(cmd(stoptweening;diffusealpha,.7;faderight,.5;decelerate,.3;faderight,0;diffusealpha,0))(self);
		end;
	end;
};

t[#t+1] = LoadActor("efx3")..{
	OnCommand=cmd(x,adjustp1x+cx-122;zoom,.7;diffusealpha,0;blend,'BlendMode_Add';diffusealpha,0);
	GoBackSelectingSongMessageCommand=cmd(stoptweening;diffusealpha,0.7;decelerate,.2;diffusealpha,0);
};

t[#t+1] = LoadActor("efx2")..{
	OnCommand=cmd(x,adjustp1x+cx-122;zoom,.75;diffusealpha,0;blend,'BlendMode_Add';diffusealpha,0);
	StartSelectingStepsMessageCommand=cmd(queuecommand,'Repeat');
	RepeatCommand=cmd(stoptweening;diffusealpha,0;rotationz,-180;linear,.9;addrotationz,-70;diffusealpha,.1;linear,.6;addrotationz,-90;diffusealpha,0;linear,.5;addrotationz,-20;sleep,1;queuecommand,'Repeat');
};

t[#t+1] = LoadActor("efx2")..{
	OnCommand=cmd(x,adjustp1x+cx-122;zoom,.75;diffusealpha,0;blend,'BlendMode_Add';diffusealpha,0);
	StepsChosenMessageCommand=cmd(stoptweening;diffuse,0.5,0.5,1,0;diffusealpha,0;rotationz,-45;linear,.2;addrotationz,-70;diffusealpha,.5;linear,.5;addrotationz,-180;diffusealpha,0);
};

t[#t+1] = LoadActor("steps 1x4")..{
	InitCommand=cmd(animate,0);
	OnCommand=cmd(x,adjustp1x+cx-122;zoom,.75;diffusealpha,0;blend,'BlendMode_Add';diffusealpha,0);
	CheckCommand=function(self) 
		self:visible(GAMESTATE:GetCurrentGroup() == "quest zone");		
	end;
	StepPositionMessageCommand=function(self, params)
		if params.Valid == 0 then self:visible(false); return; end;
		self:setstate(params.Step);
	end;
	OnCommand=cmd(stoptweening;x,adjustp1x+cx-121;y,-43;zoom,0.75;diffusealpha,0;playcommand,'Check');
	StartSelectingStepsMessageCommand=cmd(stoptweening;diffusealpha,0;accelerate,.1;diffusealpha,1;playcommand,'Check');
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;diffusealpha,0;playcommand,'Check');
	CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;playcommand,'Check');
	OffCommand=cmd(stoptweening;decelerate,.1;diffuse,.9,.9,.9,1;sleep,.3;decelerate,.1;diffusealpha,0;playcommand,'Check');
};

t[#t+1] = LoadActor("stepcomplete") .. {
	InitCommand=cmd(animate,0);
	--OnCommand=cmd(x,adjustp1x+cx-122;zoom,.75;diffusealpha,0;blend,'BlendMode_Add';diffusealpha,0);
	CheckCommand=function(self) 
		self:visible(GAMESTATE:GetCurrentGroup() == "quest zone");		
	end;
	StepPositionMessageCommand=function(self, params)
		--Warn("valid: " .. tostring(params.Valid) );
		if params.Valid == 0 then self:visible(false); return; end;

		if params.Cleared then
			self:visible(true);
			self:diffusealpha(1);
		else
			self:visible(false);
			self:diffusealpha(0);
		end;
	end;
	OnCommand=cmd(stoptweening;x,adjustp1x+cx-121;y,32;zoom,0.75;diffusealpha,0;sleep,0.05;queuecommand,'Check');
};

-- SingleNumbers (orange)
t[#t+1] = LoadFont("numbersorangebig")..{
	OnCommand=cmd(stoptweening;playcommand,'Set';x,adjustp1x+cx-121;y,23;zoom,0.75;diffusealpha,0);
	SetCommand=cmd(ifsteps,'PlayerNumber_P1',0;settextbymeter,'PlayerNumber_P1');
	StartSelectingStepsMessageCommand=cmd(stoptweening;ifsteps,'PlayerNumber_P1',0;diffusealpha,0;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;diffusealpha,0);
	CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'Set');
	OffCommand=cmd(stoptweening;decelerate,.1;diffuse,.9,.9,.9,1;sleep,.3;decelerate,.1;diffusealpha,0);
};
-- DoubleNumbers( green )
t[#t+1] = LoadFont("numbersgreenbig")..{
	OnCommand=cmd(stoptweening;playcommand,'Set';x,adjustp1x+cx-121;y,23;zoom,0.75;diffusealpha,0);
	SetCommand=cmd(ifsteps,'PlayerNumber_P1',2;settextbymeter,'PlayerNumber_P1');
	StartSelectingStepsMessageCommand=cmd(stoptweening;ifsteps,'PlayerNumber_P1',2;diffusealpha,0;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;diffusealpha,0);
	CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'Set');
	OffCommand=cmd(stoptweening;decelerate,.1;diffuse,.9,.9,.9,1;sleep,.3;decelerate,.1;diffusealpha,0);
};
-- SinglePerformanceNumbers( pink )
t[#t+1] = LoadFont("numberspurplebig")..{
	OnCommand=cmd(stoptweening;playcommand,'Set';x,adjustp1x+cx-121;y,23;zoom,0.75;diffusealpha,0);
	SetCommand=cmd(ifsteps,'PlayerNumber_P1',1;settextbymeter,'PlayerNumber_P1');
	StartSelectingStepsMessageCommand=cmd(stoptweening;ifsteps,'PlayerNumber_P1',1;diffusealpha,0;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;diffusealpha,0);
	CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'Set');
	OffCommand=cmd(stoptweening;decelerate,.1;diffuse,.9,.9,.9,1;sleep,.3;decelerate,.1;diffusealpha,0);
};
-- DoublePerformanceNumbers( blue )
t[#t+1] = LoadFont("numbersbluebig")..{
	OnCommand=cmd(stoptweening;playcommand,'Set';x,adjustp1x+cx-121;y,23;zoom,0.75;diffusealpha,0);
	SetCommand=cmd(ifsteps,'PlayerNumber_P1',3;settextbymeter,'PlayerNumber_P1');
	StartSelectingStepsMessageCommand=cmd(stoptweening;ifsteps,'PlayerNumber_P1',3;diffusealpha,0;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;diffusealpha,0);
	CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'Set');
	OffCommand=cmd(stoptweening;decelerate,.1;diffuse,.9,.9,.9,1;sleep,.3;decelerate,.1;diffusealpha,0);
};

-- DoublePerformanceNumbers( yellow )
t[#t+1] = LoadFont("numbersyellowbig")..{
	OnCommand=cmd(stoptweening;playcommand,'Set';x,adjustp1x+cx-121;y,22;zoom,0.75;diffusealpha,0);
	SetCommand=cmd(ifsteps,'PlayerNumber_P1',5;settextbymeter,'PlayerNumber_P1');
	StartSelectingStepsMessageCommand=cmd(stoptweening;ifsteps,'PlayerNumber_P1',5;diffusealpha,0;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;diffusealpha,0);
	CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'Set');
	OffCommand=cmd(stoptweening;decelerate,.1;diffuse,.9,.9,.9,1;sleep,.3;decelerate,.1;diffusealpha,0);
};

-- QuestZoneNumbers( diffuse )
t[#t+1] = LoadFont("numbersdiffusebig")..{
	OnCommand=cmd(stoptweening;playcommand,'Set';x,adjustp1x+cx-121;y,23;zoom,0.75;diffusealpha,0);
	SetCommand=cmd(ifsteps,'PlayerNumber_P1',4;settextbymeter,'PlayerNumber_P1');
	StartSelectingStepsMessageCommand=cmd(stoptweening;ifsteps,'PlayerNumber_P1',4;diffusealpha,0;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;diffusealpha,0);
	CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'Set');
	OffCommand=cmd(stoptweening;decelerate,.1;diffuse,.9,.9,.9,1;sleep,.3;decelerate,.1;diffusealpha,0);
};

t[#t+1] = LoadActor("stepsstate") .. {
	CheckCommand=function(self) 
		local b = GetIfUnplayedStepDiff('PlayerNumber_P1');
		self:visible(GAMESTATE:GetCurrentGroup() ~= "quest zone" and GAMESTATE:GetCurrentGroup() ~= "random");
		if b > 0 then
			self:setstate(b-1);
		else
			self:visible(false);
		end
	end;
	InitCommand=cmd(animate,0;pause;);
	OnCommand=cmd(stoptweening;x,adjustp1x+cx-121;y,-43;zoom,0.75;diffusealpha,0;playcommand,'Check');
	--SetCommand=cmd();
	StartSelectingStepsMessageCommand=cmd(stoptweening;diffusealpha,0;accelerate,.1;diffusealpha,1;playcommand,'Check');
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;diffusealpha,0;playcommand,'Check');
	CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;playcommand,'Check');
	OffCommand=cmd(stoptweening;decelerate,.1;diffuse,.9,.9,.9,1;sleep,.3;decelerate,.1;diffusealpha,0;playcommand,'Check');
};

t[#t+1] = LoadActor("records_normal")..{
	OnCommand=cmd(stoptweening;x,adjustp1x+cx-260;y,5;zoom,.75;diffusealpha,0);
	--SetCommand=cmd();
	StartSelectingStepsMessageCommand=cmd(stoptweening;visible,GAMESTATE:GetCurrentGroup() ~= "quest zone";diffusealpha,0;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;diffusealpha,0);
	CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1);
	OffCommand=cmd(stoptweening;decelerate,.1;diffuse,.9,.9,.9,1;sleep,.3;decelerate,.1;diffusealpha,0);
};

local xHelp = adjustp1x+cx-260;

t[#t+1] = LoadActor("effect_records")..{
	InitCommand=cmd(blend,'BlendMode_Add');
	OnCommand=cmd(stoptweening;x,xHelp;y,4;zoom,.75;diffusealpha,0);
	--SetCommand=cmd();
	--StartSelectingStepsMessageCommand=cmd(stoptweening;diffusealpha,0;accelerate,.1;diffusealpha,1);
	ChangeStepsMessageCommand=function(self,params)
		if params.Player == "PlayerNumber_P1" and params.Direction == -1 then
			(cmd(stoptweening;visible,GAMESTATE:GetCurrentGroup() ~= "quest zone" and GAMESTATE:GetCurrentGroup() ~= "random";x,xHelp-10;diffusealpha,.7;x,xHelp+10;fadeleft,.5;decelerate,.3;x,xHelp-10;fadeleft,0;diffusealpha,0))(self);
		elseif params.Player == "PlayerNumber_P1" and params.Direction == 1 then
			(cmd(stoptweening;visible,GAMESTATE:GetCurrentGroup() ~= "quest zone" and GAMESTATE:GetCurrentGroup() ~= "random";x,xHelp+10;diffusealpha,.7;x,xHelp-10;faderight,.5;decelerate,.3;x,xHelp+10;faderight,0;diffusealpha,0))(self);
		end;
	end;
	--[[NextStepMessageCommand=function(self,params)
		if params.Player == "PlayerNumber_P1" and params.Direction = 1 then
			
		end;
	end;]]	
	--CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1);
	OffCommand=cmd(stoptweening;diffusealpha,0);
};

-- HighScore Para el player 1
t[#t+1] = LoadFont("_karnivore lite 20px")..{
	InitCommand=cmd(stoptweening;x,adjustp1x+cx-305;y,-31;settext,"";zoom,.75;horizalign,'HorizAlign_Left');
	RefreshTextCommand=function(self)
		if GAMESTATE:GetCurrentGroup() ~= "music train" then
			local cur_song = GAMESTATE:GetCurrentSong();
			local cur_steps = GAMESTATE:GetCurrentSteps(PLAYER_1);
			--local HSList = PROFILEMAN:GetMachineProfile():GetHighScoreList(cur_song,cur_steps):GetHighScores();
			local prf = PROFILEMAN:GetProfile(PLAYER_1);
			if not prf:IsMachine() and cur_steps ~= nil then
				local HSList = prf:GetHighScoreList(cur_song,cur_steps):GetHighScores();
				if (#HSList ~= 0) then
					local name = HSList[1]:GetName();	
					local score = HSList[1]:GetScore();	
					self:settext(comma_value(score));
				else
					self:settext(" ");
				end;
			else
				self:settext(" ");
			end;
		else
			if GAMESTATE:GetCurrentGroup() ~= "random" then
				local cur_song = GAMESTATE:GetCurrentCourse();
				local crs = GAMESTATE:GetCurrentCourse();
				if crs == nil then return ""; end;

				local trails = crs:GetAllTrails();
				local cur_steps = trails[1];

				--local HSList = PROFILEMAN:GetMachineProfile():GetHighScoreList(cur_song,cur_steps):GetHighScores();
				local prf = PROFILEMAN:GetProfile(PLAYER_1);
				if not prf:IsMachine() and cur_steps ~= nil then
					local HSList = prf:GetHighScoreList(cur_song,cur_steps):GetHighScores();
					if (#HSList ~= 0) then
						local name = HSList[1]:GetName();	
						local score = HSList[1]:GetScore();	
						self:settext(comma_value(score));
					else
						self:settext(" ");
					end;
				else
					self:settext(" ");
				end;
			else
				self:settext(" ");
			end;
		end;
	end;
	CurrentStepsP1ChangedMessageCommand=cmd(visible,GAMESTATE:GetCurrentGroup() ~= "quest zone" and GAMESTATE:GetCurrentGroup() ~= "random";playcommand,'RefreshText');
	OffCommand=cmd(stoptweening;visible,false);
};

-- HighScore Para el player 1 (machine) (worldbest)
t[#t+1] = LoadFont("_karnivore lite 20px")..{
	InitCommand=cmd(stoptweening;x,adjustp1x+cx-305;y,9;settext,"";zoom,.75;horizalign,'HorizAlign_Left');
	RefreshTextCommand=function(self)
		self:settext(" ");
		if GAMESTATE:GetCurrentGroup() ~= "music train" then
			self:settext(" ");
			--se debe enviar 1 sola vez
			--and IsSMOnlineLoggedIn(PLAYER_1)
			if IsNetConnected() and IsSMOnlineLoggedIn(PLAYER_1) then
				AskSongStepScore(PLAYER_1);
			end
		end;
	end;
	UpdateRankStepScoreP1MessageCommand=function(self)
		if GetMaxStepScore(PLAYER_1) ~= 0 then
			self:settext( comma_value(GetMaxStepScore(PLAYER_1)) );
		end
	end;
	CurrentStepsP1ChangedMessageCommand=cmd(visible,GAMESTATE:GetCurrentGroup() ~= "quest zone" and GAMESTATE:GetCurrentGroup() ~= "random";playcommand,'RefreshText');
	OffCommand=cmd(stoptweening;visible,false);
};

t[#t+1] = LoadFont("_youth 20px")..{
	InitCommand=cmd(stoptweening;x,adjustp1x+cx-305;y,-3;settext,"";zoom,.65;horizalign,'HorizAlign_Left');
	RefreshTextCommand=function(self)
		if GAMESTATE:GetCurrentGroup() ~= "music train" then
			self:settext(" ");
			--se debe enviar 1 sola vez
			--and IsSMOnlineLoggedIn(PLAYER_1)
			--if IsNetConnected() then
			--	AskSongStepScore(PLAYER_1);
			--end
		end;
	end;
	UpdateRankStepScoreP1MessageCommand=function(self)
		if GetMaxStepScore(PLAYER_1) ~= 0 then
			self:settext( string.upper(tostring(Get1StStepScoreName(PLAYER_1))) );
		end
	end;
	CurrentStepsP1ChangedMessageCommand=cmd(visible,GAMESTATE:GetCurrentGroup() ~= "quest zone" and GAMESTATE:GetCurrentGroup() ~= "random";playcommand,'RefreshText');
	OffCommand=cmd(stoptweening;visible,false);
};

-- HighScore Para el player 1 (machine) (worldbest)
t[#t+1] = LoadFont("_karnivore lite 20px")..{
	InitCommand=cmd(stoptweening;x,adjustp1x+cx-305;y,49;settext,"";zoom,.75;horizalign,'HorizAlign_Left');
	RefreshTextCommand=function(self)
		if GAMESTATE:GetCurrentGroup() ~= "music train" then
			local cur_song = GAMESTATE:GetCurrentSong();
			local cur_steps = GAMESTATE:GetCurrentSteps(PLAYER_1);

			if cur_steps == nil then return end;

			local HSList = PROFILEMAN:GetMachineProfile():GetHighScoreList(cur_song,cur_steps):GetHighScores();
			--local HSList = PROFILEMAN:GetProfile(PLAYER_1):GetHighScoreList(cur_song,cur_steps):GetHighScores();
			if (#HSList ~= 0) then
				--local name = HSList[1]:GetName();	
				local score = HSList[1]:GetScore();	
				self:settext(comma_value(score));
			else
				self:settext(" ");
			end;
		else
			if GAMESTATE:GetCurrentGroup() ~= "random" then
				local cur_song = GAMESTATE:GetCurrentCourse();
				local crs = GAMESTATE:GetCurrentCourse();
				if crs == nil then return ""; end;

				local trails = crs:GetAllTrails();
				local cur_steps = trails[1];

				local HSList = PROFILEMAN:GetMachineProfile():GetHighScoreList(cur_song,cur_steps):GetHighScores();
				--local HSList = PROFILEMAN:GetProfile(PLAYER_1):GetHighScoreList(cur_song,cur_steps):GetHighScores();
				if (#HSList ~= 0) then
					--local name = HSList[1]:GetName();	
					local score = HSList[1]:GetScore();	
					self:settext(comma_value(score));
				else
					self:settext(" ");
				end;
			else
				self:settext(" ");
			end;
		end;
	end;
	CurrentStepsP1ChangedMessageCommand=cmd(visible,GAMESTATE:GetCurrentGroup() ~= "quest zone" and GAMESTATE:GetCurrentGroup() ~= "random";playcommand,'RefreshText');
	OffCommand=cmd(stoptweening;visible,false);
};

t[#t+1] = LoadFont("_youth 20px")..{
	InitCommand=cmd(stoptweening;x,adjustp1x+cx-305;y,37;settext,"";zoom,.65;horizalign,'HorizAlign_Left');
	RefreshTextCommand=function(self)
		if GAMESTATE:GetCurrentGroup() ~= "music train" then
			local cur_song = GAMESTATE:GetCurrentSong();
			local cur_steps = GAMESTATE:GetCurrentSteps(PLAYER_1);
			if cur_steps == nil then return end;
			local HSList = PROFILEMAN:GetMachineProfile():GetHighScoreList(cur_song,cur_steps):GetHighScores();
			--local HSList = PROFILEMAN:GetProfile(PLAYER_1):GetHighScoreList(cur_song,cur_steps):GetHighScores();
			if (#HSList ~= 0) then
				local name = HSList[1]:GetName();	
				--local score = HSList[1]:GetScore();	
				self:settext(string.upper(name));
			else
				self:settext(" ");
			end;
		else
			if GAMESTATE:GetCurrentGroup() ~= "random" then
				local cur_song = GAMESTATE:GetCurrentCourse();
				local crs = GAMESTATE:GetCurrentCourse();
				if crs == nil then return ""; end;

				local trails = crs:GetAllTrails();
				local cur_steps = trails[1];

				local HSList = PROFILEMAN:GetMachineProfile():GetHighScoreList(cur_song,cur_steps):GetHighScores();
				--local HSList = PROFILEMAN:GetProfile(PLAYER_1):GetHighScoreList(cur_song,cur_steps):GetHighScores();
				if (#HSList ~= 0) then
					local name = HSList[1]:GetName();	
					--local score = HSList[1]:GetScore();	
					self:settext(string.upper(name));
				else
					self:settext(" ");
				end;
			else
				self:settext(" ");
			end;	
		end;
	end;
	CurrentStepsP1ChangedMessageCommand=cmd(visible,GAMESTATE:GetCurrentGroup() ~= "quest zone" and GAMESTATE:GetCurrentGroup() ~= "random";playcommand,'RefreshText');
	OffCommand=cmd(stoptweening;visible,false);
};

end;

--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--Player 2 - Back
if GAMESTATE:IsSideJoined('PlayerNumber_P2') then

adjustp2x = 35;

--Player 2 - Glow
for i=1,2 do
	xRightP2 = cx+185;
	xLeftP2 = cx+60;
	t[#t+1] = LoadActor("2p_arrow.png")..{
		InitCommand=function(self)
			if i==1 then 
				self:x(xRightP2-adjustp2x); 
			elseif i==2 then 
				self:x(xLeftP2-adjustp2x); 
				self:rotationz(180); 
			end;
		end;
		OnCommand=function(self)
			if i == 1 then
				(cmd(stoptweening;blend,'BlendMode_Add';diffusealpha,0;zoom,.75;bob; effectmagnitude, 2,0,0; effectperiod, .9))(self);
			elseif i==2 then 
				(cmd(stoptweening;blend,'BlendMode_Add';diffusealpha,0;zoom,.75;bob; effectmagnitude, -2,0,0; effectperiod, .9))(self);
			end;
		end;
		--
		StartSelectingStepsMessageCommand=cmd(stoptweening;diffusealpha,0;sleep,.2;decelerate,.2;diffusealpha,1;queuecommand,'Loop');
		ChangeStepsMessageCommand=function(self, params)
			--Para cambios generales de steps
			if params.Player == PLAYER_2 then
				if params.Direction == -1 then	--der
					if i==1 then 
						(cmd(stoptweening;diffusealpha,1;linear,.5;x,xRightP2-adjustp2x; queuecommand, 'Loop'))(self);
					elseif i==2 then 
						(cmd(stoptweening;diffusealpha,1;linear,.05;x,cx+45-adjustp2x;decelerate,.05;decelerate,.4;x,xLeftP2-adjustp2x; queuecommand, 'Loop'))(self);
					end;
				elseif params.Direction == 1 then	--izq
					if i==2 then 
						(cmd(stoptweening;diffusealpha,1;linear,.5;x,xLeftP2-adjustp2x; queuecommand, 'Loop'))(self);
					elseif i==1 then 
						(cmd(stoptweening;diffusealpha,1;linear,.05;x,cx+200-adjustp2x;decelerate,.05;decelerate,.4;x,xRightP2-adjustp2x; queuecommand, 'Loop'))(self);
					end;
				end;
			end;
		end;

		--[[LoopCommand=function(self)
			if i==1 then 
				(cmd(stoptweening;x,xRightP2-adjustp2x;decelerate,.4;x,xRightP2-adjustp2x-3;decelerate,.4;x,xRightP2-adjustp2x; queuecommand, 'Loop'))(self);
			elseif i==2 then 
				(cmd(stoptweening;x,xLeftP2-adjustp2x;decelerate,.4;x,xLeftP2-adjustp2x+3;decelerate,.4;x,xLeftP2-adjustp2x; queuecommand, 'Loop'))(self);
			end;
		end;]]
		LoopCommand=cmd();
		--
		SongUnchosenMessageCommand=cmd(stoptweening;diffusealpha,1;decelerate,.1;diffusealpha,0);
		--
		StepsChosenMessageCommand=function(self,params)
			if params.Player == PLAYER_2 then 
				--self:stoptweening();
				--self:diffusealpha(1);
				self:playcommand('Loop'); 
			end;
		end;

		--FastCommand=cmd(stoptweening;diffusealpha,1;decelerate,.1;decelerate,.1);
		StepsPreselectedCancelledMessageCommand=function(self,params)
			self:diffusealpha(1);
			if params.Player == PLAYER_2 then self:playcommand('Loop'); end;
		end;
		--
		OffCommand=function(self)
			if i==1 then 
				(cmd(stoptweening;decelerate,.1;diffusealpha,0;x,xRightP2-adjustp2x-5))(self);
			elseif i==2 then 
				(cmd(stoptweening;decelerate,.1;diffusealpha,0;x,xLeftP2-adjustp2x+5))(self);
			end;
		end;
	};		
end;

t[#t+1] = LoadActor("diff_ball_bg")..{
	InitCommand=cmd(x,cx+122-adjustp2x;zoom,.75;diffusealpha,0);
	SetBallDiffCommand=cmd(stoptweening;);
	StartSelectingStepsMessageCommand=cmd(stoptweening;diffusealpha,0;decelerate,.3;diffusealpha,1);
	--CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'SetBallDiff');
	OffCommand=cmd(stoptweening;diffusealpha,1;sleep,.4;decelerate,.4;diffusealpha,0);
} 

t[#t+1] = LoadActor("diff_balls")..{
	InitCommand=cmd(x,cx+123-adjustp2x;zoom,.75;diffusealpha,0);
	SetBallDiffCommand=cmd(stoptweening;animate,0;pause;setstate,GetDiffNumberBalls('PlayerNumber_P2'));
	StartSelectingStepsMessageCommand=cmd(stoptweening;diffusealpha,0;playcommand,'SetBallDiff';decelerate,.3;diffusealpha,1);
	CurrentStepsP2ChangedMessageCommand=cmd(stoptweening;sleep,.1;queuecommand,'SetBallDiff');
	OffCommand=cmd(stoptweening;diffusealpha,1;sleep,.4;decelerate,.4;diffusealpha,0);
};
t[#t+1] = LoadActor("efx1")..{
	OnCommand=cmd(stoptweening;x,cx+123-adjustp2x;y,5;zoom,.75;diffusealpha,0;blend,'BlendMode_Add';);
	ChangeStepsMessageCommand=function(self,params)
		if params.Player == "PlayerNumber_P2" and params.Direction == -1 then
			(cmd(stoptweening;visible,GAMESTATE:GetCurrentGroup() ~= "quest zone" and GAMESTATE:GetCurrentGroup() ~= "random";diffusealpha,.7;fadeleft,.5;decelerate,.3;fadeleft,0;diffusealpha,0))(self);
		elseif params.Player == "PlayerNumber_P2" and params.Direction == 1 then
			(cmd(stoptweening;visible,GAMESTATE:GetCurrentGroup() ~= "quest zone" and GAMESTATE:GetCurrentGroup() ~= "random";diffusealpha,.7;faderight,.5;decelerate,.3;faderight,0;diffusealpha,0))(self);
		end;
	end;
};

t[#t+1] = LoadActor("efx3")..{
	OnCommand=cmd(x,cx+123-adjustp2x;zoom,.7;diffusealpha,0;blend,'BlendMode_Add';diffusealpha,0);
	GoBackSelectingSongMessageCommand=cmd(stoptweening;diffusealpha,0.7;decelerate,.2;diffusealpha,0);
};

t[#t+1] = LoadActor("efx2")..{
	OnCommand=cmd(x,cx+123-adjustp2x;zoom,.75;diffusealpha,0;blend,'BlendMode_Add';diffusealpha,0;rotationx,180);
	StartSelectingStepsMessageCommand=cmd(queuecommand,'Repeat');
	RepeatCommand=cmd(stoptweening;diffusealpha,0;rotationz,-180;linear,.9;addrotationz,-70;diffusealpha,.1;linear,.6;addrotationz,-90;diffusealpha,0;linear,.5;addrotationz,-20;sleep,1;queuecommand,'Repeat');
};

t[#t+1] = LoadActor("efx2")..{
	OnCommand=cmd(x,cx+123-adjustp2x;zoom,.75;diffusealpha,0;blend,'BlendMode_Add';diffusealpha,0);
	StepsChosenMessageCommand=cmd(stoptweening;diffuse,0.5,0.5,1,0;diffusealpha,0;rotationz,-45;linear,.2;addrotationz,-70;diffusealpha,.5;linear,.5;addrotationz,-180;diffusealpha,0);
};

-- SingleNumbers (orange)
t[#t+1] = LoadFont("numbersorangebig")..{
	OnCommand=cmd(stoptweening;playcommand,'Set';x,cx+121-adjustp2x;y,23;zoom,0.75;diffusealpha,0);
	SetCommand=cmd(ifsteps,'PlayerNumber_P2',0;settextbymeter,'PlayerNumber_P2');
	StartSelectingStepsMessageCommand=cmd(stoptweening;ifsteps,'PlayerNumber_P2',0;diffusealpha,0;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;diffusealpha,0);
	CurrentStepsP2ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'Set');
	OffCommand=cmd(stoptweening;decelerate,.1;diffuse,.9,.9,.9,1;sleep,.3;decelerate,.1;diffusealpha,0);
};
-- DoubleNumbers( green )
t[#t+1] = LoadFont("numberspurplebig")..{
	OnCommand=cmd(stoptweening;playcommand,'Set';x,cx+121-adjustp2x;y,23;zoom,0.75;diffusealpha,0);
	SetCommand=cmd(ifsteps,'PlayerNumber_P2',1;settextbymeter,'PlayerNumber_P2');
	StartSelectingStepsMessageCommand=cmd(stoptweening;ifsteps,'PlayerNumber_P2',1;diffusealpha,0;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;diffusealpha,0);
	CurrentStepsP2ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'Set');
	OffCommand=cmd(stoptweening;decelerate,.1;diffuse,.9,.9,.9,1;sleep,.3;decelerate,.1;diffusealpha,0);
};
-- SinglePerformanceNumbers( green )
t[#t+1] = LoadFont("numbersgreenbig")..{
	OnCommand=cmd(stoptweening;playcommand,'Set';x,cx+121-adjustp2x;y,23;zoom,0.75;diffusealpha,0);
	SetCommand=cmd(ifsteps,'PlayerNumber_P2',2;settextbymeter,'PlayerNumber_P2');
	StartSelectingStepsMessageCommand=cmd(stoptweening;ifsteps,'PlayerNumber_P2',2;diffusealpha,0;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;diffusealpha,0);
	CurrentStepsP2ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'Set');
	OffCommand=cmd(stoptweening;decelerate,.1;diffuse,.9,.9,.9,1;sleep,.3;decelerate,.1;diffusealpha,0);
};
-- DoublePerformanceNumbers( blue )
t[#t+1] = LoadFont("numbersbluebig")..{
	OnCommand=cmd(stoptweening;playcommand,'Set';x,cx+121-adjustp2x;y,23;zoom,0.75;diffusealpha,0);
	SetCommand=cmd(ifsteps,'PlayerNumber_P2',4;settextbymeter,'PlayerNumber_P2');
	StartSelectingStepsMessageCommand=cmd(stoptweening;ifsteps,'PlayerNumber_P2',4;diffusealpha,0;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;diffusealpha,0);
	CurrentStepsP2ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'Set');
	OffCommand=cmd(stoptweening;decelerate,.1;diffuse,.9,.9,.9,1;sleep,.3;decelerate,.1;diffusealpha,0);
};

-- DoublePerformanceNumbers( yellow )
t[#t+1] = LoadFont("numbersyellowbig")..{
	OnCommand=cmd(stoptweening;playcommand,'Set';x,cx+121-adjustp2x;y,22;zoom,0.75;diffusealpha,0);
	SetCommand=cmd(ifsteps,'PlayerNumber_P2',5;settextbymeter,'PlayerNumber_P2');
	StartSelectingStepsMessageCommand=cmd(stoptweening;ifsteps,'PlayerNumber_P2',5;diffusealpha,0;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;diffusealpha,0);
	CurrentStepsP2ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'Set');
	OffCommand=cmd(stoptweening;decelerate,.1;diffuse,.9,.9,.9,1;sleep,.3;decelerate,.1;diffusealpha,0);
};

-- QuestZoneNumbers( diffuse )
t[#t+1] = LoadFont("numbersdiffusebig")..{
	OnCommand=cmd(stoptweening;playcommand,'Set';x,cx+121-adjustp2x;y,23;zoom,0.75;diffusealpha,0);
	SetCommand=cmd(ifsteps,'PlayerNumber_P2',4;settextbymeter,'PlayerNumber_P2');
	StartSelectingStepsMessageCommand=cmd(stoptweening;ifsteps,'PlayerNumber_P2',4;diffusealpha,0;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;diffusealpha,0);
	CurrentStepsP2ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'Set');
	OffCommand=cmd(stoptweening;decelerate,.1;diffuse,.9,.9,.9,1;sleep,.3;decelerate,.1;diffusealpha,0);
};

t[#t+1] = LoadActor("steps 1x4")..{
	InitCommand=cmd(animate,0);
	OnCommand=cmd(x,cx+121-adjustp2x;zoom,.75;diffusealpha,0;blend,'BlendMode_Add';diffusealpha,0);
	CheckCommand=function(self) 
		self:visible(GAMESTATE:GetCurrentGroup() == "quest zone");		
	end;
	StepPositionMessageCommand=function(self, params)
		if params.Valid == 0 then self:visible(false); return; end;
		self:setstate(params.Step);
	end;
	OnCommand=cmd(stoptweening;x,cx+121-adjustp2x;y,-43;zoom,0.75;diffusealpha,0;playcommand,'Check');
	StartSelectingStepsMessageCommand=cmd(stoptweening;diffusealpha,0;accelerate,.1;diffusealpha,1;playcommand,'Check');
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;diffusealpha,0;playcommand,'Check');
	CurrentStepsP2ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;playcommand,'Check');
	OffCommand=cmd(stoptweening;decelerate,.1;diffuse,.9,.9,.9,1;sleep,.3;decelerate,.1;diffusealpha,0;playcommand,'Check');
};

t[#t+1] = LoadActor("stepcomplete") .. {
	InitCommand=cmd(animate,0);
	--OnCommand=cmd(x,cx+121-adjustp2x;zoom,.75;diffusealpha,0;blend,'BlendMode_Add';diffusealpha,0);
	CheckCommand=function(self) 
		self:visible(GAMESTATE:GetCurrentGroup() == "quest zone");		
	end;
	StepPositionMessageCommand=function(self, params)
		if params.Valid == 0 then self:visible(false); return; end;

		if params.Cleared then
			self:visible(true);
			self:diffusealpha(1);
		else
			self:visible(false);
			self:diffusealpha(0);
		end;
	end;
	OnCommand=cmd(stoptweening;x,cx+121-adjustp2x;y,32;zoom,0.75;diffusealpha,0;sleep,0.05;queuecommand,'Check');
};

t[#t+1] = LoadActor("stepsstate") .. {
	CheckCommand=function(self) 
		local b = GetIfUnplayedStepDiff('PlayerNumber_P2');
		self:visible(GAMESTATE:GetCurrentGroup() ~= "quest zone" and GAMESTATE:GetCurrentGroup() ~= "random");
		if b > 0 then
			self:setstate(b-1);
		else
			self:visible(false);
		end
	end;
	InitCommand=cmd(animate,0;pause;);
	OnCommand=cmd(stoptweening;x,cx+121-adjustp2x;y,-43;zoom,0.75;diffusealpha,0;playcommand,'Check');
	--SetCommand=cmd();
	StartSelectingStepsMessageCommand=cmd(stoptweening;diffusealpha,0;accelerate,.1;diffusealpha,1;playcommand,'Check');
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;diffusealpha,0;playcommand,'Check');
	CurrentStepsP2ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;playcommand,'Check');
	OffCommand=cmd(stoptweening;decelerate,.1;diffuse,.9,.9,.9,1;sleep,.3;decelerate,.1;diffusealpha,0;playcommand,'Check');
};

t[#t+1] = LoadActor("records_normal")..{
	OnCommand=cmd(stoptweening;x,adjustp2x+cx+260-70;y,5;zoom,.75;diffusealpha,0);
	--SetCommand=cmd();
	StartSelectingStepsMessageCommand=cmd(stoptweening;visible,GAMESTATE:GetCurrentGroup() ~= "quest zone";diffusealpha,0;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;diffusealpha,0);
	CurrentStepsP2ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1);
	OffCommand=cmd(stoptweening;decelerate,.1;diffuse,.9,.9,.9,1;sleep,.3;decelerate,.1;diffusealpha,0);
};

local xHelp = adjustp2x+cx+260-70;

t[#t+1] = LoadActor("effect_records")..{
	InitCommand=cmd(blend,'BlendMode_Add');
	OnCommand=cmd(stoptweening;x,xHelp;y,4;zoom,.75;diffusealpha,0);
	--SetCommand=cmd();
	--StartSelectingStepsMessageCommand=cmd(stoptweening;diffusealpha,0;accelerate,.1;diffusealpha,1);
	ChangeStepsMessageCommand=function(self,params)
		if params.Player == "PlayerNumber_P2" and params.Direction == -1 then
			(cmd(stoptweening;visible,GAMESTATE:GetCurrentGroup() ~= "quest zone" and GAMESTATE:GetCurrentGroup() ~= "random";x,xHelp-10;diffusealpha,.7;x,xHelp+10;fadeleft,.5;decelerate,.3;x,xHelp-10;fadeleft,0;diffusealpha,0))(self);
		elseif params.Player == "PlayerNumber_P2" and params.Direction == 1 then
			(cmd(stoptweening;visible,GAMESTATE:GetCurrentGroup() ~= "quest zone" and GAMESTATE:GetCurrentGroup() ~= "random";x,xHelp+10;diffusealpha,.7;x,xHelp-10;faderight,.5;decelerate,.3;x,xHelp+10;faderight,0;diffusealpha,0))(self);
		end;
	end;
	--[[NextStepMessageCommand=function(self,params)
		if params.Player == "PlayerNumber_P2" then
			
		end;
	end;]]
	--CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1);
	OffCommand=cmd(stoptweening;diffusealpha,0);
};

-- HighScore Para el player 2
t[#t+1] = LoadFont("_karnivore lite 20px")..{
	InitCommand=cmd(stoptweening;x,adjustp2x+cx+305-160;y,-31;settext,"";zoom,.75;horizalign,'HorizAlign_Left');
	RefreshTextCommand=function(self)
		if GAMESTATE:GetCurrentGroup() ~= "music train"  and GAMESTATE:GetCurrentGroup() ~= "random" then
			local cur_song = GAMESTATE:GetCurrentSong();
			local cur_steps = GAMESTATE:GetCurrentSteps(PLAYER_2);
			if cur_steps == nil then return end;
			--local HSList = PROFILEMAN:GetMachineProfile():GetHighScoreList(cur_song,cur_steps):GetHighScores();
			local prf = PROFILEMAN:GetProfile(PLAYER_2);
			if not prf:IsMachine() then
				local HSList = prf:GetHighScoreList(cur_song,cur_steps):GetHighScores();
				if (#HSList ~= 0) then
					local name = HSList[1]:GetName();	
					local score = HSList[1]:GetScore();	
					self:settext(comma_value(score));
				else
					self:settext(" ");
				end;
			else
				self:settext(" ");
			end;
		else
			local cur_song = GAMESTATE:GetCurrentCourse();
			local crs = GAMESTATE:GetCurrentCourse();
			if crs == nil then return ""; end;

			local trails = crs:GetAllTrails();
			local cur_steps = trails[1];

			--local HSList = PROFILEMAN:GetMachineProfile():GetHighScoreList(cur_song,cur_steps):GetHighScores();
			local prf = PROFILEMAN:GetProfile(PLAYER_2);
			if not prf:IsMachine() then
				local HSList = prf:GetHighScoreList(cur_song,cur_steps):GetHighScores();
				if (#HSList ~= 0) then
					local name = HSList[1]:GetName();	
					local score = HSList[1]:GetScore();	
					self:settext(comma_value(score));
				else
					self:settext(" ");
				end;
			else
				self:settext(" ");
			end;
		end
	end;
	CurrentStepsP2ChangedMessageCommand=cmd(visible,GAMESTATE:GetCurrentGroup() ~= "quest zone" and GAMESTATE:GetCurrentGroup() ~= "random";playcommand,'RefreshText');
	OffCommand=cmd(stoptweening;visible,false);
};

-- HighScore Para el player 2 (online) (worldbest)
t[#t+1] = LoadFont("_karnivore lite 20px")..{
	InitCommand=cmd(stoptweening;x,adjustp2x+cx+305-160;y,9;settext,"";zoom,.75;horizalign,'HorizAlign_Left');
	RefreshTextCommand=function(self)
		if GAMESTATE:GetCurrentGroup() ~= "music train" then
			self:settext(" ");
			--se debe enviar 1 sola vez
			--and IsSMOnlineLoggedIn(PLAYER_2)
			if IsNetConnected() and IsSMOnlineLoggedIn(PLAYER_2) then
				AskSongStepScore(PLAYER_2);
			end
		end;
	end;
	UpdateRankStepScoreP2MessageCommand=function(self)
		if GetMaxStepScore(PLAYER_2) ~= 0 then
			self:settext( comma_value(GetMaxStepScore(PLAYER_2)) );
		end
	end;
	CurrentStepsP2ChangedMessageCommand=cmd(visible,GAMESTATE:GetCurrentGroup() ~= "quest zone" and GAMESTATE:GetCurrentGroup() ~= "random";playcommand,'RefreshText');
	OffCommand=cmd(stoptweening;visible,false);
};

t[#t+1] = LoadFont("_youth 20px")..{
	InitCommand=cmd(stoptweening;x,adjustp2x+cx+305-160;y,-3;settext,"";zoom,.65;horizalign,'HorizAlign_Left');
	RefreshTextCommand=function(self)
		self:settext(" ");
		if GAMESTATE:GetCurrentGroup() ~= "music train" then
			self:settext(" ");
			--se debe enviar 1 sola vez
			--and IsSMOnlineLoggedIn(PLAYER_2)
			--if IsNetConnected() then
			--	AskSongStepScore(PLAYER_2);
			--end
		end;
	end;
	UpdateRankStepScoreP2MessageCommand=function(self)
		if GetMaxStepScore(PLAYER_2) ~= 0 then
			self:settext( string.upper(tostring(Get1StStepScoreName(PLAYER_2))) );
		end
	end;
	CurrentStepsP2ChangedMessageCommand=cmd(visible,GAMESTATE:GetCurrentGroup() ~= "quest zone" and GAMESTATE:GetCurrentGroup() ~= "random";playcommand,'RefreshText');
	OffCommand=cmd(stoptweening;visible,false);
};

-- HighScore Para el player 1 (machine) (worldbest)
t[#t+1] = LoadFont("_karnivore lite 20px")..{
	InitCommand=cmd(stoptweening;x,adjustp2x+cx+305-160;y,49;settext,"";zoom,.75;horizalign,'HorizAlign_Left');
	RefreshTextCommand=function(self)
		if GAMESTATE:GetCurrentGroup() ~= "music train"  and GAMESTATE:GetCurrentGroup() ~= "random" then
			local cur_song = GAMESTATE:GetCurrentSong();
			local cur_steps = GAMESTATE:GetCurrentSteps(PLAYER_2);
			if cur_steps == nil then return end;
			local HSList = PROFILEMAN:GetMachineProfile():GetHighScoreList(cur_song,cur_steps):GetHighScores();
			--local HSList = PROFILEMAN:GetProfile(PLAYER_2):GetHighScoreList(cur_song,cur_steps):GetHighScores();
			if (#HSList ~= 0) then
				--local name = HSList[1]:GetName();	
				local score = HSList[1]:GetScore();	
				self:settext(comma_value(score));
			else
				self:settext(" ");
			end;
		else
			local cur_song = GAMESTATE:GetCurrentCourse();
			local crs = GAMESTATE:GetCurrentCourse();
			if crs == nil then return ""; end;

			local trails = crs:GetAllTrails();
			local cur_steps = trails[1];

			local HSList = PROFILEMAN:GetMachineProfile():GetHighScoreList(cur_song,cur_steps):GetHighScores();
			--local HSList = PROFILEMAN:GetProfile(PLAYER_2):GetHighScoreList(cur_song,cur_steps):GetHighScores();
			if (#HSList ~= 0) then
				--local name = HSList[1]:GetName();	
				local score = HSList[1]:GetScore();	
				self:settext(comma_value(score));
			else
				self:settext(" ");
			end;
		end
	end;
	CurrentStepsP2ChangedMessageCommand=cmd(visible,GAMESTATE:GetCurrentGroup() ~= "quest zone" and GAMESTATE:GetCurrentGroup() ~= "random";playcommand,'RefreshText');
	OffCommand=cmd(stoptweening;visible,false);
};

t[#t+1] = LoadFont("_youth 20px")..{
	InitCommand=cmd(stoptweening;x,adjustp2x+cx+305-160;y,37;settext,"";zoom,.65;horizalign,'HorizAlign_Left');
	RefreshTextCommand=function(self)
		if GAMESTATE:GetCurrentGroup() ~= "music train"  and GAMESTATE:GetCurrentGroup() ~= "random" then
			local cur_song = GAMESTATE:GetCurrentSong();
			local cur_steps = GAMESTATE:GetCurrentSteps(PLAYER_2);
			if cur_steps == nil then return end;
			local HSList = PROFILEMAN:GetMachineProfile():GetHighScoreList(cur_song,cur_steps):GetHighScores();
			--local HSList = PROFILEMAN:GetProfile(PLAYER_2):GetHighScoreList(cur_song,cur_steps):GetHighScores();
			if (#HSList ~= 0) then
				local name = HSList[1]:GetName();	
				--local score = HSList[1]:GetScore();	
				self:settext(string.upper(name));
			else
				self:settext(" ");
			end;
		else
			local cur_song = GAMESTATE:GetCurrentCourse();
			local crs = GAMESTATE:GetCurrentCourse();
			if crs == nil then return ""; end;

			local trails = crs:GetAllTrails();
			local cur_steps = trails[1];
			
			local HSList = PROFILEMAN:GetMachineProfile():GetHighScoreList(cur_song,cur_steps):GetHighScores();
			--local HSList = PROFILEMAN:GetProfile(PLAYER_2):GetHighScoreList(cur_song,cur_steps):GetHighScores();
			if (#HSList ~= 0) then
				local name = HSList[1]:GetName();	
				--local score = HSList[1]:GetScore();	
				self:settext(string.upper(name));
			else
				self:settext(" ");
			end;
		end
	end;
	CurrentStepsP2ChangedMessageCommand=cmd(visible,GAMESTATE:GetCurrentGroup() ~= "quest zone" and GAMESTATE:GetCurrentGroup() ~= "random";playcommand,'RefreshText');
	OffCommand=cmd(stoptweening;visible,false);
};

end;

--////////////////////////////////////////////////////////////////////////////////////////////
--////////////////////////////////////////////////////////////////////////////////////////////
--Ready button for both players
for i=1,4 do
	t[#t+1] = LoadActor("ready")..{
	InitCommand=function(self)
		if i==1 then self:x(cx-123+adjustp1x);	--der
		elseif i==2 then self:x(cx-123+adjustp1x); self:blend('BlendMode_Add');	--der
		elseif i==3 then self:x(cx+122-adjustp1x);	--izq
		elseif i==4 then self:x(cx+122-adjustp1x); self:blend('BlendMode_Add');	--izq
		end;
	(cmd(y,48;zoom,0.75;diffusealpha,0))(self);
	end;
	StepsChosenMessageCommand=function(self,params)
		self:stoptweening();
		if params.Player == PLAYER_1 then
			if i==1 then self:diffusealpha(1); end;
			if i==2 then (cmd(diffusealpha,1;diffuseshift;effectcolor2,color("1,1,1,.3");effectcolor1,color("1,1,1,0");effectperiod,.2))(self); end;
		elseif params.Player == PLAYER_2 then
			if i==3 then self:diffusealpha(1); end;
			if i==4 then (cmd(diffusealpha,1;diffuseshift;effectcolor2,color("1,1,1,.3");effectcolor1,color("1,1,1,0");effectperiod,.2))(self); end;
		end;
	end;
	StepsPreselectedCancelledMessageCommand=function(self,params)
		self:stoptweening();
		if params.Player == PLAYER_1 then
			if i==1 or i==2 then (cmd(stopeffect;diffusealpha,0))(self); end;
		elseif params.Player == PLAYER_2 then
			if i==3 or i==4 then (cmd(stopeffect;diffusealpha,0))(self); end;
		end;
	end;
	GoBackSelectingSongMessageCommand=cmd(stopeffect;diffusealpha,0);
	OffCommand=cmd(stoptweening;diffusealpha,0);
}
end;

return t;

--code by ??? - ( con modificaciones por v1toko )