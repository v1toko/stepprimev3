local t = Def.ActorFrame {	
	ChangeStepsMessageCommand=function(self, params)
		if params.Direction == 1 then	--der
			MESSAGEMAN:Broadcast("NextStep");
		elseif params.Direction == -1 then	--izq
			MESSAGEMAN:Broadcast("PreviousStep");
		end;
	end;
}

--comandos iniciales
local common = cmd(stoptweening;diffusealpha,0;linear,.1;diffusealpha,.8;sleep,.1;diffusealpha,1);

--------------------------------------------------------------------------------------------------
--UpLeft--
t[#t+1] = Def.ActorFrame {
	InitCommand=cmd(x,cx-85;y,cy-85;zoom, 0.65);
	OnCommand=cmd(stoptweening;linear,.1;x,4;y,4);
	SongUnchosenMessageCommand=cmd(stoptweening;decelerate,.05;x,2;y,2;decelerate,.3;x,4;y,4);
	--GoBackSelectingGroupMessageCommand=cmd(playcommand,'SongUnchosen');
	--GoBackSelectingSongMessageCommand=cmd(playcommand,'SongUnchosen');
	--StartSelectingStepsMessageCommand=cmd(playcommand,'SongUnchosen');
	--StartSelectingSongMessageCommand =cmd(playcommand,'SongUnchosen');
	OffCommand=cmd(stoptweening;x,4;y,4;decelerate,.1;x,40;y,40;accelerate,.1;x,4;y,4);
	children = {
		--gray arrow
		LoadActor("arrows")..{
			OnCommand=function(self)
			if( GAMESTATE:IsBasicMode() ) then (common)(self); else (cmd(stoptweening;diffusealpha,0))(self); end; end;
			InitCommand=cmd(animate,false;setstate,4;pause;horizalign,'HorizAlign_Left';vertalign,'VertAlign_Top');
			GoFullModeMessageCommand=cmd(stoptweening;diffusealpha,0);
			GoBackSelectingGroupMessageCommand=cmd(stoptweening;diffusealpha,0;linear,.2;diffusealpha,.8);
			StartSelectingSongMessageCommand =cmd(stoptweening;diffusealpha,.8;sleep,.2;linear,0;diffusealpha,0);
			--OffCommand=cmd(stoptweening;diffusealpha,0);
		};

		LoadActor("arrows")..{
			OnCommand=function(self)
			if not( GAMESTATE:IsBasicMode() ) then (common)(self); else (cmd(stoptweening;diffusealpha,0))(self); end; end;
			GoBackSelectingSongMessageCommand=function(self)
			if not ( GAMESTATE:IsBasicMode() ) then return; else (cmd(stoptweening;diffusealpha,1;linear,.2;diffusealpha,0))(self); end; end;
			StartSelectingStepsMessageCommand=function(self)
			if not ( GAMESTATE:IsBasicMode() ) then return; else (cmd(stoptweening;diffusealpha,0;sleep,.2;decelerate,.2;diffusealpha,1))(self); end; end;
			InitCommand=cmd(animate,false;setstate,0;pause;horizalign,'HorizAlign_Left';vertalign,'VertAlign_Top');
			GoFullModeMessageCommand=cmd(stoptweening;diffusealpha,1);
			GoBackSelectingGroupMessageCommand=cmd(stoptweening;diffusealpha,1;linear,.2;diffusealpha,0);
			StartSelectingSongMessageCommand =cmd(stoptweening;diffusealpha,0;sleep,.2;decelerate,.2;diffusealpha,1);
			--OffCommand=cmd(stoptweening;linear,.2;diffusealpha,0);
		};

		LoadActor("arrows")..{
			InitCommand=cmd(animate,false;setstate,0;pause;horizalign,'HorizAlign_Left';vertalign,'VertAlign_Top');
			OnCommand=cmd(stoptweening;blend,'BlendMode_Add';diffusealpha,0;sleep,.3;queuecommand,'SongUnchosen');
			SongUnchosenMessageCommand=cmd(stoptweening;fadebottom,0;faderight,0;sleep,0;diffusealpha,1;sleep,.25;fadebottom,0;faderight,0;accelerate,.2;diffusealpha,0);
			StartSelectingStepsMessageCommand=cmd(playcommand,'SongUnchosen');
			GoBackSelectingGroupMessageCommand=cmd(playcommand,'SongUnchosen');
			StartSelectingSongMessageCommand =cmd(playcommand,'SongUnchosen');
			--OffCommand=cmd(stoptweening;diffusealpha,0);
		};

		LoadActor("arrows")..{
			InitCommand=cmd(animate,false;setstate,4;pause;diffusecolor,1,1,1,1;horizalign,'HorizAlign_Left';vertalign,'VertAlign_Top';blend,'BlendMode_Add';diffuseshift;effectcolor1,1,1,1,0;effectcolor2,1,1,1,.5;effectperiod,2);
			--OffCommand=cmd(stoptweening;diffusealpha,0;zoom,0);
		};
	};
}

--------------------------------------------------------------------------------------------------
--UpRight--
t[#t+1] = Def.ActorFrame {
	InitCommand=cmd(x,cx+85;y,cy-85;zoom, 0.65);
	OnCommand=cmd(stoptweening;linear,.1;x,SCREEN_WIDTH+1-4;y,4);
	SongUnchosenMessageCommand=cmd(stoptweening;decelerate,.05;x,SCREEN_WIDTH+1;y,0;decelerate,.3;x,SCREEN_WIDTH+1-4;y,4);
	--StartSelectingStepsMessageCommand=cmd(playcommand,'SongUnchosen');
	--GoBackSelectingGroupMessageCommand=cmd(playcommand,'SongUnchosen');
	--GoBackSelectingSongMessageCommand=cmd(playcommand,'SongUnchosen');
	--StartSelectingSongMessageCommand =cmd(playcommand,'SongUnchosen');
	OffCommand=cmd(stoptweening;x,SCREEN_WIDTH+1-4;y,4;decelerate,.1;x,SCREEN_WIDTH+1-40;y,40;accelerate,.1;x,SCREEN_WIDTH+1-4;y,4);
	children = {
		--gray
		LoadActor("arrows")..{
			OnCommand=function(self)
			if( GAMESTATE:IsBasicMode() ) then (common)(self); else (cmd(stoptweening;diffusealpha,0))(self); end; end;
			InitCommand=cmd(animate,false;setstate,5;pause;horizalign,'HorizAlign_Right';vertalign,'VertAlign_Top');
			GoFullModeMessageCommand=cmd(stoptweening;diffusealpha,0);
			GoBackSelectingGroupMessageCommand=cmd(stoptweening;diffusealpha,0;linear,.2;diffusealpha,.8);
			StartSelectingSongMessageCommand =cmd(stoptweening;diffusealpha,.8;sleep,.2;linear,0;diffusealpha,0);
			--OffCommand=cmd(stoptweening;diffusealpha,0);
			
		};
		LoadActor("arrows")..{
			OnCommand=function(self)
			if not ( GAMESTATE:IsBasicMode() ) then (common)(self); else (cmd(stoptweening;diffusealpha,0))(self); end; end;
			GoBackSelectingSongMessageCommand=function(self)
			if not ( GAMESTATE:IsBasicMode() ) then return; else (cmd(stoptweening;diffusealpha,1;linear,.2;diffusealpha,0))(self); end; end;
			StartSelectingStepsMessageCommand=function(self)
			if not ( GAMESTATE:IsBasicMode() ) then return; else (cmd(stoptweening;diffusealpha,0;sleep,.2;decelerate,.2;diffusealpha,1))(self); end; end;
			InitCommand=cmd(animate,false;setstate,1;pause;horizalign,'HorizAlign_Right';vertalign,'VertAlign_Top');
			GoFullModeMessageCommand=cmd(stoptweening;diffusealpha,1);
			GoBackSelectingGroupMessageCommand=cmd(stoptweening;diffusealpha,1;linear,.2;diffusealpha,0);
			StartSelectingSongMessageCommand=cmd(stoptweening;diffusealpha,0;sleep,.2;decelerate,.2;diffusealpha,1);
			--OffCommand=cmd(stoptweening;linear,.2;diffusealpha,0);
		};
		LoadActor("arrows")..{
			InitCommand=cmd(animate,false;setstate,1;pause;horizalign,'HorizAlign_Right';vertalign,'VertAlign_Top');
			OnCommand=cmd(stoptweening;blend,'BlendMode_Add';diffusealpha,0;sleep,.3;queuecommand,'SongUnchosen');
			SongUnchosenMessageCommand=cmd(stoptweening;fadebottom,0;fadeleft,0;sleep,0;diffusealpha,1;sleep,.25;fadebottom,0;fadeleft,0;accelerate,.2;fadebottom,2;fadeleft,2;diffusealpha,0);
			StartSelectingStepsMessageCommand=cmd(playcommand,'SongUnchosen');
			GoBackSelectingGroupMessageCommand=cmd(playcommand,'SongUnchosen');
			StartSelectingSongMessageCommand =cmd(playcommand,'SongUnchosen');
			OffCommand=cmd(stoptweening;diffusealpha,0);
		};
		LoadActor("arrows")..{
			InitCommand=cmd(animate,false;setstate,1;pause;diffusecolor,1,1,1,1;horizalign,'HorizAlign_Right';vertalign,'VertAlign_Top';blend,'BlendMode_Add';diffuseshift;effectcolor1,1,1,1,0;effectcolor2,1,1,1,.5;effectperiod,2);
			--OffCommand=cmd(stoptweening;diffusealpha,0;zoom,0);
		};
	};
};

--------------------------------------------------------------------------------------------------
--DownRight--
t[#t+1] = Def.ActorFrame {
	InitCommand=cmd(x,cx+85;y,cy+85;zoom, 0.65);
	OnCommand=cmd(stoptweening;linear,.1;x,SCREEN_WIDTH+1-4;y,SCREEN_HEIGHT-4);
	NextSongMessageCommand=cmd(stoptweening);
	NextStepMessageCommand=cmd(playcommand,'NextSong');
	NextGroupMessageCommand=cmd(playcommand,'NextSong');
	--StartSelectingStepsMessageCommand=cmd(playcommand,'NextSong');
	OffCommand=cmd(stoptweening;x,SCREEN_WIDTH+1-4;y,SCREEN_HEIGHT-4;decelerate,.1;x,SCREEN_WIDTH+1-40;y,SCREEN_HEIGHT-40;accelerate,.1;x,SCREEN_WIDTH+1+4;y,SCREEN_HEIGHT+4);
	children = {
		LoadActor("arrows")..{
			InitCommand=cmd(animate,false;setstate,3;pause;horizalign,'HorizAlign_Right';vertalign,'VertAlign_Bottom');
			OnCommand=common;
			--OffCommand=cmd(stoptweening;diffusealpha,0);
			OffCommand=cmd(stoptweening;);
		};
		LoadActor("arrows_fx")..{
			OnCommand=cmd(diffusealpha,0);
			InitCommand=cmd(blend, 'BlendMode_Add'; baserotationz,45; addx, -45; addy, -45; diffusealpha, 0);
			NextSongMessageCommand=cmd(stoptweening;diffusealpha,0;zoomx,1;stoptweening;diffusealpha,1;linear,.3;zoomx,0;diffusealpha,.3;linear,0;diffusealpha,0;zoomx,1);
			NextStepMessageCommand=cmd(playcommand,'NextSong');
			OffCommand=cmd(stoptweening;diffusealpha,0);
			--OffCommand=cmd(stoptweening;linear,.2;diffusealpha,0);
		};
		--[[LoadActor("arrows")..{
			InitCommand=cmd(animate,false;setstate,3;pause;horizalign,'HorizAlign_Right';vertalign,'VertAlign_Bottom');
			OnCommand=cmd(stoptweening;blend,'BlendMode_Add';diffusealpha,0;sleep,.3;queuecommand,'NextSong');
			NextSongMessageCommand=cmd(stoptweening;fadetop,0;fadeleft,0;sleep,0;diffusealpha,1;sleep,0.15;fadetop,0;fadeleft,0;accelerate,.2;fadetop,2;fadeleft,2;diffusealpha,0);
			NextStepMessageCommand=cmd(playcommand,'NextSong');
			--more langer than nextsong
			SongUnchosenMessageCommand=cmd(stoptweening;fadetop,0;fadeleft,0;sleep,0;diffusealpha,1;sleep,0.25;fadetop,0;fadeleft,0;accelerate,.2;fadetop,2;fadeleft,2;diffusealpha,0);
			StartSelectingSongMessageCommand=cmd(stoptweening;fadetop,0;fadeleft,0;sleep,0;diffusealpha,1;sleep,0.25;fadetop,0;fadeleft,0;accelerate,.2;fadetop,2;fadeleft,2;diffusealpha,0);
			OffCommand=cmd(stoptweening;diffusealpha,0);
		};
		LoadActor("arrows")..{
			InitCommand=cmd(animate,false;setstate,3;pause;horizalign,'HorizAlign_Right';vertalign,'VertAlign_Bottom';blend,'BlendMode_Add';diffuseshift;effectcolor1,1,1,1,0;effectcolor2,1,1,1,.5;effectperiod,2);
			OffCommand=cmd(stoptweening;diffusealpha,0;zoom,0);
		};]]
	};
};

--------------------------------------------------------------------------------------------------
--DownLeft--
t[#t+1] = Def.ActorFrame {
	InitCommand=cmd(x,cx-85;y,cy+85;zoom, 0.65;);
	OnCommand=cmd(stoptweening;linear,.1;x,4;y,SCREEN_HEIGHT-4);
	PreviousSongMessageCommand=cmd(stoptweening;);
	PreviousStepMessageCommand=cmd(playcommand,'PreviousSong');
	PrevGroupMessageCommand=cmd(playcommand,'PreviousSong');
	--StartSelectingStepsMessageCommand=cmd(playcommand,'PreviousSong');
	OffCommand=cmd(stoptweening;x,4;y,SCREEN_HEIGHT-4;decelerate,.1;x,40;y,SCREEN_HEIGHT-40;accelerate,.1;x,4; y,SCREEN_HEIGHT-4);
	children = {
		LoadActor("arrows")..{
			OnCommand=common;
			InitCommand=cmd(animate,false;setstate,2;pause;horizalign,'HorizAlign_Left';vertalign,'VertAlign_Bottom');
			--OffCommand=cmd(stoptweening;);
			OffCommand=cmd(stoptweening;);
		};
		LoadActor("arrows_fx")..{
			OnCommand=cmd(diffusealpha,0);
			InitCommand=cmd(blend, 'BlendMode_Add'; baserotationz,-45; addx, 45; addy, -45; diffusealpha, 0);
			PreviousSongMessageCommand=cmd(stoptweening;diffusealpha,0;zoomx,1;stoptweening;diffusealpha,1;linear,.3;zoomx,0;diffusealpha,.3;linear,0;diffusealpha,0;zoomx,1);
			PreviousStepMessageCommand=cmd(playcommand,'PreviousSong');
			OffCommand=cmd(stoptweening;diffusealpha,0);
			--OffCommand=cmd(stoptweening;linear,.2;diffusealpha,0);
		};
		--[[LoadActor("arrows")..{
			InitCommand=cmd(animate,false;setstate,2;pause;horizalign,'HorizAlign_Left';vertalign,'VertAlign_Bottom');
			OnCommand=cmd(stoptweening;blend,'BlendMode_Add';diffusealpha,0;sleep,.3;queuecommand,'PreviousSong');
			PreviousSongMessageCommand=cmd(stoptweening;fadetop,0;faderight,0;sleep,0;diffusealpha,1;sleep,0.15;fadetop,0;faderight,0;accelerate,.2;fadetop,2;faderight,2;diffusealpha,0);
			PreviousStepMessageCommand=cmd(playcommand,'PreviousSong');
			
			SongUnchosenMessageCommand=cmd(stoptweening;fadetop,0;faderight,0;sleep,0;diffusealpha,1;sleep,.25;fadetop,0;faderight,0;accelerate,.2;fadetop,2;faderight,2;diffusealpha,0);
			StartSelectingSongMessageCommand=cmd(stoptweening;fadetop,0;faderight,0;sleep,0;diffusealpha,1;sleep,.25;fadetop,0;faderight,0;accelerate,.2;fadetop,2;faderight,2;diffusealpha,0);
			OffCommand=cmd(stoptweening;diffusealpha,0);
		};
		LoadActor("arrows")..{
			InitCommand=cmd(animate,false;setstate,2;pause;horizalign,'HorizAlign_Left';vertalign,'VertAlign_Bottom';blend,'BlendMode_Add';diffuseshift;effectcolor1,1,1,1,0;effectcolor2,1,1,1,.5;effectperiod,2);
			OffCommand=cmd(stoptweening;diffusealpha,0;zoom,0);
		};]]
	};
};

return t;