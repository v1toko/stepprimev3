local delay = 4.5;

--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-- Para que guarde los datos en la profile de la maquina
--PROFILEMAN:SaveMachineProfile();

local bHasRecordP1 = false;
local bHasRecordP2 = false;

local bHasMachineP1 = false;
local bHasMachineP2 = false;
local bHasPersonalP1 = false;
local bHasPersonalP2 = false;

if GAMESTATE:IsSideJoined(PLAYER_1) then
	bHasRecordP1 = STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_1):GetPersonalHighScoreIndex() == 0
	bHasRecordP1 = bHasRecordP1 or STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_1):GetMachineHighScoreIndex() == 0

	bHasPersonalP1 = STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_1):GetPersonalHighScoreIndex() == 0
	bHasMachineP1 = STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_1):GetMachineHighScoreIndex() == 0
end

if GAMESTATE:IsSideJoined(PLAYER_2) then
	bHasRecordP2 = STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_2):GetPersonalHighScoreIndex() == 0
	bHasRecordP2 = bHasRecordP2 or STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_2):GetMachineHighScoreIndex() == 0

	bHasPersonalP2 = STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_2):GetPersonalHighScoreIndex() == 0
	bHasMachineP2 = STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_2):GetMachineHighScoreIndex() == 0
end

--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function GradetoInt(grade)
	if (grade=='Grade_Failed') then return 8; end;
	if (grade=='Grade_Tier01') then return 1; end;
	if (grade=='Grade_Tier02') then return 2; end;
	if (grade=='Grade_Tier03') then return 3; end;
	if (grade=='Grade_Tier04') then return 4; end;
	if (grade=='Grade_Tier05') then return 5; end;
	if (grade=='Grade_Tier06') then return 6; end;
	if (grade=='Grade_Tier07') then return 8; end;
	--Si no es ninguno de los anteriores entonces
	return 8;
end;
local grades = { 'SS','S','Sp','A','B','C','D','F' };

--misses qls no sirven de nada, si ya controlamos desde getgrade
function filtro(grade,misses)
	--if GradetoInt(grade) <=3 and misses ~= 0 then return grades[4]; end;
	return grades[ GradetoInt(grade) ];
end;

function GetAnnouncer()
	local p1_joined = GAMESTATE:IsSideJoined('PlayerNumber_P1');
	local p2_joined = GAMESTATE:IsSideJoined('PlayerNumber_P2');
	
	if( p1_joined and p2_joined ) then
		local g1 = GradetoInt( STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_1):GetGrade()  );
		local g2 = GradetoInt( STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_2):GetGrade()  );
	
		if( g1 <= g2 ) then
			return grades[g1];
		else
			return grades[g2];
		end;
	end;
		
	if( p1_joined and not p2_joined) then
		return filtro( STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_1):GetGrade() , STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_1):GetTapNoteScores('TapNoteScore_Miss') ); end;
	
	if( not p1_joined and p2_joined) then
		return filtro( STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_2):GetGrade() , STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_2):GetTapNoteScores('TapNoteScore_Miss') ); end;
end;

function ShowForPlayer( pn )
	return GAMESTATE:IsSideJoined(pn);
end;

--  Da prioridad a la carga de title's con el nombre
-- title.png o title.jpg. Si no existen alguno de estos, carga el background 
function GetPath()
	if GAMESTATE:GetCurrentGroup() ~= "music train" and GAMESTATE:GetCurrentGroup() ~= "random" then
		local song = GAMESTATE:GetCurrentSong();
		local songdir = song:GetSongDir().."title";
		local bgpath = song:GetBackgroundPath();
		
		if( FILEMAN:DoesFileExist(songdir..".png")  or  FILEMAN:DoesFileExist(songdir..".jpg")  ) then
			return (songdir);
		else
			if not bgpath then
				return (THEME:GetPathG("Common", "fallback background"));
			else
				return bgpath;
			end;
		end
	else
		if GAMESTATE:GetCurrentGroup() == "music train" then
			local song = GAMESTATE:GetCurrentSong();
			local songdir = song:GetSongDir().."title";
			local bgpath = song:GetBackgroundPath();
			
			if( FILEMAN:DoesFileExist(songdir..".png")  or  FILEMAN:DoesFileExist(songdir..".jpg")  ) then
				return (songdir);
			else
				if not bgpath then
					return (THEME:GetPathG("Common", "fallback background"));
				else
					return bgpath;
				end;
			end
		else
			local crs = GAMESTATE:GetCurrentCourse();
			if crs == nil then return (THEME:GetPathG("Common", "fallback background")); end;
			
			local entries = GAMESTATE:GetCurrentTrail(GAMESTATE:GetMasterPlayerNumber()):GetTrailEntry(0);
			local song = entries:GetSong();

			local songdir = song:GetSongDir().."title";
			local bgpath = song:GetBackgroundPath();
			
			if( FILEMAN:DoesFileExist(songdir..".png")  or  FILEMAN:DoesFileExist(songdir..".jpg")  ) then
				return (songdir);
			else
				if not bgpath then
					return (THEME:GetPathG("Common", "fallback background"));
				else
					return bgpath;
				end;
			end
		end;
	end;
end;

--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

local t = Def.ActorFrame {};


t[#t+1] = 	LoadActor(GetPath())..{
				InitCommand=cmd(stretchto,0,0,SCREEN_WIDTH,SCREEN_HEIGHT;diffusealpha,.4);
			};	 

t[#t+1] = 	LoadActor("_timer")..{
				InitCommand=cmd(zoom,0.7;x,SCREEN_CENTER_X;y,-10;decelerate,.15;y,15;decelerate,.05;y,15);
				OnCommand=cmd(diffusealpha,1;visible,true);
				OffCommand=cmd(x,SCREEN_CENTER_X;y,0;decelerate,.15;y,15;decelerate,.05;y,-58);
			}; 

t[#t+1] = 	LoadActor("dance_grade")..{
				OnCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y*0.25;zoom,.75);
			};

if (GAMESTATE:IsPlayerEnabled(PLAYER_1) and STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_1):GetBreakOn()) or (GAMESTATE:IsPlayerEnabled(PLAYER_2) and STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_2):GetBreakOn()) then
	t[#t+1] = 	LoadActor("star")..{
					OnCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y*0.25;zoom,.75);
				};			
end;
	
t[#t+1] = 	LoadActor("dance_grade_bg")..{
				OnCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y*0.37;zoom,.75);
			};

t[#t+1] = 	LoadFont("SongTitle") .. {
				InitCommand=cmd(stoptweening;diffuse,0.35,0.89,0.98,1;);
				OnCommand=function(self)
					local song = GAMESTATE:GetCurrentSong();
					local course = GAMESTATE:GetCurrentCourse();
					if GAMESTATE:GetCurrentGroup() == "music train" then --aunque sea un course, el random muestra el nombre de la cancion
						self:settext(course:GetDisplayFullTitle());
					else
						self:settext(song:GetDisplayMainTitle());
					end
					(cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y*0.37;zoom,0.4;maxwidth,SCREEN_CENTER_X*2.2;diffusealpha,0;linear,delay*.1;diffusealpha,1))(self)
				end;
			};

t[#t+1] = 	LoadActor("_scores") .. { 
				OnCommand=cmd(addy,5);
			};

-- referencia para los speed frames 
local mods_frames = { 
			--speed {"0.5x","1x","2x","3x","4x","5x","6x","RV","AC","DC","EW","AV"}
			["0.5x"] = 6, ["1x"] = 0, ["2x"] = 1, ["3x"] = 2, ["4x"] = 3, ["5x"] = 4, ["6x"] = 5, ["RV"] = 8, ["AC"] = 10, ["DC"] = 11, ["EW"] = 7, ["AV"] = 9,
			--display ( falta el random skin )
			["V"] = 16,["NS"] = 18, ["AP"] = 17, ["FD"] = 19, ["FL"] = 20, ["BGA_Off"] = 22, ["RNS"] = 21,
			--path {"X", "NX", "UA", "DR", "SN"}
			["X"] = 64, ["NX"] = 65, ["UA"] = 66, ["DR"] = 67, ["SN"] = 71,
			--judgement {"ej", "nj", "hj"}
			["EJ"] = 85, ["NJ"] = 86, ["HJ"] = 87, ["VJ"] = 88, ["XJ"] = 89,
			--alternate {"RS", "M", "RG", "SI", "RI", "HJ"}
			["RS"] = 82, ["M"] = 80, ["RG"] = 95, ["SI"] = 69, ["RI"] = 70,
			--rank
			["Rank"] = 144,
			--rush {"80", "90", "110", "120", "130", "140", "150"}
			["80"] = 112, ["90"] = 113, ["110"] = 114, ["120"] = 115, ["130"] = 116, ["140"] = 117, ["150"] = 118
					};

function GetStateByModName(mod)
	return mods_frames[mod];
end;

-- Columna de Códigos P1
if GAMESTATE:IsSideJoined(PLAYER_1) then
t[#t+1] = 	GetCodesColumn( PLAYER_1 )..{
	InitCommand=cmd(x,24);
	OffCommand=cmd(visible,false);
};
end;

-- Columna de Códigos P2
if GAMESTATE:IsSideJoined(PLAYER_2) then
t[#t+1] = 	GetCodesColumn( PLAYER_2 )..{
	InitCommand=cmd(x,SCREEN_RIGHT-24);
	OffCommand=cmd(visible,false);
};
end;

t[#t+1] = 	LoadActor("_grades") .. {
				InitCommand=cmd(addy,-12);
			};

t[#t+1] = 	LoadActor("_diff_FM") .. {
				InitCommand=cmd(y,SCREEN_BOTTOM+200;sleep,1);	
				OnCommand=cmd(diffusealpha,1;linear,.2;y,SCREEN_BOTTOM-105);
				OffCommand=cmd();
			};

t[#t+1] = 	LoadActor("_profile");

if bHasRecordP2 or bHasRecordP1 then
	t[#t+1] = 	LoadActor(THEME:GetPathS("","new record.wav")) .. {
				InitCommand=cmd(stop);	
				OnCommand=cmd(sleep,7;queuecommand,'Play');
				PlayCommand=function(self)									
					(cmd(play))(self);
				end;
				OffCommand=cmd(stop);
			};

	if GAMESTATE:IsSideJoined(PLAYER_1) then
		if bHasPersonalP1 then
			t[#t+1] = 	LoadActor("_diff_FM/hl_small") .. {
					InitCommand=cmd(x,202; y,SCREEN_BOTTOM-140;zoom,.75;diffusealpha,0);	
					OnCommand=cmd(sleep,1;linear,1;;diffusealpha,1;queuecommand,'Loop');
					LoopCommand=cmd(stoptweening;linear,.5;diffusealpha,0;linear,.8;diffusealpha,1;sleep,.5;queuecommand,"Loop");
					OffCommand=cmd(stoptweening;stopeffect;diffusealpha,0);
				};
		end

		--world best
		--[[if bHasMachineP1 then
			t[#t+1] = 	LoadActor("_diff_FM/hl_big") .. {
					InitCommand=cmd(x,202; y,SCREEN_BOTTOM-105;zoom,.75;diffusealpha,0);	
					OnCommand=cmd(sleep,1;linear,1;;diffusealpha,1;queuecommand,'Loop');
					LoopCommand=cmd(stoptweening;linear,.5;diffusealpha,0;linear,.8;diffusealpha,1;sleep,.5;queuecommand,"Loop");
					OffCommand=cmd(;diffusealpha,0);
				};
		end]]

		if bHasMachineP1 then
			t[#t+1] = 	LoadActor("_diff_FM/hl_big") .. {
					InitCommand=cmd(x,202; y,SCREEN_BOTTOM-65;zoom,.75;diffusealpha,0);	
					OnCommand=cmd(sleep,1;linear,1;;diffusealpha,1;queuecommand,'Loop');
					LoopCommand=cmd(stoptweening;linear,.5;diffusealpha,0;linear,.8;diffusealpha,1;sleep,.5;queuecommand,"Loop");
					OffCommand=cmd(stoptweening;stopeffect;diffusealpha,0);
				};
		end
	end

	if GAMESTATE:IsSideJoined(PLAYER_2) then
		if bHasPersonalP2 then
			t[#t+1] = 	LoadActor("_diff_FM/hl_small") .. {
					InitCommand=cmd(x,SCREEN_RIGHT-202; y,SCREEN_BOTTOM-140;zoom,.75;diffusealpha,0);	
					OnCommand=cmd(sleep,1;linear,1;;diffusealpha,1;queuecommand,'Loop');
					LoopCommand=cmd(stoptweening;linear,.5;diffusealpha,0;linear,.8;diffusealpha,1;sleep,.5;queuecommand,"Loop");
					OffCommand=cmd(stoptweening;stopeffect;diffusealpha,0);
				};
		end

		--world best
		--[[if bHasMachineP2 then
			t[#t+1] = 	LoadActor("_diff_FM/hl_big") .. {
					InitCommand=cmd(x,SCREEN_RIGHT-202; y,SCREEN_BOTTOM-105;zoom,.75;diffusealpha,0);	
					OnCommand=cmd(sleep,1;linear,1;;diffusealpha,1;queuecommand,'Loop');
					LoopCommand=cmd(stoptweening;linear,.5;diffusealpha,0;linear,.8;diffusealpha,1;sleep,.5;queuecommand,"Loop");
					OffCommand=cmd(;diffusealpha,0);
				};
		end]]

		if bHasMachineP2 then
			t[#t+1] = 	LoadActor("_diff_FM/hl_big") .. {
					InitCommand=cmd(x,SCREEN_RIGHT-202; y,SCREEN_BOTTOM-65;zoom,.75;diffusealpha,0);	
					OnCommand=cmd(sleep,1;linear,1;;diffusealpha,1;queuecommand,'Loop');
					LoopCommand=cmd(stoptweening;linear,.5;diffusealpha,0;linear,.8;diffusealpha,1;sleep,.5;queuecommand,"Loop");
					OffCommand=cmd(stoptweening;stopeffect;diffusealpha,0);
				};
		end
	end;
end

return t;