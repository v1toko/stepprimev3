local function StepsPN(pn)
return Def.ActorFrame{ 
                     OnCommand=cmd(visible,GAMESTATE:IsHumanPlayer(pn););
	PlayerJoinedMessageCommand=cmd(visible,GAMESTATE:IsHumanPlayer(pn););
	Def.ActorFrame{	--routine
		OnCommand=function(self)
			self:visible(false);
		local steps = GAMESTATE:GetCurrentSteps(pn);
		local steptype = steps:GetStepsType();
			if steptype=="StepsType_Pump_Routine" then
				self:visible(true);
			end
		end;
		SongChosenMessageCommand=cmd(playcommand,"On");
		PlayerJoinedMessageCommand=cmd(playcommand,"On");
		CurrentStepsP1ChangedMessageCommand=cmd(playcommand,"On");
		CurrentStepsP2ChangedMessageCommand=cmd(playcommand,"On");
	LoadActor("Routine") .. {
		InitCommand=cmd(zoomx,0.20;zoomy,0.4;y,20;x,40;); };

	};

	Def.ActorFrame{	--double
		OnCommand=function(self)
			self:visible(false);
		local steps = GAMESTATE:GetCurrentSteps(pn);
		local steptype = steps:GetStepsType();
			if steptype=="StepsType_Pump_Double" then
				self:visible(true);
			end
		end;
		SongChosenMessageCommand=cmd(playcommand,"On");
		PlayerJoinedMessageCommand=cmd(playcommand,"On");
		CurrentStepsP1ChangedMessageCommand=cmd(playcommand,"On");
		CurrentStepsP2ChangedMessageCommand=cmd(playcommand,"On");
	LoadActor("Double") .. {
		InitCommand=cmd(zoomx,0.40;zoomy,0.4;y,20;x,40;); };
	};


	Def.ActorFrame{	--halfdouble
		OnCommand=function(self)
			self:visible(false);
		local steps = GAMESTATE:GetCurrentSteps(pn);
		local steptype = steps:GetStepsType();
			if steptype=="StepsType_Pump_Halfdouble" then
				self:visible(true);
			end
		end;
		SongChosenMessageCommand=cmd(playcommand,"On");
		PlayerJoinedMessageCommand=cmd(playcommand,"On");
		CurrentStepsP1ChangedMessageCommand=cmd(playcommand,"On");
		CurrentStepsP2ChangedMessageCommand=cmd(playcommand,"On");
	LoadActor("Halfdouble") .. {
		InitCommand=cmd(zoomx,0.40;zoomy,0.4;y,20;x,40;); };
	};

	Def.ActorFrame{	--single
		OnCommand=function(self)
			self:visible(false);
		local steps = GAMESTATE:GetCurrentSteps(pn);
		local steptype = steps:GetStepsType();
			if steptype=="StepsType_Pump_Single" then
				self:visible(true);
			end
		end;
		SongChosenMessageCommand=cmd(playcommand,"On");
		PlayerJoinedMessageCommand=cmd(playcommand,"On");
		CurrentStepsP1ChangedMessageCommand=cmd(playcommand,"On");
		CurrentStepsP2ChangedMessageCommand=cmd(playcommand,"On");
	LoadActor("Single") .. {
		InitCommand=cmd(zoomx,0.40;zoomy,0.4;y,20;x,40;); };
                                 };
};
end;
local t = Def.ActorFrame {
	StepsPN(PLAYER_1)..{
	InitCommand=cmd(y,363;x,SCREEN_LEFT+227;);
	--OnCommand=cmd(visible,GAMESTATE:IsHumanPlayer(PLAYER_1););
	--PlayerJoinedMessageCommand=cmd(visible,GAMESTATE:IsHumanPlayer(PLAYER_1););
	};
	StepsPN(PLAYER_2)..{
	InitCommand=cmd(y,363;x,SCREEN_RIGHT-307;);
	--OnCommand=cmd(visible,GAMESTATE:IsHumanPlayer(PLAYER_2););
	--PlayerJoinedMessageCommand=cmd(visible,GAMESTATE:IsHumanPlayer(PLAYER_2););
	};
};
return t;
