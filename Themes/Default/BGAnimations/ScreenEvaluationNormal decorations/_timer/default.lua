local t = Def.ActorFrame {}

t[#t+1] = Def.ActorProxy {
	BeginCommand=function(self) 
		local Timer = SCREENMAN:GetTopScreen():GetChild('Timer'); 
		self:SetTarget(Timer); 
		Timer:silent(true);
		Timer:SetWarningSeconds(0);
		end;
	OnCommand=cmd(stoptweening;x,1;y,15);
	OffCommand=cmd(linear,.2;y,-15;diffusealpha,0);
}


return t