local t = Def.ActorFrame {};

function GetPlayerScores( player )
	local timmings = {};
	local curstats = STATSMAN:GetCurStageStats():GetPlayerStageStats(player);
	local timmings = {
			string.format("%03d",curstats:GetTapNoteScores('TapNoteScore_CheckpointHit') + curstats:GetTapNoteScores('TapNoteScore_W1') + curstats:GetTapNoteScores('TapNoteScore_W2') )
		,
			string.format("%03d",curstats:GetTapNoteScores('TapNoteScore_W3') )
		,
			string.format("%03d",curstats:GetTapNoteScores('TapNoteScore_W4') )
		,
			string.format("%03d",curstats:GetTapNoteScores('TapNoteScore_W5') )
		,
			string.format("%03d",curstats:GetTapNoteScores('TapNoteScore_CheckpointMiss')+ curstats:GetTapNoteScores('TapNoteScore_Miss') )
		,
			string.format("%03d",curstats:MaxCombo() )
		,
			string.format("%06d",curstats:GetScore() )
			--string.format("%06d",curstats:GetActualDancePoints() )
		,
			string.format("%.3f",curstats:GetCaloriesBurned() )
		,
	};
	return timmings;
end;

local jud_names = { "perfect", "great", "good", "bad", "miss", "max", "total", "kcal" };
local jud_ypos = { 120 , 145, 169, 194, 219, 245, 270, 294 };

local numbers_delay = {};
local delta = .15;
local init = 2;

for i=1,8 do
	numbers_delay[i] = init + delta*(i-1);
end;

local delay = 0.8

--[[
function delay_s(delay)
    delay = delay or 1;
    local time_to = os.time() + delay;
    while os.time() < time_to do end;
end
]]

local total = 30;
local veces = 0;
local del = .07;

-- Announcer sound
t[#t+1] = LoadActor(THEME:GetPathS('','Evaluation/RANK_'..GetAnnouncer()..'.WAV')) .. {
	InitCommand=cmd(sleep,4.5;queuecommand,'Play');
	PlayCommand=cmd(play);
	OffCommand=cmd(stop);
};

t[#t+1] = LoadActor(THEME:GetPathS('','Evaluation/RANK_'..GetAnnouncer()..'_B.WAV')) .. {
	InitCommand=cmd(sleep,4.5;queuecommand,'Play');
	PlayCommand=cmd(play);
	OffCommand=cmd(stop);
};

t[#t+1] = LoadActor(THEME:GetPathS('','Evaluation/RANK.WAV')) .. {
	InitCommand=cmd(sleep,4.5;queuecommand,'Play');
	PlayCommand=cmd(play);
	OffCommand=cmd(stop);
};

t[#t+1] = LoadActor(THEME:GetPathS('','Evaluation/RESULT_BGM')) .. {
	InitCommand=cmd(queuecommand,'Play');
	PlayCommand=cmd(play);
	OffCommand=cmd(stop);
};

for i = 0, (#jud_names-1) do
t[#t+1] = 	LoadActor("jud_detail")..{
				InitCommand=cmd(x,SCREEN_CENTER_X;y,(SCREEN_CENTER_Y-120)+(25*i);zoom,.75;animate,0;pause;setstate,i;zoomy,0);
				OnCommand=cmd(sleep,.5;linear,.5;zoomy,.75);
				OffCommand=cmd(linear,.5;zoomy,0);
			};      
end;

t[#t+1] = LoadActor(THEME:GetPathS('','so')) .. {
	InitCommand=cmd(stop);
	OnCommand=cmd(sleep,numbers_delay[1]+.09;queuecommand,'Sound');
	DelayCommand=cmd(sleep,del;queuecommand,'Sound');
	SoundCommand=cmd(play);
	OffCommand=cmd(pause;stop);
};

if (ShowForPlayer('PlayerNumber_P1')) then
local jud_scores_1 = GetPlayerScores( PLAYER_1 );
for i=1,8 do
	t[#t+1] = NewRollingNumbers(jud_scores_1[i],numbers_delay[i],140,jud_ypos[i],'HorizAlign_Left')..{
		OffCommand=cmd(stoptweening;linear,.3;diffusealpha,0);
	};
end;
end;

if (ShowForPlayer('PlayerNumber_P2')) then
local jud_scores_2 = GetPlayerScores( PLAYER_2 );
for i=1,8 do
	t[#t+1] = NewRollingNumbers(jud_scores_2[i],numbers_delay[i],SCREEN_RIGHT-140,jud_ypos[i],'HorizAlign_Right')..{
		OffCommand=cmd(stoptweening;linear,.2;diffusealpha,0);
	};
end;
end;

return t;