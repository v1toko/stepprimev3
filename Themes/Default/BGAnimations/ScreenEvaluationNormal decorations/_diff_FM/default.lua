local t = Def.ActorFrame {};

cx = (SCREEN_WIDTH/2);
cy = (SCREEN_HEIGHT/2);

--///////////////////////////////////////////////////////////////////////////////////////////
--///////////////////////////////////////////////////////////////////////////////////////////
local backSelectSteps = cmd(stoptweening;decelerate,.3;diffusealpha,.3);
local ballSelectSteps = cmd(stoptweening;decelerate,.3;diffusealpha,1);
--///////////////////////////////////////////////////////////////////////////////////////////
--///////////////////////////////////////////////////////////////////////////////////////////

function GetDiffNumber(pn)

	if GAMESTATE:GetCurrentGroup() ~= "music train"  and GAMESTATE:GetCurrentGroup() ~= "random" and GAMESTATE:GetCurrentGroup() ~= "quest zone" then
		--Clasificacion de pasos x estilo, single, double, couple o halfdouble
		--todo "StepsType_Pump_Couple"
		
		--Cada estilo posee, beginner, easy, medium, hard, challenge, y edit.
		dif = GAMESTATE:GetCurrentSteps(pn):GetDifficulty();
		style = GAMESTATE:GetCurrentSteps(pn):GetStepsType();
		meter = GAMESTATE:GetCurrentSteps(pn):GetMeter();
		desc = GAMESTATE:GetCurrentSteps(pn):GetDescription();

		if string.find(desc, "SP") ~= nil then return 1 end;
		if string.find(desc, "DP") ~= nil and meter < 50 then return 3 end;
		if string.find(desc, "DP") ~= nil and meter >= 50 then return 5 end;

		--Cada estilo posee, beginner, easy, medium, hard, challenge, y edit.
		if style=='StepsType_Pump_Single' then return (0);
		--si tenemos un edit (performance > 10) es comun y corriente
		--elseif style=='StepsType_Pump_Single' and dif=='Difficulty_Edit' and meter > 10 then return(0);
		elseif style=='StepsType_Pump_Double' then return (2);
		--elseif style=='StepsType_Pump_Single' and dif=='Difficulty_Edit' then return(1); 
		elseif style=='StepsType_Pump_Routine' then return(3); 
		end;
	elseif GAMESTATE:GetCurrentGroup() == "quest zone" then
		return 4;
	else
		local crs = GAMESTATE:GetCurrentCourse();
		if crs == nil then return ""; end;

		if crs:GetCourseStyle()[1] == "DOUBLE" then
			return 2;
		else
			return 0; 
		end
	end;
	
	return 0;
end;

function GetDiffNumberBallsEval(pn)

	if GAMESTATE:GetCurrentGroup() ~= "music train"  and GAMESTATE:GetCurrentGroup() ~= "random" then
		--Clasificacion de pasos x estilo, single, double, couple o halfdouble
		--todo "StepsType_Pump_Couple"
		
		--Cada estilo posee, beginner, easy, medium, hard, challenge, y edit.
		dif = GAMESTATE:GetCurrentSteps(pn):GetDifficulty();
		style = GAMESTATE:GetCurrentSteps(pn):GetStepsType();
		meter = GAMESTATE:GetCurrentSteps(pn):GetMeter();
		desc = GAMESTATE:GetCurrentSteps(pn):GetDescription();

		if string.find(desc, "SP") ~= nil then return 1 end;
		if string.find(desc, "DP") ~= nil then 
			if meter < 50 then return 3 end;
			if meter >= 50 then return 4 end;
		end;

		--Cada estilo posee, beginner, easy, medium, hard, challenge, y edit.
		if style=='StepsType_Pump_Single' then return (0);
		--si tenemos un edit (performance > 10) es comun y corriente
		--elseif style=='StepsType_Pump_Single' and dif=='Difficulty_Edit' and meter > 10 then return(0);
		elseif style=='StepsType_Pump_Double' then return (2);
		--elseif style=='StepsType_Pump_Single' and dif=='Difficulty_Edit' then return(1); 
		elseif style=='StepsType_Pump_Routine' then return(3); 
		end;
	else
		local crs = GAMESTATE:GetCurrentCourse();
		if crs == nil then return ""; end;

		if crs:GetCourseStyle()[1] == "DOUBLE" then
			return 2;
		else
			return 0; 
		end
	end;
	
	return 0;
end;

--[[
function Actor:SetMeterValue(i,val,b)
	local num;	

	SCREENMAN:SystemMessage("asdoiuhasiud");

	--Warn("SetMeterValue: i: " .. tostring(i) .. " GetDiffNum(i): " .. GetDiffNum(i) .. " val: " .. tostring(val) .. " b: " .. tostring(b) .. " meter: " .. aSteps[i]:GetMeter());
	if (GetDiffNum(i)==val or b) and i <= numSteps then 				
		if GAMESTATE:GetCurrentGroup() == "music train" then 
			self:settext("??"); 
		else
			num=aSteps[i]:GetMeter();
			if num <=9 then 
				self:settext("0"..num); 
			else 
				if num < 50 then
					self:settext(num); 
				else
					self:settext("??"); 
				end
			end;
		end;		
	else 
		self:settext(""); 
	end;	
end;
]]

function Actor:settextbymetereval(pn)
	if GAMESTATE:GetCurrentGroup() == 'music train' then self:settext('??'); return; end;
	
	local meter = GAMESTATE:GetCurrentSteps(pn):GetMeter();

	desc = GAMESTATE:GetCurrentSteps(pn):GetDescription();
	desc = string.upper(desc);

	local bDP = false;
	if string.find(desc, "DP") ~= nil then bDP = true end;

	if meter<=9 then 
		self:settext('0'..meter); 
	else 
		if meter < 50 then
			self:settext(meter); 
		else
			if bDP then
				--busca los espacios
				self:settext('02');
				for v in string.gmatch(desc, "OPX[0-9]") do
				    for a in string.gmatch(v, "[^OPX,]+") do
			            local n = tonumber(a)
			            self:settext('0'..n);
		            end
				end
			else
				self:settext("??"); 
			end			
		end
	end;
end;

-- Regresa booleano si ya fue jugado
function GetIfUnplayedStepDiff(pn)
	--Carga de Steps "disponibles" en la cancion actual
	if GAMESTATE:GetCurrentGroup() == "quest zone" or GAMESTATE:GetCurrentGroup() == "random" or GAMESTATE:GetCurrentGroup() == "music train" then return 0 end;

	local step = GAMESTATE:GetCurrentSteps(pn);

	if not step then return 0 end;
	
	desc = step:GetDescription();
	desc = string.lower(desc);

	if string.find(desc, "ucs") ~= nil then return 1 end;
	if string.find(desc, "new") ~= nil then return 2 end;
	if string.find(desc, "another") ~= nil then return 3 end;

	return 0;
end;

function Actor:ifstepseval(player,style)
	if GetDiffNumber(player)~=style then
		self:visible(false);
	else
		self:visible(true);
	end;
end;

adjustp1x = 35;

--///////////////////////////////////////////////////////////////////////////////////////////
--///////////////////////////////////////////////////////////////////////////////////////////
--///////////////////////////////////////////////////////////////////////////////////////////
--Player 1
if GAMESTATE:IsSideJoined('PlayerNumber_P1') then
--Back
t[#t+1] = LoadActor("diff_ball_bg")..{
	InitCommand=cmd(x,adjustp1x+cx-123;zoom,.75;);
	--InitCommand=cmd(x,cx-123;zoom,.75;);
	SetBallDiffCommand=cmd(stoptweening;);
	StartSelectingStepsMessageCommand=cmd(stoptweening;decelerate,.3;diffusealpha,1);
	--CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'SetBallDiff');
	OffCommand=cmd(stoptweening;diffusealpha,1;sleep,.4;linear,.2;addy,200);
}

t[#t+1] = LoadActor("diff_balls")..{
	InitCommand=cmd(x,adjustp1x+cx-122;zoom,.75;pause;animate,0;queuecommand,'SetBallDiff');
	--InitCommand=cmd(x,cx-122;zoom,.75;);
	SetBallDiffCommand=cmd(stoptweening;animate,0;pause;setstate,GetDiffNumberBallsEval('PlayerNumber_P1'));
	StartSelectingStepsMessageCommand=cmd(stoptweening;decelerate,.3;diffusealpha,1);
	CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'SetBallDiff');
	OffCommand=cmd(stoptweening;diffusealpha,1;sleep,.4;linear,.2;addy,200);
}

t[#t+1] = LoadActor("efx3")..{
	OnCommand=cmd(x,adjustp1x+cx-122;zoom,.7;diffusealpha,0;blend,'BlendMode_Add';diffusealpha,0);
	OffCommand=cmd(stoptweening;diffusealpha,0.7;decelerate,.2;diffusealpha,0);
};

t[#t+1] = LoadActor("efx2")..{
	OnCommand=cmd(x,adjustp1x+cx-122;zoom,.75;diffusealpha,0;blend,'BlendMode_Add';diffusealpha,0;queuecommand,'Repeat');
	RepeatCommand=cmd(stoptweening;rotationz,0;diffusealpha,0.225;linear,2;rotationz,-360;queuecommand,'Repeat');
	OffCommand=cmd(stoptweening;linear,.1;diffusealpha,0);
};

-- SingleNumbers (orange)
t[#t+1] = LoadFont("numbersorangebig")..{
	OnCommand=cmd(stoptweening;playcommand,'Set';x,adjustp1x+cx-121;y,23;zoom,0.75;);
	SetCommand=cmd(ifstepseval,'PlayerNumber_P1',0;settextbymetereval,'PlayerNumber_P1');
	StartSelectingStepsMessageCommand=cmd(stoptweening;ifstepseval,'PlayerNumber_P1',0;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;);
	CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'Set');
	OffCommand=cmd(stoptweening;diffusealpha,1;sleep,.4;linear,.2;addy,200);
};
-- DoubleNumbers( green )
t[#t+1] = LoadFont("numbersgreenbig")..{
	OnCommand=cmd(stoptweening;playcommand,'Set';x,adjustp1x+cx-121;y,23;zoom,0.75;);
	SetCommand=cmd(ifstepseval,'PlayerNumber_P1',2;settextbymetereval,'PlayerNumber_P1');
	StartSelectingStepsMessageCommand=cmd(stoptweening;ifstepseval,'PlayerNumber_P1',2;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;);
	CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'Set');
	OffCommand=cmd(stoptweening;diffusealpha,1;sleep,.4;linear,.2;addy,200);
};
-- SinglePerformanceNumbers( pink )
t[#t+1] = LoadFont("numberspurplebig")..{
	OnCommand=cmd(stoptweening;playcommand,'Set';x,adjustp1x+cx-121;y,23;zoom,0.75;);
	SetCommand=cmd(ifstepseval,'PlayerNumber_P1',1;settextbymetereval,'PlayerNumber_P1');
	StartSelectingStepsMessageCommand=cmd(stoptweening;ifstepseval,'PlayerNumber_P1',1;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;);
	CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'Set');
	OffCommand=cmd(stoptweening;diffusealpha,1;sleep,.4;linear,.2;addy,200);
};
-- DoublePerformanceNumbers( blue )
t[#t+1] = LoadFont("numbersbluebig")..{
	OnCommand=cmd(stoptweening;playcommand,'Set';x,adjustp1x+cx-121;y,23;zoom,0.75;);
	SetCommand=cmd(ifstepseval,'PlayerNumber_P1',3;settextbymetereval,'PlayerNumber_P1');
	StartSelectingStepsMessageCommand=cmd(stoptweening;ifstepseval,'PlayerNumber_P1',3;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;);
	CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'Set');
	OffCommand=cmd(stoptweening;diffusealpha,1;sleep,.4;linear,.2;addy,200);
};

-- DoublePerformanceNumbers( yellow )
t[#t+1] = LoadFont("numbersyellowbig")..{
	OnCommand=cmd(stoptweening;playcommand,'Set';x,adjustp1x+cx-121;y,22;zoom,0.75;);
	SetCommand=cmd(ifstepseval,'PlayerNumber_P1',5;settextbymetereval,'PlayerNumber_P1');
	StartSelectingStepsMessageCommand=cmd(stoptweening;ifstepseval,'PlayerNumber_P1',5;diffusealpha,0;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;diffusealpha,0);
	CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'Set');
	OffCommand=cmd(stoptweening;diffusealpha,1;sleep,.4;linear,.2;addy,200);
};

t[#t+1] = LoadActor("stepsstate") .. {
	CheckCommand=function(self) 
		local b = GetIfUnplayedStepDiff('PlayerNumber_P1');
		self:visible(GAMESTATE:GetCurrentGroup() ~= "quest zone" and GAMESTATE:GetCurrentGroup() ~= "random");
		if b > 0 then
			self:setstate(b-1);
		else
			self:visible(false);
		end
	end;
	InitCommand=cmd(animate,0;pause;);
	OnCommand=cmd(stoptweening;x,adjustp1x+cx-121;y,-43;zoom,0.75;diffusealpha,1;playcommand,'Check');
	--SetCommand=cmd();
	StartSelectingStepsMessageCommand=cmd(stoptweening;diffusealpha,0;accelerate,.1;diffusealpha,1;playcommand,'Check');
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;diffusealpha,0;playcommand,'Check');
	CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;playcommand,'Check');
	OffCommand=cmd(stoptweening;decelerate,.1;diffuse,.9,.9,.9,1;sleep,.3;decelerate,.1;diffusealpha,0;playcommand,'Check');
};

t[#t+1] = LoadActor("records_normal")..{
	OnCommand=cmd(stoptweening;x,adjustp1x+cx-260;y,5;zoom,.75;diffusealpha,0;sleep,1.2;linear,.2;diffusealpha,1);
	--SetCommand=cmd();
	StartSelectingStepsMessageCommand=cmd(stoptweening;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;);
	CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1);
	OffCommand=cmd(stoptweening;decelerate,.1;diffuse,.9,.9,.9,1;sleep,.3;decelerate,.1;zoomx,0);
};

local xHelp = adjustp1x+cx-260;

t[#t+1] = LoadActor("effect_records")..{
	InitCommand=cmd(blend,'BlendMode_Add');
	OnCommand=cmd(stoptweening;x,xHelp;y,4;zoom,.75;diffusealpha,0;sleep,1.3;queuecommand,'Change');
	--SetCommand=cmd();
	--StartSelectingStepsMessageCommand=cmd(stoptweening;diffusealpha,0;accelerate,.1;diffusealpha,1);
	ChangeCommand=function(self,params)
		--if params.Player == "PlayerNumber_P1" and params.Direction == -1 then
			(cmd(stoptweening;x,xHelp-10;diffusealpha,.7;x,xHelp+5;decelerate,.3;x,xHelp-10;;diffusealpha,0))(self);
		--elseif params.Player == "PlayerNumber_P1" and params.Direction == 1 then
		--	(cmd(stoptweening;x,xHelp+10;diffusealpha,.7;x,xHelp-10;faderight,.5;decelerate,.3;x,xHelp+10;faderight,0;diffusealpha,0))(self);
		--end;
	end;
	--[[NextStepMessageCommand=function(self,params)
		if params.Player == "PlayerNumber_P2" then
			
		end;
	end;]]
	--CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1);
	OffCommand=cmd(stoptweening;diffusealpha,0);
};

--[[ t[#t+1] =	Def.RollingNumbers {
				File = THEME:GetPathF("","_karnivore lite 20px");
				InitCommand=cmd(stoptweening;Load,"RollingNumbersRecords";x,adjustp1x+cx-305;y,-31;settext,"";zoom,.75;horizalign,'HorizAlign_Left';targetnumber,0;queuecommand,'RefreshText');
				RefreshTextCommand=function(self)
					local cur_song = GAMESTATE:GetCurrentSong();
					local cur_steps = GAMESTATE:GetCurrentSteps(PLAYER_1);
					--local HSList = PROFILEMAN:GetMachineProfile():GetHighScoreList(cur_song,cur_steps):GetHighScores();
					local prf = PROFILEMAN:GetProfile(PLAYER_1);
					if not prf:IsMachine() and cur_steps ~= nil then
						local HSList = prf:GetHighScoreList(cur_song,cur_steps):GetHighScores();
						if (#HSList ~= 0) then
							local name = HSList[1]:GetName();	
							local score = HSList[1]:GetScore();	
							--self:settext(comma_value(score));
							self:targetnumber(score);
						else
							self:targetnumber(0);
							--self:settext(" ");
						end;
					else
						self:targetnumber(0);
						--self:settext(" ");
					end;
				end;
				OffCommand=cmd(stoptweening;visible,false);
			};
]]
-- HighScore Para el player 1
t[#t+1] = 	LoadFont("_karnivore lite 20px")..{
	InitCommand=cmd(stoptweening;x,adjustp1x+cx-305;y,-31;settext,"";zoom,.75;horizalign,'HorizAlign_Left';playcommand,'RefreshText');
	RefreshTextCommand=function(self)
		if GAMESTATE:GetCurrentGroup() ~= "music train"  and GAMESTATE:GetCurrentGroup() ~= "random" then
			local cur_song = GAMESTATE:GetCurrentSong();
			local cur_steps = GAMESTATE:GetCurrentSteps(PLAYER_1);
			--local HSList = PROFILEMAN:GetMachineProfile():GetHighScoreList(cur_song,cur_steps):GetHighScores();
			local prf = PROFILEMAN:GetProfile(PLAYER_1);
			if not prf:IsMachine() and cur_steps ~= nil then
				local HSList = prf:GetHighScoreList(cur_song,cur_steps):GetHighScores();
				if (#HSList ~= 0) then
					local name = HSList[1]:GetName();	
					local score = HSList[1]:GetScore();	
					self:settext(comma_value(score));
				else
					self:settext(" ");
				end;
			else
				self:settext(" ");
			end;
		else
			local cur_song = GAMESTATE:GetCurrentCourse();
			local crs = GAMESTATE:GetCurrentCourse();
			if crs == nil then return ""; end;

			local trails = crs:GetAllTrails();
			local cur_steps = trails[1];

			--local HSList = PROFILEMAN:GetMachineProfile():GetHighScoreList(cur_song,cur_steps):GetHighScores();
			local prf = PROFILEMAN:GetProfile(PLAYER_1);
			if not prf:IsMachine() and cur_steps ~= nil then
				local HSList = prf:GetHighScoreList(cur_song,cur_steps):GetHighScores();
				if (#HSList ~= 0) then
					local name = HSList[1]:GetName();	
					local score = HSList[1]:GetScore();	
					self:settext(comma_value(score));
				else
					self:settext(" ");
				end;
			else
				self:settext(" ");
			end;
		end;
	end;
	OffCommand=cmd(stoptweening;visible,false);
}; 

-- HighScore Para el player 1 (machine) (worldbest)
t[#t+1] = LoadFont("_karnivore lite 20px") .. {
	InitCommand=cmd(stoptweening;x,adjustp1x+cx-305;y,9;settext,"";zoom,.75;horizalign,'HorizAlign_Left';playcommand,'RefreshText');
	RefreshTextCommand=function(self)
		self:settext(" ");
		if GAMESTATE:GetCurrentGroup() ~= "music train" then
			self:settext(" ");
			--se debe enviar 1 sola vez
			--and IsSMOnlineLoggedIn(PLAYER_1)
			if IsNetConnected() and IsSMOnlineLoggedIn(PLAYER_1) then
				AskSongStepScore(PLAYER_1);
			end
		end;
	end;
	UpdateRankStepScoreP1MessageCommand=function(self)
		if GetMaxStepScore(PLAYER_1) ~= 0 then
			self:settext( comma_value(GetMaxStepScore(PLAYER_1)) );
		end
	end;
	OffCommand=cmd(stoptweening;visible,false);
}; 

t[#t+1] = LoadFont("_youth 20px") .. {
	InitCommand=cmd(stoptweening;x,adjustp1x+cx-305;y,-3;settext,"";zoom,.65;horizalign,'HorizAlign_Left';playcommand,'RefreshText');
	RefreshTextCommand=function(self)
		if GAMESTATE:GetCurrentGroup() ~= "music train" then
			self:settext(" ");
			--se debe enviar 1 sola vez
			--and IsSMOnlineLoggedIn(PLAYER_1)
			--if IsNetConnected() then
			--	AskSongStepScore(PLAYER_1);
			--end
		end;
	end;
	UpdateRankStepScoreP1MessageCommand=function(self)
		if GetMaxStepScore(PLAYER_1) ~= 0 then
			self:settext( string.upper(tostring(Get1StStepScoreName(PLAYER_1))) );
		end
	end;
	OffCommand=cmd(stoptweening;visible,false);
};

-- HighScore Para el player 1 (machine) (worldbest)
t[#t+1] = LoadFont("_karnivore lite 20px") .. {
	InitCommand=cmd(stoptweening;x,adjustp1x+cx-305;y,49;settext,"";zoom,.75;horizalign,'HorizAlign_Left';playcommand,'RefreshText');
	RefreshTextCommand=function(self)
		if GAMESTATE:GetCurrentGroup() ~= "music train"  and GAMESTATE:GetCurrentGroup() ~= "random" then
			local cur_song = GAMESTATE:GetCurrentSong();
			local cur_steps = GAMESTATE:GetCurrentSteps(PLAYER_1);

			if cur_steps == nil then return end;

			local HSList = PROFILEMAN:GetMachineProfile():GetHighScoreList(cur_song,cur_steps):GetHighScores();
			--local HSList = PROFILEMAN:GetProfile(PLAYER_1):GetHighScoreList(cur_song,cur_steps):GetHighScores();
			if (#HSList ~= 0) then
				--local name = HSList[1]:GetName();	
				local score = HSList[1]:GetScore();	
				self:settext(comma_value(score));
			else
				self:settext(" ");
			end;
		else
			local cur_song = GAMESTATE:GetCurrentCourse();
			local crs = GAMESTATE:GetCurrentCourse();
			if crs == nil then return ""; end;

			local trails = crs:GetAllTrails();
			local cur_steps = trails[1];

			local HSList = PROFILEMAN:GetMachineProfile():GetHighScoreList(cur_song,cur_steps):GetHighScores();
			--local HSList = PROFILEMAN:GetProfile(PLAYER_1):GetHighScoreList(cur_song,cur_steps):GetHighScores();
			if (#HSList ~= 0) then
				--local name = HSList[1]:GetName();	
				local score = HSList[1]:GetScore();	
				self:settext(comma_value(score));
			else
				self:settext(" ");
			end;
		end;
	end;
	OffCommand=cmd(stoptweening;visible,false);
};

t[#t+1] = LoadFont("_youth 20px") .. {
	InitCommand=cmd(stoptweening;x,adjustp1x+cx-305;y,37;settext,"";zoom,.65;horizalign,'HorizAlign_Left';playcommand,'RefreshText');
	RefreshTextCommand=function(self)
		if GAMESTATE:GetCurrentGroup() ~= "music train"  and GAMESTATE:GetCurrentGroup() ~= "random" then
			local cur_song = GAMESTATE:GetCurrentSong();
			local cur_steps = GAMESTATE:GetCurrentSteps(PLAYER_1);
			if cur_steps == nil then return end;
			local HSList = PROFILEMAN:GetMachineProfile():GetHighScoreList(cur_song,cur_steps):GetHighScores();
			--local HSList = PROFILEMAN:GetProfile(PLAYER_1):GetHighScoreList(cur_song,cur_steps):GetHighScores();
			if (#HSList ~= 0) then
				local name = HSList[1]:GetName();	
				--local score = HSList[1]:GetScore();	
				self:settext(string.upper(name));
			else
				self:settext(" ");
			end;
		else
			local cur_song = GAMESTATE:GetCurrentCourse();
			local crs = GAMESTATE:GetCurrentCourse();
			if crs == nil then return ""; end;

			local trails = crs:GetAllTrails();
			local cur_steps = trails[1];

			local HSList = PROFILEMAN:GetMachineProfile():GetHighScoreList(cur_song,cur_steps):GetHighScores();
			--local HSList = PROFILEMAN:GetProfile(PLAYER_1):GetHighScoreList(cur_song,cur_steps):GetHighScores();
			if (#HSList ~= 0) then
				local name = HSList[1]:GetName();	
				--local score = HSList[1]:GetScore();	
				self:settext(string.upper(name));
			else
				self:settext(" ");
			end;
		end;
	end;
	OffCommand=cmd(stoptweening;visible,false);
};

end;

--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--Player 2 - Back
if GAMESTATE:IsSideJoined('PlayerNumber_P2') then

t[#t+1] = LoadActor("diff_ball_bg")..{
	InitCommand=cmd(x,cx+122-adjustp1x;zoom,.75);
	SetBallDiffCommand=cmd(stoptweening;);
	StartSelectingStepsMessageCommand=cmd(stoptweening;decelerate,.3;diffusealpha,1);
	--CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'SetBallDiff');
	OffCommand=cmd(stoptweening;diffusealpha,1;sleep,.4;linear,.2;addy,200);
} 

t[#t+1] = LoadActor("diff_balls")..{
	InitCommand=cmd(x,cx+123-adjustp1x;zoom,.75;pause;animate,0;queuecommand,'SetBallDiff');
	SetBallDiffCommand=cmd(stoptweening;animate,0;pause;setstate,GetDiffNumberBallsEval('PlayerNumber_P2'));
	StartSelectingStepsMessageCommand=cmd(stoptweening;playcommand,'SetBallDiff';decelerate,.3;diffusealpha,1);
	CurrentStepsP2ChangedMessageCommand=cmd(stoptweening;sleep,.1;queuecommand,'SetBallDiff');
	OffCommand=cmd(stoptweening;diffusealpha,1;sleep,.4;linear,.2;addy,200);
};

t[#t+1] = LoadActor("efx3")..{
	OnCommand=cmd(x,cx+123-adjustp2x;zoom,.7;diffusealpha,0;blend,'BlendMode_Add';diffusealpha,0);
	OffCommand=cmd(stoptweening;diffusealpha,0.7;decelerate,.2;diffusealpha,0);
};

t[#t+1] = LoadActor("efx2")..{
	OnCommand=cmd(x,cx+123-adjustp2x;zoom,.75;diffusealpha,0;blend,'BlendMode_Add';diffusealpha,0;rotationx,180;queuecommand,'Repeat');
	RepeatCommand=cmd(stoptweening;rotationz,0;diffusealpha,0.225;linear,2;rotationz,-360;queuecommand,'Repeat');
	OffCommand=cmd(stoptweening;linear,.1;diffusealpha,0);
};

-- SingleNumbers (orange)
t[#t+1] = LoadFont("numbersorangebig")..{
	OnCommand=cmd(stoptweening;playcommand,'Set';x,cx+121-adjustp1x;y,23;zoom,0.75;);
	SetCommand=cmd(ifstepseval,'PlayerNumber_P2',0;settextbymetereval,'PlayerNumber_P2');
	StartSelectingStepsMessageCommand=cmd(stoptweening;ifstepseval,'PlayerNumber_P2',0;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;);
	CurrentStepsP2ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'Set');
	OffCommand=cmd(stoptweening;diffusealpha,1;sleep,.4;linear,.2;addy,200);
};
-- DoubleNumbers( green )
t[#t+1] = LoadFont("numberspurplebig")..{
	OnCommand=cmd(stoptweening;playcommand,'Set';x,cx+121-adjustp1x;y,23;zoom,0.75;);
	SetCommand=cmd(ifstepseval,'PlayerNumber_P2',1;settextbymetereval,'PlayerNumber_P2');
	StartSelectingStepsMessageCommand=cmd(stoptweening;ifstepseval,'PlayerNumber_P2',1;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;);
	CurrentStepsP2ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'Set');
	OffCommand=cmd(stoptweening;diffusealpha,1;sleep,.4;linear,.2;addy,200);
};
-- SinglePerformanceNumbers( green )
t[#t+1] = LoadFont("numbersgreenbig")..{
	OnCommand=cmd(stoptweening;playcommand,'Set';x,cx+121-adjustp1x;y,23;zoom,0.75;);
	SetCommand=cmd(ifstepseval,'PlayerNumber_P2',2;settextbymetereval,'PlayerNumber_P2');
	StartSelectingStepsMessageCommand=cmd(stoptweening;ifstepseval,'PlayerNumber_P2',2;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;);
	CurrentStepsP2ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'Set');
	OffCommand=cmd(stoptweening;diffusealpha,1;sleep,.4;linear,.2;addy,200);
};
-- DoublePerformanceNumbers( blue )
t[#t+1] = LoadFont("numbersbluebig")..{
	OnCommand=cmd(stoptweening;playcommand,'Set';x,cx+121-adjustp1x;y,23;zoom,0.75;);
	SetCommand=cmd(ifstepseval,'PlayerNumber_P2',3;settextbymetereval,'PlayerNumber_P2');
	StartSelectingStepsMessageCommand=cmd(stoptweening;ifstepseval,'PlayerNumber_P2',3;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;);
	CurrentStepsP2ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'Set');
	OffCommand=cmd(stoptweening;diffusealpha,1;sleep,.4;linear,.2;addy,200);
};

t[#t+1] = LoadActor("stepsstate") .. {
	CheckCommand=function(self) 
		local b = GetIfUnplayedStepDiff('PlayerNumber_P2');
		self:visible(GAMESTATE:GetCurrentGroup() ~= "quest zone" and GAMESTATE:GetCurrentGroup() ~= "random");
		if b > 0 then
			self:setstate(b-1);
		else
			self:visible(false);
		end
	end;
	InitCommand=cmd(animate,0;pause;);
	OnCommand=cmd(stoptweening;x,cx+121-adjustp1x;y,-43;zoom,0.75;diffusealpha,1;playcommand,'Check');
	--SetCommand=cmd();
	StartSelectingStepsMessageCommand=cmd(stoptweening;diffusealpha,0;accelerate,.1;diffusealpha,1;playcommand,'Check');
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;diffusealpha,0;playcommand,'Check');
	CurrentStepsP2ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;playcommand,'Check');
	OffCommand=cmd(stoptweening;decelerate,.1;diffuse,.9,.9,.9,1;sleep,.3;decelerate,.1;diffusealpha,0;playcommand,'Check');
};

t[#t+1] = LoadActor("records_normal")..{
	OnCommand=cmd(stoptweening;x,adjustp1x+cx+260-70;y,5;zoom,.75;;diffusealpha,0;sleep,1.2;linear,.2;diffusealpha,1);
	--SetCommand=cmd();
	StartSelectingStepsMessageCommand=cmd(stoptweening;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;);
	CurrentStepsP2ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1);
	OffCommand=cmd(stoptweening;decelerate,.1;diffuse,.9,.9,.9,1;sleep,0;decelerate,.1;zoomx,0);
};

-- DoublePerformanceNumbers( yellow )
t[#t+1] = LoadFont("numbersyellowbig")..{
	OnCommand=cmd(stoptweening;playcommand,'Set';x,cx+121-adjustp1x;y,22;zoom,0.75;);
	SetCommand=cmd(ifstepseval,'PlayerNumber_P2',5;settextbymetereval,'PlayerNumber_P2');
	OffCommand=cmd(stoptweening;diffusealpha,1;sleep,.4;linear,.2;addy,200);
};

local xHelp = adjustp1x+cx+260-70;

t[#t+1] = LoadActor("effect_records")..{
	InitCommand=cmd(blend,'BlendMode_Add');
	OnCommand=cmd(stoptweening;x,xHelp;y,4;zoom,.75;diffusealpha,0;sleep,1.3;queuecommand,'Change');
	--SetCommand=cmd();
	--StartSelectingStepsMessageCommand=cmd(stoptweening;diffusealpha,0;accelerate,.1;diffusealpha,1);
	ChangeCommand=function(self,params)
		--if params.Player == "PlayerNumber_P2" and params.Direction == -1 then
			(cmd(stoptweening;x,xHelp-10;diffusealpha,.7;x,xHelp-10;decelerate,.3;x,xHelp-10;diffusealpha,0))(self);
		--elseif params.Player == "PlayerNumber_P2" and params.Direction == 1 then
		--	(cmd(stoptweening;x,xHelp+10;diffusealpha,.7;x,xHelp-10;faderight,.5;decelerate,.3;x,xHelp+10;faderight,0;diffusealpha,0))(self);
		--end;
	end;
	--[[NextStepMessageCommand=function(self,params)
		if params.Player == "PlayerNumber_P2" then
			
		end;
	end;]]
	--CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1);
	OffCommand=cmd(stoptweening;diffusealpha,0);
};

-- HighScore Para el player 2
t[#t+1] = LoadFont("_karnivore lite 20px")..{
	InitCommand=cmd(stoptweening;x,adjustp1x+cx+305-160;y,-31;settext,"";zoom,.75;horizalign,'HorizAlign_Left';playcommand,'RefreshText');
	RefreshTextCommand=function(self)
		if GAMESTATE:GetCurrentGroup() ~= "music train"  and GAMESTATE:GetCurrentGroup() ~= "random" then
			local cur_song = GAMESTATE:GetCurrentSong();
			local cur_steps = GAMESTATE:GetCurrentSteps(PLAYER_2);
			if cur_steps == nil then return end;
			--local HSList = PROFILEMAN:GetMachineProfile():GetHighScoreList(cur_song,cur_steps):GetHighScores();
			local prf = PROFILEMAN:GetProfile(PLAYER_2);
			if not prf:IsMachine() then
				local HSList = prf:GetHighScoreList(cur_song,cur_steps):GetHighScores();
				if (#HSList ~= 0) then
					local name = HSList[1]:GetName();	
					local score = HSList[1]:GetScore();	
					self:settext(comma_value(score));
				else
					self:settext(" ");
				end;
			else
				self:settext(" ");
			end;
		else
			local cur_song = GAMESTATE:GetCurrentCourse();
			local crs = GAMESTATE:GetCurrentCourse();
			if crs == nil then return ""; end;

			local trails = crs:GetAllTrails();
			local cur_steps = trails[1];

			--local HSList = PROFILEMAN:GetMachineProfile():GetHighScoreList(cur_song,cur_steps):GetHighScores();
			local prf = PROFILEMAN:GetProfile(PLAYER_2);
			if not prf:IsMachine() then
				local HSList = prf:GetHighScoreList(cur_song,cur_steps):GetHighScores();
				if (#HSList ~= 0) then
					local name = HSList[1]:GetName();	
					local score = HSList[1]:GetScore();	
					self:settext(comma_value(score));
				else
					self:settext(" ");
				end;
			else
				self:settext(" ");
			end;
		end
	end;
	OffCommand=cmd(stoptweening;visible,false);
};

-- HighScore Para el player 1 (machine) (worldbest)
t[#t+1] = LoadFont("_karnivore lite 20px")..{
	InitCommand=cmd(stoptweening;x,adjustp1x+cx+305-160;y,9;settext,"";zoom,.75;horizalign,'HorizAlign_Left';playcommand,'RefreshText');
	RefreshTextCommand=function(self)
		if GAMESTATE:GetCurrentGroup() ~= "music train" then
			self:settext(" ");
			--se debe enviar 1 sola vez
			--and IsSMOnlineLoggedIn(PLAYER_2)
			--Warn(tostring(IsSMOnlineLoggedIn(PLAYER_2)));
			if IsNetConnected() and IsSMOnlineLoggedIn(PLAYER_2) then
				AskSongStepScore(PLAYER_2);
			end
		end;
	end;
	UpdateRankStepScoreP2MessageCommand=function(self)
		if GetMaxStepScore(PLAYER_2) ~= 0 then
			self:settext( comma_value(GetMaxStepScore(PLAYER_2)) );
		end
	end;
	OffCommand=cmd(stoptweening;visible,false);
};

t[#t+1] = LoadFont("_youth 20px")..{
	InitCommand=cmd(stoptweening;x,adjustp1x+cx+305-160;y,-3;settext,"";zoom,.65;horizalign,'HorizAlign_Left';playcommand,'RefreshText');
	RefreshTextCommand=function(self)
		self:settext(" ");
		if GAMESTATE:GetCurrentGroup() ~= "music train" then
			self:settext(" ");
			--se debe enviar 1 sola vez			
			--if IsNetConnected() and IsSMOnlineLoggedIn(PLAYER_2) then
			--	AskSongStepScore(PLAYER_2);
			--end
		end;
	end;
	UpdateRankStepScoreP2MessageCommand=function(self)
		if GetMaxStepScore(PLAYER_2) ~= 0 then
			self:settext( string.upper(tostring(Get1StStepScoreName(PLAYER_2))) );
		end
	end;
	OffCommand=cmd(stoptweening;visible,false);
};

-- HighScore Para el player 1 (machine) (worldbest)
t[#t+1] = LoadFont("_karnivore lite 20px")..{
	InitCommand=cmd(stoptweening;x,adjustp1x+cx+305-160;y,49;settext,"";zoom,.75;horizalign,'HorizAlign_Left';playcommand,'RefreshText');
	RefreshTextCommand=function(self)
		if GAMESTATE:GetCurrentGroup() ~= "music train"  and GAMESTATE:GetCurrentGroup() ~= "random" then
			local cur_song = GAMESTATE:GetCurrentSong();
			local cur_steps = GAMESTATE:GetCurrentSteps(PLAYER_2);
			if cur_steps == nil then return end;
			local HSList = PROFILEMAN:GetMachineProfile():GetHighScoreList(cur_song,cur_steps):GetHighScores();
			--local HSList = PROFILEMAN:GetProfile(PLAYER_2):GetHighScoreList(cur_song,cur_steps):GetHighScores();
			if (#HSList ~= 0) then
				--local name = HSList[1]:GetName();	
				local score = HSList[1]:GetScore();	
				self:settext(comma_value(score));
			else
				self:settext(" ");
			end;
		else
			local cur_song = GAMESTATE:GetCurrentCourse();
			local crs = GAMESTATE:GetCurrentCourse();
			if crs == nil then return ""; end;

			local trails = crs:GetAllTrails();
			local cur_steps = trails[1];

			local HSList = PROFILEMAN:GetMachineProfile():GetHighScoreList(cur_song,cur_steps):GetHighScores();
			--local HSList = PROFILEMAN:GetProfile(PLAYER_2):GetHighScoreList(cur_song,cur_steps):GetHighScores();
			if (#HSList ~= 0) then
				--local name = HSList[1]:GetName();	
				local score = HSList[1]:GetScore();	
				self:settext(comma_value(score));
			else
				self:settext(" ");
			end;
		end
	end;
	OffCommand=cmd(stoptweening;visible,false);
};

t[#t+1] = LoadFont("_youth 20px")..{
	InitCommand=cmd(stoptweening;x,adjustp1x+cx+305-160;y,37;settext,"";zoom,.65;horizalign,'HorizAlign_Left';playcommand,'RefreshText');
	RefreshTextCommand=function(self)
		if GAMESTATE:GetCurrentGroup() ~= "music train"  and GAMESTATE:GetCurrentGroup() ~= "random" then
			local cur_song = GAMESTATE:GetCurrentSong();
			local cur_steps = GAMESTATE:GetCurrentSteps(PLAYER_2);
			if cur_steps == nil then return end;
			local HSList = PROFILEMAN:GetMachineProfile():GetHighScoreList(cur_song,cur_steps):GetHighScores();
			--local HSList = PROFILEMAN:GetProfile(PLAYER_2):GetHighScoreList(cur_song,cur_steps):GetHighScores();
			if (#HSList ~= 0) then
				local name = HSList[1]:GetName();	
				--local score = HSList[1]:GetScore();	
				self:settext(string.upper(name));
			else
				self:settext(" ");
			end;
		else
			local cur_song = GAMESTATE:GetCurrentCourse();
			local crs = GAMESTATE:GetCurrentCourse();
			if crs == nil then return ""; end;

			local trails = crs:GetAllTrails();
			local cur_steps = trails[1];
			
			local HSList = PROFILEMAN:GetMachineProfile():GetHighScoreList(cur_song,cur_steps):GetHighScores();
			--local HSList = PROFILEMAN:GetProfile(PLAYER_2):GetHighScoreList(cur_song,cur_steps):GetHighScores();
			if (#HSList ~= 0) then
				local name = HSList[1]:GetName();	
				--local score = HSList[1]:GetScore();	
				self:settext(string.upper(name));
			else
				self:settext(" ");
			end;
		end
	end;
	OffCommand=cmd(stoptweening;visible,false);
};

end;

return t;
