local Player_1 = 'PlayerNumber_P1';
local Player_2 = 'PlayerNumber_P2';

local function PlayerMessage( pn )
	local xpos = {122, 122, 210, 242};	-- [back, text, lim, timer] positions 
	local horz;
	local factor; 
	
	if pn == Player_1 then
		horz = 'HorizAlign_Left';
		factor = -3;
	elseif pn == Player_2 then
		xpos = { SCREEN_WIDTH-xpos[1],SCREEN_WIDTH-xpos[2],SCREEN_WIDTH-xpos[3], SCREEN_WIDTH-xpos[4] }; 
		horz = 'HorizAlign_Right';
		factor = 3;
	else return;
	end;
		
	local t = Def.ActorFrame {
		LoadActor(THEME:GetPathG("","Messages/spinner"))..{
			InitCommand=cmd(x,xpos[2];y,SCREEN_HEIGHT-64;zoom,.75;diffusealpha,0;pause;animate,0);
			OnCommand=cmd(sleep,.1;linear,.1;diffusealpha,0); 
			ProfileAlreadyLoadedMessageCommand=function(self,params)
				if params.Player == pn then
					(cmd(diffusealpha,0))(self);
				end;
			end;
			StorageDevicesChangedMessageCommand=function(self)

				if MEMCARDMAN:GetCardState(pn) == 'MemoryCardState_checking' then
					(cmd(play;animate,1;setstate,0;diffusealpha,1))(self);
				end;

				if MEMCARDMAN:GetCardState(pn) == 'MemoryCardState_ready' then
					(cmd(pause;animate,0;setstate,0;diffusealpha,0))(self);					
				end;
			end;
		};

		LoadActor(THEME:GetPathG("","Messages/back"))..{
			InitCommand=cmd(x,xpos[1]*factor;y,SCREEN_HEIGHT-64);
			OnCommand=cmd(linear,.4;x,xpos[1];zoom,.75);
			ProfileAlreadyLoadedMessageCommand=function(self,params)
				if params.Player == pn then
					(cmd(visible,false))(self);
				end;
			end;
			StorageDevicesChangedMessageCommand=function(self)

				if MEMCARDMAN:GetCardState(pn) == 'MemoryCardState_checking' then
					(cmd(diffusealpha,0))(self);
				end;			
			end;
		};

		LoadActor(THEME:GetPathG("","Messages/text"))..{
			InitCommand=cmd(x,xpos[1]*factor;y,SCREEN_HEIGHT-64);
			OnCommand=cmd(linear,.4;x,xpos[1];zoom,.75);
			ProfileAlreadyLoadedMessageCommand=function(self,params)
				if params.Player == pn then
					(cmd(visible,false))(self);
				end;
			end;
			StorageDevicesChangedMessageCommand=function(self)

				if MEMCARDMAN:GetCardState(pn) == 'MemoryCardState_checking' then
					(cmd(diffusealpha,0))(self);
				end;				
			end;
		};
	}; 
	return t; 
end;


local t = Def.ActorFrame {
	LoadActor( "GAME_MODE loop.wav") .. {
		OnCommand=cmd(play);
	};
}

t[#t+1] =LoadActor("fondo") .. {
	InitCommand=cmd(Center;zoomto,SCREEN_WIDTH,SCREEN_HEIGHT);
};

t[#t+1] = Def.ActorFrame {

	-- Mensajes de que jugador se unio al juego
	PlayerJoinedMessageCommand=function( self, params )
		if (params.Player == 'PlayerNumber_P1') then
			MESSAGEMAN:Broadcast('Player1Joined'); end;
		if (params.Player == 'PlayerNumber_P2') then
			MESSAGEMAN:Broadcast('Player2Joined'); end;
	end;

	ContinueCommand = function(self)
		SCREENMAN:GetTopScreen():Finish();
	end;
}

t[#t+1] = LoadActor( THEME:GetPathS("","titlescreen_start") )..{
	FinishSelectProfileMessageCommand=function (self) 
		self:play();		
		self:queuecommand('Continue'); --delay
	end;

	ContinueCommand = function(self)
		SCREENMAN:GetTopScreen():Finish();
	end;
};

--1st Player
t[#t+1] = PlayerMessage(Player_1)..{ FinishSelectProfileMessageCommand=cmd(zoom,0); };	--Si se une al juego el Player1, no mostrar mas 

--2nd Player
t[#t+1] = PlayerMessage(Player_2)..{ FinishSelectProfileMessageCommand=cmd(zoom,0); };	--Si se une al juego el Player1, no mostrar mas
	
t[#t+1] = LoadFont("MenuTimer")..{
	OnCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_TOP+15;zoom,0.78;queuecommand,"tiempo");	
	tiempoCommand=function(self)
		local tiempesito = math.floor(SCREENMAN:GetTopScreen():GetChild("Timer"):GetSeconds())
		if tiempesito < 16 then
			self:diffusealpha(1);
		end;
		if tiempesito < 10 then
			self:settext("0"..tiempesito);		
		else 
			self:settext(tiempesito);
		end;
		self:sleep(0.25);
		self:queuecommand("tiempo");
	end;
};
	
t[#t+1] =LoadActor("1")..{
	InitCommand=cmd(x,SCREEN_CENTER_X-320;y,SCREEN_CENTER_Y;zoom,0.7);
	OnCommand=cmd(x,SCREEN_CENTER_X-320;y,SCREEN_CENTER_Y;zoom,0.7;linear,0.3;x,SCREEN_CENTER_X-320;y,SCREEN_CENTER_Y);
};

t[#t+1] =LoadActor("2")..{
	InitCommand=cmd(x,SCREEN_CENTER_X+300;y,SCREEN_CENTER_Y;zoom,0.7);
	OnCommand=cmd(x,SCREEN_CENTER_X+300;y,SCREEN_CENTER_Y;zoom,0.7;linear,0.3;x,SCREEN_CENTER_X+300;y,SCREEN_CENTER_Y);
};

t[#t+1] =LoadActor("text")..{
	InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;zoom,0.75;diffusealpha,0);
	OnCommand=cmd(linear,0.2;diffusealpha,1;zoom,.68);
};

t[#t+1] =LoadActor("text")..{
	InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-100;zoom,0.68;diffusealpha,0);
	OnCommand=cmd(linear,0.2;y,SCREEN_CENTER_Y;diffusealpha,1;zoom,0.68;linear,.3;diffusealpha,0;zoom,0.8);
};

t[#t+1] =LoadActor("text")..{
	InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y+100;zoom,0.68;diffusealpha,0);
	OnCommand=cmd(linear,0.2;y,SCREEN_CENTER_Y;diffusealpha,1;zoom,0.68;linear,.31;diffusealpha,0;zoom,0.75);
};
	
t[#t+1] = LoadActor("profiles.lua");

return t;
