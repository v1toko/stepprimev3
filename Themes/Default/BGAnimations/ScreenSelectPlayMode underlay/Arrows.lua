local t = Def.ActorFrame{}

--- // Top Arrows



t[#t+1] = LoadActor("../_common/UpLeft_Japanese")..{
	InitCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X-45;y,SCREEN_CENTER_Y-45;sleep,0.1;bouncebegin,0.2;linear,0.1;diffusealpha,0.5);
	OnCommand=cmd(linear,0.2;diffusealpha,1;x,SCREEN_LEFT+45;y,SCREEN_CENTER_Y-195);
	MenuUpP1MessageCommand=cmd(stoptweening;glow,.5,1,1,.5;x,SCREEN_LEFT+40;y,SCREEN_CENTER_Y-200;sleep,0.12;decelerate,0.4;glow,1,1,1,0;x,SCREEN_LEFT+45;y,SCREEN_CENTER_Y-195);
	MenuUpP2MessageCommand=cmd(stoptweening;glow,.5,1,1,.5;x,SCREEN_LEFT+40;y,SCREEN_CENTER_Y-200;sleep,0.12;decelerate,0.4;glow,1,1,1,0;x,SCREEN_LEFT+45;y,SCREEN_CENTER_Y-195);		
	
}
t[#t+1] = LoadActor("../_common/UpRight_Japanese")..{
	InitCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X+45;y,SCREEN_CENTER_Y-45;sleep,0.1;bouncebegin,0.2;linear,0.1;diffusealpha,0.5);
	OnCommand=cmd(linear,0.2;diffusealpha,1;x,SCREEN_RIGHT-45;y,SCREEN_CENTER_Y-195);
	MenuDownP1MessageCommand=cmd(stoptweening;glow,.5,1,1,.5;x,SCREEN_RIGHT-40;y,SCREEN_CENTER_Y-200;sleep,0.12;decelerate,0.4;glow,1,1,1,0;x,SCREEN_RIGHT-45;y,SCREEN_CENTER_Y-195);
	MenuDownP2MessageCommand=cmd(stoptweening;glow,.5,1,1,.5;x,SCREEN_RIGHT-40;y,SCREEN_CENTER_Y-200;sleep,0.12;decelerate,0.4;glow,1,1,1,0;x,SCREEN_RIGHT-45;y,SCREEN_CENTER_Y-195);
}

--- // Bottom Arrows

t[#t+1] = LoadActor("../_common/DownLeft_Japanese")..{
	InitCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X-45;y,SCREEN_CENTER_Y+45;sleep,0.1;bouncebegin,0.2;linear,0.1;diffusealpha,0.5);
	OnCommand=cmd(linear,0.2;diffusealpha,1;x,SCREEN_LEFT+45;y,SCREEN_CENTER_Y+195);
}
t[#t+1] = LoadActor("../_common/DownRight_Japanese")..{
	InitCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X+45;y,SCREEN_CENTER_Y+45;sleep,0.1;bouncebegin,0.2;linear,0.1;diffusealpha,0.5);
	OnCommand=cmd(linear,0.2;diffusealpha,1;x,SCREEN_RIGHT-45;y,SCREEN_CENTER_Y+195);
}


return t;