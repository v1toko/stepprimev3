local delay = 4.5;

--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-- Para que guarde los datos en la profile de la maquina
--PROFILEMAN:SaveMachineProfile();

--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function GradetoInt(grade)
	if (grade=='Grade_Failed') then return 8; end;
	if (grade=='Grade_Tier01') then return 1; end;
	if (grade=='Grade_Tier02') then return 2; end;
	if (grade=='Grade_Tier03') then return 3; end;
	if (grade=='Grade_Tier04') then return 4; end;
	if (grade=='Grade_Tier05') then return 5; end;
	if (grade=='Grade_Tier06') then return 6; end;
	if (grade=='Grade_Tier07') then return 8; end;
	--Si no es ninguno de los anteriores entonces
	return 8;
end;
local grades = { 'SS','S','Sp','A','B','C','D','F' };

--misses qls no sirven de nada, si ya controlamos desde getgrade
function filtro(grade,misses)
	--if GradetoInt(grade) <=3 and misses ~= 0 then return grades[4]; end;
	return grades[ GradetoInt(grade) ];
end;

function GetAnnouncer()
	local p1_joined = GAMESTATE:IsSideJoined('PlayerNumber_P1');
	local p2_joined = GAMESTATE:IsSideJoined('PlayerNumber_P2');
	
	if( p1_joined and p2_joined ) then
		local g1 = GradetoInt( STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_1):GetGrade()  );
		local g2 = GradetoInt( STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_2):GetGrade()  );
	
		if( g1 <= g2 ) then
			return grades[g1];
		else
			return grades[g2];
		end;
	end;
		
	if( p1_joined and not p2_joined) then
		return filtro( STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_1):GetGrade() , STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_1):GetTapNoteScores('TapNoteScore_Miss') ); end;
	
	if( not p1_joined and p2_joined) then
		return filtro( STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_2):GetGrade() , STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_2):GetTapNoteScores('TapNoteScore_Miss') ); end;
end;

function ShowForPlayer( pn )
	return GAMESTATE:IsSideJoined(pn);
end;

--  Da prioridad a la carga de title's con el nombre
-- title.png o title.jpg. Si no existen alguno de estos, carga el background 
function GetPath()
	local song = GAMESTATE:GetCurrentSong();
	local songdir = song:GetSongDir().."title";
	local bgpath = song:GetBackgroundPath();
	
	if( FILEMAN:DoesFileExist(songdir..".png")  or  FILEMAN:DoesFileExist(songdir..".jpg")  ) then
		return (songdir);
	else
		if not bgpath then
			return (THEME:GetPathG("Common", "fallback background"));
		else
			return bgpath;
		end;
	end
end;

--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

local t = Def.ActorFrame {};


t[#t+1] = 	LoadActor(GetPath())..{
				InitCommand=cmd(stretchto,0,0,SCREEN_WIDTH,SCREEN_HEIGHT;diffusealpha,.4);
			};	 

t[#t+1] = 	LoadActor("_timer")..{
				InitCommand=cmd(zoom,0.7;x,SCREEN_CENTER_X;y,-20;decelerate,.15;y,5);
				OnCommand=cmd(diffusealpha,1;visible,true);
				OffCommand=cmd(x,SCREEN_CENTER_X;y,5;decelerate,.05;y,-58);
			}; 

t[#t+1] = 	LoadActor("_scores") .. { 
				OnCommand=cmd(addy,5);
			};

t[#t+1] = 	LoadActor("quest_grade")..{
				OnCommand=cmd(x,SCREEN_CENTER_X;y,45;zoom,.75);
			};
	
t[#t+1] = 	LoadActor("quest_grade_bg")..{
				OnCommand=cmd(x,SCREEN_CENTER_X;y,75;zoom,.75);
			};

t[#t+1] = 	LoadFont("Special/_myriad pro 20px") .. {
				InitCommand=cmd(stoptweening;);
				OnCommand=function(self)
					local song = GAMESTATE:GetCurrentSong();

					self:settext(song:GetDisplayMainTitle() .. " - " .. GAMESTATE:GetCurrentSteps(GAMESTATE:GetMasterPlayerNumber()):GetMissionGoal());
					(cmd(x,SCREEN_CENTER_X;y,75;zoom,.8;diffusealpha,0;linear,delay*.1;diffusealpha,1))(self)
				end;
			};

t[#t+1] = 	LoadFont("Special/_myriad pro 20px") .. {
				InitCommand=cmd(stoptweening;diffuse,0.35,0.89,0.98,1;);
				OnCommand=function(self)
					local song = GAMESTATE:GetCurrentSong();

					self:settext(song:GetChapter());
					(cmd(x,SCREEN_CENTER_X;y,100;zoom,.8;diffusealpha,0;linear,delay*.1;diffusealpha,1))(self)
				end;
			};

t[#t+1] = 	LoadActor("_grades") .. {
				InitCommand=cmd(addy,-12);
			};

t[#t+1] = 	LoadActor("_diff_FM") .. {
				InitCommand=cmd(y,SCREEN_BOTTOM+200;sleep,1);	
				OnCommand=cmd(diffusealpha,1;linear,.2;y,SCREEN_BOTTOM-90);
				OffCommand=cmd();
			};

t[#t+1] = 	LoadActor("_profile") .. {
				OnCommand=cmd(addy,5);
			};

if STATSMAN:GetCurStageStats():GetPlayerStageStats(GAMESTATE:GetMasterPlayerNumber()):GetMissionFailed() then
	t[#t+1] = 	LoadActor('quest_success')..{
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y+36;zoom,.8;setstate,1;animate,0;pause;diffusealpha,0;zoom,1;sleep,delay+2;zoom,1.3;decelerate,.1;zoom,1;diffusealpha,1);
		OffCommand=cmd(linear,.1;diffusealpha,0);
	};
	t[#t+1] = 	LoadActor('quest_success')..{
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y+36;zoom,.8;setstate,1;animate,0;pause;zoom,1;blend,'BlendMode_Add');
		OnCommand=cmd(diffusealpha,0;sleep,delay+2+.1;linear,.1;diffusealpha,1;linear,.6;diffusealpha,0);
	};
	t[#t+1] = 	LoadActor('quest_success')..{
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y+36;zoom,.8;setstate,1;animate,0;pause;zoom,1;blend,'BlendMode_Add');
		OnCommand=cmd(diffusealpha,0;sleep,delay+2;zoom,1.3;decelerate,.1;zoom,1;diffusealpha,1;linear,.3;zoom,1.5;diffusealpha,0);
	};
else
	t[#t+1] = 	LoadActor('quest_success')..{
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y+36;zoom,.8;setstate,0;animate,0;pause;diffusealpha,0;zoom,1;sleep,delay+2;zoom,1.3;decelerate,.1;zoom,1;diffusealpha,1);
		OffCommand=cmd(linear,.1;diffusealpha,0);
	};
	t[#t+1] = 	LoadActor('quest_success')..{
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y+36;zoom,.8;setstate,0;animate,0;pause;zoom,1;blend,'BlendMode_Add');
		OnCommand=cmd(diffusealpha,0;sleep,delay+2+.1;linear,.1;diffusealpha,1;linear,.6;diffusealpha,0);
	};
	t[#t+1] = 	LoadActor('quest_success')..{
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y+36;zoom,.8;setstate,0;animate,0;pause;zoom,1;blend,'BlendMode_Add');
		OnCommand=cmd(diffusealpha,0;sleep,delay+2;zoom,1.3;decelerate,.1;zoom,1;diffusealpha,1;linear,.3;zoom,1.5;diffusealpha,0);
	};
end

return t;