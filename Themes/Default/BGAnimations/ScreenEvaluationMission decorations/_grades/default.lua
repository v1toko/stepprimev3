local t = Def.ActorFrame {};

function GradeActor(pn,delay)
	local grade = filtro( STATSMAN:GetCurStageStats():GetPlayerStageStats(pn):GetGrade() , 
						  STATSMAN:GetCurStageStats():GetPlayerStageStats(pn):GetTapNoteScores('TapNoteScore_Miss')  -- Ingresa como parámetro la cantidad de miss steps
						 );
	return Def.ActorFrame {
		LoadActor(grade..'.png')..{
			InitCommand=cmd(diffusealpha,0;zoom,1;sleep,delay;zoom,1.3;decelerate,.1;zoom,1;diffusealpha,1);
			OffCommand=cmd(linear,.1;diffusealpha,0);
		};
		LoadActor(grade..'.png')..{
			InitCommand=cmd(zoom,1;blend,'BlendMode_Add');
			OnCommand=cmd(diffusealpha,0;sleep,delay+.1;linear,.1;diffusealpha,1;linear,.6;diffusealpha,0);
		};
		LoadActor(grade..'.png')..{
			InitCommand=cmd(zoom,1;blend,'BlendMode_Add');
			OnCommand=cmd(diffusealpha,0;sleep,delay;zoom,1.3;decelerate,.1;zoom,1;diffusealpha,1;linear,.3;zoom,1.5;diffusealpha,0);
		};
	};
end;

t[#t+1] = GradeActor(GAMESTATE:GetMasterPlayerNumber(),4.5)..{
	OnCommand=cmd(stoptweening;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-30;zoom,.75;sleep,4.5;sleep,.6;decelerate,.2;zoom,.75;);
};

return t;
