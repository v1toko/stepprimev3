local t = Def.ActorFrame {}

-- determina si el corazón debe mostrarse con el efecto fadein/fadeout
function ShowHeartOnStageEffect2(heartIndex, pn)
	heartsPlayed = GAMESTATE:GetNumStagesLeft(pn);
	heartsLeft = PREFSMAN:GetPreference('SongsPerPlay') + (heartsPlayed - PREFSMAN:GetPreference('SongsPerPlay'));
	stagesForThisSong = GAMESTATE:GetNumStagesForCurrentSongAndStepsOrCourse();	

	adjust = 0

	if stagesForThisSong == 4 then
		adjust = 0
	end

	if stagesForThisSong == 2 then
		adjust = 1
	end	

	if stagesForThisSong == 1 then
		adjust = 2
	end

	if (heartsLeft - stagesForThisSong)-adjust < heartIndex-1 then
		return true;
	end;

	return false;
end;

--frame que define el background y font para el nombre
local function PlayerName2( Player )
	local xpos;
	local ypos;
	local horz; 
	local adx;
	local ady;
	local hdx;
	local hdy;
	local avdy;
	local avdx;
	local adjustx;
	local pn = (Player == PLAYER_1) and 1 or 2;	--pn = 1 o 2, dependiendo del nº del jugador
	
	if Player == PLAYER_1 then
		horz = 'HorizAlign_Left'; 
		xpos = 40;
		ypos = -5;
		adx = 13;
		ady = 12;
		hdx = 49.5;
		hdy = -1;
		avdy = 5;
		avdx = -35;
		adjustx = -3;
	elseif Player == PLAYER_2 then
		horz = 'HorizAlign_Right'; 
		xpos = -90;
		ypos = -5;
		adx = 7;
		ady = 12;
		hdx = 22;
		hdy = -1;
		avdy = 5;
		avdx = -60;
		adjustx = 85;
	else 
		return;
	end;

---	--- --- --- --- --- --- ---	--- --- --- --- --- --- ---	--- --- --- --- --- ---
	local t = Def.ActorFrame {		 
		LoadActor(THEME:GetPathG("","Messages/avatars"))..{
			Name = "Avatars";
			InitCommand=cmd(horizalign,horz; zoom,.75; x, xpos+avdx; y, ypos+avdy; setstate, 0; animate, 0; pause);	
		};

		LoadActor(THEME:GetPathG("","Messages/player_name"))..{
			InitCommand=cmd(horizalign,horz; zoom,.75;);
		};

		LoadFont("","_youth 20px") .. {
			Name = "Name";
			InitCommand=cmd(horizalign,horz;x,xpos+adjustx;y,ypos;zoom,0.6);
		}; 

		LoadFont("","lvl_numbers") .. {
			Name = "Level";
			InitCommand=cmd(horizalign,horz;x,xpos+adx;y,ypos+ady;zoom,0.65;settext,"00000");
		};		
	};

	local songs = PREFSMAN:GetPreference('SongsPerPlay');
	for i=0,songs-1 do
		t[#t+1] = LoadActor(THEME:GetPathG("","Messages/heart")) .. {
			InitCommand=cmd(horizalign,horz;x,xpos+adx+hdx+(14.3*i);y,ypos+ady+hdy;zoom,0.75);
			LoopCommand=cmd(stoptweening;sleep,2;zoom,2;diffusealpha,0;linear,0.4;diffusealpha,1;zoom,.75;linear,.3;zoom,.85;linear,.2;zoom,.75);
			OnCommand=function(self, params)		
				if not GAMESTATE:IsPlayerEnabled(Player) then return end;

				(cmd(ShowHeartOnStage,i,Player))(self);

				if ShowHeartOnStageEffect2(i, Player) then
					if not PREFSMAN:GetPreference("InfiniteHearts") then
						self:playcommand("Loop");
					end;
				end;
			end;
		};
	end;
	
	return t;
end;

-- Player 1 Name
t[#t+1] = PlayerName2( PLAYER_1 ) .. {
	InitCommand=cmd(x,SCREEN_CENTER_X-330;y,SCREEN_HEIGHT+100);
	OnCommand=function(self)
		if GAMESTATE:IsPlayerEnabled('PlayerNumber_P1') and MEMCARDMAN:GetCardState('PlayerNumber_P1') == 'MemoryCardState_ready' then		
			(cmd(stoptweening;sleep,0.5;decelerate,.3;y,SCREEN_HEIGHT-20))(self); -- para el frame en si

			local name = self:GetChild("Name");		-- si la memoria ya esta lista, carga el nombre
			local temp = MEMCARDMAN:GetName('PlayerNumber_P1');
			if temp ~= nil then
				name:settext( string.upper(string.sub(temp,1,10)) ); --corta el nombre hasta 10 digitos
			end;

			local avatar = self:GetChild("Avatars");
			local index = MEMCARDMAN:GetAvatarIndex('PlayerNumber_P1');
			avatar:setstate(index);

			local level = self:GetChild("Level");
			local index = MEMCARDMAN:GetLevel('PlayerNumber_P1');
			level:settext(string.format("%05d",index));

			(cmd(stoptweening;y,SCREEN_HEIGHT-20))(self); -- para el frame en si
		elseif GAMESTATE:IsPlayerEnabled('PlayerNumber_P1') and MEMCARDMAN:GetCardState('PlayerNumber_P1') == 'MemoryCardState_none' then	
			(cmd(stoptweening;sleep,0.5;decelerate,.3;y,SCREEN_HEIGHT-20))(self); -- para el frame en si

			--GAMESTATE:JoinPlayer(params.PlayerNumber); 

			local name = self:GetChild("Name");		-- si la memoria ya esta lista, carga el nombre
			local temp = "PUMPITUP"
			name:settext( string.upper(string.sub(temp,1,10)) ); --corta el nombre hasta 10 digitos

			local avatar = self:GetChild("Avatars");
			local index = 0
			avatar:setstate(index);

			local level = self:GetChild("Level");
			local index = 0
			level:settext(string.format("%05d",index));

			(cmd(stoptweening;y,SCREEN_HEIGHT-20))(self); -- para el frame en si
		end;
	end;
};
	
-- Player 2 Name
t[#t+1] = PlayerName2( PLAYER_2 ) .. {
	InitCommand=cmd(x,SCREEN_CENTER_X+330;y,SCREEN_HEIGHT+100);
	OnCommand=function(self)
		if GAMESTATE:IsPlayerEnabled('PlayerNumber_P2') and MEMCARDMAN:GetCardState('PlayerNumber_P2') == 'MemoryCardState_ready' then		
			(cmd(stoptweening;sleep,0.5;decelerate,.3;y,SCREEN_HEIGHT-20))(self); -- para el frame en si

			local name = self:GetChild("Name");		-- si la memoria ya esta lista, carga el nombre
			local temp = MEMCARDMAN:GetName('PlayerNumber_P2');
			if temp ~= nil then
				name:settext( string.upper(string.sub(temp,1,10)) ); --corta el nombre hasta 10 digitos
			end;

			local avatar = self:GetChild("Avatars");
			local index = MEMCARDMAN:GetAvatarIndex('PlayerNumber_P2');
			avatar:setstate(index);

			local level = self:GetChild("Level");
			local index = MEMCARDMAN:GetLevel('PlayerNumber_P2');
			level:settext(string.format("%05d",index));

			(cmd(stoptweening;y,SCREEN_HEIGHT-20))(self); -- para el frame en si
		elseif GAMESTATE:IsPlayerEnabled('PlayerNumber_P2') and MEMCARDMAN:GetCardState('PlayerNumber_P2') == 'MemoryCardState_none' then	
			(cmd(stoptweening;sleep,0.5;decelerate,.3;y,SCREEN_HEIGHT-20))(self); -- para el frame en si

			--GAMESTATE:JoinPlayer(params.PlayerNumber); 

			local name = self:GetChild("Name");		-- si la memoria ya esta lista, carga el nombre
			local temp = "PUMPITUP"
			name:settext( string.upper(string.sub(temp,1,10)) ); --corta el nombre hasta 10 digitos

			local avatar = self:GetChild("Avatars");
			local index = 0
			avatar:setstate(index);

			local level = self:GetChild("Level");
			local index = 0
			level:settext(string.format("%05d",index));

			(cmd(stoptweening;y,SCREEN_HEIGHT-20))(self); -- para el frame en si
		end;
	end;
};

local l = GAMESTATE:GetNumHeatsGained(PLAYER_1) + GAMESTATE:GetNumHeatsGained(PLAYER_2);

t[#t+1] = LoadActor(THEME:GetPathS("", "HEART_PLUS")) .. {
			OnCommand=function(self, params)		
				if l > 0 then
					(cmd(sleep,2;queuecommand,'Play'))(self);
				end
			end;
			PlayCommand=cmd(play);
		}
	

return t;