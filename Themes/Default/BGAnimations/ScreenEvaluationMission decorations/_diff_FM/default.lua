local t = Def.ActorFrame {};

cx = (SCREEN_WIDTH/2);
cy = (SCREEN_HEIGHT/2);

--///////////////////////////////////////////////////////////////////////////////////////////
--///////////////////////////////////////////////////////////////////////////////////////////
local backSelectSteps = cmd(stoptweening;decelerate,.3;diffusealpha,.3);
local ballSelectSteps = cmd(stoptweening;decelerate,.3;diffusealpha,1);
--///////////////////////////////////////////////////////////////////////////////////////////
--///////////////////////////////////////////////////////////////////////////////////////////

function GetStep()
	local song = GAMESTATE:GetCurrentSong();

	aSteps = SongUtil.GetPlayableSteps(song);
	numSteps = #aSteps;

	local pn = GAMESTATE:GetMasterPlayerNumber();
	steps = GAMESTATE:GetCurrentSteps(pn);

	for j=1,numSteps do
		if steps == aSteps[j] then 
			return j;
		end;
	end;

	return 0;
end;

function GetDiffNumber(pn)

	if GAMESTATE:GetCurrentGroup() ~= "music train"  and GAMESTATE:GetCurrentGroup() ~= "random" then
		--Clasificacion de pasos x estilo, single, double, couple o halfdouble
		--todo "StepsType_Pump_Couple"
		
		--Cada estilo posee, beginner, easy, medium, hard, challenge, y edit.
		dif = GAMESTATE:GetCurrentSteps(pn):GetDifficulty();
		style = GAMESTATE:GetCurrentSteps(pn):GetStepsType();
		meter = GAMESTATE:GetCurrentSteps(pn):GetMeter();
		desc = GAMESTATE:GetCurrentSteps(pn):GetDescription();

		if string.find(desc, "SP") ~= nil then return 1 end;
		if string.find(desc, "DP") ~= nil then return 3 end;

		--Cada estilo posee, beginner, easy, medium, hard, challenge, y edit.
		if style=='StepsType_Pump_Single' then return (0);
		--si tenemos un edit (performance > 10) es comun y corriente
		--elseif style=='StepsType_Pump_Single' and dif=='Difficulty_Edit' and meter > 10 then return(0);
		elseif style=='StepsType_Pump_Double' then return (2);
		--elseif style=='StepsType_Pump_Single' and dif=='Difficulty_Edit' then return(1); 
		elseif style=='StepsType_Pump_Routine' then return(3); 
		end;
	else
		local crs = GAMESTATE:GetCurrentCourse();
		if crs == nil then return ""; end;

		if crs:GetCourseStyle()[1] == "DOUBLE" then
			return 2;
		else
			return 0; 
		end
	end;
	
	return 0;
end;

--[[
function Actor:SetMeterValue(i,val,b)
	local num;	

	SCREENMAN:SystemMessage("asdoiuhasiud");

	--Warn("SetMeterValue: i: " .. tostring(i) .. " GetDiffNum(i): " .. GetDiffNum(i) .. " val: " .. tostring(val) .. " b: " .. tostring(b) .. " meter: " .. aSteps[i]:GetMeter());
	if (GetDiffNum(i)==val or b) and i <= numSteps then 				
		if GAMESTATE:GetCurrentGroup() == "music train" then 
			self:settext("??"); 
		else
			num=aSteps[i]:GetMeter();
			if num <=9 then 
				self:settext("0"..num); 
			else 
				if num < 50 then
					self:settext(num); 
				else
					self:settext("??"); 
				end
			end;
		end;		
	else 
		self:settext(""); 
	end;	
end;
]]

function Actor:settextbymetereval(pn)
	if GAMESTATE:GetCurrentGroup() == 'music train' then self:settext('??'); return; end;
	
	local meter = GAMESTATE:GetCurrentSteps(pn):GetMeter();
	if meter<=9 then 
		self:settext('0'..meter); 
	else 
		if meter < 50 then
			self:settext(meter); 
		else
			self:settext("??"); 
		end
	end;
end;

function Actor:ifsteps(player,style)
	if GetDiffNumber(player)~=style then
		self:visible(false);
	else
		self:visible(true);
	end;
end;

adjustp1x = 35;

--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--Player 2 - Back
if GAMESTATE:IsSideJoined(GAMESTATE:GetMasterPlayerNumber()) then

t[#t+1] = LoadActor("diff_ball_bg")..{
	InitCommand=cmd(x,cx+122-adjustp1x;zoom,.75);
	SetBallDiffCommand=cmd(stoptweening;);
	StartSelectingStepsMessageCommand=cmd(stoptweening;decelerate,.3;diffusealpha,1);
	--CurrentStepsP1ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'SetBallDiff');
	OffCommand=cmd(stoptweening;diffusealpha,1;sleep,.4;linear,.2;addy,200);
} 

t[#t+1] = LoadActor("diff_balls")..{
	InitCommand=cmd(x,cx+123-adjustp1x;zoom,.75;pause;animate,0;queuecommand,'SetBallDiff');
	SetBallDiffCommand=cmd(stoptweening;animate,0;pause;setstate,GetDiffNumber(GAMESTATE:GetMasterPlayerNumber()));
	StartSelectingStepsMessageCommand=cmd(stoptweening;playcommand,'SetBallDiff';decelerate,.3;diffusealpha,1);
	CurrentStepsP2ChangedMessageCommand=cmd(stoptweening;sleep,.1;queuecommand,'SetBallDiff');
	OffCommand=cmd(stoptweening;diffusealpha,1;sleep,.4;linear,.2;addy,200);
};

t[#t+1] = LoadActor("efx3")..{
	OnCommand=cmd(x,cx+123-adjustp1x;zoom,.7;diffusealpha,0;blend,'BlendMode_Add';diffusealpha,0);
	OffCommand=cmd(stoptweening;diffusealpha,0.7;decelerate,.2;diffusealpha,0);
};

t[#t+1] = LoadActor("efx2")..{
	OnCommand=cmd(x,cx+123-adjustp1x;zoom,.75;diffusealpha,0;blend,'BlendMode_Add';diffusealpha,0;queuecommand,'Repeat');
	RepeatCommand=cmd(stoptweening;rotationz,0;diffusealpha,0.225;linear,2;rotationz,-360;queuecommand,'Repeat');
	OffCommand=cmd(stoptweening;linear,.1;diffusealpha,0);
};

-- SingleNumbers (orange)
t[#t+1] = LoadFont("numbersorangebig")..{
	OnCommand=cmd(stoptweening;playcommand,'Set';x,cx+121-adjustp1x;y,23;zoom,0.75;);
	SetCommand=cmd(ifsteps,GAMESTATE:GetMasterPlayerNumber(),0;settextbymetereval,GAMESTATE:GetMasterPlayerNumber());
	StartSelectingStepsMessageCommand=cmd(stoptweening;ifsteps,GAMESTATE:GetMasterPlayerNumber(),0;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;);
	CurrentStepsP2ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'Set');
	OffCommand=cmd(stoptweening;diffusealpha,1;sleep,.4;linear,.2;addy,200);
};
-- DoubleNumbers( green )
t[#t+1] = LoadFont("numberspurplebig")..{
	OnCommand=cmd(stoptweening;playcommand,'Set';x,cx+121-adjustp1x;y,23;zoom,0.75;);
	SetCommand=cmd(ifsteps,GAMESTATE:GetMasterPlayerNumber(),1;settextbymetereval,GAMESTATE:GetMasterPlayerNumber());
	StartSelectingStepsMessageCommand=cmd(stoptweening;ifsteps,GAMESTATE:GetMasterPlayerNumber(),1;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;);
	CurrentStepsP2ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'Set');
	OffCommand=cmd(stoptweening;diffusealpha,1;sleep,.4;linear,.2;addy,200);
};
-- SinglePerformanceNumbers( green )
t[#t+1] = LoadFont("numbersgreenbig")..{
	OnCommand=cmd(stoptweening;playcommand,'Set';x,cx+121-adjustp1x;y,23;zoom,0.75;);
	SetCommand=cmd(ifsteps,GAMESTATE:GetMasterPlayerNumber(),2;settextbymetereval,GAMESTATE:GetMasterPlayerNumber());
	StartSelectingStepsMessageCommand=cmd(stoptweening;ifsteps,GAMESTATE:GetMasterPlayerNumber(),2;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;);
	CurrentStepsP2ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'Set');
	OffCommand=cmd(stoptweening;diffusealpha,1;sleep,.4;linear,.2;addy,200);
};
-- DoublePerformanceNumbers( blue )
t[#t+1] = LoadFont("numbersbluebig")..{
	OnCommand=cmd(stoptweening;playcommand,'Set';x,cx+121-adjustp1x;y,23;zoom,0.75;);
	SetCommand=cmd(ifsteps,GAMESTATE:GetMasterPlayerNumber(),3;settextbymetereval,GAMESTATE:GetMasterPlayerNumber());
	StartSelectingStepsMessageCommand=cmd(stoptweening;ifsteps,GAMESTATE:GetMasterPlayerNumber(),3;accelerate,.1;diffusealpha,1);
	SongUnchosenMessageCommand=cmd(stoptweening;accelerate,.1;decelerate,.1;);
	CurrentStepsP2ChangedMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.1;queuecommand,'Set');
	OffCommand=cmd(stoptweening;diffusealpha,1;sleep,.4;linear,.2;addy,200);
};

t[#t+1] = LoadActor("steps")..{
	OnCommand=cmd(stoptweening;animate,0;pause;setstate,GetStep()-1;x,cx+121-adjustp1x;y,-43;zoom,0.75;);
	OffCommand=cmd(stoptweening;decelerate,.1;diffuse,.9,.9,.9,1;sleep,0;decelerate,.1;diffusealpha,0);
};

end;

return t;
