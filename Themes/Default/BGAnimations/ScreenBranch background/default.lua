local t = Def.ActorFrame{
	InitCommand=cmd(playcommand,"Check");
	CoinModeChangedMessageCommand=cmd(playcommand,"Check");
	CoinInsertedMessageCommand=cmd(playcommand,"Check");
	CheckCommand=function(self)
		Check();
	end
}

local rmax = 7;
local num = math.random(1,rmax);

t[#t+1] = LoadActor( "PRIME_"..num )..{
InitCommand=cmd(draworder,1;Center;zoomto,SCREEN_WIDTH,SCREEN_HEIGHT;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;diffusealpha,1;decelerate,100;diffusealpha,1);
};
	
return t;
	
