local FileNames = {
	"_NDA_IRO";
	"LASH";
	"A";
	"M";
}
local initCmds = {
	cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;);
	cmd(x,SCREEN_CENTER_X-145;y,SCREEN_CENTER_Y+34);
	cmd(x,SCREEN_CENTER_X-100;y,SCREEN_CENTER_Y-2);
	cmd(x,SCREEN_CENTER_X+100;y,SCREEN_CENTER_Y-2);
}
local onCmds = {
	cmd(diffusealpha,0;sleep,3.65;linear,0.5;diffusealpha,1);
	cmd(diffusealpha,0;sleep,4.15;linear,0.5;diffusealpha,1;);
	cmd(diffusealpha,0;zoom,1;linear,2;diffusealpha,1;sleep,1;linear,0.65;zoom,0.28;x,SCREEN_CENTER_X-145;diffusealpha,1;);
	cmd(diffusealpha,0;zoom,1;linear,2;diffusealpha,1;sleep,1;linear,0.65;zoom,0.28;x,SCREEN_CENTER_X+58;diffusealpha,1;);
}

local t = Def.ActorFrame {}

for i=1,#FileNames do
	t[#t+1] = LoadActor(FileNames[i])..{
		InitCommand=initCmds[i];
		OnCommand=onCmds[i];
	}
end


return t

--SCREEN_CENTER_X
--SCREEN_CENTER_Y