return Def.ActorFrame {
	LoadActor(THEME:GetPathG("","background".."/USB_BACK.M2V"))..{
		InitCommand=cmd(FullScreen);	
	};
	
	Def.Quad {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;zoomto,SCREEN_WIDTH,SCREEN_HEIGHT;diffuse,0,0,0,1);
		OnCommand=cmd(stoptweening;diffusealpha,0;linear,.4;diffusealpha,.7);
		IncomingTitleMenuMessageCommand=cmd(stoptweening;sleep,.2;linear,.2;diffusealpha,0);
	};
};
