--[[
local t = Def.ActorFrame{
	OffCommand=function(self)
		--Desde ak, cuando termina la presentacion, pasa a la pantalla real de selecion de juego.
		MESSAGEMAN:Broadcast( "Finished" );
		--SCREENMAN:SetNewScreen("ScreenSelectPlayMode");
	end;	
}

t[#t+1] =Def.Quad {
	InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;zoomto,SCREEN_WIDTH,SCREEN_HEIGHT;diffuse,0,0,0,1;diffusealpha,0);
	OnCommand=cmd(stoptweening;linear,.4;diffusealpha,1);
};

t[#t+1] =LoadActor( "bg" )..{
	InitCommand=cmd(FullScreen;zoom,1;diffusealpha,0;decelerate,.2;zoom,.5;diffusealpha,1);
};

t[#t+1] =LoadActor( "bg" )..{
	InitCommand=cmd(FullScreen;blend,'BlendMode_Add';zoom,1;diffusealpha,0;decelerate,.2;zoom,.5;diffusealpha,.8;sleep,.1;decelerate,1;diffusealpha,0);
};

local base = cmd(diffusealpha,0;zoom,.47;sleep,.2;linear,.1;diffusealpha,1;decelerate,1;zoom,.5);  -- base comun
local shine  = cmd(blend,'BlendMode_Add';zoom,.47;diffusealpha,0;sleep,.2;linear,.1;diffusealpha,1;linear,.7;zoom,.5;diffusealpha,0);  --brillo normal
--local shine2 = cmd(blend,'BlendMode_Add';zoom,.47;diffusealpha,0;sleep,.2;linear,.1;diffusealpha,.5;linear,.7;zoom,.5;diffusealpha,0); --menos brillo
local shine2 = cmd(diffusealpha,0); --menos brillo
local glow = cmd(diffusealpha,0;zoom,1;sleep,.05;linear,.05;diffusealpha,1;decelerate,.1;zoom,.47;sleep,0;linear,.1;diffusealpha,0); -- inicial glow

--center
t[#t+1] =LoadActor( "common" )..{
	InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y);
	OnCommand=base;
};

t[#t+1] =LoadActor( "shine" )..{
	InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y);
	OnCommand=shine;
};

t[#t+1] =LoadActor( "common" )..{
	InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;blend,'BlendMode_Add');
	OnCommand=glow;
};

--arcade
t[#t+1] =LoadActor( "arcade" )..{
	InitCommand=cmd(x,SCREEN_CENTER_X-180;y,SCREEN_CENTER_Y+100);
	OnCommand=base;
};

t[#t+1] =LoadActor( "shine" )..{
	InitCommand=cmd(x,SCREEN_CENTER_X-180;y,SCREEN_CENTER_Y+100);
	OnCommand=shine2;
};

t[#t+1] =LoadActor( "arcade" )..{
	InitCommand=cmd(x,SCREEN_CENTER_X-180;y,SCREEN_CENTER_Y+100;blend,'BlendMode_Add');
	OnCommand=glow;
};

--battle
t[#t+1] =LoadActor( "battle" )..{
	InitCommand=cmd(x,SCREEN_CENTER_X+180;y,SCREEN_CENTER_Y+100);
	OnCommand=base;
};

t[#t+1] =LoadActor( "shine" )..{
	InitCommand=cmd(x,SCREEN_CENTER_X+180;y,SCREEN_CENTER_Y+100);
	OnCommand=shine2;
};

t[#t+1] =LoadActor( "battle" )..{
	InitCommand=cmd(x,SCREEN_CENTER_X+180;y,SCREEN_CENTER_Y+100;blend,'BlendMode_Add');
	OnCommand=glow;
};

-- timer
t[#t+1] =LoadActor( "timer" )..{
	InitCommand=cmd(x,SCREEN_CENTER_X+180;y,SCREEN_CENTER_Y-100);
	OnCommand=base;
};

t[#t+1] =LoadActor( "shine" )..{
	InitCommand=cmd(x,SCREEN_CENTER_X+180;y,SCREEN_CENTER_Y-100);
	OnCommand=shine;
};

t[#t+1] =LoadActor( "timer" )..{
	InitCommand=cmd(x,SCREEN_CENTER_X+180;y,SCREEN_CENTER_Y-100;blend,'BlendMode_Add');
	OnCommand=glow;
};

--upleft
t[#t+1] =LoadActor( "common" )..{
	InitCommand=cmd(x,SCREEN_CENTER_X-180;y,SCREEN_CENTER_Y-100);
	OnCommand=base;
};

t[#t+1] =LoadActor( "shine" )..{
	InitCommand=cmd(x,SCREEN_CENTER_X-180;y,SCREEN_CENTER_Y-100);
	OnCommand=shine2;
};

t[#t+1] =LoadActor( "common" )..{
	InitCommand=cmd(x,SCREEN_CENTER_X-180;y,SCREEN_CENTER_Y-100;blend,'BlendMode_Add');
	OnCommand=glow;
};

return t;
]]

return Def.ActorFrame {};