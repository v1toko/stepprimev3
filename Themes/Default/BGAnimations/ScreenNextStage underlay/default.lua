local rmax = 3;
local num = math.random(1,rmax);

return Def.ActorFrame {
	LoadActor(THEME:GetPathG("","background".."/NST"..num)) .. {
		InitCommand = cmd(Center;zoomto,SCREEN_WIDTH,SCREEN_HEIGHT);
		OnCommand = cmd(stoptweening;sleep,4);
	};
	LoadActor(THEME:GetPathS("","ActionSounds/nextstage")) .. {
		OnCommand=cmd(play);
	};
}