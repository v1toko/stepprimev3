local song = GAMESTATE:GetCurrentSong()
local course = GAMESTATE:GetCurrentCourse()

--assert(song,"WHY THIS HAPPENS LOL")
if GAMESTATE:GetCurrentGroup() ~= "music train" and GAMESTATE:GetCurrentGroup() ~= "random" then
	if not song then
		song = SONGMAN:GetRandomSong()
		GAMESTATE:SetCurrentSong( song )
		
		for k,v in ipairs(PlayerNumber) do
			local stepsplayer = GAMESTATE:GetCurrentSteps(v)
			steps = song:GetOneSteps( stepsplayer:GetStepsType() , stepsplayer:GetDifficulty() )
			GAMESTATE:SetCurrentSteps( steps )
		end
	end
end;

--  Da prioridad a la carga de title's con el nombre
-- title.png o title.jpg. Si no existen alguno de estos, carga el background 
function GetPath()
	if GAMESTATE:GetCurrentGroup() ~= "music train" and GAMESTATE:GetCurrentGroup() ~= "random" then
		local song = GAMESTATE:GetCurrentSong();
		local songdir = song:GetSongDir().."title";
		local bgpath = song:GetBackgroundPath();
		
		if( FILEMAN:DoesFileExist(songdir..".png")  or  FILEMAN:DoesFileExist(songdir..".jpg")  ) then
			return (songdir);
		else
			if not bgpath then
				return (THEME:GetPathG("Common", "fallback background"));
			else
				return bgpath;
			end;
		end
	else
		local crs = GAMESTATE:GetCurrentCourse();
		if crs == nil then return (THEME:GetPathG("Common", "fallback background")); end;
		
		local entries = GAMESTATE:GetCurrentTrail(GAMESTATE:GetMasterPlayerNumber()):GetTrailEntry(0);
		local song = entries:GetSong();

		local songdir = song:GetSongDir().."title";
		local bgpath = song:GetBackgroundPath();
		
		if( FILEMAN:DoesFileExist(songdir..".png")  or  FILEMAN:DoesFileExist(songdir..".jpg")  ) then
			return (songdir);
		else
			if not bgpath then
				return (THEME:GetPathG("Common", "fallback background"));
			else
				return bgpath;
			end;
		end
	end;
end;

return LoadActor(GetPath())..{
	InitCommand=cmd(stretchto,0,0,SCREEN_WIDTH,SCREEN_HEIGHT;);
	OnCommand=function(self)
		if GAMESTATE:IsBasicMode() then	--basic noteskin para el stage solamente
			local bns = THEME:GetMetric('Common','DefaultBasicNoteSkinName');	
			GAMESTATE:ApplyPreferredModifiers(PLAYER_1,bns);
			GAMESTATE:ApplyPreferredModifiers(PLAYER_2,bns);
		end;
	end;
};