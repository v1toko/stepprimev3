local function LifeBar(pn)
	local function update(self)
		local lifemeter = SCREENMAN:GetTopScreen():GetLifeMeter(pn):GetLife();
		local this = self:GetChildren();

		local top = math.floor(240*lifemeter-120);
		if(lifemeter >= 1) then top = 120; end;
		
		if( lifemeter <= 0.3 ) then
			(cmd(stoptweening;diffusealpha,1;x,top))(this.red);
			this.blue:diffusealpha(0);
			this.red_bar:diffusealpha(1);
            this.red_resplandor:diffusealpha(1);
		else
			(cmd(stoptweening;diffusealpha,1;x,top))(this.blue);
			this.red:diffusealpha(0);
			this.red_bar:diffusealpha(0);
            this.red_resplandor:diffusealpha(0);
		end;
		
		local greybartop = math.floor(-(40+top)/240);
		if( ((1-lifemeter)+.15) <= 0) then
			this.grey_bar:cropright(1);
		else
			this.grey_bar:cropright((1-lifemeter)+.15);
		end;
		
		if(lifemeter>=1) then
			this.grey_bar:cropright(1);
			this.blue_bar:cropright(1);
			this.colors:diffusealpha(1);
		else
			this.colors:diffusealpha(0);
			this.blue_bar:cropright(1-lifemeter);
		end;
	end;
		
	return Def.ActorFrame {
		InitCommand=cmd(player,pn;SetUpdateFunction,update);
		children = {
			LoadActor("bg_single")..{
				--InitCommand=cmd(x,-160;);
			};
	        LoadActor("danger_single")..{ 
				Name="red_resplandor"; 
				InitCommand=cmd(diffuseshift;effectperiod,.1;effectcolor1,1,1,1,1;effectcolor2,1,1,1,.5);
			};
			LoadActor("red_bar_single")..{ 
				Name="red_bar"; 
				InitCommand=cmd(diffuseshift;effectperiod,.1;effectcolor1,1,0,0,1;effectcolor2,1,0,0,1);
			};
			LoadActor("blue_bar_single")..{ 
				Name="blue_bar"; 
				InitCommand=cmd(horizalign,'HorizAlign_Left';x,-127;pulse;effectclock,"beat";effectmagnitude,.7,1,1;effectperiod,.5);
			};
			LoadActor("life_single")..{ 
				Name="grey_bar"; 
				InitCommand=cmd(horizalign,'HorizAlign_Left';x,-127;zoomy,0.7;zoomx,0.96);
			};
			LoadActor("life_single")..{ 
				Name="colors"; 
				InitCommand=cmd(horizalign,'HorizAlign_Left';x,-127;zoomy,0.7;zoomx,0.96;blend,'BlendMode_Add';diffuseshift;effectperiod,.1;effectcolor1,1,1,1,1;effectcolor2,1,1,1,.94;diffusealpha,0);
			};
			LoadActor("lifeframe_single")..{
				--InitCommand=cmd(x,-160;);
			};
			LoadActor("life_tip")..{
				Name="blue";
				InitCommand=cmd(diffuseshift;effectperiod,.1;effectcolor1,1,1,1,1;effectcolor2,1,1,1,.8);
			};
			LoadActor("life_tip_danger")..{
				Name="red";
				InitCommand=cmd(diffuseshift;effectperiod,.1;effectcolor1,1,1,1,1;effectcolor2,1,1,1,.8);
			};
		};
	};
end
------------------------BARRA DE VIDA DOUBLE---------------------------------------

local function LifeBardouble(pn)
	local function update(self)
		local lifemeter = SCREENMAN:GetTopScreen():GetLifeMeter(pn):GetLife();
		local this = self:GetChildren();

		local top = math.floor(480*lifemeter-238);
		if(lifemeter >= 1) then top = 243; end;
		
		if( lifemeter <= 0.3 ) then
			(cmd(stoptweening;diffusealpha,1;x,top))(this.red);
			this.blue:diffusealpha(0);
			this.danger:diffusealpha(1);
            this.caution:diffusealpha(1);
		else
			(cmd(stoptweening;diffusealpha,1;x,top))(this.blue);
			this.red:diffusealpha(0);
			this.danger:diffusealpha(0);
            this.caution:diffusealpha(0);
		end;
		
		local greybartop = math.floor(-(40+top)/240);
		if( ((1-lifemeter)+.15) <= 0) then
			this.SOLIDODOUBLE:cropright(1);
		else
			this.SOLIDODOUBLE:cropright((1-lifemeter)+.15);
		end;
		
		if(lifemeter>=1) then
			this.SOLIDODOUBLE:cropright(0);
			this.blue_bar:cropright(1);		
		else
			this.SOLIDODOUBLE:stopeffect();
			this.blue_bar:cropright(1-lifemeter);
		end;
	end;
		
	return Def.ActorFrame {
		InitCommand=cmd(player,pn;SetUpdateFunction,update);
		children = {
			LoadActor("bg_double")..{
				--InitCommand=cmd(x,-117);
			};
	        LoadActor("danger_double")..{ 
				Name="caution"; 
				InitCommand=cmd(diffuseshift;effectperiod,.1;effectcolor1,1,1,1,1;effectcolor2,1,1,1,.5);
			};
			LoadActor("red_bar_double")..{ 
				Name="danger"; 
				InitCommand=cmd(diffuseshift;effectperiod,.1;effectcolor1,1,0,0,1;effectcolor2,1,0,0,1);
			};
			LoadActor("blue_bar_double")..{ 
				Name="blue_bar"; 
				InitCommand=cmd(x,self:GetX()-self:GetWidth()/2;horizalign,'HorizAlign_Left';pulse;effectclock,"beat";effectmagnitude,.7,1,1;effectperiod,.9);
			};
			LoadActor("life_double")..{ 
				Name="SOLIDODOUBLE"; 
				InitCommand=cmd(x,self:GetX()+5-self:GetWidth()/2;horizalign,'HorizAlign_Left';zoomy,0.7; zoomx, 0.98);
			};
			--LoadActor("barhot_double")..{ 
			--	Name="barhot_double"; 
			--	InitCommand=cmd(horizalign,'HorizAlign_Left';x,-419;zoom,.5;zoomx,.49;zoomy,.48,'BlendMode_Add';diffuseshift;effectperiod,.1;effectcolor1,1,1,1,1;effectcolor2,1,1,1,.94;diffusealpha,0);
			--};--blend,'BlendMode_Add';
			LoadActor("lifeframe_double")..{
				--InitCommand=cmd(x,-160);
			};
			
			LoadActor("life_tip")..{
				Name="blue";
				InitCommand=cmd(blend,'BlendMode_Add';diffuseshift;effectperiod,.1;effectcolor1,1,1,1,1;effectcolor2,1,1,1,.8);
			};
			LoadActor("life_tip_danger")..{
				Name="red";
				InitCommand=cmd(diffuseshift;effectperiod,.1;effectcolor1,1,1,1,1;effectcolor2,1,1,1,.8);
			};
		};
	};
end

stage = 0;
if not GAMESTATE:IsCourseMode() then
	stage = STATSMAN:GetStagesPlayed()+1;
	if GAMESTATE:IsEventMode() or PREFSMAN:GetPreference("InfiniteHearts") or GAMESTATE:IsDemonstration() then
		stage = 0;
	end;
else
	if GAMESTATE:GetCurrentGroup() == "music train" then
		stage = GAMESTATE:GetCourseSongIndex()+2;
	else
		stage = STATSMAN:GetStagesPlayed()+1;
		if GAMESTATE:IsEventMode() or PREFSMAN:GetPreference("InfiniteHearts") or GAMESTATE:IsDemonstration() then
			stage = 0;
		end;
	end
	--por alguna razon en esta parte dice stage = -1
	--SCREENMAN:SystemMessage("stage: " .. tostring(stage));
end;

function GetStageNumberActor()
	local folder = "FullMode";
	--if GAMESTATE:GetCurrentGroup() == "quest zone" then folder = "QuestZone"; end;	
		
	return (THEME:GetPathG("","Stages_"..folder.."/stages"));
end

local t = Def.ActorFrame {};

local style = GAMESTATE:GetCurrentStyle():GetStyleType();
local pn;
if( GAMESTATE:IsSideJoined(PLAYER_1) ) then pn = PLAYER_1 else pn = PLAYER_2	 end;

if( (style=='StyleType_OnePlayerTwoSides') or (style=='StyleType_TwoPlayersSharedSides') ) then
	t[#t+1] = 	LoadActor(GetStageNumberActor())..{ 
					InitCommand=cmd(FromCenterX,-285;FromTop,20;zoom,.68);
					OnCommand=cmd(setstate,stage;animate,0);
					ChangeCourseSongOutMessageCommand=function(self)
						self:setstate(GAMESTATE:GetCourseSongIndex()+1);
					end;
				};

	t[#t+1] = 	LifeBardouble(pn)..{
					InitCommand=cmd(FromTop,20;x,THEME:GetMetric(Var "LoadingScreen","PlayerP1OnePlayerTwoSidesX")); 
				};
else
	t[#t+1] = 	LoadActor(GetStageNumberActor())..{ 
					InitCommand=cmd(CenterX;FromTop,20;zoom,.68);
					OnCommand=cmd(setstate,stage;animate,0);
					ChangeCourseSongOutMessageCommand=function(self)
						self:setstate(GAMESTATE:GetCourseSongIndex()+1);
					end;
				}; 

	t[#t+1] = LifeBar(PLAYER_1)..{ 
					Condition=GAMESTATE:IsPlayerEnabled(PLAYER_1); 
					InitCommand=cmd(FromTop,20;x,THEME:GetMetric(Var "LoadingScreen","PlayerP1OnePlayerOneSideX")); 
				};
	t[#t+1] = LifeBar(PLAYER_2)..{ 
					Condition=GAMESTATE:IsPlayerEnabled(PLAYER_2); 
					InitCommand=cmd(rotationy,180;FromTop,20;x,THEME:GetMetric(Var "LoadingScreen","PlayerP2OnePlayerOneSideX")); 
				};
end;

return t;