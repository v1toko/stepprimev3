-- Funciones de utilidad para el Theme Fiesta Ex - by xMAx

-- Dibuja una plataforma tipo pump, en xpos,ypos con una profundidad prof (inclinacion)
function PlatPiu (xpos,ypos, zoom_)
local plataforma = Def.ActorFrame{
	--OnCommand=cmd(stoptweening;SetDrawByZPosition,true;fov,prof;vanishpoint,xpos,ypos-36);
	children = {
--LoadActor(THEME:GetPathG("","newPlataform/soporte"))..{
--	InitCommand=cmd(stoptweening;x,xpos;y,ypos+29;zoom,.8);	
--};-
LoadActor(THEME:GetPathG("","newPlataform/plat"))..{
	InitCommand=cmd(stoptweening;x,xpos;y,ypos;diffusealpha,.8;zoom,zoom_);	
};
LoadActor(THEME:GetPathG("","newPlataform/center"))..{
	InitCommand=cmd(stoptweening;zoom,zoom_;x,xpos;y,ypos-5;blend,'BlendMode_Add';queuecommand,'Loop');
	LoopCommand=cmd(stoptweening;zoom,zoom_;diffusealpha,0;linear,.3;diffusealpha,.7;linear,.3;diffusealpha,0;zoom,zoom_+0.3;queuecommand,'Loop');
};
LoadActor(THEME:GetPathG("","newPlataform/arrow"))..{
	InitCommand=cmd(stoptweening;zoom,zoom_;x,xpos;y,ypos-60;queuecommand,'Loop');
	LoopCommand=cmd(stoptweening;y,ypos-90;decelerate,.3;y,ypos-50;decelerate,.4;y,ypos-90;queuecommand,'Loop');
};};};
return plataforma; 
end;

function SimplePlatPiu (xpos,ypos)
local plataforma = Def.ActorFrame{
	children = {
LoadActor(THEME:GetPathG("","simplePlataform/plat"))..{
	InitCommand=cmd(stoptweening;x,xpos;y,ypos;diffusealpha,.8);
	OffCommand=cmd(stoptweening);
};
LoadActor(THEME:GetPathG("","simplePlataform/center"))..{
	InitCommand=cmd(stoptweening;x,xpos;y,ypos-10;blend,'BlendMode_Add';queuecommand,'Loop');
	LoopCommand=cmd(stoptweening;diffusealpha,0;zoom,1;linear,.3;diffusealpha,.7;linear,.3;diffusealpha,0;zoom,1.3;queuecommand,'Loop');
	OffCommand=cmd(stoptweening);
};
LoadActor(THEME:GetPathG("","simplePlataform/arrow"))..{
	InitCommand=cmd(stoptweening;x,xpos;y,ypos-30;queuecommand,'Loop');
	LoopCommand=cmd(stoptweening;y,ypos-100;decelerate,.3;y,ypos-60;decelerate,.4;y,ypos-100;queuecommand,'Loop');
	OffCommand=cmd(stoptweening);
};};};
return plataforma; 
end;

-- Nuevos Numeros giratorios! Para el screen evaluation
-- Texto, Delay, Xlim_left, YLim_center, Horizaling
function NewRollingNumbers(text, del_text, xlim, ylim, horiza)
	local r = Def.ActorFrame {};
	
	if (horiza=='HorizAlign_Left') then
		for i=1,#text do
		r[#r+1] = LoadFont("_karnivore lite 20px")..{
			InitCommand=cmd(horizalign,horiza;x,xlim+(i-4)*13;y,ylim);
			OnCommand=cmd(settext,string.sub(text,i,i);diffusealpha,0;sleep,del_text + (i)*(.2);linear,.1;diffusealpha,1); 
		};
		end;
	else
		for i=1,#text do
		r[#r+1] = LoadFont("_karnivore lite 20px")..{
			InitCommand=cmd(horizalign,horiza;x,xlim-(i-4)*13;y,ylim);
			OnCommand=cmd(settext,string.sub(text,#text-(i-1),#text-(i-1));diffusealpha,0;sleep,del_text + (i)*(.2);linear,.1;diffusealpha,1);  
		};
		end;
	end;

	local dir;
	if (horiza=='HorizAlign_Left') then dir = 1; else dir = -1; end;
	
	for i=1,#text do
	r[#r+1] = Def.RollingNumbers {
		File = THEME:GetPathF("","_karnivore lite 20px");
		InitCommand=cmd(Load,"RollingNumbers";horizalign,horiza;targetnumber,0;x,xlim+dir*(i-4)*13;y,ylim;diffusealpha,0;sleep,del_text+(i-1)*(.2);queuecommand,'Lot');
		LotCommand=cmd(linear,.2;targetnumber,9;diffusealpha,1;sleep,0;linear,.1;diffusealpha,0);
	};
	end;
	
	--[[r[#r+1] = LoadFont("_karnivore lite 20px")..{
			InitCommand=cmd(horizalign,horiza;x,xlim-(i-1)*13;y,ylim;zoom,.45;zoomy,.38);
			OnCommand=cmd(settext,string.sub(text,#text-(i-1),#text-(i-1));diffusealpha,0;sleep,del_text + (i)*(.2);linear,.1;diffusealpha,1);  
		};]]
	
return r;
end;

--[[function GetIfPlayerProfileIsMachine(pn)
	prf = PROFILEMAN:GetProfile(pn);
	if PROFILEMAN:IsPersistentProfile(pn) then
	return prf:IsMachine();
end;]]

function Actor:BMvisible()
	if( GAMESTATE:IsBasicMode() ) then
		self:visible(true);
	else
		self:visible(false);
	end;
end;

function Actor:BMinvisible()
	if( GAMESTATE:IsBasicMode() ) then
		self:visible(false);
	else
		self:visible(true);
	end;
end;

function table.val_to_str ( v )
  if "string" == type( v ) then
    v = string.gsub( v, "\n", "\\n" )
    if string.match( string.gsub(v,"[^'\"]",""), '^"+$' ) then
      return "'" .. v .. "'"
    end
    return '"' .. string.gsub(v,'"', '\\"' ) .. '"'
  else
    return "table" == type( v ) and table.tostring( v ) or
      tostring( v )
  end
end;

function table.key_to_str ( k )
  if "string" == type( k ) and string.match( k, "^[_%a][_%a%d]*$" ) then
    return k
  else
    return "[" .. table.val_to_str( k ) .. "]"
  end
end;

function table.tostring( tbl )
  local result, done = {}, {}
  for k, v in ipairs( tbl ) do
    table.insert( result, table.val_to_str( v ) )
    done[ k ] = true
  end
  for k, v in pairs( tbl ) do
    if not done[ k ] then
      table.insert( result,
        table.key_to_str( k ) .. "=" .. table.val_to_str( v ) )
    end
  end
  return "{" .. table.concat( result, "," ) .. "}"
end;

function comma_value(n) -- credit http://richard.warburton.it
	local left,num,right = string.match(n,'^([^%d]*%d)(%d*)(,-)$')
	return left..(num:reverse():gsub('(%d%d%d)','%1.'):reverse())..right
end;

--ajustamos el tiempo exacto con +1, 
--para que no empiece por ejemplo desde "39" y se vean "40"
function GetSelectMusicTime()
	return PREFSMAN:GetPreference("SelectMusicTime")+1;
end;

function HandleWheel( self,offsetFromCenter,itemIndex,numItems )
	local x = offsetFromCenter * 380

	function GetRotationY(offsetFromCenter)
		if offsetFromCenter > 0.9 then
			return 91.5+(scale(offsetFromCenter+1,-1,1,-.5,.5)-0.9)*8;
		elseif offsetFromCenter < -0.9 then
			return -91.5+(scale(offsetFromCenter-1,-1,1,-.5,.5)+0.9)*8;
		else
			return offsetFromCenter*91.5/0.9;
		end;
	end;

	function GetZoom(self, offsetFromCenter)
		if offsetFromCenter > 0.5 then	
			self:y( 20 );
			return 0.9;
		elseif offsetFromCenter < -0.5 then
			self:y( 20 );
			return 0.9;
		else
			if offsetFromCenter <= 0 then
				self:y( scale(offsetFromCenter, -0.5, 0, 20, 0) );
				return scale(offsetFromCenter, -0.5, 0, 0.9, 1);
			elseif offsetFromCenter >= 0 then
				self:y( scale(offsetFromCenter, 0, 0.5, 0, 20) );
				return scale(offsetFromCenter, 0, 0.5, 1, 0.9);
			end
		end;
	end;

	local mm = offsetFromCenter ~= 0 and (math.abs(offsetFromCenter)/offsetFromCenter) or 1 
	local lmtofsc = math.min(math.abs(offsetFromCenter),1)*mm 
	local naspx = SCREEN_CENTER_X/187;  
	local xd = 380; 
	local x2 = 74*naspx; 
	local x3 = 90.5*naspx; 
	--local x4 = 110*naspx; 
	local xp = xd*offsetFromCenter 
	if math.abs(offsetFromCenter) > 1 then 
	xp = (xd+((math.abs(offsetFromCenter)-1)*x2))*mm end; 
	if math.abs(offsetFromCenter) > 2 then 
	xp = (xd+x2+((math.abs(offsetFromCenter)-2)*x3))*mm end; 
	--if math.abs(offsetFromCenter) > 4 then 
	--xp = (xd+x3+((math.abs(offsetFromCenter)-3)*x4))*mm end; 
	self:x( xp ); 

	--[[if offsetFromCenter >= 1 then 
		x = scale(offsetFromCenter+1,-1,1,-.5,.5) * 380
	elseif offsetFromCenter <= -1 then
		x = scale(offsetFromCenter-1,-1,1,-.5,.5) * 380
	end	]]

	local d = GetZoom(self, offsetFromCenter);
	self:zoom(d);
	--self:x( x )
	self:z((-clamp(math.abs(offsetFromCenter),0,1)*170)-math.abs(offsetFromCenter))
	self:rotationy( GetRotationY(offsetFromCenter) )
end

--[[ --old
function HandleWheel( self,offsetFromCenter,itemIndex,numItems )
	local x = offsetFromCenter * 380

	if offsetFromCenter >= 1 then 
		x = scale(offsetFromCenter+1,-1,1,-.5,.5) * 380
	elseif offsetFromCenter <= -1 then
		x = scale(offsetFromCenter-1,-1,1,-.5,.5) * 380
	end

	--self:zoom(math.cos(offsetFromCenter/math.pi))

	--self:y( y )
	self:x( x )
	self:z((-clamp(math.abs(offsetFromCenter),0,1)*170)-math.abs(offsetFromCenter))
	self:rotationy( clamp( offsetFromCenter, -1, 1) * 95)
end
]]

----------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------
function ResetAllCodes()
	--PREFSMAN:SetPreference( 'LongVerSongSeconds', 123 );
	--PREFSMAN:SetPreference( 'MarathonVerSongSeconds', 140 );
	--PREFSMAN:SetPreference( 'BGBrightness', 1 );
	PREFSMAN:SetPreference( 'CurrentGame', 'pump' );
	--PREFSMAN:SetPreference( 'LifeDifficultyScale', tonumber(string.format("%.1f",1)) );	-- no HJ
	--PREFSMAN:SetPreference( 'TimingWindowScale', tonumber(string.format("%.1f",1)) );	-- no HJ
	PREFSMAN:SetPreference( 'BGAOff', false);
	PREFSMAN:SetPreference( 'MusicWheelSwitchSpeed', 3 );
	PREFSMAN:SetPreference( 'SongsPerPlay',5);
	PREFSMAN:SetPreference( 'DefaultModifiers', 'Overhead');
end;

--PREFSMAN:SetPreference('BannerCacheMode','BannerCacheMode_Full');
--PREFSMAN:SetPreference('MusicWheelUsesSections','MusicWheelUsesSections_Always');
PREFSMAN:SetPreference('AllowExtraStage',false);
PREFSMAN:SetPreference('FastLoad',true);
--PREFSMAN:SetPreference('MenuTimer',true);
PREFSMAN:SetPreference('EventMode',false);
PREFSMAN:SetPreference('SongsPerPlay',5);
--PREFSMAN:SetPreference('DisplayAspectRatio',4/3);
--PREFSMAN:SetPreference('DisplayWidth',640);
--PREFSMAN:SetPreference('DisplayHeight',480);
PREFSMAN:SetPreference('Premium','Premium_DoubleFor1Credit');
PREFSMAN:SetPreference('MemoryCards',true);
PREFSMAN:SetPreference('MusicWheelUsesSections','MusicWheelUsesSections_Never');

