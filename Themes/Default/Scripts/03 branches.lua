function InitialScreen()
	if (GAMESTATE:GetCoinMode() == 'CoinMode_Pay' and GAMESTATE:GetCoins() > 0)
	or GAMESTATE:GetCoinMode() == 'CoinMode_Free'
	then
		return "ScreenLogo"
	else	--CoinMode_Home
		return "ScreenTitleMenu"
	end
end

function GameplayNext()
	--Cuando pierdas
	if (STATSMAN:GetCurStageStats():AllFailed()) and not (GAMESTATE:IsEventMode()) then
		--Pierdes, fuera
		return "ScreenStageBreak"
	--¡Pasaste!
	else
		if not GAMESTATE:GetCurrentSong():IsMission() then
			return "ScreenEvaluationNormal"
		else
			return "ScreenEvaluationMission"
		end
	end
end

function EvaluationNext()
	local stagesleft = GAMESTATE:GetSmallestNumStagesLeftForAnyHumanPlayer()
	--[[local isextrastage;
	if (GAMESTATE:GetEarnedExtraStage() == 'EarnedExtraStage_Extra1') then
		isextrastage = true;
	else
		isextrastage = false;
	end;]]

	--SCREENMAN:SystemMessage("stagesleft: " .. tostring(stagesleft));

	--hasta que no haya un shortcut
	--if isextrastage then return "ScreenNextStage"; end;

	--or STATSMAN:GetBestGrade() >= 6)
	if (stagesleft < 1 or GAMESTATE:IsEventMode()) then
		--return "ScreenGameOver"
		return GetProfileSaveOrGameOver();
	else

		local groups = SONGMAN:GetSongGroupNames();
		local genres = SONGMAN:GetSongGenresNames();

		local out = {};
		for _,v in ipairs(groups) do
			if SONGMAN:GetIfGroupIsSelectable(v) then
				table.insert(out,v);
			end
		end

		for _,v in ipairs(genres) do
			if SONGMAN:GetIfGenreIsSelectable(v) then
				table.insert(out,v);
			end
		end

		if #out > 0 and stagesleft > 0 then
			--si quedamos en shortcut desabilitamos rank mode, pero debemos sacar tb el rank de CW
			if GAMESTATE:GetHighestNumStagesLeftForAnyHumanPlayer() <= 1 then
				GAMESTATE:SetRankMode(false);
			end
			
			return "ScreenNextStage"
		else
			return GetProfileSaveOrGameOver();
		end;
	end
end

function GameOverNext()
	if (GAMESTATE:GetCoinMode() == 'CoinMode_Pay' and GAMESTATE:GetCoins() > 0)
	or GAMESTATE:GetCoinMode() == 'CoinMode_Free'
	then
		return "ScreenTitleMenu"
	else
		return "ScreenInit"
	end
end

function StageBreakNext()
	--en event mode nunca perdemos
	if PREFSMAN:GetPreference("InfiniteHearts") then
		return "ScreenEvaluationNormal"
	else
		--return "ScreenGameOver"
		return GetProfileSaveOrGameOver()
	end
end

function GetProfileSaveOrGameOver()
	local prfP1 = nil;
	local prfP2 = nil;

	if GAMESTATE:IsPlayerEnabled(PLAYER_1) then 
		prfP1 = PROFILEMAN:GetProfile(PLAYER_1);
		if not PROFILEMAN:ProfileWasLoadedFromMemoryCard(PLAYER_1) then
			prfP1 = nil;
		end;
	end;

	if GAMESTATE:IsPlayerEnabled(PLAYER_2) then 
		prfP2 = PROFILEMAN:GetProfile(PLAYER_2);
		if not PROFILEMAN:ProfileWasLoadedFromMemoryCard(PLAYER_2) then
			prfP2 = nil;
		end;
	end;

	if prfP1 ~= nil or prfP2 ~= nil then 
		return "ScreenProfileSave" 
	else 
		return "ScreenGameOver" 
	end;
end
