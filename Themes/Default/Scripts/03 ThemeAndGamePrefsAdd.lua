function GamePrefBasicModeGroup()
	local sNames=SONGMAN:GetSongGroupNames();
	local t = {
		Name = "GamePrefBasicModeGroup";
		LayoutType = "ShowOneInRow";
		SelectType = "SelectOne";
		OneChoiceForAllPlayers = true;
		ExportOnChange = false;
		Choices = sNames;
		LoadSelections = function(self, list, pn)
			WriteGamePrefToFile("BasicModeGroup",sNames[1]);	-- setea el primer grupo
			list[1] = true;
		end;
		SaveSelections = function(self, list, pn)
			local val = sNames[1];	--default
			for i=1,#list do
				if list[i] then
					val = sNames[i];
				end;
			end;
			WriteGamePrefToFile("BasicModeGroup",val);
			MESSAGEMAN:Broadcast("PreferenceSet", { Message == "Set Preference" } );
			THEME:ReloadMetrics();
		end;
	};
	setmetatable( t, t );
	return t;
end;

function GamePrefFullModeGroup()
	local sNames=SONGMAN:GetSongGroupNames();
	local t = {
		Name = "GamePrefFullModeGroup";
		LayoutType = "ShowOneInRow";
		SelectType = "SelectOne";
		OneChoiceForAllPlayers = true;
		ExportOnChange = false;
		Choices = sNames;
		LoadSelections = function(self, list, pn)
			WriteGamePrefToFile("FullModeGroup",sNames[1]);	-- setea el primer grupo
			list[1] = true;	
		end;
		SaveSelections = function(self, list, pn)
			local val = sNames[1];	--default
			for i=1,#sNames do
				if list[i] then
					val = sNames[i];
				end;
			end;
			WriteGamePrefToFile("FullModeGroup",val);
			MESSAGEMAN:Broadcast("PreferenceSet", { Message == "Set Preference" } );
			THEME:ReloadMetrics();
		end;
	};
	setmetatable( t, t );
	return t;
end