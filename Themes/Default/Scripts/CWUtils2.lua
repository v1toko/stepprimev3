function GetCodesColumn( CUR_PLAYER )
local t = Def.ActorFrame {};
--x,24;

---------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------

local blank = THEME:GetPathG("","CWUtils").."//_blank.png";
local flash = THEME:GetPathG("","CWUtils").."//flash.png";

---------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------
-- Sprite para la velocidad
local speed_codes = {};
local cur_speed_item;
t[#t+1] = Def.ActorFrame {
	InitCommand=cmd(stoptweening;y,110;zoom,.75);
	SpeedCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;zoom,.75;decelerate,.1;zoom,.75;decelerate,.2;zoom,.75))(self);
	end;
	children = {
		Def.Sprite {
			InitCommand=cmd(stoptweening;playcommand,'UpDate');
			SpeedCodeMessageCommand=function(self,params)
				if (params.Player ~= CUR_PLAYER) then return; end;
				(cmd(stoptweening;playcommand,'UpDate'))(self);
			end;
			UpDateCommand=function(self)
				speed_codes = nil;
				speed_codes = {};
				for i=1,#(speed[1]) do
					if IsCodeOn(CUR_PLAYER,"Speed",i) then
						speed_codes[#speed_codes+1] = i;
					end;
				end;
				if speed_codes ~= nil then cur_speed_item = 1; else cur_speed_item = 0; end;
				self:stoptweening();
				self:queuecommand('Loop');
			end;
			LoopCommand=function(self)
				local curSpeedM = GetPlayerSpeedMMod(CUR_PLAYER);

				self:stoptweening();
				if cur_speed_item == 0 then self:Load(""); return; end;
				self:Load((THEME:GetPathG("","Mods") .. "/mods 16x10.png"));
				self:animate(0);
				self:pause(0);

				state = GetStateByModName(speed[1][speed_codes[cur_speed_item]]);
				--SCREENMAN:SystemMessage("state: " .. tostring(state));
				--si es av, no muestra rotativos los +100 -100 etc.
				if state >= 145 and state <= 150 then
					state = 9;
				end;

				self:setstate(state);
				self:sleep(3);
				if #speed_codes == cur_speed_item then cur_speed_item = 1; else cur_speed_item = cur_speed_item+1; end;
				self:queuecommand('Loop');
			end;
		};
		Def.BitmapText {
			Name="Auto Velocity";
			Font="_boost ssi 40px";
			InitCommand=cmd(stoptweening;playcommand,'UpDate');
			SpeedCodeMessageCommand=function(self,params)
				if (params.Player ~= CUR_PLAYER) then return; end;
				(cmd(stoptweening;playcommand,'UpDate'))(self);
			end;
			UpDateCommand=function(self)
				self:stoptweening();
				self:queuecommand('Loop');
			end;
			LoopCommand=function(self)
				local curSpeedM = GetPlayerSpeedMMod(CUR_PLAYER);
				--self:visible(true);
				state = GetStateByModName(speed[1][speed_codes[cur_speed_item]]);

				--SCREENMAN:SystemMessage("code: " .. tostring(curSpeedM) .. " code 2 " .. state .. " code 3 " .. speed[1][speed_codes[cur_speed_item]]);

				if(curSpeedM == nil) then
					curSpeedM = "";
				end;
				
				if( state == 9 or (state >= 145 and state <= 150) ) then
					curSpeedM = GetPlayerSpeedMMod(CUR_PLAYER);
				else
					curSpeedM = "";
				end;

				--

				self:settext(tostring(curSpeedM));
				self:y(20);
				self:zoom(0.5);
				self:stoptweening();
				self:sleep(3);
				self:queuecommand('Loop');
			end;
		};
	};
};


t[#t+1] = LoadActor(flash)..{
	InitCommand=cmd(stoptweening;y,110;blend,'BlendMode_Add';diffusealpha,0);
	SpeedCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;diffusealpha,0;zoom,.75;linear,.05;diffusealpha,1;zoom,.8;sleep,.05;decelerate,.6;diffusealpha,0))(self);
	end;
};
---------------------------------------------------------------------
---------------------------------------------------------------------
-- Sprite para el display
-- BGAOff es i=6
local display_codes = {};
local cur_display_item;
t[#t+1] = Def.ActorFrame {
	InitCommand=cmd(stoptweening;y,150;zoom,.75);
	DisplayCodeMessageCommand=function(self,params)
		if params.BGAOffChange then
			(cmd(stoptweening;zoom,.75;decelerate,.1;zoom,.75;decelerate,.2;zoom,.75))(self);
			return;
		end;
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;zoom,.75;decelerate,.1;zoom,.75;decelerate,.2;zoom,.75))(self);
	end;
	children = {
		Def.Sprite {
			InitCommand=cmd(stoptweening;playcommand,'UpDate');
			DisplayCodeMessageCommand=function(self,params)
				if params.BGAOffChange then
					(cmd(stoptweening;playcommand,'UpDate'))(self);
					return;
				end;
				if (params.Player ~= CUR_PLAYER) then return; end;
				(cmd(stoptweening;playcommand,'UpDate'))(self);
			end;
			UpDateCommand=function(self)
				display_codes = nil;
				display_codes = {};
				for i=1,#(display[1]) do
					if IsCodeOn(CUR_PLAYER,"Display",i) then
						display_codes[#display_codes+1] = i;
					end;
				end;
				if #display_codes ~= 0 then cur_display_item = 1; else cur_display_item = 0; end;
				self:stoptweening();
				self:queuecommand('Loop');
			end;
			LoopCommand=function(self)
				self:stoptweening();
				if cur_display_item == 0 then self:Load(blank); MESSAGEMAN:Broadcast("NoOneDisplayCode",{ Player = CUR_PLAYER }); return; end;
				self:Load((THEME:GetPathG("","Mods") .. "/mods 16x10.png"));
				self:animate(0);
				self:pause(0);
				self:setstate(GetStateByModName(display[1][display_codes[cur_display_item]]));
				self:sleep(3);
				if #display_codes == cur_display_item then cur_display_item = 1; else cur_display_item = cur_display_item+1; end;
				self:queuecommand('Loop');
			end;
		};
};
};
t[#t+1] = LoadActor(flash)..{
	InitCommand=cmd(stoptweening;y,150;blend,'BlendMode_Add';diffusealpha,0);
	DisplayCodeMessageCommand=function(self,params)
		if params.BGAOffChange then
			(cmd(stoptweening;diffusealpha,0;zoom,.75;linear,.05;diffusealpha,1;zoom,.8;sleep,.05;decelerate,.6;diffusealpha,0))(self);
			return;
		end;
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;diffusealpha,0;zoom,.75;linear,.05;diffusealpha,1;zoom,.8;sleep,.05;decelerate,.6;diffusealpha,0))(self);
	end;
	NoOneDisplayCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
			(cmd(stoptweening;diffusealpha,0;zoom,.8))(self);
	end;
};
---------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------
-- Sprite para los noteskin
local noteskin_codes = {};
local cur_noteskin_item;
t[#t+1] = Def.ActorFrame {
	InitCommand=cmd(stoptweening;y,190;zoom,.75);
	NoteSkinCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;zoom,.75;decelerate,.1;zoom,.75;decelerate,.2;zoom,.75))(self);
	end;
	children = {
		Def.Sprite {
			InitCommand=cmd(stoptweening;playcommand,'UpDate');
			NoteSkinCodeMessageCommand=function(self,params)
				if (params.Player ~= CUR_PLAYER) then return; end;
				(cmd(stoptweening;playcommand,'UpDate'))(self);
			end;
			DefaultNoteSkinCodeMessageCommand=function(self,params)
				if (params.Player ~= CUR_PLAYER) then return; end;
				(cmd(stoptweening;Load,blank))(self);
			end;
			UpDateCommand=function(self)
				noteskin_codes = nil;
				noteskin_codes = {};
				for i=1,#(noteskin) do
					if IsCodeOn(CUR_PLAYER,"Noteskin",i) then
						noteskin_codes[#noteskin_codes+1] = i;
					end;
				end;
				if #noteskin_codes ~= 0 then cur_noteskin_item = 1; else cur_noteskin_item = 0; end;
				self:stoptweening();
				self:queuecommand('Loop');
			end;
			LoopCommand=function(self)
				self:stoptweening();
				if cur_noteskin_item == 0 then self:Load(blank); return; end;
				self:Load("NoteSkins/Labels/"..noteskin[noteskin_codes[cur_noteskin_item]]..".png");
				self:sleep(3);
				if #noteskin_codes == cur_noteskin_item then cur_noteskin_item = 1; else cur_noteskin_item = cur_noteskin_item+1; end;
				self:queuecommand('Loop');
			end;
		};
};
};
t[#t+1] = LoadActor(flash)..{
	InitCommand=cmd(stoptweening;y,190;blend,'BlendMode_Add';diffusealpha,0);
	NoteSkinCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;diffusealpha,0;zoom,.75;linear,.05;diffusealpha,1;zoom,.8;sleep,.05;decelerate,.6;diffusealpha,0))(self);
	end;
	DefaultNoteSkinCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;diffusealpha,0;zoom,.8))(self);
	end;
};
---------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------
-- Sprite para el path
local path_codes = {};
local cur_path_item;
t[#t+1] = Def.ActorFrame {
	InitCommand=cmd(stoptweening;y,230;zoom,.75);
	PathCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;zoom,.75;decelerate,.1;zoom,.75;decelerate,.2;zoom,.75))(self);
	end;
	children = {
		Def.Sprite {
			InitCommand=cmd(stoptweening;playcommand,'UpDate');
			PathCodeMessageCommand=function(self,params)
				if (params.Player ~= CUR_PLAYER) then return; end;
				(cmd(stoptweening;playcommand,'UpDate'))(self);
			end;
			UpDateCommand=function(self)
				path_codes = nil;
				path_codes = {};
				for i=1,#(path[1]) do
					if IsCodeOn(CUR_PLAYER,"Path",i) then
						path_codes[#path_codes+1] = i;
					end;
				end;
				if #path_codes ~= 0 then cur_path_item = 1; else cur_path_item = 0; end;
				self:stoptweening();
				self:queuecommand('Loop');
			end;
			LoopCommand=function(self)
				self:stoptweening();
				if cur_path_item == 0 then self:Load(blank); MESSAGEMAN:Broadcast("NoOnePathCode",{Player=CUR_PLAYER}); return; end;
				self:Load((THEME:GetPathG("","Mods") .. "/mods 16x10.png"));
				self:animate(0);
				self:pause(0);
				self:setstate(GetStateByModName(path[1][path_codes[cur_path_item]]));
				self:sleep(3);
				if #path_codes == cur_path_item then cur_path_item = 1; else cur_path_item = cur_path_item+1; end;
				self:queuecommand('Loop');
			end;
		};
};
};
t[#t+1] = LoadActor(flash)..{
	InitCommand=cmd(stoptweening;y,230;blend,'BlendMode_Add';diffusealpha,0);
	PathCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;diffusealpha,0;zoom,.75;linear,.05;diffusealpha,1;zoom,.8;sleep,.05;decelerate,.6;diffusealpha,0))(self);
	end;
	NoOnePathCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;diffusealpha,0;zoom,.8))(self);
	end;
};
---------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------
-- Sprite para el alternate
local alternate_codes = {};
local cur_alternate_item;
t[#t+1] = Def.ActorFrame {
	InitCommand=cmd(stoptweening;y,270;zoom,.75);
	AlternateCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;zoom,.75;decelerate,.1;zoom,.75;decelerate,.2;zoom,.75))(self);
	end;
	children = {
		Def.Sprite {
			InitCommand=cmd(stoptweening;playcommand,'UpDate');
			AlternateCodeMessageCommand=function(self,params)
				if (params.Player ~= CUR_PLAYER) then return; end;
				(cmd(stoptweening;playcommand,'UpDate'))(self);
			end;
			UpDateCommand=function(self)
				alternate_codes = nil;
				alternate_codes = {};
				for i=1,#(alternate[1]) do
					if IsCodeOn(CUR_PLAYER,"Alternate",i) then
						alternate_codes[#alternate_codes+1] = i;
					end;
				end;
				if #alternate_codes ~= 0 then cur_alternate_item = 1; else cur_alternate_item = 0; end;
				self:stoptweening();
				self:queuecommand('Loop');
			end;
			LoopCommand=function(self)
				self:stoptweening();
				if cur_alternate_item == 0 then self:Load(blank); MESSAGEMAN:Broadcast("NoOneAlternateCode",{Player=CUR_PLAYER}); return; end;
				self:Load((THEME:GetPathG("","Mods") .. "/mods 16x10.png"));
				self:animate(0);
				self:pause(0);
				self:setstate(GetStateByModName(alternate[1][alternate_codes[cur_alternate_item]]));
				self:sleep(3);
				if #alternate_codes == cur_alternate_item then cur_alternate_item = 1; else cur_alternate_item = cur_alternate_item+1; end;
				self:queuecommand('Loop');
			end;
		};
};
};
t[#t+1] = LoadActor(flash)..{
	InitCommand=cmd(stoptweening;y,270;blend,'BlendMode_Add';diffusealpha,0);
	AlternateCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;diffusealpha,0;zoom,.75;linear,.05;diffusealpha,1;zoom,.8;sleep,.05;decelerate,.6;diffusealpha,0))(self);
	end;
	NoOneAlternateCodeMessageCommand=function(self,params)
		if (params.Player ~= CUR_PLAYER) then return; end;
		(cmd(stoptweening;diffusealpha,0;zoom,.8))(self);
	end;
};
---------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------
-- Sprite para el rush
local rush_codes = {};
local cur_rush_item;
t[#t+1] = Def.ActorFrame {
	InitCommand=cmd(stoptweening;y,310;zoom,.75);
	RushCodeMessageCommand=cmd(stoptweening;zoom,.75;decelerate,.1;zoom,.75;decelerate,.2;zoom,.75);
	children = {
		Def.Sprite {
			InitCommand=cmd(stoptweening;playcommand,'UpDate');
			RushCodeMessageCommand=cmd(stoptweening;playcommand,'UpDate');
			UpDateCommand=function(self)
				rush_codes = nil;
				rush_codes = {};
				for i=1,#(rush[1]) do
					if IsCodeOn(CUR_PLAYER,"Rush",i) then
						rush_codes[#rush_codes+1] = i;
					end;
				end;
				if #rush_codes ~= 0 then cur_rush_item = 1; else cur_rush_item = 0; end;
				self:stoptweening();
				self:queuecommand('Loop');
			end;
			LoopCommand=function(self)
				self:stoptweening();
				if cur_rush_item == 0 then self:Load(blank); MESSAGEMAN:Broadcast("NoOneRushCode"); return; end;
				self:Load((THEME:GetPathG("","Mods") .. "/mods 16x10.png"));
				self:animate(0);
				self:pause(0);
				self:setstate(GetStateByModName(rush[1][rush_codes[cur_rush_item]]));
				self:sleep(3);
				if #rush_codes == cur_rush_item then cur_rush_item = 1; else cur_rush_item = cur_rush_item+1; end;
				self:queuecommand('Loop');
			end;
		};
};
};
t[#t+1] = LoadActor(flash)..{
	InitCommand=cmd(stoptweening;y,310;blend,'BlendMode_Add';diffusealpha,0);
	RushCodeMessageCommand=cmd(stoptweening;diffusealpha,0;zoom,.75;linear,.05;diffusealpha,1;zoom,.8;sleep,.05;decelerate,.6;diffusealpha,0);
	NoOneRushCodeMessageCommand=cmd(stoptweening;diffusealpha,0;zoom,.8);
};
---------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------
-- Sprite para el judgement
local judge_codes = {};
local cur_judge_item;
t[#t+1] = Def.ActorFrame {
	InitCommand=cmd(stoptweening;y,350;zoom,.75);
	JudgeCodeMessageCommand=cmd(stoptweening;zoom,.75;decelerate,.1;zoom,.75;decelerate,.2;zoom,.75);
	children = {
		Def.Sprite {
			InitCommand=cmd(stoptweening;playcommand,'UpDate');
			JudgeCodeMessageCommand=cmd(stoptweening;playcommand,'UpDate');
			UpDateCommand=function(self)
				judge_codes = nil;
				judge_codes = {};
				for i=1,#(judge[1]) do
					if IsCodeOn(CUR_PLAYER,"Judge",i) then
						judge_codes[#judge_codes+1] = i;
					end;
				end;
				if #judge_codes ~= 0 then cur_judge_item = 1; else cur_judge_item = 0; end;
				self:stoptweening();
				self:queuecommand('Loop');
			end;
			LoopCommand=function(self)
				self:stoptweening();
				if cur_judge_item == 0 then self:Load(blank); MESSAGEMAN:Broadcast("NoOneJudgeCode"); return; end;
				self:Load((THEME:GetPathG("","Mods") .. "/mods 16x10.png"));
				self:animate(0);
				self:pause(0);
				self:setstate(GetStateByModName(judge[1][judge_codes[cur_judge_item]]));
				self:sleep(3);
				if #judge_codes == cur_judge_item then cur_judge_item = 1; else cur_judge_item = cur_judge_item+1; end;
				self:queuecommand('Loop');
			end;
		};
};
};
t[#t+1] = LoadActor(flash)..{
	InitCommand=cmd(stoptweening;y,350;blend,'BlendMode_Add';diffusealpha,0);
	JudgeCodeMessageCommand=cmd(stoptweening;diffusealpha,0;zoom,.75;linear,.05;diffusealpha,1;zoom,.8;sleep,.05;decelerate,.6;diffusealpha,0);
	NoOneJudgeCodeMessageCommand=cmd(stoptweening;diffusealpha,0;zoom,.8);
};
---------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------
-- Sprite para el rank, va sobre el velocity
local rank_codes = {};
local cur_rank_item;
t[#t+1] = Def.ActorFrame {
	InitCommand=cmd(stoptweening;y,70;zoom,.75);
	RankCodeMessageCommand=cmd(stoptweening;zoom,.75;decelerate,.1;zoom,.75;decelerate,.2;zoom,.75);
	children = {
		Def.Sprite {
			InitCommand=cmd(stoptweening;playcommand,'UpDate');
			RankCodeMessageCommand=cmd(stoptweening;playcommand,'UpDate');
			UpDateCommand=function(self)
				rank_codes = nil;
				rank_codes = {};
				for i=1,#(rank[1]) do
					if IsCodeOn(CUR_PLAYER,"Rank",i) then
						rank_codes[#rank_codes+1] = i;
					end;
				end;
				if #rank_codes ~= 0 then cur_rank_item = 1; else cur_rank_item = 0; end;
				self:stoptweening();
				self:queuecommand('Loop');
			end;
			LoopCommand=function(self)
				self:stoptweening();
				if cur_rank_item == 0 then self:Load(blank); MESSAGEMAN:Broadcast("NoOneRankCode"); return; end;
				self:Load((THEME:GetPathG("","Mods") .. "/mods 16x10.png"));
				self:animate(0);
				self:pause(0);
				self:setstate(GetStateByModName(rank[1][rank_codes[cur_rank_item]]));
				self:sleep(3);
				if #rank_codes == cur_rank_item then cur_rank_item = 1; else cur_rank_item = cur_rank_item+1; end;
				self:queuecommand('Loop');
			end;
		};
};
};
t[#t+1] = LoadActor(flash)..{
	InitCommand=cmd(stoptweening;y,70;blend,'BlendMode_Add';diffusealpha,0);
	RankCodeMessageCommand=cmd(stoptweening;diffusealpha,0;zoom,.75;linear,.05;diffusealpha,1;zoom,.8;sleep,.05;decelerate,.6;diffusealpha,0);
	NoOneRankCodeMessageCommand=cmd(stoptweening;diffusealpha,0;zoom,.8);
};
---------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------
return t;
end;