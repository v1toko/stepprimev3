--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-- 														UTILIDADES PARA EL CW - by xMAx 2013
--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-- noteskin black list
local noteskin_BL = { "default", "alisson", "angrid", "bobsponja", "energia", "heart", "infinity", "phantom3", "xuxa", "routine-p1", "routine-p2", "routine-p3", "delta-note" };

function GetNoteSkinList()
	local toret = {};
	local ns_list = NOTESKIN:GetNoteSkinNames();

	for i=1,#ns_list do
		local isinthebl = false;
		for j=1,#noteskin_BL do
			if ns_list[i]==noteskin_BL[j] then isinthebl = true; end;
		end;
		if not isinthebl then
			toret[#toret+1]=ns_list[i];
		end;
	end;
	
	return toret;
end;

local default_noteskin = THEME:GetMetric('Common','DefaultNoteSkinName');

-- array = {{nombre},{comando}}
speed = { {"1x","2x","3x","4x","5x","6x", "0.5x","RV","AC","DC","EW","AV","+100","-100","+10","-10","+1","-1"},
                {"1x","2x","3x","4x","5x","6x", "0.5x","3x,10% randomspeed","Boost","Brake","Expand","m650","+100","-100","+10","-10","+1","-1"} 
};

display = { {"V","NS", "AP", "FD", "FL", "RNS", "BGA_Off"},
			      {"Hidden","Stealth","Sudden","Dark","Blink","RandomNoteskin","BGAOff"}
};
-- si cambiamos el orden del bgaoff tengo que cambiar el i del cwutils2

noteskin = GetNoteSkinList();

--TODO: DROP / Reverse
path = { {"X", "NX", "UA", "DR", "SN"},
			   {"XMode","150% space","drop, flip","drop","25% tornado"}
};

alternate = { {"RS", "M", "SI", "RI", "RS", "M", "SI", "RI"},
			  {"Shuffle","Mirror","Sink","Rise","Shuffle","Mirror","Sink","Rise"}
};

judge = { {"HJ", "JR", "VJ", "HJ", "JR", "VJ" },
			  {"HJ", "ReverseGrade", "VJ", "HJ", "ReverseGrade", "VJ" }
};

rank = { {"Rank"}, {"Rank"} };

rush = {{"80", "90", "110", "120", "130", "140", "150"},
			  {"0.8xMusic","0.9xMusic","1.1xMusic","1.2xMusic","1.3xMusic","1.4xMusic","1.5xMusic"}
};

system = { "reset" };

vInvalidChannels = { ["remix"] = true, ["full"] = true, ["shortcut"] = true, ["quest zone"] = true, ["random"] = true, ["music train"] = true, ["co-op"] = true };
--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function IsCodeOn( pn, option, i )
	local player_options_string = GAMESTATE:GetPlayerState(pn):GetPlayerOptionsString('ModsLevel_Preferred');
	
	--///////////////////////////////////////////////////////
	if (option=="Speed") then
		local player_options = GAMESTATE:GetPlayerState(pn):GetCurrentPlayerOptions();
		local curSpeed = player_options:XMod();	-- velocidad en p. flotante
		local curMMod  = player_options:MMod();  -- MMod, nil = no hay MMod
		----
		local HasRandomSpeed;
		if (string.find(player_options_string,"RandomSpeed") ~= nil ) then	-- para los c�digos que quedan
			HasRandomSpeed=true; else HasRandomSpeed=false; end;
		---
		
		if (i>=1 and i<=7) and (curMMod~=nil) then	-- tengo MMod(Autovelocity), se desactivan las velocidades en el sm automaticamente
			return false;
		end;		
		
		if (i>=1 and i<=6) then -- 1x to 6x
			if not (HasRandomSpeed) then	-- cuando hay random speed, tambien hay una velocidad, pero igual se muestra como que no hay velocidad
				if ( (string.format("%ix",curSpeed)) == speed[1][i]) then 
					return true; else return false; end;
			else
				return false;
			end;
		end;

		if (i==7) then	-- 0.5x
			if curSpeed == math.floor(curSpeed) then 
				return false; 
			else 
				return true; 
			end;
		end;
			
		if (i==8) then	-- random speed
			return HasRandomSpeed;
		end;

		if(i==12 and curMMod~=nil) then
			return true;
		end;

		if(i >= 13 and i <= 18 and curMMod~=nil) then
			return true;
		end;
		
		if (string.find(player_options_string,speed[2][i]) ~= nil ) then	-- para los c�digos que quedan
			return true; else return false; end;
	end;
	--///////////////////////////////////////////////////////
	if (option=="Display") then
		if (display[2][i]=="BGAOff") then 	-- verifica el BGAOff
			return PREFSMAN:GetPreference("BGAOff"); end;
		if ( string.find(player_options_string,display[2][i]) ~= nil ) then
			return true; else return false; end;
	end;
	--///////////////////////////////////////////////////////
	if (option=="Noteskin") then	-- verifica que noteskin esta activo
		--if (noteskin[i]==(GAMESTATE:GetPlayerState(pn):GetCurrentPlayerOptions():GetNoteSkin())) then return true; else return false; end; no funca no se porque...
		--[[if ( string.find(string.upper(player_options_string),string.upper(noteskin[i])) ~= nil ) then
			return true; else return false; end;]]--
		local options_array = GAMESTATE:GetPlayerState(pn):GetPlayerOptionsArray('ModsLevel_Preferred')
		for k=1,#options_array do
			if (noteskin[i]==options_array[k]) then return true; end;
		end;
		return false;
	end;
	--///////////////////////////////////////////////////////
	if (option=="Path") then
		if (i==1 or i==3) then	-- Xmode y reverse
			if ( string.find(player_options_string,path[2][i]) ~= nil ) then
				return true; else return false; end;
		end;

		if (i==2) then -- space
			if ( string.find(player_options_string,"Space") ~= nil ) then
				return true; else return false; end;
		end;
		
		if (i==5) then 
			if ( string.find(player_options_string,"Tornado") ~= nil ) then
				return true; else return false; end;
		end;
		
		-- FALTA LA IMPLEMENTACION PARA EL DP--
		if (i==4) then -- drop
			if ( string.find(player_options_string,"Drop") ~= nil ) then
				return true; else return false; end;
		end;
		
		return false;-- si no ocurre ninguna de las anteriores, false
	end;
	--///////////////////////////////////////////////////////
	if (option=="Alternate") then
		--if (i==1 or i==2 or (i==5 or i==6)) then
		if ( string.find(player_options_string,alternate[2][i]) ~= nil ) then
			return true; 
		else 
			return false; 
		end;
		--end;
		
		--[[local cur_bumpy = GAMESTATE:GetPlayerState(pn):GetCurrentPlayerOptions():Bumpy();
		if i==3 and cur_bumpy==3 then return true; end;
		if i==4 and cur_bumpy==-3 then return true; end;
		if i==7 and cur_bumpy==3 then return true; end;
		if i==8 and cur_bumpy==-3 then return true; end;
		return false;	-- si no ocurre ninguna de las anteriores, false]]
	end;

	if (option=="Judge") then

		--Warn("judge codeon: " .. tostring(i) .. " timing: " .. tostring(tonumber(string.format("%.1f",PREFSMAN:GetPreference("TimingWindowScale")))))
		
		--hj
		if (i==1 or i==4) then	
			local timing = tonumber(string.format("%.2f",PREFSMAN:GetPreference("TimingWindowScale")));
			if timing == 0.85 then return true; else return false; end;
		end;

		if (i==2 or i==5) then	
			if ( string.find(player_options_string,judge[2][i]) ~= nil ) then
				return true; else return false; end;
		end;

		--vj
		if (i==3 or i==6) then	
			local timing = tonumber(string.format("%.2f",PREFSMAN:GetPreference("TimingWindowScale")));
			if timing == 0.75 then return true; else return false; end;
		end;
				
		return false;	-- si no ocurre ninguna de las anteriores, false
	end;

	if (option=="Rank") then
		
		if( GAMESTATE:IsRankMode() ) then return true end;
				
		return false;	-- si no ocurre ninguna de las anteriores, false
	end;

	--///////////////////////////////////////////////////////
	if (option=="Rush") then
		local song_options = GAMESTATE:GetSongOptions('ModsLevel_Preferred');
		--Warn(song_options .. " " .. rush[2][i]);
		--song_options = string.lower(song_options)
		if ( string.find(song_options,rush[2][i]) ~= nil ) then
			return true; 
		else 
			return false; 
		end;
	end;
	--///////////////////////////////////////////////////////
	if (option=="System") then	-- el reset siempre se ve activo
		return true;
	end;
	--///////////////////////////////////////////////////////
	return false;--esto no tendria que pasar.
end;

function TurnOffCode( pn, code )
	if (code=="BGAOff") then PREFSMAN:SetPreference("BGAOff",false); end;
	--if (code=="XJ") then PREFSMAN:SetPreference("TimingWindowScale",tonumber(string.format("%.1f",0.9))); end;
	if (code=="VJ") then PREFSMAN:SetPreference("TimingWindowScale",tonumber(string.format("%.1f",BaseDifficulty))); end;
	if (code=="HJ") then PREFSMAN:SetPreference("TimingWindowScale",tonumber(string.format("%.1f",BaseDifficulty))); end;
	--if (code=="NJ") then PREFSMAN:SetPreference("TimingWindowScale",tonumber(string.format("%.1f",0.9))); end;
	--if (code=="EJ") then PREFSMAN:SetPreference("TimingWindowScale",tonumber(string.format("%.1f",0.9))); end;

	if (code=="Rank") then 
		GAMESTATE:SetRankMode(false); 
		PREFSMAN:SetPreference("TimingWindowScale",tonumber(string.format("%.1f",BaseDifficulty))); 
		--msg suscrito en screenselectmusic
		MESSAGEMAN:Broadcast("UpdateWheelSongsAndSteps");
	end;

	if code ~= nil then	GAMESTATE:ApplyPreferredModifiers(pn,"no "..code); end;
end;

function TurnOnCode( pn, code )
	if (code=="BGAOff") and not GAMESTATE:IsRankMode() then PREFSMAN:SetPreference("BGAOff",true); end;
	--if (code=="XJ") and not GAMESTATE:IsRankMode() then PREFSMAN:SetPreference("TimingWindowScale",tonumber(string.format("%.1f",0.6))); end;
	if (code=="VJ") and not GAMESTATE:IsRankMode() then PREFSMAN:SetPreference("TimingWindowScale",tonumber(string.format("%.2f",0.75))); end;
	if (code=="HJ") and not GAMESTATE:IsRankMode() then PREFSMAN:SetPreference("TimingWindowScale",tonumber(string.format("%.2f",0.85))); end;
	--if (code=="NJ") and not GAMESTATE:IsRankMode() then PREFSMAN:SetPreference("TimingWindowScale",tonumber(string.format("%.1f",1))); end;
	--if (code=="EJ") and not GAMESTATE:IsRankMode() then PREFSMAN:SetPreference("TimingWindowScale",tonumber(string.format("%.1f",1.3))); end;

	Warn("code " .. code);

	if (code=="Rank") and not vInvalidChannels[GAMESTATE:GetCurrentGroup()] then 
		GAMESTATE:SetRankMode(true); 
		PREFSMAN:SetPreference("BGAOff",false); 
		PREFSMAN:SetPreference("TimingWindowScale",tonumber(string.format("%.2f",0.75))); 
		--msg suscrito en screenselectmusic
		MESSAGEMAN:Broadcast("UpdateWheelSongsAndSteps");
	end;

	--SCREENMAN:SystemMessage("code: " .. code);

	if code ~= nil then GAMESTATE:ApplyPreferredModifiers(pn,code); end;
end;

function TurnOnCodes( pn, codes )
	for i=1,#codes do
		TurnOnCode(pn, codes[i]);
	end;	
end;

function TurnOffCodes( pn, codes )
	for i=1,#codes do
		TurnOffCode(pn, codes[i]);
	end;	
end;

function GetPlayerSpeed( pn )
	return GAMESTATE:GetPlayerState(pn):GetCurrentPlayerOptions():XMod();
end;

function GetPlayerSpeedMMod( pn )
	return GAMESTATE:GetPlayerState(pn):GetCurrentPlayerOptions():MMod();
end;

function GetCodeByPos(option, code)
	if option == "Speed" then return speed[1][code]; end;
	if option == "Display" then return display[1][code]; end;
	if option == "Noteskin" then return noteskin[code]; end;
	if option == "Path" then return path[1][code]; end;
	if option == "Alternate" then return alternate[1][code]; end;
	if option == "Rush" then return rush[1][code]; end;
	if option == "Judge" then return judge[1][code]; end;
	--if option == "Rank" then return "Rank"; end;
end;

function WorkThisCode( pn, option, code )
	--//////////////////////////////////////////////////////////////////////////////////////////////	

	if (option == "Speed") then
		if (code==7) then -- se le suma +0.5 de velocidad a la velocidad actual
			if IsCodeOn(pn,"Speed",12) then	-- autovelocity?
				TurnOnCode(pn,"3.5x");	-- se pone la velocidad en 3.5x
				MESSAGEMAN:Broadcast("SpeedCode",{Player = pn});
				return;
			end;
			local curSpeed = GetPlayerSpeed(pn);
			if IsCodeOn(pn,"Speed",7) then	-- ya esta activado
				TurnOnCode(pn,(tostring(curSpeed-0.5).."x"));
				MESSAGEMAN:Broadcast("SpeedCode",{Player = pn});
				return;
			else	--no esta activado
				TurnOnCode(pn,(tostring(curSpeed+0.5).."x"));
				MESSAGEMAN:Broadcast("SpeedCode",{Player = pn});
				return;
			end;
		end;
		if (code<=6 and code>=1) then	-- si el codigo es de 1x a 6x
			TurnOnCode(pn,speed[2][code]); 	--aplicando una velocidad diferente las demas se desactivan solas
			TurnOnCode(pn,"no randomspeed"); --sacamos el random si estaba activado
			MESSAGEMAN:Broadcast("SpeedCode",{Player = pn});
			return;
		end;
		if (code==8 or code==12) then	-- "RV","AV"
			TurnOnCode(pn,speed[2][code]);	--aplicando una velocidad diferente las demas se desactivan solas
			MESSAGEMAN:Broadcast("SpeedCode",{Player = pn});
			return;
		end;
		--si tenemos av habilitado
		if IsCodeOn(pn,"Speed",12) then
			if (code>=13 and code<=18) then	-- si el codigo es +100, -100, +10, -10, +1 o -1
				local curSpeedM = GetPlayerSpeedMMod(pn);
				if code == 13 then
					curSpeedM = curSpeedM+100;
				end;
				if code == 14 then
					curSpeedM = curSpeedM-100;
				end;
				if code == 15 then
					curSpeedM = curSpeedM+10;
				end;
				if code == 16 then
					curSpeedM = curSpeedM-10;
				end;
				if code == 17 then
					curSpeedM = curSpeedM+1;
				end;
				if code == 18 then
					curSpeedM = curSpeedM-1;
				end;
				TurnOnCode(pn,"no randomspeed");
				TurnOnCode(pn,speed[2][12]);
				TurnOnCode(pn,"m"..curSpeedM);
				MESSAGEMAN:Broadcast("SpeedCode",{Player = pn});
				return;
			end;
		end;
		if IsCodeOn(pn,"Speed",code) then -- "AC","DC","EW" (9,10,11)
			TurnOffCode(pn,speed[2][code]);	--da de baja los codigos del 9 al 11 si alguno estaba activado
			MESSAGEMAN:Broadcast("SpeedCode",{Player = pn});
			return;
		else	-- el codigo no estaba activado
			TurnOffCodes(pn,{speed[2][9],speed[2][10],speed[2][11]}); --da de baja los codigos del 9 al 11 si alguno estaba activado, y leugo activa el correspondiente
			TurnOnCode(pn,speed[2][code]);
			MESSAGEMAN:Broadcast("SpeedCode",{Player = pn});
			return;
		end;
	
	end;	-- fin del speed
	
	--//////////////////////////////////////////////////////////////////////////////////////////////
	if (option=="Display") then
		if IsCodeOn(pn,"Display",code) then
			TurnOffCode(pn,display[2][code]);
			if display[2][code] == "BGAOff" then
				MESSAGEMAN:Broadcast("DisplayCode",{Player = pn, BGAOffChange = true});	-- para avisar que cambio la opcion del BGAOff
			else
				MESSAGEMAN:Broadcast("DisplayCode",{Player = pn, BGAOffChange = false});
			end;
			return;
		else
			TurnOnCode(pn,display[2][code]);
			if display[2][code] == "BGAOff" then
				MESSAGEMAN:Broadcast("DisplayCode",{Player = pn, BGAOffChange = true});	-- para avisar que cambio la opcion del BGAOff
			else
				MESSAGEMAN:Broadcast("DisplayCode",{Player = pn, BGAOffChange = false});
			end;
			return;
		end;
	end;--{"Hidden","Stealth","Sudden","Dark","Blink","BGAOff"};
	--//////////////////////////////////////////////////////////////////////////////////////////////
	if (option=="Noteskin") then	--todo ok
		if IsCodeOn(pn,"Noteskin",code) then	-- hay algun noteskin?
			TurnOnCode(pn,default_noteskin);	-- default noteskin
			MESSAGEMAN:Broadcast("DefaultNoteSkinCode",{Player = pn});
			return;
		else
			TurnOnCode(pn,noteskin[code]);
			MESSAGEMAN:Broadcast("NoteSkinCode",{Player = pn});
			return;
		end;
	end;
	--//////////////////////////////////////////////////////////////////////////////////////////////
	if (option=="Path") then
		if (code==1 or code==3) and IsCodeOn(pn,"Path",code) then
			TurnOffCode(pn,path[2][code]);
			MESSAGEMAN:Broadcast("PathCode",{Player = pn});
			return;
		end;
		if (code==2) and IsCodeOn(pn,"Path",code) then	-- space?
			TurnOnCode(pn,"Overhead");	--con esto sacas el "space"
			MESSAGEMAN:Broadcast("PathCode",{Player = pn});
			return;
		end;
		if (code==4) and IsCodeOn(pn,"Path",code) then	-- space?
			TurnOnCode(pn,"no drop");	--con esto sacas el "space"
			MESSAGEMAN:Broadcast("PathCode",{Player = pn});
			return;
		end;
		if (code==5) and IsCodeOn(pn,"Path",code) then	-- tornado?
			TurnOnCode(pn,"no tornado");	-- como tiene el 25% delante no podemos usar el TurnOff
			MESSAGEMAN:Broadcast("PathCode",{Player = pn});
			return;
		end;
		TurnOnCode(pn,path[2][code]);
		MESSAGEMAN:Broadcast("PathCode",{Player = pn});
		return;
	end;--{"xmode","150% space","reverse","","25% tornado"};
	--//////////////////////////////////////////////////////////////////////////////////////////////
	if (option=="Alternate") then
		if code==1 or code==5 then
			if not IsCodeOn(pn,"Alternate",code) then
				TurnOnCode(pn,alternate[2][code]);
				TurnOffCode(pn,alternate[2][2]); --mirror
				MESSAGEMAN:Broadcast("AlternateCode",{Player = pn});
				return;
			else
				TurnOffCode(pn,alternate[2][code]);
				MESSAGEMAN:Broadcast("AlternateCode",{Player = pn});
				return;
			end;
		end

		if code==2 or code==6 then
			if not IsCodeOn(pn,"Alternate",code) then
				TurnOnCode(pn,alternate[2][code]);
				TurnOffCode(pn,alternate[2][1]); --rs
				MESSAGEMAN:Broadcast("AlternateCode",{Player = pn});
				return;
			else
				TurnOffCode(pn,alternate[2][code]);
				MESSAGEMAN:Broadcast("AlternateCode",{Player = pn});
				return;
			end;
		end

		if code==4 or code==8 then
			if not IsCodeOn(pn,"Alternate",code) then
				TurnOnCode(pn,alternate[2][code]);
				TurnOffCode(pn,alternate[2][3]); --sink
				MESSAGEMAN:Broadcast("AlternateCode",{Player = pn});
				return;
			else 
				TurnOffCode(pn,alternate[2][code]);
				MESSAGEMAN:Broadcast("AlternateCode",{Player = pn});
				return;
			end;
		end

		if code==3 or code==7 then
			if not IsCodeOn(pn,"Alternate",code) then
				TurnOnCode(pn,alternate[2][code]);
				TurnOffCode(pn,alternate[2][4]); --rise
				MESSAGEMAN:Broadcast("AlternateCode",{Player = pn});
				return;
			else
				TurnOffCode(pn,alternate[2][code]);
				MESSAGEMAN:Broadcast("AlternateCode",{Player = pn});
				return;
			end;
		end

		TurnOnCode(pn,alternate[2][code]);
		MESSAGEMAN:Broadcast("AlternateCode",{Player = pn});
		return;
	end;--"Shuffle","Mirror","sink","rise","Shuffle","Mirror","sink","rise"
	--//////////////////////////////////////////////////////////////////////////////////////////////
	if (option=="Rush") then
		if IsCodeOn(pn,"Rush",code) then
			TurnOnCodes(pn,{"1.0xmusic","no haste", "no effect"});	--para sacar cualquier rush, el player no impor
			MESSAGEMAN:Broadcast("RushCode",{Player = pn});
			return;
		else
			TurnOnCodes(pn,{rush[2][code],"no haste", "effectpitch"});
			MESSAGEMAN:Broadcast("RushCode",{Player = pn});
			return;
		end;
	end;
	--///////////////////////////////////////////////////////////////////////////////////////////
	if (option=="Judge") then
		--Warn("judge: code: " .. judge[2][code]);
		if IsCodeOn(pn,"Judge",code) then
			TurnOffCode(pn,judge[2][code]);	--sacamos el judge que habiamos puesto
			MESSAGEMAN:Broadcast("JudgeCode",{Player = pn});
			return;
		else
			TurnOnCode(pn,judge[2][code]);
			MESSAGEMAN:Broadcast("JudgeCode",{Player = pn});
			return;
		end;
	end;
	--/////////////////////////////////////////////////////////////////////////////////////
	if (option=="Rank") then
		--Warn("rank: code: " .. rank[2][code]);
		if IsCodeOn(pn,"Rank",code) then
			TurnOffCode(pn,"Rank");	--sacamos el judge que habiamos puesto
			TurnOffCode(pn,judge[2][3]); --vj for rank
			MESSAGEMAN:Broadcast("RankCode",{Player = pn});
			MESSAGEMAN:Broadcast("JudgeCode",{Player = pn});
			MESSAGEMAN:Broadcast("GoFullMode");
			return;
		else
			if not vInvalidChannels[GAMESTATE:GetCurrentGroup()] then
				TurnOnCode(pn,"Rank");
				TurnOnCode(pn,judge[2][3]); --vj for rank
				TurnOffCode(pn,display[2][6]); --bgaon for rank
				MESSAGEMAN:Broadcast("RankCode",{Player = pn});
				MESSAGEMAN:Broadcast("JudgeCode",{Player = pn});
				MESSAGEMAN:Broadcast("DisplayCode",{Player = pn, BGAOffChange = false});
				MESSAGEMAN:Broadcast("GoRankMode");
			end
			return;
		end;
	end;
	--//////////////////////////////////////////////////////////////////////////////////////////////
	if (option=="System") then
		GAMESTATE:ResetPlayerOptions(pn);
		GAMESTATE:ApplyPreferredModifiers(pn,default_noteskin);
		GAMESTATE:ApplyPreferredModifiers(pn,"1.0xMusic");
		TurnOffCode(pn,"BGAOff");
		TurnOffCode(pn,"HJ"); --quitamos cualquier para que nos dejen en 0.9 y no se vea el grafico del judge
		--TurnOffCode(pn,"EJ"); 
		--TurnOffCode(pn,"NJ"); 
		--TurnOffCode(pn,"XJ"); 
		TurnOffCode(pn,"VJ"); 

		if GAMESTATE:IsRankMode() then
			TurnOffCode(pn,"Rank"); 
			MESSAGEMAN:Broadcast("GoFullMode");
		end;

		MESSAGEMAN:Broadcast("SpeedCode",{Player = pn});
		MESSAGEMAN:Broadcast("NoteSkinCode",{Player = pn});
		MESSAGEMAN:Broadcast("DefaultNoteSkinCode",{Player = pn});
		MESSAGEMAN:Broadcast("DisplayCode",{Player = pn});
		MESSAGEMAN:Broadcast("AlternateCode",{Player = pn});
		MESSAGEMAN:Broadcast("PathCode",{Player = pn});
		MESSAGEMAN:Broadcast("RushCode",{Player = pn});
		MESSAGEMAN:Broadcast("JudgeCode",{Player = pn});
		MESSAGEMAN:Broadcast("RankCode",{Player = pn});
		return;
	end; 
	--//////////////////////////////////////////////////////////////////////////////////////////////
end;
--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-- Actores
local ActorsPos = {
	(cmd(horizalign,'HorizAlign_Left';x,-138;zoomx,0;diffusealpha,0)),
	(cmd(horizalign,'HorizAlign_Left';x,-137;zoomx,1.4;diffusealpha,.5)),
	(cmd(horizalign,'HorizAlign_Center';x,0;zoomx,1.4;diffusealpha,1;linear,.1;zoom,1.45;decelerate,.12;zoom,1.4)),
	(cmd(horizalign,'HorizAlign_Right';x,135;zoomx,1.4;diffusealpha,.5)),
	(cmd(horizalign,'HorizAlign_Right';x,138;zoomx,0;diffusealpha,0)),
};

local FocusPos = {
	(cmd(horizalign,'HorizAlign_Left';x,-148;zoomx,0;diffusealpha,0)),
	(cmd(horizalign,'HorizAlign_Left';x,-30;diffusealpha,0;zoom,0;sleep,.25;zoom,1.4;linear,.05;x,-132;diffusealpha,.5)),
	(cmd(horizalign,'HorizAlign_Center';x,0;zoom,0;sleep,.15;linear,.05;zoom,1.45;diffusealpha,1;linear,.05;zoom,1.4)),
	(cmd(horizalign,'HorizAlign_Right';x,30;diffusealpha,0;zoom,0;sleep,.25;zoom,1.4;linear,.05;x,132;diffusealpha,.5)),
	(cmd(horizalign,'HorizAlign_Right';x,138;zoomx,0;diffusealpha,0)),
};

local UnFocusPos = {
	(cmd(horizalign,'HorizAlign_Left';x,-148;zoomx,0;diffusealpha,0)),
	(cmd(horizalign,'HorizAlign_Left';x,-132;diffusealpha,.5;linear,.05;x,-30;diffusealpha,0)),
	(cmd(horizalign,'HorizAlign_Center';x,0;zoom,1.4;diffusealpha,1;sleep,.05;linear,.05;zoom,1.45;linear,.05;zoom,0)),
	(cmd(horizalign,'HorizAlign_Right';x,132;diffusealpha,.5;linear,.05;x,30;diffusealpha,0)),
	(cmd(horizalign,'HorizAlign_Right';x,148;zoomx,0;diffusealpha,0)),
};
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Wheel = { 	
	cc = 1, 	-- current code
	name = "",	-- option
	GetOptionLine = function( self, option, codes, CUR_PLAYER )	-- la varialbe interna tambien se llama CUR_PLAYER
		local t = Def.ActorFrame {};
		
		-- correccion para que por lo menos cada seccion tenga 5 iconos
		while #codes < 5 do
			local temp = #codes;
			for i = 1,temp do
				codes[temp+i] = codes[i];
			end;
		end;
		
		--
		self.name = option;	-- name = option;
		local current_code = self.cc;	-- para no trabajar con la variable co directamente se utiliza current_code
		local size = #codes;	-- size = cantidad de elementos en {codes}
		
		-- Funcion local GetActors(), regresa los actores segun el indice => curOption
		local function GetActors()
			if current_code == 1 then
				return { size-1, size, 1, 2, 3 };
			elseif current_code == 2 then
				return { size, 1, 2, 3, 4 };
			elseif current_code == size then
				return { size-2, size-1, size, 1, 2 };
			elseif current_code == size-1 then
				return { size-3, size-2, size-1, size, 1 };
			else
				return { current_code-2,current_code-1, current_code, current_code+1, current_code+2 };
			end;
		end;
		
		local function SetCc( cod ) 
			self.cc = cod;
		end;
		
		--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		local Actors = {};	-- arreglo para los actores
		local factor;	-- factor, para saber si se movi� a la derecha(-1) o izquierda(1)
		local arr;		-- ni idea
		-- ActorFrame que maneja los iconos
		t[#t+1] = Def.ActorFrame {
			InitCommand=function(self)
				Actors = GetActors();
			end;
			OptionsListNextMessageCommand=function(self,params)
				if( params.Player ~= CUR_PLAYER ) then return; end;
				if( IsSelectingOption(CUR_PLAYER) or ( option ~= GetCurrentOptionName(CUR_PLAYER)) ) then return; end;
				if current_code == #codes then current_code = 1; else current_code = current_code+1; end;
				factor = -1;
				Actors = GetActors();
				MESSAGEMAN:Broadcast("CodeListChanged", { Player = CUR_PLAYER });
				MESSAGEMAN:Broadcast("ModChanged", { Player = CUR_PLAYER, code = GetCodeByPos(option, current_code), option = option });
			end;
			OptionsListPrevMessageCommand=function(self,params)
				if( params.Player ~= CUR_PLAYER ) then return; end;
				if( IsSelectingOption(CUR_PLAYER) or ( option ~= GetCurrentOptionName(CUR_PLAYER)) ) then return; end;
				if current_code == 1 then current_code = #codes; else current_code = current_code-1; end;
				factor = 1;
				Actors = GetActors();
				MESSAGEMAN:Broadcast("CodeListChanged", { Player = CUR_PLAYER });
				MESSAGEMAN:Broadcast("ModChanged", { Player = CUR_PLAYER, code = GetCodeByPos(option, current_code), option = option });
			end;
			OptionSelectedMessageCommand=function(self,params)
				if( params.Player ~= CUR_PLAYER) then return; end;		
				if GetCurrentOptionName(CUR_PLAYER) == "System" then return; end;

				SetCc( 1 );
				current_code = 1;
				Actors = GetActors();
				MESSAGEMAN:Broadcast("UpdateCodeList", { Player = CUR_PLAYER });	-- actualiza la lista de c�digos, cual si y cual no est� activo
				MESSAGEMAN:Broadcast("ModChanged", { Player = CUR_PLAYER, code = GetCodeByPos(GetCurrentOptionName(CUR_PLAYER), current_code), option = option });
			end;
			GoBackSelectingOptionMessageCommand=function(self,params)
				if( params.Player ~= CUR_PLAYER ) then return; end;
				if( option == GetCurrentOptionName(CUR_PLAYER) )then
					Actors = GetActors();
					MESSAGEMAN:Broadcast("UnFocusCo", { Player = CUR_PLAYER });
				end;				
			end;
			CodeSeletedMessageCommand=function(self,params)
				if( params.Player ~= CUR_PLAYER ) then return; end;
				if( option == GetCurrentOptionName(CUR_PLAYER) )then
					Actors = GetActors();
					WorkThisCode( CUR_PLAYER, option, current_code );	-- trabaja este codigo, act/desact
					MESSAGEMAN:Broadcast("RefreshCodes");	-- para refrescar tanto del p1 como del p2
					MESSAGEMAN:Broadcast("CodeSelected", { Player = CUR_PLAYER });
				end;
			end;
		}
		
		local function GetMyIcon( c_option, code )
			if c_option == "Noteskin" then
				if FILEMAN:DoesFileExist( ("/NoteSkins/Labels/" .. code .. ".png") ) then					
					return ("/NoteSkins/Labels/" .. code .. ".png");
				else
					return THEME:GetPathG("_blank","");
				end;
			end;

			return (THEME:GetPathG("","Mods") .. "/mods 16x10.png");
		end;
		
		-- Iconos
		for i=1,#codes do
			if option ~= "System" then
				t[#t+1] = Def.ActorFrame {
					LoadActor( GetMyIcon(option, codes[i]) ) .. {
						InitCommand=function(self)

							if option ~= "Noteskin" then
								(cmd(pause;animate,0;setstate,GetStateByModName(codes[i])))(self);
							end;

							self:zoomy(1.4);	-- solo cambia ak
							Actors = GetActors();
							self:stoptweening(); 
							if 	    i==Actors[1] then (ActorsPos[1])(self);
							elseif 	i==Actors[2] then (ActorsPos[2])(self);
							elseif 	i==Actors[3] then (ActorsPos[3])(self); --center
							elseif 	i==Actors[4] then (ActorsPos[4])(self);
							elseif 	i==Actors[5] then (ActorsPos[5])(self);
							else (cmd(stoptweening;diffusealpha,0))(self);
							end;
							self:visible(false);
							
							if (IsCodeOn(CUR_PLAYER,option,i)==true) then
								self:diffusecolor(1,1,1,1);
							else
								self:diffusecolor(.3,.3,.3,1);
							end;
							
						end;
						RefreshCodesMessageCommand=function(self)
							if( option == GetCurrentOptionName(CUR_PLAYER) ) then
								if (IsCodeOn(CUR_PLAYER,option,i)==true) then
									----Warn("IsCodeOn: " .. option .. " i: " .. tostring(i) .. " code: " .. codes[i]);
									self:linear(0);
									self:diffusecolor(1,1,1,1);
								else
									self:linear(0);
									self:diffusecolor(.3,.3,.3,1);
								end;
							else
								self:visible(false);
							end;
						end;
						CodeListChangedMessageCommand=function(self,params)
							if( params.Player ~= CUR_PLAYER ) then return; end;
							self:finishtweening(); -- termina todas las animaciones
							if factor == nil then factor = -1; end;
							if 	    i==Actors[1] then self:linear(.18);(ActorsPos[1])(self);
							elseif 	i==Actors[2] then if factor==-1 then self:x( -(self:GetZoomedWidth())/2 );end; self:linear(.18);(ActorsPos[2])(self);
							elseif 	i==Actors[3] then self:x( factor*(self:GetZoomedWidth())/2 + (self:GetX()));self:linear(.18);(ActorsPos[3])(self);--center
							elseif 	i==Actors[4] then if factor== 1 then self:x( (self:GetZoomedWidth())/2 );end; self:linear(.18);(ActorsPos[4])(self);
							elseif 	i==Actors[5] then self:linear(.18);(ActorsPos[5])(self);
							else (cmd(stoptweening;diffusealpha,0))(self);
							end;
						end;
						UpdateCodeListMessageCommand=function(self,params)
							if( params.Player ~= CUR_PLAYER ) then return; end;						

							if( option == GetCurrentOptionName(CUR_PLAYER) ) then
								self:playcommand('RefreshCodes');
								self:queuecommand('OnFocus');
							else
								self:visible(false);
							end;
						end;
						OnFocusCommand=function(self)	
							--if( params.Player ~= CUR_PLAYER ) then return; end;
							self:stoptweening(); --detiene todas las animaciones
							if 	    i==Actors[1] then (FocusPos[1])(self);
							elseif 	i==Actors[2] then (FocusPos[2])(self);
							elseif 	i==Actors[3] then (FocusPos[3])(self);--center
							elseif 	i==Actors[4] then (FocusPos[4])(self);
							elseif 	i==Actors[5] then (FocusPos[5])(self);
							else (cmd(stoptweening;diffusealpha,0))(self);
							end;
							self:zoomy(1.4);	-- solo cambia ak
							self:visible(true);
						end;
						UnFocusCoMessageCommand=function(self,params)
							if( params.Player ~= CUR_PLAYER ) then return; end;
							self:stoptweening(); --detiene todas las animaciones
							if 	    i==Actors[1] then (UnFocusPos[1])(self);
							elseif 	i==Actors[2] then (UnFocusPos[2])(self);
							elseif 	i==Actors[3] then (UnFocusPos[3])(self);--center
							elseif 	i==Actors[4] then (UnFocusPos[4])(self);
							elseif 	i==Actors[5] then (UnFocusPos[5])(self);
							else (cmd(stoptweening;diffusealpha,0))(self);
							end;
						end;
						CodeSelectedMessageCommand=function(self,params)
							if( params.Player ~= CUR_PLAYER ) then return; end;
							self:finishtweening();
							
							if Actors[3]==i then 
								(cmd(decelerate,.1;zoom,1.5;decelerate,.4;zoom,1.4))(self);
							end;
						end;
					};

					--disabled icon overlay
					LoadActor( THEME:GetPathG("","Mods") .. "/mods 16x10.png" ) .. {
						InitCommand=function(self)

							if GetCurrentOptionName(CUR_PLAYER) ~= "Rank" then
								(cmd(pause;animate,0;visible,false;setstate,128))(self);
								return;
							end;

							if vInvalidChannels[GAMESTATE:GetCurrentGroup()] then
								(cmd(visible,true))(self);
							else
								(cmd(visible,false))(self);
							end

							(cmd(animate,0;pause;setstate,128))(self);

							self:zoomy(1.4);	-- solo cambia ak
							Actors = GetActors();
							self:stoptweening(); 
							if 	    i==Actors[1] then (ActorsPos[1])(self);
							elseif 	i==Actors[2] then (ActorsPos[2])(self);
							elseif 	i==Actors[3] then (ActorsPos[3])(self); --center
							elseif 	i==Actors[4] then (ActorsPos[4])(self);
							elseif 	i==Actors[5] then (ActorsPos[5])(self);
							else (cmd(stoptweening;diffusealpha,0))(self);
							end;
							self:visible(false);	
						end;
						RefreshCodesMessageCommand=function(self)
							if GetCurrentOptionName(CUR_PLAYER) ~= "Rank" then
								(cmd(visible,false))(self);
								return;
							end;

							if vInvalidChannels[GAMESTATE:GetCurrentGroup()] then
								(cmd(visible,true))(self);
							else
								(cmd(visible,false))(self);
							end
						end;
						CodeListChangedMessageCommand=function(self,params)
							if( params.Player ~= CUR_PLAYER ) then return; end;

							if GetCurrentOptionName(CUR_PLAYER) ~= "Rank" or option ~= "Rank" then
								(cmd(visible,false))(self);
								return;
							end;

							if vInvalidChannels[GAMESTATE:GetCurrentGroup()] then
								(cmd(visible,true))(self);
							else
								(cmd(visible,false))(self);
							end

							self:finishtweening(); -- termina todas las animaciones
							if factor == nil then factor = -1; end;
							if 	    i==Actors[1] then self:linear(.18);(ActorsPos[1])(self);
							elseif 	i==Actors[2] then if factor==-1 then self:x( -(self:GetZoomedWidth())/2 );end; self:linear(.18);(ActorsPos[2])(self);
							elseif 	i==Actors[3] then self:x( factor*(self:GetZoomedWidth())/2 + (self:GetX()));self:linear(.18);(ActorsPos[3])(self);--center
							elseif 	i==Actors[4] then if factor== 1 then self:x( (self:GetZoomedWidth())/2 );end; self:linear(.18);(ActorsPos[4])(self);
							elseif 	i==Actors[5] then self:linear(.18);(ActorsPos[5])(self);
							else (cmd(stoptweening;diffusealpha,0))(self);
							end;
						end;
						UpdateCodeListMessageCommand=function(self,params)
							if( params.Player ~= CUR_PLAYER ) then return; end;						

							if GetCurrentOptionName(CUR_PLAYER) ~= "Rank" then
								(cmd(visible,false))(self);
								return;
							end;							

							if vInvalidChannels[GAMESTATE:GetCurrentGroup()] then
								(cmd(visible,true))(self);
								self:playcommand('RefreshCodes');
								self:queuecommand('OnFocus');
							else
								(cmd(visible,false))(self);
							end
						end;
						OnFocusCommand=function(self)	

							if GetCurrentOptionName(CUR_PLAYER) ~= "Rank" then
								(cmd(visible,false))(self);
								return;
							end;

							if vInvalidChannels[GAMESTATE:GetCurrentGroup()] then
								(cmd(visible,true))(self);
							else
								(cmd(visible,false))(self);
							end

							--if( params.Player ~= CUR_PLAYER ) then return; end;
							self:stoptweening(); --detiene todas las animaciones
							if 	    i==Actors[1] then (FocusPos[1])(self);
							elseif 	i==Actors[2] then (FocusPos[2])(self);
							elseif 	i==Actors[3] then (FocusPos[3])(self);--center
							elseif 	i==Actors[4] then (FocusPos[4])(self);
							elseif 	i==Actors[5] then (FocusPos[5])(self);
							else (cmd(stoptweening;diffusealpha,0))(self);
							end;
							self:zoomy(1.4);	-- solo cambia ak
							self:visible(true);
						end;
						UnFocusCoMessageCommand=function(self,params)
							if( params.Player ~= CUR_PLAYER ) then return; end;

							if GetCurrentOptionName(CUR_PLAYER) ~= "Rank" then
								(cmd(visible,false))(self);
								return;
							end;

							if vInvalidChannels[GAMESTATE:GetCurrentGroup()] then
								(cmd(visible,true))(self);
							else
								(cmd(visible,false))(self);
							end

							self:stoptweening(); --detiene todas las animaciones
							if 	    i==Actors[1] then (UnFocusPos[1])(self);
							elseif 	i==Actors[2] then (UnFocusPos[2])(self);
							elseif 	i==Actors[3] then (UnFocusPos[3])(self);--center
							elseif 	i==Actors[4] then (UnFocusPos[4])(self);
							elseif 	i==Actors[5] then (UnFocusPos[5])(self);
							else (cmd(stoptweening;diffusealpha,0))(self);
							end;
						end;
						--[[ CodeSelectedMessageCommand=function(self,params)
							if( params.Player ~= CUR_PLAYER ) then return; end;

							if option ~= "Rank" then
								(cmd(pause;animate,0;visible,false))(self);
								return;
							end;

							self:finishtweening();
							
							if Actors[3]==i then 
								(cmd(decelerate,.1;zoom,1.5;decelerate,.4;zoom,1.4))(self);
							end;
						end; ]]
					};
				}; --actor frame
			end; --fin del if que excluye el "System" option
		end;	-- fin del for
	return t;
	end;
};

function Wheel:new (o)
	o = o or {}   -- create object if user does not provide one
	setmetatable(o, self)
	self.__index = self
	return o
end;

-- Funciona todo bien :)
