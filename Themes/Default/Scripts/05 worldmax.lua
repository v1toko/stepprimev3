function missionmetercropright()
	local meter = GAMESTATE:GetCurrentMission():GetLevel()

	if meter == 1  then return 0.79
	elseif meter == 2 then return 0.61
	elseif meter == 3 then return 0.4
	elseif meter == 4 then return 0.2
	end

	return 0
end
function missionmetercolor()
	local meter = GAMESTATE:GetCurrentMission():GetLevel()

	    if meter == 1 then	return color("#00FFFF")
	elseif meter == 2 then	return color("#33FF00")
	elseif meter == 3 then	return color("#FFFF00")
	elseif meter == 4 then	return color("#FF9900")
	elseif meter == 5 then	return color("#FF0000")
	else					return color("#FFFFFF")
	end
end
function GetPlayerProfile( pn )
	if PROFILEMAN:IsPersistentProfile( pn ) then
		return PROFILEMAN:GetProfile( pn )
	end
	return PROFILEMAN:GetMachineProfile()
end
--usar con hide_if, no con Condition, no sé por que.
function IsArrowModsVisible()
	return GAMESTATE:GetCurrentMission():GetIfAllowShowArrowMods()
end
--nos da el tipo de la ultima barrera que pasamos =B
function GetLastBarrierType()
	local sType = GAMESTATE:GetLastBarrierType()
	
	Trace("GetLastBarrierType " .. tostring(sType) )
	
	return sType
end

--obtener si mostramos la barrera que indica que quitamos el bloqueo después de un Boss
--lo podemos usar en Condition
function GetIfShowBlockOff()
	--local bIsBoss = GAMESTATE:GetCurrentMission():IsBoss()
	--local bIsPassed = GAMESTATE:GetCurrentMission():IsPassed()
	--if bIsBoss and not bIsPassed then return true end
	
	--return false
	--local profile = GetPlayerProfile( GAMESTATE:GetFirstHumanPlayer() )
	
	--local bCond = GAMESTATE:GetCurrentMission():CheckBarrierData( profile )
	
	--return bCond
	
	local bCond = GAMESTATE:GetIfBarrierWasPassed()
	
	Trace("GetIfBarrierWasPassed " .. tostring(bCond) )
	
	return bCond
end