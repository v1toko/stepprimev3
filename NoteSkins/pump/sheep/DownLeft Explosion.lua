local iState = 0;

return Def.ActorFrame {
	NOTESKIN:LoadActor(Var "Button", "Tap Note") .. {
		InitCommand=cmd(blend,"BlendMode_Add";playcommand,"Glow");
		W1Command=cmd(playcommand,"Glow");
		W2Command=cmd(playcommand,"Glow");
		W3Command=cmd(playcommand,"Glow");
		W4Command=cmd();
		W5Command=cmd();
		HoldingOnCommand=cmd(playcommand,"Glow");
		HitMineCommand=cmd(playcommand,"Glow");
		HeldCommand=cmd(playcommand,"Glow");
		GlowCommand=cmd(setstate,0;finishtweening;diffusealpha,1.0;zoom,1.0;linear,0.15;diffusealpha,0.9;zoom,1.1;linear,0.15;diffusealpha,0.0;zoom,1.2);
	};
	LoadActor("GLOW 5x2")..{
		InitCommand=cmd(pause;setstate,0;zoom,1;blend,"BlendMode_Add";diffusealpha,0;playcommand,"Glow");
		W1Command=cmd(playcommand,"Glow");
		W2Command=cmd(playcommand,"Glow");
		W3Command=cmd(playcommand,"Glow");
		W4Command=cmd();
		W5Command=cmd();
		HoldingOnCommand=cmd(playcommand,"Glow");
		HitMineCommand=cmd(playcommand,"Glow");
		HeldCommand=cmd(playcommand,"Glow");
		GlowCommand=cmd(finishtweening;diffusealpha,1;zoom,.9;linear,0.2;zoom,1.15;diffusealpha,0);
	};
	LoadActor("_explosion")..{
		InitCommand=cmd(blend,"BlendMode_Add";pause;playcommand,"Frames");
		W1Command=function(self) self:finishtweening(); iState = 0 self:playcommand("Frames") end;
		W2Command=function(self) self:finishtweening(); iState = 0 self:playcommand("Frames") end;
		W3Command=function(self) self:finishtweening(); iState = 0 self:playcommand("Frames") end;
		W4Command=cmd();
		W5Command=cmd();
		HoldingOnCommand=function(self) self:finishtweening(); iState = 0 self:playcommand("Frames") end;
		HitMineCommand=function(self) self:finishtweening(); iState = 0 self:playcommand("Frames") end;
		HeldCommand=function(self) self:finishtweening(); iState = 0 self:playcommand("Frames") end;
		
		FramesCommand=function(self)
			iState = iState+1
			
			self:setstate(iState);
			self:diffusealpha(1);
			self:linear(0.06);
			if iState < self:GetNumStates() then
				self:queuecommand("Frames");
			end
		end;
	};
	
	Def.Quad {
		InitCommand=cmd(diffuse,1,1,1,0;zoomto,SCREEN_WIDTH*100,SCREEN_HEIGHT*100;zoomz,SCREEN_WIDTH*SCREEN_HEIGHT);
		HitMineCommand=cmd(finishtweening;diffusealpha,1;linear,0.3;diffusealpha,0);
	};
}