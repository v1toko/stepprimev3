local function Beat(self)
	local this = self:GetChildren()
	this.Glowpart:diffusealpha(0);
	local beat = GAMESTATE:GetSongBeat()
	local part = beat%1
	local eff = scale(part,0,0.5,1,0)
	if beat >=0 then
		this.Glowpart:diffusealpha(eff);
	end
end

return Def.ActorFrame {
	InitCommand=cmd(SetUpdateFunction,Beat);
	LoadActor("BASE 1x2")..{
		InitCommand=cmd(pause;setstate,0;y,-1);
	};
	LoadActor("BASE 1x2")..{
		Name="Glowpart";
		InitCommand=cmd(pause;setstate,1;blend,"BlendMode_Add";diffuseshift;effectcolor1,1,1,1,0.9;effectcolor2,1,1,1,1;effectclock,"Beat";effecttiming,0,0,0.5,0.5);
	};
	LoadActor("GLOW 5x2")..{
		InitCommand=cmd(pause;setstate,7;zoom,1;diffusealpha,0);
		PressCommand=cmd(finishtweening;diffusealpha,1;zoom,0.9;linear,0.2;diffusealpha,0;zoom,1.2);
	};
}