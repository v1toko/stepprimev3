return Def.ActorFrame {
	LoadActor("BASE 1x2")..{
		InitCommand=cmd(pause;setstate,0);
	};
	LoadActor("BASE 1x2")..{
		Name="Glowpart";
		InitCommand=cmd(pause;setstate,1;diffusealpha,0.8;blend,"BlendMode_Add";effectclock,"beat";diffuseramp;);
	};

}